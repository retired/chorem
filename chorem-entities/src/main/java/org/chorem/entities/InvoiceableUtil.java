/*
 * #%L
 * Chorem :: entities
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem.entities;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.ScriptEvaluator;
import org.nuiton.wikitty.WikittyClient;

/**
 * Classe permettant de rassembler les methodes utils pour l'evaluation des
 * scripts pour Invoiceable.
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class InvoiceableUtil {

//    /** to use log facility, just put in your code: log.info(\"...\"); */
//    static private Log log = LogFactory.getLog(InvoiceableUtil.class);
//
//    static public boolean evalCondition(WikittyClient client, Invoiceable invoiceable) {
//        Map<String, Object> bindings = new HashMap<String, Object>();
//        bindings.put("wikittyClient", client);
//        bindings.put("invoiceable", invoiceable);
//        Object resultEval = ScriptEvaluator.eval(null,
//                invoiceable.getName(), invoiceable.getCondition(),
//                invoiceable.getMimetype(), bindings);
//        boolean result= Boolean.TRUE.equals(resultEval);
//        return result;
//    }
//
//    static public double evalValue(WikittyClient client, Invoiceable invoiceable) {
//        Map<String, Object> bindings = new HashMap<String, Object>();
//        bindings.put("wikittyClient", client);
//        bindings.put("invoiceable", invoiceable);
//        Object resultEval = ScriptEvaluator.eval(null,
//                invoiceable.getName(), invoiceable.getValue(),
//                invoiceable.getMimetype(), bindings);
//
//        double result = 0;
//        if (resultEval instanceof Number) {
//            result = ((Number) resultEval).doubleValue();
//        }
//        return result;
//    }
}

/*
 * #%L
 * Chorem :: entities
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem;

import au.com.bytecode.opencsv.CSVReader;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.entities.Category;
import org.chorem.entities.CategoryImpl;
import org.chorem.entities.Company;
import org.chorem.entities.CompanyImpl;
import org.chorem.entities.ContactDetailsImpl;
import org.chorem.entities.EmployeeImpl;
import org.chorem.entities.FinancialTransaction;
import org.chorem.entities.FinancialTransactionImpl;
import org.chorem.entities.Invoice;
import org.chorem.entities.InvoiceImpl;
import org.chorem.entities.PersonImpl;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ApplicationConfig.Action.Step;
import org.nuiton.wikitty.entities.BusinessEntityImpl;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.query.WikittyQueryResult;

import java.io.FileReader;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Les donnees doivent etre exportees de la base postgresql avec les commandes:
 * <pre>
 * \copy employee to '/tmp/chorem-employee.csv' delimiter ';' CSV HEADER
 * \copy company to '/tmp/chorem-company.csv' delimiter ';' CSV HEADER
 * \copy person to '/tmp/chorem-person.csv' delimiter ';' CSV HEADER
 * \copy contracttype to '/tmp/chorem-contracttype.csv' delimiter ';' CSV HEADER
 * \copy contract to '/tmp/chorem-contract.csv' delimiter ';' CSV HEADER
 *
 * pour bill
 * \copy (SELECT subcategory.name AS category, number, issuedate, hopedate, realdate, value, company.name AS company from bill LEFT OUTER JOIN subcategory ON bill.subcategory=subcategory.topiaid LEFT OUTER JOIN task ON bill.task=task.topiaid LEFT OUTER JOIN contact ON bill.contact=contact.topiaid LEFT OUTER JOIN company ON contact.company=company.topiaid order by company.name) TO '/tmp/bill.csv' delimiter ';' CSV HEADER
 * \copy (SELECT date,subcategory.name AS category,tag,value from tagvalue LEFT OUTER JOIN subcategory ON tagvalue.subcategory=subcategory.topiaid) TO '/tmp/cost.csv' delimiter ';' CSV HEADER
 * </pre>
 * 
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ImportChoremTopia {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(ImportChoremTopia.class);

    enum COMPANY_HEADER {
        topiaid, topiaversion, topiacreatedate,
        name, type, headoffice, activity,
        address, phone, mail, web, fax,
        relation, othercostemployee, percentcostemployeepay
    };

    enum PERSON_HEADER {
       topiaid, topiaversion, topiacreatedate,
       firstname, lastname,
       address, birthdate, phone, mobile, mail
    };

    enum EMPLOYEE_HEADER {
       topiaid, topiaversion, topiacreatedate,
       phone, mobile, mail, job, company, service, responsable, person, contract
    };

    enum CONTRACT_TYPE_HEADER {
        topiaid, topiaversion, topiacreatedate,
        name
    }

    enum CONTRACT_HEADER {
        topiaid, topiaversion, topiacreatedate,
        type, hiringdate, timecost, beginhiringdate, endhiringdate, dayduration,
        monthduration, basepay, othercost, employee, contracttype
    }

    enum BILL_HEADER {
        category,number,issuedate,hopedate,realdate,value,company
    }

    enum COST_HEADER {
        date,category,tag,value
    }

    protected List<ContactDetailsImpl> contacts = new LinkedList<ContactDetailsImpl>();
    protected Map<String, CompanyImpl> companies = new HashMap<String, CompanyImpl>();
    protected Map<String, PersonImpl> persons = new HashMap<String, PersonImpl>();
    protected Map<String, EmployeeImpl> employees = new HashMap<String, EmployeeImpl>();
    protected Map<String, InvoiceImpl> invoices = new HashMap<String, InvoiceImpl>();
    protected Map<String, FinancialTransactionImpl> costs = new HashMap<String, FinancialTransactionImpl>();
    protected Map<String, CategoryImpl> categories = new HashMap<String, CategoryImpl>();

    // a ne pas stocker dans wikitty
    protected Map<String, String> contractType = new HashMap<String, String>();

    protected ApplicationConfig config;

    public ImportChoremTopia(ApplicationConfig config) {
        this.config = config;
    }

    /**
     * Try to parse date, if not possible, return null.
     * acceptable pattern
     * - "yyyy-MM-dd HH:mm:ss"
     * - "yyyy-MM-dd"
     *
     * @param s date to parse
     * @return Date object or null if s is not date with right pattern
     */
    protected Date parseDate(String s) {
        Date result = null;
        try {
            result = DateUtils.parseDate(s, "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd");
        } catch (ParseException ex) {
            log.debug(String.format("Bad date format '%s'", s));
        }
        return result;
    }

    protected String getWikittyId(BusinessEntityImpl e) {
        String result = null;
        if (e != null) {
            result = e.getWikittyId();
        }
        return result;
    }

    /**
     * Commit all imported data. Step number must be the last.
     * @param domain le domain permettant de trouver la base de donnees dans
     * lequel le commit doit etre fait
     */
    @Step(99)
    public void commit() {
        List<BusinessEntityImpl> data = new LinkedList<BusinessEntityImpl>();

        data.addAll(categories.values());
        data.addAll(companies.values());
        data.addAll(persons.values());
        data.addAll(employees.values());
        data.addAll(contacts);
        data.addAll(invoices.values());
        data.addAll(costs.values());

        ChoremClient proxy = ChoremClient.getClient(config);
        data = proxy.store(data);
        System.out.println(String.format("%s Commited data", data.size()));
    }

    @Step(1)
    public void importCompany(String filename) {
        System.out.println(String.format("Try to import company '%s'", filename));
        try {
            FileReader in = new FileReader(filename);
            // separateur ';' String delimiter '"', skip first line (header)
            CSVReader reader = new CSVReader(in, ';', '"', 1);
            for(String[] line : reader.readAll()) {
                String topiaId = line[COMPANY_HEADER.topiaid.ordinal()];

                // Read company info
                String name = line[COMPANY_HEADER.name.ordinal()];
                String type = line[COMPANY_HEADER.type.ordinal()];

                CompanyImpl company = new CompanyImpl();
                company.setName(name);
                company.setType(type);

                companies.put(topiaId, company);

                // Read company contacts details
                String address = line[COMPANY_HEADER.address.ordinal()];
                String fax = line[COMPANY_HEADER.fax.ordinal()];
                String mail = line[COMPANY_HEADER.mail.ordinal()];
                String phone = line[COMPANY_HEADER.phone.ordinal()];
                String web = line[COMPANY_HEADER.web.ordinal()];

                ContactDetailsImpl contactAddress = new ContactDetailsImpl();
                contactAddress.setType("address");
                contactAddress.setName("main");
                contactAddress.setValue(address);
                contactAddress.setTarget(company.getWikittyId());
                contacts.add(contactAddress);

                ContactDetailsImpl contactFax = new ContactDetailsImpl();
                contactFax.setType("fax");
                contactFax.setName("main");
                contactFax.setValue(fax);
                contactFax.setTarget(company.getWikittyId());
                contacts.add(contactFax);

                ContactDetailsImpl contactMail = new ContactDetailsImpl();
                contactMail.setType("mail");
                contactMail.setName("main");
                contactMail.setValue(mail);
                contactMail.setTarget(company.getWikittyId());
                contacts.add(contactMail);

                ContactDetailsImpl contactPhone = new ContactDetailsImpl();
                contactPhone.setType("phone");
                contactPhone.setName("main");
                contactPhone.setValue(phone);
                contactPhone.setTarget(company.getWikittyId());
                contacts.add(contactPhone);

                ContactDetailsImpl contactWeb = new ContactDetailsImpl();
                contactWeb.setType("web");
                contactWeb.setName("main");
                contactWeb.setValue(web);
                contactWeb.setTarget(company.getWikittyId());
                contacts.add(contactWeb);
            }
            reader.close();

            
        } catch (Exception eee) {
            log.fatal(String.format("Can't import file '%s'", filename));
            if (log.isDebugEnabled()) {
                log.debug("StackTrace", eee);
            }
            System.exit(1);
        }
        System.out.println(String.format("%s companies loaded", companies.size()));
    }

    @Step(1)
    public void importPerson(String filename) {
        System.out.println(String.format("Try to import person '%s'", filename));
        try {
            FileReader in = new FileReader(filename);
            // separateur ';' String delimiter '"', skip first line (header)
            CSVReader reader = new CSVReader(in, ';', '"', 1);
            for(String[] line : reader.readAll()) {
                String topiaId = line[COMPANY_HEADER.topiaid.ordinal()];

                // Read company info
                String firstname = line[PERSON_HEADER.firstname.ordinal()];
                String lastname = line[PERSON_HEADER.lastname.ordinal()];
                String birthdate = line[PERSON_HEADER.birthdate.ordinal()];

                PersonImpl person = new PersonImpl();
                person.setFirstName(firstname);
                person.setLastName(lastname);
                person.setBirthDate(parseDate(birthdate));

                persons.put(topiaId, person);

                // Read company contacts details
                String address = line[PERSON_HEADER.address.ordinal()];
                String mail = line[PERSON_HEADER.mail.ordinal()];
                String phone = line[PERSON_HEADER.phone.ordinal()];
                String mobile = line[PERSON_HEADER.mobile.ordinal()];

                ContactDetailsImpl contactAddress = new ContactDetailsImpl();
                contactAddress.setType("address");
                contactAddress.setName("main");
                contactAddress.setValue(address);
                contactAddress.setTarget(person.getWikittyId());
                contacts.add(contactAddress);

                ContactDetailsImpl contactMobile = new ContactDetailsImpl();
                contactMobile.setType("phone");
                contactMobile.setName("mobile");
                contactMobile.setValue(mobile);
                contactMobile.setTarget(person.getWikittyId());
                contacts.add(contactMobile);

                ContactDetailsImpl contactMail = new ContactDetailsImpl();
                contactMail.setType("mail");
                contactMail.setName("main");
                contactMail.setValue(mail);
                contactMail.setTarget(person.getWikittyId());
                contacts.add(contactMail);

                ContactDetailsImpl contactPhone = new ContactDetailsImpl();
                contactPhone.setType("phone");
                contactPhone.setName("main");
                contactPhone.setValue(phone);
                contactPhone.setTarget(person.getWikittyId());
                contacts.add(contactPhone);
            }
            reader.close();


        } catch (Exception eee) {
            log.fatal(String.format("Can't import file '%s'", filename));
            if (log.isDebugEnabled()) {
                log.debug("StackTrace", eee);
            }
            System.exit(1);
        }
        System.out.println(String.format("%s persons loaded", persons.size()));
    }

    @Step(1) // independant de tout
    public void importContractType(String filename) {
        System.out.println(String.format("Try to import contract type '%s'", filename));
        try {
            FileReader in = new FileReader(filename);
            // separateur ';' String delimiter '"', skip first line (header)
            CSVReader reader = new CSVReader(in, ';', '"', 1);
            for(String[] line : reader.readAll()) {
                String topiaId = line[CONTRACT_TYPE_HEADER.topiaid.ordinal()];

                // Read company info
                String name = line[CONTRACT_TYPE_HEADER.name.ordinal()];

                contractType.put(topiaId, name);
            }
            reader.close();
        } catch (Exception eee) {
            log.fatal(String.format("Can't import file '%s'", filename));
            if (log.isDebugEnabled()) {
                log.debug("StackTrace", eee);
            }
            System.exit(1);
        }
        System.out.println(String.format("%s contracts types loaded", contractType.size()));
    }

    @Step(2) // il faut importer apres company et person
    public void importEmployee(String filename) {
        System.out.println(String.format("Try to import employee '%s'", filename));
        try {
            FileReader in = new FileReader(filename);
            // separateur ';' String delimiter '"', skip first line (header)
            CSVReader reader = new CSVReader(in, ';', '"', 1);
            for(String[] line : reader.readAll()) {
                String topiaId = line[EMPLOYEE_HEADER.topiaid.ordinal()];

                // Read company info
                String company = line[EMPLOYEE_HEADER.company.ordinal()];
                String person = line[EMPLOYEE_HEADER.person.ordinal()];
                String job = line[EMPLOYEE_HEADER.job.ordinal()];

                EmployeeImpl employee = new EmployeeImpl();
                employee.setCompany(getWikittyId(companies.get(company)));
                employee.setPerson(getWikittyId(persons.get(person)));
                employee.setDescription(job);

                employees.put(topiaId, employee);

                // Read company contacts details
                String mail = line[EMPLOYEE_HEADER.mail.ordinal()];
                String phone = line[EMPLOYEE_HEADER.phone.ordinal()];
                String mobile = line[EMPLOYEE_HEADER.mobile.ordinal()];

                ContactDetailsImpl contactMobile = new ContactDetailsImpl();
                contactMobile.setType("phone");
                contactMobile.setName("mobile");
                contactMobile.setValue(mobile);
                contactMobile.setTarget(employee.getWikittyId());
                contacts.add(contactMobile);

                ContactDetailsImpl contactMail = new ContactDetailsImpl();
                contactMail.setType("mail");
                contactMail.setName("main");
                contactMail.setValue(mail);
                contactMail.setTarget(employee.getWikittyId());
                contacts.add(contactMail);

                ContactDetailsImpl contactPhone = new ContactDetailsImpl();
                contactPhone.setType("phone");
                contactPhone.setName("main");
                contactPhone.setValue(phone);
                contactPhone.setTarget(employee.getWikittyId());
                contacts.add(contactPhone);
            }
            reader.close();


        } catch (Exception eee) {
            log.fatal(String.format("Can't import file '%s'", filename));
            if (log.isDebugEnabled()) {
                log.debug("StackTrace", eee);
            }
            System.exit(1);
        }
        System.out.println(String.format("%s employees loaded", employees.size()));
    }

    @Step(3) // il faut importer apres employee
    public void importContract(String filename) {
        System.out.println(String.format("Try to import contract '%s'", filename));
        int count = 0;
        try {
            FileReader in = new FileReader(filename);
            // separateur ';' String delimiter '"', skip first line (header)
            CSVReader reader = new CSVReader(in, ';', '"', 1);
            for(String[] line : reader.readAll()) {
                count++;
                String employeeId = line[CONTRACT_HEADER.employee.ordinal()];

                EmployeeImpl employee = employees.get(employeeId);
                if (employee != null) {

                    // Read info
                    String beginDate = line[CONTRACT_HEADER.beginhiringdate.ordinal()];
                    String endDate = line[CONTRACT_HEADER.endhiringdate.ordinal()];
                    String typeId = line[CONTRACT_HEADER.contracttype.ordinal()];
                    String salary = line[CONTRACT_HEADER.basepay.ordinal()];

                    employee.setBeginDate(parseDate(beginDate));
                    employee.setEndDate(parseDate(endDate));
                    // 2012-11-14 ble : salary and type are now in entity employeeHR
                    //employee.setSalary(Float.parseFloat(salary));
                    //employee.setType(contractType.get(typeId));
                }
            }
            reader.close();
        } catch (Exception eee) {
            log.fatal(String.format("Can't import file '%s'", filename));
            if (log.isDebugEnabled()) {
                log.debug("StackTrace", eee);
            }
            System.exit(1);
        }
        System.out.println(String.format("%s contracts loaded", count));
    }

    protected String getPayerId(String proposed, String codelutin) {
        if (StringUtils.equals(codelutin, proposed)) {
            return null;
        }
        return proposed;
    }

    @Step(4)
    public void importInvoices(String filename) {
        System.out.println(String.format("Try to import invoices '%s'", filename));
        try {
            ChoremClient proxy = ChoremClient.getClient(config);

            // Pour les invoices existantes
            WikittyQuery invoicesQuery = new WikittyQueryMaker()
                    .exteq(Invoice.EXT_INVOICE).end().setLimit(WikittyQuery.MAX);
            WikittyQueryResult<Invoice> invoicesResult =
                    proxy.findAllByQuery(Invoice.class, invoicesQuery);
            Set<String> knowInvoices = new HashSet<String>();
            Set<String> passedInvoices = new HashSet<String>();
            Set<String> rejectedInvoices = new HashSet<String>();
            Set<String> treatedInvoices = new HashSet<String>();
            for (Invoice c : invoicesResult) {
                knowInvoices.add(c.getReference());
            }

            // Pour les categories
            WikittyQuery categoryQuery = new WikittyQueryMaker()
                    .exteq(Category.EXT_CATEGORY).end().setLimit(WikittyQuery.MAX);
            WikittyQueryResult<Category> categoryResult =
                    proxy.findAllByQuery(Category.class, categoryQuery);
            // key: category name; value: id
            Map<String, String> categories = new HashMap<String, String>();
            Set<String> unknowCategories = new HashSet<String>();
            for (Category c : categoryResult) {
                categories.put(c.getName(), c.getWikittyId());
            }

            // Pour les customer
            WikittyQuery customerQuery = new WikittyQueryMaker()
                    .exteq(Company.EXT_COMPANY)
                    .end().setLimit(WikittyQuery.MAX);
            WikittyQueryResult<Company> customerResult =
                    proxy.findAllByQuery(Company.class, customerQuery);
            // key: name; value: id
            Map<String, String> customers = new HashMap<String, String>();
            Set<String> unknowCustomers = new HashSet<String>();
            for (Company c : customerResult) {
                customers.put(c.getName(), c.getWikittyId());
                // ajout d'alias pour certain client qui ont change de nom
                if ("Ecole des Mines Nantes - EMN".equals(c.getName())) {
                    customers.put("Ecole des Mines Nantes", c.getWikittyId());
                } else if ("IRSTEA".equals(c.getName())) {
                    customers.put("Cemagref", c.getWikittyId());
                } else if ("IRSTEA".equals(c.getName())) {
                    customers.put("Cemagref", c.getWikittyId());
                } else if ("Herboristerie Cailleau".equals(c.getName())) {
                    customers.put("Cailleau Herboristerie", c.getWikittyId());
                } else if ("XWiki SAS".equals(c.getName())) {
                    customers.put("XPertNet", c.getWikittyId());
                }
            }

            // Pour les supplier
            WikittyQuery supplierQuery = new WikittyQueryMaker()
                    .eq(Company.FQ_FIELD_COMPANY_NAME, "Code Lutin").end();
            String codeLutinId = proxy.findByQuery(supplierQuery);

            FileReader in = new FileReader(filename);
            // separateur ';' String delimiter '"', skip first line (header)
            CSVReader reader = new CSVReader(in, ';', '"', 1);
            for(String[] line : reader.readAll()) {
                String number = line[BILL_HEADER.number.ordinal()];
                if (knowInvoices.contains(number)) {
                    passedInvoices.add(number);
                } else {
                    String category = line[BILL_HEADER.category.ordinal()];
                    String company = line[BILL_HEADER.company.ordinal()];
                    if (codeLutinId == null
                            || !categories.containsKey(category)
                            || !customers.containsKey(company)) {
                        rejectedInvoices.add(number);
                        // check des infos
                        if (!categories.containsKey(category)) {
                            unknowCategories.add(category);
                        }

                        if (!customers.containsKey(company)) {
                            unknowCustomers.add(company);
                        }
                    } else {
                        Double amount = Double.parseDouble(line[BILL_HEADER.value.ordinal()])/100.0;
                        // on enleve la TVA, sur chorem-topia estelle mettait avec la TVA les factures
                        amount = amount * 100.0 / 119.6;
                        Date issuedate = parseDate(line[BILL_HEADER.issuedate.ordinal()]);
                        Date hopedate = parseDate(line[BILL_HEADER.hopedate.ordinal()]);
                        Date realdate = parseDate(line[BILL_HEADER.realdate.ordinal()]);

                        InvoiceImpl invoice = new InvoiceImpl();
                        invoice.setCategory(categories.get(category));
                        invoice.setBeneficiary(codeLutinId);
                        // code lutin, ne peut pas etre le beneficiaire et le payer en meme temps
                        invoice.setPayer(getPayerId(customers.get(company), codeLutinId));
                        invoice.setReference(number);
                        invoice.setAmount(amount);
                        invoice.setVAT(19.6);
                        invoice.setEmittedDate(issuedate);
                        invoice.setExpectedDate(hopedate);
                        invoice.setPaymentDate(realdate);

                        invoices.put(invoice.getWikittyId(), invoice);
                        treatedInvoices.add(number);
                    }
                }
            }

            reader.close();

            System.out.println("Invoices deja importee: " + passedInvoices);
            System.out.println("Invoices a importee: " + treatedInvoices);
            if (codeLutinId == null || !unknowCategories.isEmpty() || !unknowCustomers.isEmpty()) {
                System.out.println("-- Erreur d'import --");
                System.out.println("Invoices rejetees: " + rejectedInvoices);
                System.out.println("Code Lutin id: " + codeLutinId);
                System.out.println("Categorie manquante: " + unknowCategories);
                System.out.println("Company manquante: " + unknowCustomers);
            }
        } catch (Exception eee) {
            log.fatal(String.format("Can't import file '%s'", filename));
            if (log.isDebugEnabled()) {
                log.debug("StackTrace", eee);
            }
            System.exit(1);
        }
        System.out.println(String.format("%s invoices loaded", invoices.size()));
    }

    protected String invoiceToKey(FinancialTransaction c) {
        String result = c.getCategory() + ";" + c.getPaymentDate() + ";" + c.getReference() + ";" + c.getAmount();
        return result;
    }

    @Step(5)
    public void importCosts(String filename) {
        System.out.println(String.format("Try to import cost '%s'", filename));
        try {
            ChoremClient proxy = ChoremClient.getClient(config);

            // Pour les invoices existantes
            WikittyQuery invoicesQuery = new WikittyQueryMaker()
                    .exteq(FinancialTransaction.EXT_FINANCIALTRANSACTION).end().setLimit(WikittyQuery.MAX);
            WikittyQueryResult<FinancialTransaction> invoicesResult =
                    proxy.findAllByQuery(FinancialTransaction.class, invoicesQuery);
            Set<String> knowInvoices = new HashSet<String>();
            Set<String> passedInvoices = new HashSet<String>();
            Set<String> rejectedInvoices = new HashSet<String>();
            Set<String> treatedInvoices = new HashSet<String>();
            for (FinancialTransaction c : invoicesResult) {
                // on fait une concatenation pour optimiser les futurs recherche
                knowInvoices.add(invoiceToKey(c));
            }

            // Pour les categories
            WikittyQuery categoryQuery = new WikittyQueryMaker()
                    .exteq(Category.EXT_CATEGORY).end().setLimit(WikittyQuery.MAX);
            WikittyQueryResult<Category> categoryResult =
                    proxy.findAllByQuery(Category.class, categoryQuery);
            // key: category name; value: id
            Map<String, String> categoryNameId = new HashMap<String, String>();
            Set<String> unknowCategories = new HashSet<String>();
            for (Category c : categoryResult) {
                categoryNameId.put(c.getName(), c.getWikittyId());
            }

            // le customer est code lutin
            WikittyQuery customerQuery = new WikittyQueryMaker()
                    .eq(Company.FQ_FIELD_COMPANY_NAME, "Code Lutin").end();
            String codeLutinId = proxy.findByQuery(customerQuery);

            // la category par defaut ou creer les manquantes
            WikittyQuery fraisQuery = new WikittyQueryMaker()
                    .eq(Category.FQ_FIELD_WIKITTYTREENODE_NAME, "Frais").end();
            String frais = proxy.findByQuery(fraisQuery);
            if (frais == null) {
                CategoryImpl c = new CategoryImpl();
                c.setName("Frais");
                categories.put(c.getWikittyId(), c);
                categoryNameId.put(c.getName(), c.getWikittyId());
                frais = c.getWikittyId();
            }

            FastDateFormat formatDate = FastDateFormat.getInstance("yyyy-MM-dd");

            FileReader in = new FileReader(filename);
            // separateur ';' String delimiter '"', skip first line (header)
            CSVReader reader = new CSVReader(in, ';', '"', 1);
            for(String[] line : reader.readAll()) {
                String tag = line[COST_HEADER.tag.ordinal()];
                String category = line[COST_HEADER.category.ordinal()];
                Date date = parseDate(line[COST_HEADER.date.ordinal()]);
                Double amount = Double.parseDouble(line[COST_HEADER.value.ordinal()])/100.0;

                if (StringUtils.isNotBlank(category) && !categoryNameId.containsKey(category)) {
                    CategoryImpl c = new CategoryImpl();
                    c.setName(category);
                    c.setParent(frais);
                    categories.put(c.getWikittyId(), c);
                    categoryNameId.put(c.getName(), c.getWikittyId());
                    unknowCategories.add(category);
                }

                FinancialTransactionImpl invoice = new FinancialTransactionImpl();
                invoice.setReference(tag + " " + (date==null?"":formatDate.format(date)));
                invoice.setDescription(tag);
                invoice.setCategory(categoryNameId.get(category));
                invoice.setPayer(codeLutinId);
                invoice.setAmount(amount);
                invoice.setEmittedDate(date);
                invoice.setExpectedDate(date);
                invoice.setPaymentDate(date);

                String invoiceKey = invoiceToKey(invoice);
                if (knowInvoices.contains(invoiceKey)) {
                    passedInvoices.add(invoiceKey);
                } else if (codeLutinId == null || date == null || StringUtils.isBlank(category)) {
                    rejectedInvoices.add(invoiceKey);
                } else {
                    costs.put(invoice.getWikittyId(), invoice);
                    treatedInvoices.add(invoiceKey);
                }
            }

            reader.close();

            System.out.println("Cost deja importee: " + passedInvoices);
            System.out.println("Cost a importee: " + treatedInvoices);
            System.out.println("Category creee: " + unknowCategories);
            if (codeLutinId == null || !rejectedInvoices.isEmpty()) {
                System.out.println("-- Erreur d'import costs--");
                System.out.println("Cost rejetees: " + rejectedInvoices);
                System.out.println("Code Lutin id: " + codeLutinId);
            }
        } catch (Exception eee) {
            log.fatal(String.format("Can't import file '%s'", filename));
            if (log.isDebugEnabled()) {
                log.debug("StackTrace", eee);
            }
            System.exit(1);
        }
        System.out.println(String.format("%s costs loaded", costs.size()));
    }

}

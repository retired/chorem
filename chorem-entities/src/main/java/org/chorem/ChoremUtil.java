package org.chorem;

/*
 * #%L
 * Chorem :: entities
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ChoremUtil {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(ChoremUtil.class);

    final static public String DEFAULT_SEPARATOR_LIST = ",";

    /**
     * Transforme un tableau en une liste, si un element du tableau utilise
     * le separateur alors, il est lui meme decoupe pour que chaque element
     * finisse dans la liste retournee
     *
     * @param separator le separateur a utiliser (si null, alors DEFAULT_SEPARATOR_LIST est utilise)
     * @param s
     * @return
     */
    static public List<String> asList(String separator, String ... array) {
        if (separator == null) {
            separator = DEFAULT_SEPARATOR_LIST;
        }
        List<String> result = new ArrayList<String>();
        for (String s : array) {
            String[] tmp = StringUtils.split(s, separator);
            for (String t : tmp) {
                result.add(t);
            }
        }
        return result;
    }
    
    /**
	 * Returns the number of seconds between the two given dates
	 * @param start Start date
	 * @param end End date
	 * @return period in seconds
	 */
	public static double getPeriodInSeconds(Date start, Date end) {
		return (end.getTime()-start.getTime())/1000;
	}
	
	/**
	 * Returns the number of hours between the two given dates
	 * @param start Start date
	 * @param end End date
	 * @return period in hours
	 */
	public static double getPeriodInHours(Date start, Date end) {
		return getPeriodInSeconds(start, end)/3600;
	}
	
	/**
	 * Calculates the number of working days between two dates
	 * @param begin start date
	 * @param end end date
	 * @return number of working days
	 */
	public static int getWorkingDays(Date begin, Date end) {
		GregorianCalendar gBegin = new GregorianCalendar();
		GregorianCalendar gEnd = new GregorianCalendar();
		gBegin.setTime(begin);
		gEnd.setTime(end);
		int days = 0;
		while(gBegin.before(gEnd)) {
			if(gBegin.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY
			&& gBegin.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
				days++;
			}
			gBegin.add(Calendar.DAY_OF_YEAR, 1);
		}
		return days;
	}
}

package org.chorem.project;

/*
 * #%L
 * Chorem :: entities
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.chorem.ChoremClient;
import org.chorem.ChoremUtil;
import org.chorem.entities.Employee;
import org.chorem.entities.Task;
import org.chorem.entities.Time;
import org.chorem.entities.Worker;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.query.WikittyQueryResult;

public class TaskCalculation extends Calculation<Task> {
	public TaskCalculation(Task t, ChoremClient client) {
		super(t, t.getPrice(), t.getEstimatedDays(), client);
	}

	@Override
	public double realDays() {
		//test if task is finished
		if(e.getStatus().equalsIgnoreCase("Closed")) {
			//Calculate the days from the times objects
			Collection<Time> times = new ArrayList<Time>();

			//Fetch the time objects linked to this task
			WikittyQuery timeQuery = new WikittyQueryMaker()
			.eq(Time.ELEMENT_FIELD_TIME_TASK, e)
			.end();
			WikittyQueryResult<Time> timeResult = client.findAllByQuery(Time.class, timeQuery);
			times.addAll(timeResult.getAll());

			double totalTime = 0;
			//For each time object
			for(Time t : times) {
				//Get the number of hours worked
				Double hours =  ChoremUtil.getPeriodInHours(t.getBeginDate(), t.getEndDate());

				Double days = hours / client.getDailyHoursWorked(t.getEmployee(false));
				
				totalTime += days;
			}
			return totalTime;
		}
		else {
			//estimated days + day extension
			return nbDays + e.getDayExtension();

		}
	}

	@Override
	protected Map<Employee, Double> getPercentages() {
		Map<Employee, Double> pct = new HashMap<Employee, Double>();
		//get all the Worker objects
		WikittyQuery workerQuery = new WikittyQueryMaker()
		.eq(Worker.ELEMENT_FIELD_WORKER_TASK, e)
		.end();
		WikittyQueryResult<Worker> workerResult = client.findAllByQuery(Worker.class, workerQuery);
		
		//For each worker, get the percentages assigned on the task
		for(Worker w : workerResult.getAll()) {
			if(w.getEmployee() != null) {
				pct.put(w.getEmployee(false), (double)w.getPercentage());
			}
		}
		
		return pct;
	}

	@Override
	protected Map<Employee, Double> getTimes() {
		Map<Employee, Double> times = new HashMap<Employee, Double>();
		
		
		
		for(Time t : client.getTimes(e)) {
			Employee emp = t.getEmployee(false);
			if(emp != null) {
				if(times.containsKey(emp)) {
					times.put(emp, times.get(emp) + (ChoremUtil.getPeriodInHours(t.getBeginDate(), t.getEndDate())) );
				}
				else {
					times.put(emp,ChoremUtil.getPeriodInHours(t.getBeginDate(), t.getEndDate()));
				}
			}
			
		}
		
		return times;
	}
}

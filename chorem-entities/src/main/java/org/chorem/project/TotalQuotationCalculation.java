package org.chorem.project;

/*
 * #%L
 * Chorem :: entities
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.List;
import java.util.Map;

import org.chorem.entities.Employee;
import org.chorem.entities.Quotation;

/**
 * Calculates the total values of the quotation calculations
 * Usage : construct the cobject by sending the list of quotation in parameter, then you can get the totals
 * by using the total methods.
 * Fore the totalAmount and the total estimated days, juste call the getAmount and getEstimatedDays functions,
 * as it is calculated during the object's initialisation.
 * @author gwenn
 *
 */
public class TotalQuotationCalculation extends Calculation<Quotation> {

	/**
	 * List of quotation calculation
	 */
	protected List<QuotationCalculation> calculations;
	
	/**
	 * Initiates the calculation object by calculating the total amount and the total estimated days
	 * @param calculations list of QuotationCalculation objects used to calculate the total.
	 */
	public TotalQuotationCalculation(List<QuotationCalculation> calculations) {
		super(null, totalAmount(calculations), totalEstimatedDays(calculations), null);
		this.calculations = calculations;
	}
	
	/**
	 * Calculates the total amount of all the quotations
	 * This is a method called on initialisation. It shouldn't be called in any other context.
	 * @param calculations
	 * @return total amount
	 */
	private static double totalAmount(List<QuotationCalculation> calculations) {
		double total = 0;
		for(QuotationCalculation q : calculations) {
			total += q.getObject().getAmount();
			
		}
		return total;
	}
	
	/**
	 * Calculates the total estimatedDays
	 * This is a method called on initialisation. It shouldn't be called in any other context.
	 * @param calculations
	 * @return estimated days.
	 */
	private static double totalEstimatedDays(List<QuotationCalculation> calculations) {
		double total = 0;
		for(QuotationCalculation q : calculations) {
			total += q.getObject().getEstimatedDays();
			
		}
		return total;
	}
	
	public double getAmount() {
		return amount;
	}
	
	public double getEstimatedDays() {
		return nbDays;
	}
	

	@Override
	public double realDays() {
		double total = 0;
		for(QuotationCalculation q : calculations) {
			total += q.getRealDays();
			
		}
		return total;

	}

	@Override
	public double avgReturn() {
		
		double total = 0;
		for(QuotationCalculation q : calculations) {
			total += q.getAvgReturn();
			
		}
		return total;
	}
	
	@Override
	public double realReturn() {
		double total = 0;
		for(QuotationCalculation q : calculations) {
			total += q.getRealReturn();
			
		}
		return total;
	}
	
	@Override
	public double expectedProfit() {
		double total = 0;
		for(QuotationCalculation q : calculations) {
			total += q.getExpectedProfit();
			
		}
		return total;
	}
	
	@Override
	public double lossOrProfit() {
		double total = 0;
		for(QuotationCalculation q : calculations) {
			total += q.getLossOrProfit();
			
		}
		return total;
	}
	
	
	
	/*
	 * Those following methods should'nt be called
	 */
	
	
	@Override
	public Map<Employee, Double> getPercentages() {
		return null;
	}
	
	@Override
	public Map<Employee, Double> getTimePercentages() {
			return null;
	}
	

	@Override
	public Map<Employee, Double> getTimes() {
		return null;
	}
	
}

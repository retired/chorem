package org.chorem.project;

/*
 * #%L
 * Chorem :: entities
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.chorem.ChoremClient;
import org.chorem.entities.Employee;

/**
 * Unit to calculate various elements for a Quotation or a Task.
 * May be extended for other extensions (Project, for example), but it's not the original purpose.
 * @author gwenn
 *
 * @param <T> Type of element (Task or Quotation)
 */
public abstract class Calculation<T> {

    /**Main object (Task or Quotation)*/
    protected T e;
    /** Chorme client */
    protected ChoremClient client;
    /** Total price of the object */
    protected double amount;
    /** Estimated number of days */
    protected double nbDays;

    protected static final int SEC_PER_HOUR 			= 3600;
    protected static final int WORKING_HOURS_PER_DAY 	= 7;
    /* Seuil de rentabilite productif */

    //attributes to store the calculations
    private Double adr 				= null;
    private Double realDays 		= null;
    private Double deltaDays 		= null;
    private Double realAdr 			= null;
    private Double expectedProfit 	= null;
    private Double lossOrProfit		= null;
    private Double resultPerDay		= null;
    private Double avgReturn		= null;
    private Double realReturn		= null;


    /**
     * 
     * @param e			Element that will serve for the calculations
     * @param amount 	Amount of this element
     * @param nbDays	Estimated number of days for this element
     * @param client	Instance of ChoremClient
     */
    public Calculation(T e, double amount, double nbDays, ChoremClient client) {
        this.e = e;
        this.client = client;
        this.amount = amount;
        this.nbDays = nbDays;
    }

    /**
     * Makes all the calcuations and store them into the attributes
     */
    public void calculate() {

        this.adr = adr();
        this.realDays = realDays();
        this.deltaDays = deltaDays();
        this.realAdr = realAdr();
        this.expectedProfit = expectedProfit();
        this.lossOrProfit = lossOrProfit();
        this.resultPerDay = resultPerDay();
        this.avgReturn = avgReturn();
        this.realReturn = realReturn();
    }

    /**
     * Calculates the real number of days from the Time objects.
     * @return real number of days needed for the project/task
     */
    public abstract double realDays();


    /**
     * Return the times (in hour) per employee for the current object
     * @return time spent on the project/task (per Employee)
     */
    protected abstract Map<Employee, Double> getTimes();

    /**
     * Return the percentages for the current object
     * @return estimated time spent on the project/task (per Employee, in percentage)
     */
    protected abstract Map<Employee, Double> getPercentages();

    /**
     * Calculates the percentage of time spent on the project/task
     * @return time spent on the project/task (per Employee, in percentage)
     */
    public Map<Employee, Double> getTimePercentages() {
        Map<Employee, Double> times = getTimes();
        Map<Employee, Double> timePercentages = new HashMap<Employee, Double>(times.size());
        double sum = 0;
        Set<Employee> employees = times.keySet();
        for (Employee employee : employees) {
            sum+= times.get(employee);
        }
        // calculate percentage
        for (Map.Entry<Employee, Double> employeeTime : times.entrySet()) {
            Employee employee = employeeTime.getKey();
            Double time = employeeTime.getValue();
            timePercentages.put(employee, (time /sum)*100);

        }

        return timePercentages;
    }
    /**
     * Return the average Return for all the employees, ponderated by the percentages
     * @return average estimated return
     */
    public double avgReturn() {

        //Fetch the percentages per employee
        Map<Employee, Double> percentages = getPercentages();
        if(percentages.size() == 0) {
            return client.getConfiguration().getDailyReturn();
        }
        Set<Employee> keySet = percentages.keySet();
        double avgReturn = 0;
        for(Employee key : keySet) {
            avgReturn += client.getDailyReturn(key)*(percentages.get(key)/100);
        }

        return avgReturn;	
    }

    /**
     * Calculates the real daily return from the different times object and the daily return of the employees.
     * @return real average return
     */
    public double realReturn() {
        Map<Employee, Double> times = getTimes();
        if(times.size() == 0) {
            return client.getConfiguration().getDailyReturn();
        }
        double realReturn = 0;
        Set<Employee> keySet = times.keySet();
        for (Map.Entry<Employee, Double> employeeTime : times.entrySet()) {
            Employee key = employeeTime.getKey();
            double srp = client.getDailyReturn(key);
            double hoursPerDay = client.getDailyHoursWorked(key);

            realReturn += srp* ( employeeTime.getValue() / hoursPerDay ) ;
        }
        realReturn = realReturn/getRealDays();

        return realReturn;
    }

    /**
     * Returns the average daily rate, which is the amount divided by the estimated number of days.
     * @return the ADR
     */
    public double adr() {
        return amount/nbDays;
    }



    /**
     * Gives the difference between the real number of days and the estimated value of days spent
     * @return difference between estimation and real time
     */
    public double deltaDays() {
        return getRealDays() - nbDays;
    }

    /**
     * Calculates the real ADR from the real number of days.
     * @return real adr
     */
    public double realAdr() {
        double realDays = getRealDays();

        return amount/realDays;
    }

    /**
     * Profit calculated from the estimated ADR and the estimated numer of days
     * @return expected profit
     */
    public double expectedProfit() {
        return amount - (nbDays*getAvgReturn());
    }

    /**
     * Real profit (or loss) done when the quotation is closed.
     * @return real profit (or loss)
     */
    public double lossOrProfit() {
        return amount - (getRealDays() * getRealReturn());
    }

    /**
     * Average profit/loss per day.
     * It is not currently used in the dashboards.
     * @return average profit (or loss) per day
     */
    public double resultPerDay() {
        return getLossOrProfit() / getRealDays();
    }



    /* ##########################################################################################
     * #########################  GETTERS #######################################################
     * ##########################################################################################
     * 
     * The getters returns the attributes if it has been calculated and calculates directly the value otherwise
     */

    public double getAdr() {
        if(adr == null)
            adr = adr();
        return adr;
    }
    public double getRealDays() {
        if(realDays == null)
            realDays = realDays();
        return realDays;
    }
    public double getDeltaDays() {
        if(deltaDays == null)
            deltaDays = deltaDays();
        return deltaDays;
    }
    public double getRealAdr() {
        if(realAdr == null)
            realAdr = realAdr();
        return realAdr;
    }	
    public double getExpectedProfit() {
        if(expectedProfit == null)
            expectedProfit = expectedProfit();
        return expectedProfit;
    }	
    public double getLossOrProfit() {
        if(lossOrProfit == null)
            lossOrProfit =  lossOrProfit();
        return lossOrProfit;
    }	
    public double getResultPerDay() {
        if(resultPerDay == null)
            resultPerDay = resultPerDay();
        return resultPerDay;
    }	

    public double getAvgReturn() {
        if(avgReturn == null)
            avgReturn = avgReturn();
        return avgReturn;
    }	
    public double getRealReturn() {
        if(realReturn == null)
            realReturn = realReturn();
        return realReturn;
    }	

    public T getObject() {
        return e;
    }
}

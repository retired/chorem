package org.chorem;

/*
 * #%L
 * Chorem :: entities
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.entities.Employee;
import org.chorem.entities.Quotation;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMakerAbstract;

/**
 * Extension de WikittyQueryMaker pour ajouter de nouvelle methode qui permette
 * de centraliser l'ajout de contrainte que l'on fait dans plusieurs requetes.
 * Cet objet peut-etre utiliser directement a la place de WikittyQueryMaker.
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ChoremQueryMaker extends WikittyQueryMakerAbstract<ChoremQueryMaker> {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(ChoremQueryMaker.class);

    public ChoremQueryMaker() {
        super();
    }

    public ChoremQueryMaker(WikittyQuery query) {
        super(query);
    }

    @Override
    protected ChoremQueryMaker asM() {
        return this;
    }

    /**
     * Ajoute une contrainte qui filtre sur une company ou un employer de cette
     * company
     *
     * @param client permet de rechercher l'id de la company par defaut pour l'utilisateur courant
     * @return maker qui etait en argument
     */
    public ChoremQueryMaker filterOnCompanyOrEmployee(ChoremClient client) {
        String companyId = client.getDefaultCompany().getWikittyId();
        filterOnCompanyOrEmployee(companyId);
        return this;
    }

    /**
     * Ajoute une contrainte qui filtre sur une company ou un employer de cette
     * company
     *
     * @param companyId
     * @return maker qui etait en argument
     */
    public ChoremQueryMaker filterOnCompanyOrEmployee(String companyId) {
        this.or().ideq(companyId).eq(Employee.FQ_FIELD_EMPLOYEE_COMPANY, companyId).close();
        return this;
    }

    /**
     * Ajoute une contrainte qui filtre sur un projet ou une quotation qui pointe sur ce projet
     * company
     *
     * @param projectId
     * @return maker qui etait en argument
     */
    public ChoremQueryMaker filterOnProjectOrQuotation(String projectId) {
        this.or().ideq(projectId).eq(Quotation.FQ_FIELD_QUOTATION_PROJECT, projectId).close();
        return this;
    }

    //
    // Des methodes utilisables en function dans les select
    //

    /**
     * Applique la vat sur le montant et retourne le total.
     * ex: amount = 100 et vat = 19.6 le resultat sera 119.6
     *
     *
     * @param amount
     * @param vat
     * @return
     */
    static public double amountTTC(double amount, double vat) {
        double result = amount * (1+vat/100);
        return result;
    }

    /**
     * Calcul la TVA du par rapport au montant.
     * ex: amount = 100 et vat = 19.6 le resultat sera 19.6
     *
     *
     * @param amount
     * @param vat
     * @return
     */
    static public double amountVAT(double amount, double vat) {
        double result = amount * vat / 100;
        return result;
    }

}

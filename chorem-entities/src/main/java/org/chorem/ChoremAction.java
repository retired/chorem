/*
 * #%L
 * Chorem :: entities
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.query.WikittyQueryResult;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ChoremAction {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(ChoremAction.class);

    ApplicationConfig config;

    public ChoremAction(ApplicationConfig config) {
        this.config = config;
    }

    /**
     * Vide les objets qui portent une certaine extension
     * @param extensionName
     */
    @ApplicationConfig.Action.Step(0)
    public void clean(String extensionName) {
        System.out.println("##### EFFACEMENT: " + extensionName + " #####");
        ChoremClient proxy = ChoremClient.getClient(config);
        WikittyQuery q = new WikittyQueryMaker().exteq(extensionName)
                .end().setLimit(WikittyQuery.MAX);
        WikittyQueryResult<String> result = proxy.findAllByQuery(q);
        proxy.delete(result.getAll());
        System.out.println("... clean done");
    }

    /**
     * Vide tous les objets
     */
    @ApplicationConfig.Action.Step(0)
    public void clear() {
        System.out.println("##### EFFACEMENT TOTAL DES DONNEES #####");
        ChoremClient proxy = ChoremClient.getClient(config);
        proxy.clear();
        System.out.println("... clear done");
    }

    /**
     * Reindex les donnees existantes
     */
    @ApplicationConfig.Action.Step(0)
    public void reindex() {
        System.out.println("##### REINDEXATION #####");
        ChoremClient proxy = ChoremClient.getClient(config);
        proxy.syncSearchEngine();
        System.out.println("... reindexation done");
    }

    /**
     * Vide tous les objets
     */
    @ApplicationConfig.Action.Step(0)
    public void removeObject(String extName) {
        System.out.println("##### EFFACEMENT TOTAL DES OBJECT AVEC L'EXTENSION '"+extName+"' #####");
        ChoremClient proxy = ChoremClient.getClient(config);
        WikittyQuery query = new WikittyQueryMaker()
                .exteq(extName).end()
                .setLimit(WikittyQuery.MAX);
        WikittyQueryResult<String> ids = proxy.findAllByQuery(query);
        proxy.delete(ids.getAll());
        System.out.println("... delete of " + ids.size() + " objects done");
    }

    public void configInfo() {
        config.printConfig();
    }

    static public void help() {
        System.out.println("Chorem Main Help");
    }
    
    /*
     * Migre les champs de l'entitité Employee vers EmployeeHR (intégralité des données)
     * champs : type, paidLeave, rtt, salary, workingtime
     */
//    static public void migrateEmployeeHR(WikittyService service, Wikitty wikitty,
//        WikittyExtension oldExt, WikittyExtension newExt) {
//        
//    }
    
}

/*
 * #%L
 * Chorem :: entities
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.chorem.entities.Attachment;
import org.chorem.entities.ChoremUser;
import org.chorem.entities.Company;
import org.chorem.entities.CompanyHR;
import org.chorem.entities.CompanyImpl;
import org.chorem.entities.Configuration;
import org.chorem.entities.ConfigurationImpl;
import org.chorem.entities.ContactDetails;
import org.chorem.entities.Employee;
import org.chorem.entities.EmployeeHR;
import org.chorem.entities.Invoice;
import org.chorem.entities.InvoiceMigration;
import org.chorem.entities.Quotation;
import org.chorem.entities.Quotation18Migration;
import org.chorem.entities.QuotationMigration;
import org.chorem.entities.Task;
import org.chorem.entities.Time;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.wikitty.WikittyClient;
import org.nuiton.wikitty.WikittyService;
import org.nuiton.wikitty.WikittyServiceFactory;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.query.WikittyQueryResult;
import org.parboiled.common.StringUtils;

/**
 * Proxy pour l'application. Certaines methodes specifiques pour l'application
 * peuvent y etre ajoutees (ex: des find particulier)
 */
public class ChoremClient extends WikittyClient {

    static protected Map<String, WikittyService> ws = new HashMap<String, WikittyService>();

    protected ChoremClient(ApplicationConfig config, WikittyService ws) {
        super(config, ws);
        this.getMigrationRegistry().put(Invoice.EXT_INVOICE, new InvoiceMigration());
        this.getMigrationRegistry().put(Quotation.EXT_QUOTATION, new QuotationMigration());
        //this.getMigrationRegistry().put(Vacation.EXT_VACATION, new VacationMigration());
        //this.getMigrationRegistry().put(Employee.EXT_EMPLOYEE, new EmployeeMigration());
        this.getMigrationRegistry().put(Quotation.EXT_QUOTATION, new Quotation18Migration());
    }

    /**
     * Returns new ChoremClient instance for the specified domain
     *
     * @param domain le domain qui permet de trouve la base de donnees
     * @return
     */
    static public ChoremClient getClient(String domain) {
        ApplicationConfig config = ChoremConfig.getConfig(domain);
        ChoremClient result = getClient(config);

        return result;
    }

    /**
     * Returns new ChoremClient instance for the specified domain
     *
     * @param config la configuration a utiliser
     * @return
     */
    static public ChoremClient getClient(ApplicationConfig config) {
        WikittyService ws = getWikittyService(config);
        ChoremClient result = new ChoremClient(config, ws);

        return result;
    }

    //    /**
    //     * Returns new ChoremClient instance with specified security token
    //     *
    //     * @param token can be null, if no authorisation token already exist (no login done)
    //     * @return
    //     */
    //    static public ChoremClient getClient(String token) {
    //        ApplicationConfig config = ChoremConfig.getConfig();
    //        WikittyService ws = getWikittyService(config);
    //        ChoremClient result = new ChoremClient(config, ws);
    //        result.setSecurityToken(token);
    //
    //        return result;
    //    }
    //
    //    /**
    //     * Return new ChoremClient instance with authentication done with login and
    //     * password found in application configuration. If no login found
    //     * ChoremClient returned doesn't have authentication.
    //     *
    //     * @return
    //     */
    //    static public ChoremClient getClient() {
    //        ApplicationConfig config = ChoremConfig.getConfig();
    //        String login = config.getOption(ChoremConfigOption.CHOREM_LOGIN.key);
    //        String password = config.getOption(ChoremConfigOption.CHOREM_PASSWORD.key);
    //
    //        ChoremClient result = getClient(login, password);
    //
    //        return result;
    //    }
    //
    //    /**
    //     * return new ChoremClient instance with authentication done with
    //     * login and password in argument, only if login is not null and not blank
    //     *
    //     * @param login login used for authentication
    //     * @param password password used for authentication
    //     */
    //    static public ChoremClient getClient(String login, String password) {
    //        ChoremClient result = getClient(null);
    //
    //        if (StringUtils.isNotBlank(login)) {
    //            result.login(login, password);
    //        }
    //        return result;
    //    }

    /**
     * Returns WikittyService to use. This WikittyService is singleton.
     *
     * @param config
     * @return WikittyService instance
     */
    static protected WikittyService getWikittyService(ApplicationConfig config) {
        String domain = config.getOption(ChoremConfigOption.CHOREM_DOMAIN.getKey());
        WikittyService result = ws.get(domain);
        if (result == null) {
            synchronized(ws) {
                result = ws.get(domain);
                if (result == null) {
                    result = WikittyServiceFactory.buildWikittyService(config);

                    // register last entity versions in database
                    List<WikittyExtension> exts = new ArrayList<WikittyExtension>();

                    exts.addAll(org.chorem.entities.AcceptedAbstract.extensions);
                    exts.addAll(org.chorem.entities.AttachmentAbstract.extensions);
                    exts.addAll(org.chorem.entities.CancelledAbstract.extensions);
                    exts.addAll(org.chorem.entities.CategoryAbstract.extensions);
                    exts.addAll(org.chorem.entities.ChoremUserAbstract.extensions);
                    exts.addAll(org.chorem.entities.ClosedAbstract.extensions);
                    exts.addAll(org.chorem.entities.CompanyAbstract.extensions);
                    exts.addAll(org.chorem.entities.ConfigurationAbstract.extensions);
                    exts.addAll(org.chorem.entities.ContactDetailsAbstract.extensions);
                    exts.addAll(org.chorem.entities.CompanyHRAbstract.extensions);
                    exts.addAll(org.chorem.entities.DeliveredAbstract.extensions);
                    exts.addAll(org.chorem.entities.DraftAbstract.extensions);
                    exts.addAll(org.chorem.entities.EmployeeAbstract.extensions);
                    exts.addAll(org.chorem.entities.EmployeeHRAbstract.extensions);
                    exts.addAll(org.chorem.entities.EvaluationAbstract.extensions);
                    exts.addAll(org.chorem.entities.ExpenseClaimAbstract.extensions);
                    exts.addAll(org.chorem.entities.FinancialTransactionAbstract.extensions);
                    exts.addAll(org.chorem.entities.GoalAbstract.extensions);
                    exts.addAll(org.chorem.entities.IntervalAbstract.extensions);
                    exts.addAll(org.chorem.entities.InterviewAbstract.extensions);
                    //                    exts.addAll(org.chorem.entities.InvoiceableAbstract.extensions);
                    exts.addAll(org.chorem.entities.InvoiceAbstract.extensions);
                    exts.addAll(org.chorem.entities.MissionAbstract.extensions);
                    exts.addAll(org.chorem.entities.NoteAbstract.extensions);
                    exts.addAll(org.chorem.entities.PersonAbstract.extensions);
                    exts.addAll(org.chorem.entities.PersonSkillAbstract.extensions);
                    exts.addAll(org.chorem.entities.ProjectAbstract.extensions);
                    exts.addAll(org.chorem.entities.QuotationAbstract.extensions);
                    exts.addAll(org.chorem.entities.ReferenceYearAbstract.extensions);
                    exts.addAll(org.chorem.entities.RejectedAbstract.extensions);
                    exts.addAll(org.chorem.entities.RSVAbstract.extensions);
                    exts.addAll(org.chorem.entities.SentAbstract.extensions);
                    exts.addAll(org.chorem.entities.SkillAbstract.extensions);
                    exts.addAll(org.chorem.entities.StartedAbstract.extensions);
                    exts.addAll(org.chorem.entities.TaskAbstract.extensions);
                    exts.addAll(org.chorem.entities.TimeAbstract.extensions);
                    exts.addAll(org.chorem.entities.TouchAbstract.extensions);
                    exts.addAll(org.chorem.entities.VacationAbstract.extensions);
                    exts.addAll(org.chorem.entities.VacationRequestAbstract.extensions);
                    exts.addAll(org.chorem.entities.WarrantyAbstract.extensions);
                    exts.addAll(org.chorem.entities.WorkerAbstract.extensions);





                    exts.addAll(org.nuiton.wikitty.entities.WikittyAuthorisationAbstract.extensions);
                    exts.addAll(org.nuiton.wikitty.entities.WikittyGroupAbstract.extensions);
                    exts.addAll(org.nuiton.wikitty.entities.WikittyHookAbstract.extensions);
                    exts.addAll(org.nuiton.wikitty.entities.WikittyLabelAbstract.extensions);
                    exts.addAll(org.nuiton.wikitty.entities.WikittyTreeNodeAbstract.extensions);
                    exts.addAll(org.nuiton.wikitty.entities.WikittyUserAbstract.extensions);

                    // Ajout de TAG value specifique au objet deja genere dans Wikitty
                    //org.chorem.entities.WikittyTreeNode.attribute.name.tagvalue.help=Le nom pour cette catégorie (ex: dépense, Salaire, ...)
                    //org.chorem.entities.WikittyTreeNode.attribute.attachment.tagvalue.visible=false
                    //org.chorem.entities.WikittyTreeNode.attribute.parent.tagvalue.help=La catégorie Parente de celle-ci (ex: dépense pour Loyer)
                    //org.chorem.entities.WikittyTreeNode.attribute.parent.tagvalue.allowed=Category
                    WikittyExtension e = org.nuiton.wikitty.entities.WikittyTreeNodeAbstract.extensionWikittyTreeNode;
                    e.setVersion(WikittyUtil.incrementMajorRevision(e.getTagValue("version"))); // toujours faire attention d'etre a +1 par rapport a l'objet genere
                    e.getFieldType("name").addTagValue("help", "Le nom pour cette catégorie (ex: dépense, Salaire, ...)");
                    e.getFieldType("attachment").addTagValue("visible", "false");
                    e.getFieldType("parent").addTagValue("help", "La catégorie Parente de celle-ci (ex: dépense pour Loyer)");
                    e.getFieldType("parent").addTagValue("allowed", "Category");

                    result.storeExtension(null, exts);
                    ws.put(domain, result);
                }
            }
        }

        return result;
    }

    /**
     * Returns the attachments linked with a wikitty id ordered by date
     * 
     * @param wikittyId
     * @return the list of attachments
     */
    public List<Attachment> getAttachments(String wikittyId) {
        WikittyQuery criteria = new WikittyQueryMaker()
        .eq(Attachment.FQ_FIELD_ATTACHMENT_TARGET, wikittyId).end();
        WikittyQueryResult<Attachment> result =
                findAllByQuery(Attachment.class, criteria);
        List<Attachment> attachments = result.getAll();
        return attachments;
    }

    /**
     * Returns the contact details linked with a wikitty id
     * 
     * @param wikittyId
     * @return the list of contact details
     */
    public List<ContactDetails> getContactDetails(String wikittyId) {
        WikittyQuery criteria = new WikittyQueryMaker()
        .eq(ContactDetails.FQ_FIELD_CONTACTDETAILS_TARGET, wikittyId).end();
        WikittyQueryResult<ContactDetails> result =
                findAllByQuery(ContactDetails.class, criteria);
        List<ContactDetails> contactDetails = result.getAll();
        return contactDetails;
    }

    protected String configId;
    /**
     * Retourne la configuration générale pour l'application
     * @return
     */
    public Configuration getConfiguration() {
        if (configId == null) {
            configId = findByQuery(new WikittyQueryMaker().exteq(Configuration.EXT_CONFIGURATION).end());
        }

        // on utilise le restore pour utilise le cache wikitty, il n'y a
        // qu'une instance de cet objet
        Configuration config = restore(Configuration.class, configId);

        if (config == null) {
            // pas retrouve, on en cree un nouveau
            // TODO poussin 20120524 dans le futur faire une interface pour recalculer/modifier les valeurs
            config = new ConfigurationImpl();
            config.setDailyHoursWorked(6);
            config.setDailyReturn(420);
            store(config);
        }

        return config;
    }

    /**
     * Retourne la company par defaut pour l'utilisateur loggue, si pas d'utilisateur
     * alors on prend la company defini dans la configuration general. S'il n'y
     * a aucune company, on en cree une nouvelle (non stocke) pour que tout
     * fonctionne bien. Cette company n'existant pas reellement les recherches
     * l'utilisant ne renvoie pas de resultat
     * @return on retourne la company par defaut, ou company factice si aucun
     * defaut n'existe
     */
    public Company getDefaultCompany() {
        String companyId = null;
        ChoremUser user = getUser(ChoremUser.class);

        // on regarde si l'utilisateur a configurer sa societe par defaut
        if (user != null) {
            companyId = user.getDefaultCompany();
        }

        // on regarde si l'utilateur a une societe (au moins, on la prend au hasard :))
        WikittyQuery q = new WikittyQueryMaker()
        .select(Employee.FQ_FIELD_EMPLOYEE_COMPANY)
        .eq(Employee.FQ_FIELD_EMPLOYEE_PERSON, user)
        .end();
        companyId = findByQuery(q);

        // on a toujours pas de societe, on utilise celle de la config de l'application
        if (companyId == null) {
            companyId = getConfiguration().getDefaultCompany();
        }

        Company result = null;
        if (companyId == null) {
            // on a trouve aucun societe on fini par en cree une victive qui ne doit pas etre persiste
            result = new CompanyImpl();
            result.setName("No Company");
        } else {
            // on restore la societe trouvee
            result = restore(Company.class, companyId);
        }
        return result;
    }

    /**
     * Retourne toutes les companies possible pour l'utilisateur loggue
     * @return
     */
    public List<Company> getUserCompanies() {
        String userId = getUser().getWikittyId();
        // on recupere toutes les companies pour lequel le user est salarie
        WikittyQuery q = new WikittyQueryMaker()
        .select(Employee.FQ_FIELD_EMPLOYEE_COMPANY)
        .eq(Employee.FQ_FIELD_EMPLOYEE_PERSON, userId)
        .end();
        WikittyQueryResult<Company> result = findAllByQuery(Company.class, q);
        return result.getAll();
    }

    /**
     * Retourne la locale preferee de l'utilisateur, si l'utilisateur n'a pas
     * defini de locale, alors on prend celle du systeme.
     *
     * @return Ret
     */
    public Locale getUserLocale() {
        Locale result = Locale.getDefault();
        ChoremUser user = getUser(ChoremUser.class);
        if (user != null && StringUtils.isNotEmpty(user.getDefaultLocale())) {
            String l = user.getDefaultLocale();
            result = Locale.forLanguageTag(l);
        }

        return result;
    }


    /**
     * Fetch the task from the given quotation
     * Simple wikitty query
     * @param q Quotation
     * @return unmodifiable list of task from the quotation
     */
    public List<Task> getTasks(Quotation q) {
        WikittyQuery taskQuery = new WikittyQueryMaker()
        .eq(Task.ELEMENT_FIELD_TASK_QUOTATION, q)
        .end();

        taskQuery.addSortAscending(Quotation.ELEMENT_FIELD_INTERVAL_BEGINDATE);

        WikittyQueryResult<Task> taskResult = findAllByQuery(Task.class, taskQuery);
        return taskResult.getAll();
    }

    /**
     * Fetch the times from the given task
     * @param t Task
     * @return unmodifiable list of times from the task
     */
    public List<Time> getTimes(Task t) {
        //Fetch the time objects from the task
        WikittyQuery timeQuery = new WikittyQueryMaker()
        .eq(Time.ELEMENT_FIELD_TIME_TASK, t)
        .end();

        timeQuery.addSortAscending(Quotation.ELEMENT_FIELD_INTERVAL_BEGINDATE);

        WikittyQueryResult<Time> timeResult = findAllByQuery(Time.class, timeQuery);

        return timeResult.getAll();
    }
    /**
     * Returns the daily return of the given empoyee
     * @param e employee
     * @return daily return of employee
     */
    public double getDailyReturn(Employee e) {

        Wikitty w = this.restore(e.getWikittyId());
        Wikitty companyW = null;
        if(e.getCompany(false) != null)
            companyW = restore(e.getCompany(false).getWikittyId());

        double dailyReturn = 0;

        if(w.hasExtension(EmployeeHR.EXT_EMPLOYEEHR) && 
                w.getFieldAsDouble(EmployeeHR.EXT_EMPLOYEEHR, EmployeeHR.FIELD_EMPLOYEEHR_DAILYRETURN) != 0) {
            dailyReturn = w.getFieldAsDouble(EmployeeHR.EXT_EMPLOYEEHR, EmployeeHR.FIELD_EMPLOYEEHR_DAILYRETURN);
        }
        else if(companyW != null && companyW.hasExtension(CompanyHR.EXT_COMPANYHR)
                && companyW.getFieldAsDouble(CompanyHR.EXT_COMPANYHR, EmployeeHR.FIELD_EMPLOYEEHR_DAILYRETURN) != 0) {
            dailyReturn = companyW.getFieldAsDouble(CompanyHR.EXT_COMPANYHR, EmployeeHR.FIELD_EMPLOYEEHR_DAILYRETURN);
        }
        else {
            dailyReturn = this.getConfiguration().getDailyReturn();
        }

        return dailyReturn;
    }
    /**
     * Returns the daily hours worked of the given empoyee
     * @param e
     * @return
     */
    public double getDailyHoursWorked(Employee e) {

        Wikitty w = this.restore(e.getWikittyId());
        Wikitty companyW = null;
        if(e.getCompany(false) != null)
            companyW = restore(e.getCompany(false).getWikittyId());

        double dailyHoursWorked = 0;

        if(companyW != null && companyW.hasExtension(CompanyHR.EXT_COMPANYHR) &&
                companyW.getFieldAsDouble(CompanyHR.EXT_COMPANYHR,CompanyHR.FIELD_COMPANYHR_DAILYHOURSWORKED) !=  0) {
            if(w.hasExtension(EmployeeHR.EXT_EMPLOYEEHR) && w.getFieldAsDouble(EmployeeHR.EXT_EMPLOYEEHR, EmployeeHR.FIELD_EMPLOYEEHR_PARTIALTIME) != 0) {
                dailyHoursWorked =  companyW.getFieldAsDouble(CompanyHR.EXT_COMPANYHR, CompanyHR.FIELD_COMPANYHR_DAILYHOURSWORKED) * 
                        (w.getFieldAsDouble(EmployeeHR.EXT_EMPLOYEEHR, EmployeeHR.FIELD_EMPLOYEEHR_PARTIALTIME)/100);
            }
            else {
                dailyHoursWorked =  companyW.getFieldAsDouble(CompanyHR.EXT_COMPANYHR, CompanyHR.FIELD_COMPANYHR_DAILYHOURSWORKED);
            }

        }
        else {
            dailyHoursWorked =  this.getConfiguration().getDailyHoursWorked();
        }

        return dailyHoursWorked;
    }


}

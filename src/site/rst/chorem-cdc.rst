.. -
.. * #%L
.. * Chorem
.. * %%
.. * Copyright (C) 2011 - 2012 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

====== Chorem ======

Chorem est composé de plusieurs modules
  * gestion de contacts
  * gestion de resources
  * gestion des emprunts (livres, switch, machine, ...)
  * gestion des demandes d'achat
  * gestion de rendez-vous
  * gestion commercial
  * gestion de congés
  * gestion de projet
  * suivie d'intervention client
  * ...

====== Besoin Intranet ======

  - Gestion des salariés :
    * données administratives (wiki)
    * CV (OOo include wiki)
    * congés/absences (interface spe mais wiki + dev envoie de mail)
    * bilan RTT / CP (interface spe mais wiki)
    * photo (wiki)
  - Gestion des missions (salarié/indépendant)
    * saisie du compte rendu d'activité mensuel (CRA) (wiki)
    * ordres de mission (wiki)
    * notes de frais (interface spe mais wiki)
    * synthèse mensuelle sur mission (wiki)
    * suivis de mission avec client (wiki)
  - Espace commun
    * abonnement mailing list (wiki)
    * messagerie instantanée / blog / forum (wiki)
    * trombinoscope (wiki)
    * liens (PEE, Comité Ese...) (wiki)
    * base documentaire (wiki)
    * boîte à idées (wiki)
  - Espace privé
    * administration du site 
    * prospection commerciale (appli spe + interaction wiki)

===== Gestion de contacts =====

La gestion de contacts permet de créer des informations pour des sociétés ou pour des personnes physiques. Les personnes faisant partie d'une ou de plusieurs sociétés.

Elle doit permettre de renseigner les informations les plus commune: adresse, téléphone, mail, context de la 1ère rencontre.

Pour les sociétés un outil de graph sera utilisé pour la saisie de la hiérarchie des personnes constituant la société.

Les personnes pourront aussi avoir un coût horaire.

===== Gestion de ressources =====

La gestion de ressources doit permettre de faire l'inventaire de tout ce qui est dans la société: ordinateur, libre, salle, voiture, vidéo-projecteur, ...

Chaque ressource peut avoir un coût d'achat ou d'entretien.

Elle doit aussi permettre à des contacts de faire des réservation de ces resources pour une date et une durée donner, ou faire un emprunt d'une ressource.

===== Gestion des emprunts =====

La gestion des emprunts est peut-être à fusioner avec la gestion des ressources.

Il doit permettre d'indiquer qui à emprunté un livre, à partir de quelle date, à quelle date il pense le rendre, et à quelle date le livre à réellement été rendu.

===== Gestion des demandes d'achat =====

Ce module doit permettre au personne de demander l'achat de matériel, de livre, de ressource.

Une personne fait la demande, avec une explication de la motivation de l'achat et la date limite pour laquelle il doit disposer de ce qu'il a demandé, il indique si possible le coût, et le lieu ou l'achat peut-être fait.

D'autre personne peuvent voté contre ou pour en argumentant.

===== Gestion de rendez-vous =====

La gestion de rendez-vous doit permettre de prendre des rendez-vous pour privé ou public. Un rendez-vous peut mettre en jeu un ou plusieurs contact et une ou plusieurs resources. Les personnes doivent être notifié du rendez-vous dès sa création, ils ont moyen de le refuser ou de l'accepter. Une fois un rendez-vous accepté la personne recevra un mail ou l'application chorem l'avertira du rendez-vous quelque temps avant celui-ci.

===== Gestion commercial =====

La gestion commercial doit permettre le suivi client et propect. Avec un historique de tout ce qui à été fait et dit avec ce client au niveau commercial.

===== Gestion de congés =====

La gestion de congés doit permettre à chaque personne de poser des congés, qui seront validé par un supérieur. 

===== Gestion de projet =====

La gestion de projet doit permettre toute la gestion du cycle de vie du projet:
  * Cahier des charges
  * développement
  * test
  * bug report

Un projet aura un temps estimé et un prix de vente.

Le projet sera divisé en tâches qui auront chacune une durée estimée, une date de début ou une contraite de début (après tel autre tâche ou avant tel autre tâche) et des personnes affectées à la tâche.

Une fois fini chaque tâche devra se voir affecté une durée réelle de travaille pour chaque personne qui ayant participé à la tâche.

Il sera alors possible pour chaque projet de voir la différence entre le temps estimé au départ et le temps réel passé, de calculer le coût de revient, de faire un graph de Gantt pour les tâches.

===== Gestion de facturation =====

La gestion de facturation utilise les informations sur les projets pour permettre l'édition automatique des factures lorsqu'un lot est terminé. Il doit aussi permettre de suivre l'avancement du paiement:
  * date d'expédition de la facture
  * rappelle automatique des factures non encore payé
  * génération de lettre de relance

<%--
  #%L
  Chorem webmotion
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2011 - 2012 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<form class="well form-inline">
    <input type="hidden" name="report" value="${report}"/>
    <input type="hidden" name="useDate" value="${useDate}" />
    <c:if test="${useDate != 'false'}">
        <input class="datepicker input-small" type="text" name="start" value="${fn:escapeXml(start)}" placeholder="date from"/>
        <input class="datepicker input-small" type="text" name="end" value="${fn:escapeXml(end)}" placeholder="date to"/>
    </c:if>
    <input class="input-xxlarge" type="text" name="query" value="${fn:escapeXml(query)}" placeholder="filter query"/>
    <input type="submit" class="btn"/>
</form>

    <%-- affichage de tous les rapports demandes --%>
    <c:forEach var="r" items="${fn:split(report, ',')}">
        <jsp:include page="/fragment/dashboard/${r}"/>
    </c:forEach>

<%--
  #%L
  Chorem webmotion
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2011 - 2012 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/wikitty.tld" prefix="w"%>


<div name="vacationDiv">
    <c:if test="${not empty ws}">
        <c:forEach var="wikittyd" items="${ws}">
            
            <c:if test="${wikittyd != null}">
            <h4>Period of vacation :</h4>
            <input type="hidden" name="ids" value="${wikittyd.id}"/>
            <input type="hidden" name="${wikittyd.id}.extension" value="Interval">
            <input type="hidden" name="${wikittyd.id}.extension" value="Vacation">
            <div class="well">
                
                <table>
                    
                    <tr>
                        <td><label class="control-label">Type of leave : </label></td>
                        <td><w:input wikitty="${wikittyd}" name="${wikittyd.id}.Vacation.typeLeave" fqfield="Vacation.typeLeave"/></td>
                        <td> </td>
                        <td><label class="control-label">&nbsp;Calculated amount :</label></td>
                        <td><label id="calcAmType"> 0</label></td>
                    </tr>
                    <tr>
                        <td> </td><td> </td><td> </td>
                        <td><label class="control-label">&nbsp;Real amount : </label></td>
                        <td><w:input wikitty="${wikittyd}" name="${wikittyd.id}.Vacation.amount" fqfield="Vacation.amount"/></td>
                    </tr>
                    
                    <tr class="beginDateTR">
                        <td><label class="control-label">Begin date : </label></td>
                        <td class="beginDateTD"><w:input wikitty="${wikittyd}" name="${wikittyd.id}.Interval.beginDate" fqfield="Interval.beginDate"/></td>
                        <td>&nbsp;<input type="checkbox" class="halfBeg" value="beg" />&nbsp;half-day</td>

                    </tr>
                    <tr class="endDateTR">
                        <td><label class="control-label">End date : </label></td>
                        <td class="endDateTD"><w:input wikitty="${wikittyd}" name="${wikittyd.id}.Interval.endDate" fqfield="Interval.endDate"/></td> 
                        <td>&nbsp;<input type="checkbox" class="halfEnd" value="end" />&nbsp;half-day</td>
                    </tr>
                </table>
                        
            </div>
            </c:if>
        </c:forEach>

    </c:if>
</div>
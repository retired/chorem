<%--
  #%L
  Chorem webmotion
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2011 - 2012 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%@ taglib uri="/WEB-INF/wikitty.tld" prefix="w"%>

<h1>Vacances en attente d'agrément</h1>

<table class="table table-striped table-bordered table-condensed">
    <thead>
        <tr>
            <th>Employee</th>
            <th>Begin date</th>
            <th>End date</th>
            <th>Days</th>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <c:forEach var="q" items="${vacations}">
        <tbody>
        <tr>
            <td><w:display wikitty="${q.wikitty}" fqfield="Vacation.employee" label=""/></td>
            <td class="date"><w:display wikitty="${q.wikitty}" fqfield="Interval.beginDate" label=""/></td>
            <td class="date"><w:display wikitty="${q.wikitty}" fqfield="Interval.endDate" label=""/></td>
            <td class="number"><w:display wikitty="${q.wikitty}" toString="${days.get(q.wikittyId)}" label=""/></td>
            <td><w:display wikitty="${q.wikitty}" fqfield="Vacation.type" label=""/></td>
            <td><w:display wikitty="${q.wikitty}" fqfield="Vacation.description" label=""/></td>
        </tr>
        </tbody>
    </c:forEach>
</table>

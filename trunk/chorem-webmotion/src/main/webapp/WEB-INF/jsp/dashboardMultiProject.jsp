<%--
  #%L
  Chorem webmotion
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2011 - 2012 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/WEB-INF/wikitty.tld" prefix="w"%>

<f:setLocale value="${locale}"/>

<link rel="stylesheet" href="<c:url value='/css/jquery.fn.gantt.css'/>" />
<h1>${title}</h1>
<form class="well form-inline" method="GET" id="projectSearch">

	<div class="control-group">
		<div class="controls" style="display: inline">

		From : <input type="text" name="from" id="from" class="datepicker" value='${param.from}' />
		to : <input type="text" name="to" id="to" class="datepicker" value="${param.to}" />
		<input type="submit"/>
		</div>
	</div>

</form>

<table class="table table-striped table-bordered table-condensed">
    <thead>
        <tr>
            <th>Client</th>
            <th>Projet</th>
            <th>Montant</th>
            <th>Nombre de jours estimés</th>
            <th><abbr title="Taux journalier moyen">TJM</abbr></th>
            <th>Gain attendu</th>
            <th>Nombre de jours rééls</th>
            <th><abbr title="Taux journalier moyen">TJM</abbr> reel</th>
            <th>Différence estimation/réél</th>
            <th>Perte/gain</th>
            <th>CJM estimé</th>
            <th>CJM réel</th>
        </tr>
    </thead>
    <tbody>
    <c:forEach var="q" items="${quotations}">
        
        <tr>
            <td><w:display wikitty="${q.wikitty}" fqfield="Quotation.customer" label=""/></td>
            <td><w:display wikitty="${q.wikitty}" fqfield="Quotation.project" label=""/>
            <a href="<c:url value="/wikitty/edit/${q.wikittyId}"/>"><i
				class="icon-pencil icon-black"></i></a>
            </td>
            <td class="currency"><w:display wikitty="${q.wikitty}" fqfield="Quotation.amount" label=""/></td>
            <td class="number"><w:display wikitty="${q.wikitty}" fqfield="Quotation.estimatedDays" label=""/></td>
           
            <td class="number"> <f:formatNumber type="number" 
            maxFractionDigits="2" value="${calculations[q].getAdr()}" /></td>
            
            <td class="currency"> <f:formatNumber type="currency" 
            maxFractionDigits="2" value="${calculations[q].getExpectedProfit()}" /></td>
            <td class="number"> <f:formatNumber type="number" 
            maxFractionDigits="2" value="${calculations[q].getRealDays()}" /></td>
            
            <td class="number"> <f:formatNumber type="number" 
            maxFractionDigits="2" value="${calculations[q].getRealAdr()}" /></td>
            <td class="number"> <f:formatNumber type="number" 
            maxFractionDigits="2" value="${calculations[q].getDeltaDays()}" /></td>
            
            <td class="currency"> <f:formatNumber type="currency" 
            maxFractionDigits="2" value="${calculations[q].getLossOrProfit()}" /></td>
            <td> <f:formatNumber type="number" 
            maxFractionDigits="2" value="${calculations[q].getAvgReturn()}" /></td>
            <td> <f:formatNumber type="number" 
            maxFractionDigits="2" value="${calculations[q].getRealReturn()}" /></td>
            
        </tr>
        
    </c:forEach>
    <tr>
    	<td><b>Total</b></td> 
    	<td></td>
    	<td ><f:formatNumber type="number" 
            maxFractionDigits="2" value="${total.getAmount()}" /></td>
    	<td class="number"><f:formatNumber type="number" 
            maxFractionDigits="2" value="${total.getEstimatedDays()}" /></td>
        <td class="currency"><!--<f:formatNumber type="currency" 
            maxFractionDigits="2" value="${total.getAdr()}" />--></td>
    	<td class="currency"><f:formatNumber type="currency" 
            maxFractionDigits="2" value="${total.getExpectedProfit()}" /></td>
    	<td class="number"><f:formatNumber type="number" 
            maxFractionDigits="2" value="${total.getRealDays()}" /></td>
    	<td class="currency"><!--<f:formatNumber type="currency" 
            maxFractionDigits="2" value="${total.getRealAdr()}" />--></td>
    	<td class="number"><!--<f:formatNumber type="number" 
            maxFractionDigits="2" value="${total.getDeltaDays()}" />--></td>
    	<td class="currency"><f:formatNumber type="currency" 
            maxFractionDigits="2" value="${total.getLossOrProfit()}" /></td>
    	<td class="currency"><!--<f:formatNumber type="currency" 
            maxFractionDigits="2" value="${total.getAvgReturn()}" />--></td>
    	<td class="currency"><!--<f:formatNumber type="currency" 
            maxFractionDigits="2" value="${total.getRealReturn()}" />--></td>
    
    </tr>
    </tbody>
</table>




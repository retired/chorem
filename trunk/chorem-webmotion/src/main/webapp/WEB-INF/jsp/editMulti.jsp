<%--
  #%L
  Chorem webmotion
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2011 - 2012 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/wikitty.tld" prefix="w"%>

<c:if test="${not empty ws}">
    <%//FIXME ymartel 2012/07/02 fix the need of iso accept charset ? %>
    <form  class="form-horizontal" method="post" accept-charset="ISO-8859-15" action="<c:url value="/wikitty/saveMulti"/>" enctype="multipart/form-data">
       
        <p>
            <button class="btn btn-success" type="submit"><i class="icon-ok icon-white"></i> Save</button>
            <a class="btn btn-info" href="<c:url value="/wikitty/view/${wikitty.id}"/>"><i class="icon-remove icon-white"></i> Cancel</a>
        </p>
        <c:forEach var="wikitty" items="${ws}">

            <c:if test="${(wikitty.getExtensions() != null) && (wikitty != null)}">
                <input type="hidden" name="ids" value="${wikitty.id}"/>
                <c:forEach var="ext" items="${wikitty.getExtensions()}">
                    <fieldset>
                        <legend>${ext.name}</legend>
                        <input type="hidden" name="${wikitty.id}.extension" value="${ext.name}">
                        <c:forEach var="fieldName" items="${ext.fieldNames}">
                            <div class="control-group">
                                <label class="control-label">${fieldName}</label>
                                <div class="controls">
                                    <w:input wikitty="${wikitty}" name="${wikitty.id}.${ext.name}.${fieldName}" fqfield="${ext.name}.${fieldName}"/>
                                </div>
                            </div>
                        </c:forEach>
                    </fieldset>
                </c:forEach>
                <div align="center">------------------------------------------</div>
            </c:if>
        </c:forEach>
    </form>
</c:if>

<div class="container">
    <c:if test="${not empty extensions}">
        <a class="btn btn-success" href="<c:url value="#"/>"><i class="icon-plus icon-white"></i> Add ${extensions}</a>
    </c:if>

</div>
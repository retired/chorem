<%--
  #%L
  Chorem webmotion
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2011 - 2012 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%@ taglib uri="/WEB-INF/wikitty.tld" prefix="w"%>

<h1>${title}</h1>


<table class="table table-striped table-bordered table-condensed">
    <thead>
        <tr>
            <th>Employee</th>
            <th>Type</th>
            <th>Start date</th>
            <th>End date</th>
            <th>Days</th>
            <th>Status</th>
            <th>Request Date</th>
            <th colspan="2">Vacation Request</th>
            
        </tr>
    </thead>

    <tbody>
    <c:forEach var="q" items="${vacations}">
        <c:forEach var="emp" items="${employes}">
            <c:if test="${emp.getWikittyId() eq q.getVacationRequest(true).employeeRequest}">
                
                    <tr>

                        <td>${emp.toString()}</td>
                        <td><w:display wikitty="${q.wikitty}" fqfield="Vacation.typeLeave" label=""/></td>
                        <td class="datetime"><w:display wikitty="${q.wikitty}" fqfield="Interval.beginDate" label=""/></td>
                        <td class="date"><w:display wikitty="${q.wikitty}" fqfield="Interval.endDate" label=""/></td>
                        <td><w:display wikitty="${q.wikitty}" fqfield="Vacation.amount" label=""/></td>
                        <td><w:display wikitty="${q.wikitty}" toString="${q.getVacationRequest(false).statusRequest}" label=""/></td>
                        <td class="date">${q.getVacationRequest(false).dateRequest}</td>
                        <td><a href="hr/vacationRequest/edit/${q.vacationRequest}" />Edit request</a></td>
                        <td><c:if test="${q.getVacationRequest(true).statusRequest eq 'EN PREVISION'}">
                                <a href="hr/vacationRequest/delete/${q.vacationRequest}" />Delete request</a>
                            </c:if>
                        </td>                        
                    </tr>
                
            </c:if>
        </c:forEach>
    </c:forEach>
    </tbody>
</table>



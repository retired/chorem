<%--
  #%L
  Chorem webmotion
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2011 - 2012 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%@ taglib uri="/WEB-INF/wikitty.tld" prefix="w"%>

<div class="row">
    <div class="col-md-1">
        <p class="calendar">
            <f:formatDate value="${date}" pattern="dd"/>
            <span class="saint"><%=org.chorem.Saint.get(new java.util.Date())%></span>
            <span class="week">semaine <f:formatDate value="${date}" pattern="w"/></span>
            <em><f:formatDate value="${date}" pattern="MMM"/></em>
        </p>
    </div>
    <div class="col-xs-10">
        <div class="row">
            <div class="col-xs-12">
                factures fournisseurs prévues:
                <a href='<c:url value="/report?report=invoiceDebt&query=(FinancialTransaction.emittedDate=null or FinancialTransaction.emittedDate>NOW ) and FinancialTransaction.paymentDate=null"/>'>
                    <f:formatNumber type="currency" value="${invoiceExpectedDebt}"/> (${invoiceExpectedDebtNb})</a>
                reçues:
                <a href='<c:url value="/report?report=invoiceDebt&query=FinancialTransaction.emittedDate<NOW and (FinancialTransaction.expectedDate=null or FinancialTransaction.expectedDate>NOW+7DAYS ) and FinancialTransaction.paymentDate=null"/>'>
                    <f:formatNumber type="currency" value="${invoiceReceivedDebt}"/> (${invoiceReceivedDebtNb})</a>
                à&nbsp;payer:
                <a href='<c:url value="/report?report=invoiceDebt&query=FinancialTransaction.expectedDate<NOW+7DAYS and FinancialTransaction.paymentDate=null"/>'>
                    <strong><f:formatNumber type="currency" value="${invoiceDueDebt}"/> (${invoiceDueDebtNb})</strong></a>
                total:
                <a href='<c:url value="/report?report=invoiceDebt&query=FinancialTransaction.paymentDate=null"/>'>
                    <f:formatNumber type="currency" value="${invoiceExpectedDebt+invoiceReceivedDebt+invoiceDueDebt}"/> (${invoiceExpectedDebtNb+invoiceReceivedDebtNb+invoiceDueDebtNb})</a>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                factures clients prévues:
                    <a href='<c:url value="/report?report=invoiceIncome&query=(FinancialTransaction.emittedDate=null or FinancialTransaction.emittedDate>NOW ) and FinancialTransaction.paymentDate=null"/>'>
                        <f:formatNumber type="currency" value="${invoiceExpectedIncome}"/> (${invoiceExpectedIncomeNb})</a>
                envoyées:
                    <a href='<c:url value="/report?report=invoiceIncome&query=FinancialTransaction.emittedDate<NOW and (FinancialTransaction.expectedDate=null or FinancialTransaction.expectedDate>NOW ) and FinancialTransaction.paymentDate=null"/>'>
                        <f:formatNumber type="currency" value="${invoiceEmittedIncome}"/> (${invoiceEmittedIncomeNb})</a>
                en retard:
                    <a href='<c:url value="/report?report=invoiceIncome&query=FinancialTransaction.expectedDate<NOW and FinancialTransaction.paymentDate=null"/>'>
                        <strong><f:formatNumber type="currency" value="${invoiceDueIncome}"/> (${invoiceDueIncomeNb})</strong></a>
                total:
                    <a href='<c:url value="/report?report=invoiceDebt&query=FinancialTransaction.paymentDate=null"/>'>
                        <f:formatNumber type="currency" value="${invoiceExpectedIncome+invoiceEmittedIncome+invoiceDueIncome}"/> (${invoiceExpectedIncomeNb+invoiceEmittedIncomeNb+invoiceDueIncomeNb})</a>
            </div>
        </div>

        <div class="row spacer">&nbsp;</div>

        <div class="row">
            <div class="col-xs-12">
                Il y a ${touchNb} <a href='<c:url value="/wikitty/search?query=${touchQueryString}"/>'>
                    contact à prendre</a>
            </div>
        </div>

        <div class="row spacer">&nbsp;</div>

        <div class="row">
           <div class="col-xs-4">
               Chiffre d'affaire HT:
               <strong>
                   <a href='<c:url value="/wikitty/search?query=${annualDebtIncome.get('incomesQuery')} or ${annualDebtIncome.get('extraIncomesQuery')} %23limit=2147483647"/>'>
                       <f:formatNumber type="currency" value="${income}"/>
                   </a>
               </strong>
           </div>
           <div class="col-xs-3">
               N-1: <strong>
                   <a href='<c:url value="/wikitty/search?query=${pastAnnualDebtIncome.get('incomesQuery')} or ${pastAnnualDebtIncome.get('extraIncomesQuery')} %23limit=2147483647"/>'>
                       <f:formatNumber type="currency" value="${pastIncome}"/>
                   </a>
               </strong>
           </div>
           <div class="col-xs-4">
               Évolution : <f:formatNumber pattern="+#,##0.00;-#,##0.00" value="${income-pastIncome}"/> (<f:formatNumber type="percent" value="${incomeEvolution}"/>)
           </div>
       </div>
       <div class="row">
           <div class="col-xs-4">
               Chiffre d'affaire TTC:
               <strong>
                   <a href='<c:url value="/wikitty/search?query=${annualDebtIncome.get('incomesQuery')} or ${annualDebtIncome.get('extraIncomesQuery')} %23limit=2147483647"/>'>
                       <f:formatNumber type="currency" value="${incomeTTC}"/>
                   </a>
               </strong>
           </div>
           <div class="col-xs-3">
               N-1: <strong>
                   <a href='<c:url value="/wikitty/search?query=${pastAnnualDebtIncome.get('incomesQuery')} or ${pastAnnualDebtIncome.get('extraIncomesQuery')} %23limit=2147483647"/>'>
                       <f:formatNumber type="currency" value="${pastIncomeTTC}"/>
                   </a>
               </strong>
           </div>
           <div class="col-xs-4">
               Évolution : <f:formatNumber pattern="+#,##0.00;-#,##0.00" value="${incomeTTC-pastIncomeTTC}"/> (<f:formatNumber type="percent" value="${incomeTTCEvolution}"/>)
           </div>
       </div>
       <div class="row">
           <div class="col-xs-4">
               Dépenses TTC:
               <strong>
                   <a href='<c:url value="/wikitty/search?query=${annualDebtIncome.get('debtsQuery')} %23limit=2147483647"/>'>
                       <f:formatNumber type="currency" value="${debtsTTC}"/>
                   </a>
               </strong>
           </div>
           <div class="col-xs-3">
               N-1: <strong>
                   <a href='<c:url value="/wikitty/search?query=${pastAnnualDebtIncome.get('debtsQuery')} %23limit=2147483647"/>'>
                       <f:formatNumber type="currency" value="${pastDebtsTTC}"/>
                   </a>
               </strong>
           </div>
           <div class="col-xs-4">
               Évolution : <f:formatNumber pattern="+#,##0.00;-#,##0.00" value="${debtsTTC-pastDebtsTTC}"/> (<f:formatNumber type="percent" value="${debtsTTCEvolution}"/>)
           </div>
       </div>
       <div class="row">
           <div class="col-xs-4">
               Bénéfice/perte:
               <strong><f:formatNumber type="currency" value="${profitTTC}"/></strong>
           </div>
           <div class="col-xs-3">
               N-1: <strong><f:formatNumber type="currency" value="${pastProfitTTC}"/></strong>
           </div>
           <div class="col-xs-4">
               Évolution : <f:formatNumber pattern="+#,##0.00;-#,##0.00" value="${profitTTC-pastProfitTTC}"/> (<f:formatNumber type="percent" value="${profitTTCEvolution}"/>)
           </div>
       </div>
    </div>
</div>


                    <!--
<div class="row">
    <div class="span1">
        <p class="calendar">
            <f:formatDate value="${date}" pattern="dd"/>
            <span class="saint"><%=org.chorem.Saint.get(new java.util.Date())%></span>
            <span class="week">semaine <f:formatDate value="${date}" pattern="w"/></span>
            <em><f:formatDate value="${date}" pattern="MMM"/></em>
        </p>
    </div>
    <div class="span24">
        <ul>
            <li>
                <ul>
                    <li class="inline">
                        factures fournisseurs prévues:
                        <a href='<c:url value="/report?report=invoiceDebt&query=(FinancialTransaction.emittedDate=null or FinancialTransaction.emittedDate>NOW ) and FinancialTransaction.paymentDate=null"/>'>
                            <f:formatNumber type="currency" value="${invoiceExpectedDebt}"/> (${invoiceExpectedDebtNb})</a>
                    </li>
                    <li class="inline">
                        reçues:
                        <a href='<c:url value="/report?report=invoiceDebt&query=FinancialTransaction.emittedDate<NOW and (FinancialTransaction.expectedDate=null or FinancialTransaction.expectedDate>NOW+7DAYS ) and FinancialTransaction.paymentDate=null"/>'>
                            <f:formatNumber type="currency" value="${invoiceReceivedDebt}"/> (${invoiceReceivedDebtNb})</a>
                    </li>
                    <li class="inline">
                        à payer:
                        <a href='<c:url value="/report?report=invoiceDebt&query=FinancialTransaction.expectedDate<NOW+7DAYS and FinancialTransaction.paymentDate=null"/>'>
                            <f:formatNumber type="currency" value="${invoiceDueDebt}"/> (${invoiceDueDebtNb})</a>
                    </li>
                    <li class="inline last">
                        total:
                        <a href='<c:url value="/report?report=invoiceDebt&query=FinancialTransaction.paymentDate=null"/>'>
                            <f:formatNumber type="currency" value="${invoiceExpectedDebt+invoiceReceivedDebt+invoiceDueDebt}"/> (${invoiceExpectedDebtNb+invoiceReceivedDebtNb+invoiceDueDebtNb})</a>
                    </li>
                </ul>
            </li>
            <li>
                <ul>
                    <li class="inline">
                        factures clients prévues:
                        <a href='<c:url value="/report?report=invoiceIncome&query=(FinancialTransaction.emittedDate=null or FinancialTransaction.emittedDate>NOW ) and FinancialTransaction.paymentDate=null"/>'>
                            <f:formatNumber type="currency" value="${invoiceExpectedIncome}"/> (${invoiceExpectedIncomeNb})</a>
                    </li>
                    <li class="inline">
                        envoyées:
                        <a href='<c:url value="/report?report=invoiceIncome&query=FinancialTransaction.emittedDate<NOW and (FinancialTransaction.expectedDate=null or FinancialTransaction.expectedDate>NOW ) and FinancialTransaction.paymentDate=null"/>'>
                            <f:formatNumber type="currency" value="${invoiceEmittedIncome}"/> (${invoiceEmittedIncomeNb})</a>
                    </li>
                    <li class="inline">
                        en retard:
                        <a href='<c:url value="/report?report=invoiceIncome&query=FinancialTransaction.expectedDate<NOW and FinancialTransaction.paymentDate=null"/>'>
                            <f:formatNumber type="currency" value="${invoiceDueIncome}"/> (${invoiceDueIncomeNb})</a>
                    </li>
                    <li class="inline last">
                        total:
                        <a href='<c:url value="/report?report=invoiceDebt&query=FinancialTransaction.paymentDate=null"/>'>
                            <f:formatNumber type="currency" value="${invoiceExpectedIncome+invoiceEmittedIncome+invoiceDueIncome}"/> (${invoiceExpectedIncomeNb+invoiceEmittedIncomeNb+invoiceDueIncomeNb})</a>
                    </li>
                </ul>
            </li>
        </ul>
        <ul>
            <li>Il y a ${touchNb} <a href='<c:url value="/wikitty/search?query=${touchQueryString}"/>'>
                    contact à prendre</a></li>
        </ul>

        <ul>
            <li>
                <ul>
                    <li class="inline">
                        Chiffre d'affaire HT:
                        <strong>
                            <a href='<c:url value="/wikitty/search?query=${annualDebtIncome.get('incomesQuery')} or ${annualDebtIncome.get('extraIncomesQuery')} %23limit=2147483647"/>'>
                                <f:formatNumber type="currency" value="${income}"/>
                            </a>
                        </strong>
                    </li>
                    <li class="inline">
                        N-1: <strong>
                            <a href='<c:url value="/wikitty/search?query=${pastAnnualDebtIncome.get('incomesQuery')} or ${pastAnnualDebtIncome.get('extraIncomesQuery')} %23limit=2147483647"/>'>
                                <f:formatNumber type="currency" value="${pastIncome}"/>
                            </a>
                        </strong>
                    </li>
                    <li class="inline last">
                        Évolution : <f:formatNumber pattern="+#,##0.00;-#,##0.00" value="${income-pastIncome}"/> (<f:formatNumber type="percent" value="${incomeEvolution}"/>)
                    </li>
                </ul>
            </li>
            <li>
                <ul>
                    <li class="inline">
                        Chiffre d'affaire TTC:
                        <strong>
                            <a href='<c:url value="/wikitty/search?query=${annualDebtIncome.get('incomesQuery')} or ${annualDebtIncome.get('extraIncomesQuery')} %23limit=2147483647"/>'>
                                <f:formatNumber type="currency" value="${incomeTTC}"/>
                            </a>
                        </strong>
                    </li>
                    <li class="inline">
                        N-1: <strong>
                            <a href='<c:url value="/wikitty/search?query=${pastAnnualDebtIncome.get('incomesQuery')} or ${pastAnnualDebtIncome.get('extraIncomesQuery')} %23limit=2147483647"/>'>
                                <f:formatNumber type="currency" value="${pastIncomeTTC}"/>
                            </a>
                        </strong>
                    </li>
                    <li class="inline last">
                        Évolution : <f:formatNumber pattern="+#,##0.00;-#,##0.00" value="${incomeTTC-pastIncomeTTC}"/> (<f:formatNumber type="percent" value="${incomeTTCEvolution}"/>)
                    </li>
                </ul>
            </li>
            <li>
                <ul>
                    <li class="inline">
                        Dépenses TTC:
                        <strong>
                            <a href='<c:url value="/wikitty/search?query=${annualDebtIncome.get('debtsQuery')} %23limit=2147483647"/>'>
                                <f:formatNumber type="currency" value="${debtsTTC}"/>
                            </a>
                        </strong>
                    </li>
                    <li class="inline">
                        N-1: <strong>
                            <a href='<c:url value="/wikitty/search?query=${pastAnnualDebtIncome.get('debtsQuery')} %23limit=2147483647"/>'>
                                <f:formatNumber type="currency" value="${pastDebtsTTC}"/>
                            </a>
                        </strong>
                    </li>
                    <li class="inline last">
                        Évolution : <f:formatNumber pattern="+#,##0.00;-#,##0.00" value="${debtsTTC-pastDebtsTTC}"/> (<f:formatNumber type="percent" value="${debtsTTCEvolution}"/>)
                    </li>
                </ul>
            </li>
            <li>
                <ul>
                    <li class="inline">
                        Bénéfice/perte:
                        <strong><f:formatNumber type="currency" value="${profitTTC}"/></strong>
                    </li>
                    <li class="inline">
                        N-1: <strong><f:formatNumber type="currency" value="${pastProfitTTC}"/></strong>
                    </li>
                    <li class="inline last">
                        Évolution : <f:formatNumber pattern="+#,##0.00;-#,##0.00" value="${profitTTC-pastProfitTTC}"/> (<f:formatNumber type="percent" value="${profitTTCEvolution}"/>)
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>

                    -->
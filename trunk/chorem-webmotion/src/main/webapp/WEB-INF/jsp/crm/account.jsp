<%--
  #%L
  Chorem webmotion
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2011 - 2012 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%@ taglib uri="/WEB-INF/wikitty.tld" prefix="w"%>

<head>
  <link rel="stylesheet/less" href="<c:url value='/css/chorem-crm.css'/>">
</head>

<body>
<div class="row-fluid">

    <h1>${account.getPerson(false).firstName} ${account.getPerson(false).lastName}</h1>
    <h6><a href="<c:url value="/wikitty/view/${account.company}"/>">${account.getCompany(false).name}</a></h6>
    <div class="elementId">${account.wikittyId}</div>

    <div class="contactDetails">
        <c:forEach var="contact" items="${contacts}">
          <p>
            <c:if test="${contact.type=='adresse'}">
              <i class="icon-envelope"></i>
            </c:if>
            <c:if test="${contact.type=='telephone' || contact.type=='phone'}">
              <i class="icon-phone">tel</i>
            </c:if>
            <c:if test="${contact.type=='mail' || contact.type=='email'}">
              <i class="icon-laptop">@</i>
            </c:if>
            : <span wikittyId="${contact.wikittyId}" class="value">${contact.value}</span>
            <i class="icon-edit contactDetail-edit-inline"></i>
          </p>
        </c:forEach>
    </div>

    <!-- Left column -->
    <div class="span3">
        <h6><i class="icon-user"></i> Collègues</h6>
        <ul class="unstyled">
          <c:forEach var="colleague" items="${colleagues}">
            <li>
              <small><a href="<c:url value="/crm/account/${colleague.wikittyId}"/>">${colleague.getPerson(false).firstName} ${colleague.getPerson(false).lastName}</a></small><br/>
            </li>
          </c:forEach>
        </ul>

    </div>

    <!-- Center column -->
    <div class="span5">
      <form class="add-note">
        Titre : <input type="text" name="title" id="note-title"/>
        <textarea name="content" rows=5 cols=70 id="note-content"></textarea>
        <input type="submit" class="btn btn-success" value="Nouvelle note"/>
      </form>
      <ul class="unstyled notes">
        <c:forEach var="note" items="${notes}">
          <li>
            <h6 class="pull-left"><i class="icon-comment"></i> ${note.title}</h6>
            <small class="pull-right note-date"><f:formatDate value="${note.date}" pattern="dd-MM-yyyy" /></small>
            <div style="clear:both;"/>
            <small>${note.content}</small>
          </li>
        </c:forEach>
      </ul>
    </div>

    <!-- Right column -->
    <div class="span3">
        <h6><i class="icon-fire"></i> Devis</h6>
        <ul class="unstyled">
          <c:forEach var="q" items="${quotations}">
            <li class="quotationsItem">
              <small><a href="<c:url value="/wikitty/view/${q.wikittyId}"/>">${q.description}</a></small><br/>
              <small>${q.amount} € - status</small>
            </li>
          </c:forEach>
        </ul>

        <h6><i class="icon-file"></i> Documents</h6>
        <ul class="unstyled">
          <c:forEach var="document" items="${documents}">
            <li>
              <small><a href="<c:url value="/wikitty/view/${document.wikittyId}/Attachment/content"/>">${document.name}</a></small><br/>
            </li>
          </c:forEach>
        </ul>
    </div>

</div>

<div style="clear:both;"/>
</body>
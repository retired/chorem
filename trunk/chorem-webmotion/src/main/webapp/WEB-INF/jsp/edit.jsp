<%--
  #%L
  Chorem webmotion
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2011 - 2012 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/wikitty.tld" prefix="w"%>

<c:if test="${not empty wikitty}">
    <%//FIXME ymartel 2012/07/02 fix the need of iso accept charset ? %>
    <form  class="form-horizontal" method="post" accept-charset="ISO-8859-15" action="<c:url value="/wikitty/save"/>" enctype="multipart/form-data">
        <input type="hidden" name="id" value="${wikitty.id}"/>
        <p>
            <button class="btn btn-success" type="submit"><i class="icon-ok icon-white"></i> Save</button>
            <a class="btn btn-info" href="<c:url value="/wikitty/view/${wikitty.id}"/>"><i class="icon-remove icon-white"></i> Cancel</a>
        </p>

        <c:forEach var="ext" items="${extensions}">
            <fieldset>
                <legend>${ext.name}</legend>
                <input type="hidden" name="extension" value="${ext.name}">
                <c:forEach var="fieldName" items="${ext.fieldNames}">
                    <div class="form-group">
                        <label class="control-label col-sm-2">${fieldName}</label>
                        <div class="col-sm-3">
                            <w:input wikitty="${wikitty}" fqfield="${ext.name}.${fieldName}"/>
                        </div>
                    </div>
                </c:forEach>
            </fieldset>
        </c:forEach>
    </form>
</c:if>

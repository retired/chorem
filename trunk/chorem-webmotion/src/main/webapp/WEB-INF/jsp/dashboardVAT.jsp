<%--
  #%L
  Chorem webmotion
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2011 - 2012 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%@ taglib uri="/WEB-INF/wikitty.tld" prefix="w"%>

<h1>Budget</h1>

<table class="table table-striped table-bordered table-condensed">
    <thead>
        <tr>
            <th>Mois</th>
            <c:forEach var="d" items="${data.keySet()}">
                <th><f:formatDate pattern="MM/yyyy" value="${d}"/></th>
            </c:forEach>
        </tr>
    </thead>
    <tbody>
            <tr>
                <th><span>TVA payer</span></th>
                <c:forEach var="d" items="${data.values()}">
                    <td class="currency">
                        <a href='<c:url value="/wikitty/search?query=${d.get('vatDebtQuery')} %23limit=2147483647"/>'>
                            <span><f:formatNumber type="currency" value='${d.get("vatDebt")}'/></span>
                        </a>
                    </td>
                </c:forEach>
            </tr>
            <tr>
                <th><span>TVA encaissé</span></th>
                <c:forEach var="d" items="${data.values()}">
                    <td class="currency">
                        <a href='<c:url value="/wikitty/search?query=${d.get('vatIncomeQuery')} %23limit=2147483647"/>'>
                            <span><f:formatNumber type="currency" value='${d.get("vatIncome")}'/></span>
                        </a>
                    </td>
                </c:forEach>
            </tr>
            <tr>
                <th><span>TVA à déclarer</span></th>
                <c:forEach var="d" items="${data.values()}">
                    <td class="currency"><span><f:formatNumber type="currency" value='${d.get("vatIncome") - d.get("vatDebt")}'/></span></td>
                </c:forEach>
            </tr>
    </tbody>
</table>

<%--
  #%L
  Chorem webmotion
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2011 - 2012 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<%@ taglib uri="/WEB-INF/wikitty.tld" prefix="w"%>

<form class="form-horizontal" method="post" accept-charset="ISO-8859-15"
	action="<c:url value="/wikitty/upgrade"/>"
	enctype="multipart/form-data">
	<input type="hidden" name="id" value="${wikitty.id}">
	<c:forEach items="${extensions}" var="ext">
		<legend>${ext.name}</legend>
		<input type="hidden" name="extension" value="${ext.name}">
		<c:forEach items="${ext.fieldNames}" var="field">
			<div class="control-group">
				<label class="control-label">${field}</label>
				<div class="controls">
					<w:input wikitty="${wikitty}" fqfield="${ext.name}.${field}"
						extension="${ext}" />
				</div>
			</div>
		</c:forEach>
	</c:forEach>
	<button class="btn btn-success" type="submit">
		<i class="icon-ok icon-white"></i>
	Mettre à jour
	</button>
</form>
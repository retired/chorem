<%--
  #%L
  Chorem webmotion
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2011 - 2012 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/WEB-INF/wikitty.tld" prefix="w"%>

<f:setLocale value="${locale}"/>



<h1>${title}</h1>
<form class="well form-inline" method="GET" id="projectSearch">

	<div class="control-group">
		<div class="controls" style="display: inline">

		From : <input type="text" name="from" id="from" class="datepicker" value='${param.from}' />
		to : <input type="text" name="to" id="to" class="datepicker" value="${param.to}" />
		<select class="filterBox" name="id" id="employeeFilter">
			<option value="">Tous les employés</option>
			<c:forEach items="${timeTotals}" var="entry">
				<option value="${entry.key.wikittyId}"><w:display wikitty="${entry.key.wikitty}"
										fqfield="Employee.person" label="" /></option>
			</c:forEach>
		</select>
		<input type="submit"/>
		</div>
	</div>

<div style="max-height:50px;background-color:#EEEEEE;padding:15px;margin:0px;overflow:auto;">
	<c:forEach items="${allQuotations}" var="q">
		<input type="checkbox" name="quotations"
		 <c:if test="${empty param.quotations or quotations.contains(q)}">
		 checked="yes" 
		 </c:if>
		 value="${q.wikittyId}"/> ${q.description}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	</c:forEach>

</div>

</form>

<table class="table table-striped table-bordered table-condensed">
	<thead>
		<th>Employé</th>
		<c:forEach items="${quotations}" var="q">
			<th>${q.getProject(false).name} :<a href="<c:url value="/project?quotationFilter=${q.wikittyId}" />"> ${q.description}</a></th>
		</c:forEach>
		<th>Total</th>
	</thead>
	<tbody>
		<c:forEach items="${timePerQuotation}" var="entry">
			<tr>
				<td><w:display wikitty="${entry.key.wikitty}"
										fqfield="Employee.person" label="" /></td>
				<c:forEach items="${quotations}" var="q">
					<td>
						<c:if test="${not empty entry.value[q]}">
						<f:formatNumber type="number"
											maxFractionDigits="2" value="${percentages[entry.key][q]}" /> %
						(<f:formatNumber type="number"
											maxFractionDigits="2" value="${entry.value[q]}" /> heures)
						</c:if>
					</td>
					
					
				</c:forEach>
				<td><f:formatNumber type="number"
											maxFractionDigits="2" value="${timeTotals[entry.key]}" /> heures</td>
			</tr>

		</c:forEach>
	</tbody>
</table>




<%--
  #%L
  Chorem webmotion
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2011 - 2012 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/WEB-INF/wikitty.tld" prefix="w"%>

<f:setLocale value="${locale}" />

<link rel="stylesheet" href="<c:url value='/css/employeeEdit.css'/>" />

<h1>${title}</h1>

<form method="GET" id="companyForm">

<select class="filterBox" name="companyId" id="companySelect">
			<option>Choisir une entreprise</option>
				<c:forEach var="c" items="${companies}">
					<option value="${c.wikittyId}"
					<c:if test="${company.equals(c)}">selected</c:if>
					>${c.name}</option>
				</c:forEach>
			</select>
</form>

<form class="well form-inline" method="POST" id="company">
<h4><a href="<c:url value="/wikitty/view/${company.wikittyId}"/>">${company.wikitty}</a></h4>
<input type="hidden" name="companyId" value="${company.wikittyId}"/>

from :
<input type="text" value="${from}">
to :
<input type="text" value="${to}">
</form>
<form style='display:inline;' id='searchForm'>Recherche : <input type="text" id="searchInput"/></form>
<p><button class="btn btn-success" id="calculateBtn" year = "${lastYear}">
		<i class="icon-refresh icon-white"></i>
	Calculer les CJMs pour ${lastYear}
</button></p>
<table class="table table-striped table-bordered table-condensed tableEdit">
	<thead>
		<th class="headEmployee">Employé</th>
		<c:forEach items="${years}" var="year">
		<th class="${year}">${year}</th>
		</c:forEach>
		<th>CJM estimé</th>
	</thead>
	<tbody>
		<c:set var="count" value="0"/>
		<c:forEach items="${employees}" var="employee">
			<tr class="bodyRow row${count}" id="${employee.object.wikittyId}">
				<td class="person bodyEmployee"><w:display wikitty="${employee.object.wikitty}"
						fqfield="Employee.person" label="" />
						</td>
				<c:forEach items="${years}" var="year">
				<td class="year${year}">
				<c:if test="${not empty employee.adcPerYear[year]}">
				${employee.adcPerYear[year]}
				</c:if>
				</td>
				</c:forEach>
				<td class="estimatedAdc">${employee.estimatedAdc}</td>
				
			</tr>
			<c:set var="count" value="${count + 1}"/>
		</c:forEach>
	</tbody>
</table>
<script src="<c:url value='/js/employeeEdit.js'/>"></script>
<script src="<c:url value='/js/dashboardAdc.js'/>"></script>


<%--
  #%L
  Chorem webmotion
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2011 - 2012 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/wikitty.tld" prefix="w"%>


<c:forEach var="key" items="${result.keySet()}">
<c:forEach var="wikitty" items="${result.get(key)}">
<c:forEach var="extens" items="${extensions.keySet()}">

    <c:forEach var="ext" items="${extensions.get(extens)}">

            <legend>${ext.name}</legend>
            <input type="hidden" name="extension" value="${ext.name}">
            <c:forEach var="fieldName" items="${ext.fieldNames}">
                <fieldset>
                <div class="control-group">
                    <label class="control-label">${fieldName}</label>
                    <div class="controls">
                        <w:input wikitty="${wikitty}" fqfield="${ext.name}.${fieldName}"/>
                    </div>
               </div>
                </fieldset>
            </c:forEach>

    </c:forEach>
</c:forEach>
</c:forEach>
</c:forEach>
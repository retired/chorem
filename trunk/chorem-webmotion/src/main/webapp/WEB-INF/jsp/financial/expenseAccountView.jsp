<%--
  #%L
  Chorem :: webmotion
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2011 - 2014 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/wikitty.tld" prefix="w"%>

<head>
    <script type="text/javascript" src="<c:url value='/js/angular-ui-bootstrap-tpls-0.11.0.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/financial/expenseAccount.js'/>"></script>
    <script>
        angular.module('expenseAccountPage').value('expenseAccountInit', {
            expenseAccount: ${expenseAccount.toJson()},
            saveURL: "<c:url value="/financial/expenseAccounts"/>",
            baseURL: "<c:url value="/"/>"
            });
    </script>
</head>

<div ng-app='expenseAccountPage' ng-controller="expenseAccountView">
    <div >
        <dl>
            <dd ng-show="editEmployee">
                <input type="text" ng-model="expenseAccount.employeeName"  id="text-expenseAccount-employee" auto-complete data-source="Employee" data-model="expenseAccount" data-base-field="employee" />
                <input type="hidden" ng-model="expenseAccount.employeeId" id="hidden-expenseAccount-employee"/>
            </dd>
            <dd ng-show="!editEmployee">
                <a href="<c:url value="/wikitty/view/{{expenseAccount.employeeId}}"/>">{{expenseAccount.employeeName}}</a>
                <span class="glyphicon glyphicon-pencil" ng-click="editEmployee = !editEmployee; hasChanged = true"></span>
            </dd>
            <dd ng-show="!editPeriod">
                <span>Period from {{expenseAccount.startDate |date : "dd/MM/yyyy"}} to {{expenseAccount.endDate |date : "dd/MM/yyyy"}}</span>
                <span class="glyphicon glyphicon-pencil" ng-click="editPeriod = !editPeriod; hasChanged = true"></span>
            </dd>

            <dd ng-show="editPeriod">
                <span>From </span>
                <input id="expenseAccountBeginDate" class="form-control" type="text"
                        datepicker-popup="dd/MM/yyyy"
                        ng-model="expenseAccount.startDate"
                        is-open="startDateOpened"
                        ng-click="startDateOpened = true"/>
                <span>To :</span>
                <input id="expenseAccountEndDate" class="form-control" type="text"
                        datepicker-popup="dd/MM/yyyy"
                        ng-model="expenseAccount.endDate"
                        is-open="endDateOpened"
                        ng-click="endDateOpened = true" />
            </dd>

          </dd>
        </dl>
    </div>

    <div>
        <table>
            <tr>
                <th style='width:15%'>Justif. Nb</th>
                <th style='width:10%'>Emitted Date</th>
                <th style='width:10%'>Project</th>
                <th style='width:15%'>Description</th>
                <th style='width:15%'>Category</th>
                <th style='width:7%'>Amount</th>
                <th style='width:7%'>VAT</th>
                <th style='width:6%'>total</th>
                <th style='width:10%'>Payment Date</th>
                <th style='width:5%'></th>
            </tr>
            <tr ng-repeat="expenseEntry in expenseAccount.expenseAccountEntries" id="tr-entry-{{expenseEntry.id}}">
                <td id="expenseEntry-justif-{{expenseEntry.id}}">{{expenseEntry.justificationNumber}}</td>
                <td id="expenseEntry-emittedDate-{{expenseEntry.id}}">{{expenseEntry.emittedDate| date : "dd/MM/yyyy"}}</td>
                <td id="expenseEntry-project-{{expenseEntry.id}}"><a href="<c:url value="/wikitty/view/{{expenseEntry.projectId}}"/>">{{expenseEntry.projectName}}</a></td>
                <td id="expenseEntry-description-{{expenseEntry.id}}">{{expenseEntry.description}}</td>
                <td id="expenseEntry-category-{{expenseEntry.id}}">{{expenseEntry.categoryName}}</td>
                <td id="expenseEntry-amount-{{expenseEntry.id}}">{{expenseEntry.amount|number:2}}</td>
                <td id="expenseEntry-vat-{{expenseEntry.id}}">{{expenseEntry.VAT|number:2}}</td>
                <td id="expenseEntry-total-{{expenseEntry.id}}">{{expenseEntry.total|number:2}}</td>
                <td id="expenseEntry-paymentDate-{{expenseEntry.id}}"><span ng-if = "expenseEntry.paymentDate"> {{expenseEntry.paymentDate|date : "dd/MM/yyyy"}}</span></td>
                <td id="expenseEntry-edit-{{expenseEntry.id}}"><span class="glyphicon glyphicon-remove" ng-click="deleteLine(expenseEntry)"></span></td>
            </tr>
            <tr ng-if="showNewLine == true" ng-include src="'expenseAccountEntryEdit.html'"></tr>
        </table>
    </div>

    <div>
        <a class="btn btn-success" ng-click="newLine()" ng-show="!showNewLine"><i class="icon-list icon-white"></i>New entry</a>
        <a class="btn btn-danger" ng-click="newLine()" ng-show="showNewLine"><i class="icon-list icon-white"></i>Cancel</a>
        <a class="btn btn-success" ng-click="saveExpenseAccount()" ng-show="hasChanged"><i class="icon-od icon-white"></i>Save</a>
    </div>

</div>

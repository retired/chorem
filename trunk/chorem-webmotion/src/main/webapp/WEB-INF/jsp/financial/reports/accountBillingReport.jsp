<%--
  #%L
  Chorem webmotion
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2011 - 2012 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%@ taglib uri="/WEB-INF/wikitty.tld" prefix="w"%>

<div class="row-fluid">
  <div class="span2">
    <jsp:include page="/billingMenu"/>
  </div>

  <div class="span10">
    <!-- Javascript to display graph -->
    <script type="text/javascript">
        $(document).ready(function(){
          var sales = [
             <c:forEach var="entry" items="${data}" varStatus="counter">
                ['${entry.key}', ${entry.value.sales}]
                <c:if test="${!counter.last}">, </c:if>
             </c:forEach>
             ];

          var plot1 = $.jqplot ('sales', [sales], {
            title:'Facturation par année',
            seriesDefaults:{
                        renderer:$.jqplot.BarRenderer,
                        rendererOptions: {fillToZero: true}
                    },
            axes:{
              xaxis:{
                pad: 0,
                tickRenderer: $.jqplot.CanvasAxisTickRenderer ,
                tickOptions: {
                  angle: -30,
                  fontSize: '10pt'
                },
                renderer: $.jqplot.CategoryAxisRenderer
              },
            },
            highlighter: {
              show: true,
              tooltipAxes: 'y'
            },
            cursor: {
              show: false
            },
            series:[
              <c:forEach var="entry" items="${data}" varStatus="counter">
                {label:${entry.key}}<c:if test="${!counter.last}">,</c:if>
              </c:forEach>]
          });

        });
    </script>

    <h2>Facturation par année</h2>

    <form action="sales" method="get">
      <select name="from">
        <c:forEach var="entry" items="${allYears}" varStatus="counter">
          <option value="${entry}" <c:if test="${entry==fromYear}">selected</c:if>>${entry}</option>
        </c:forEach>
      </select>
      <select name="to">
        <c:forEach var="entry" items="${allYears}" varStatus="counter">
          <option value="${entry}" ${entry == toYear ? 'selected' : ''} >${entry}</option>
        </c:forEach>
      </select>
      <input type="submit" value="Rechercher">
    </form>

    <div id="sales" style="height:200px;"></div>


    <table class="table table-striped table-bordered table-condensed">
        <thead>
            <tr>
                <th>Année</th>
                <th>Facturation</th>
                <th>Factures</th>
                <th>€/factures</th>
                <th>Progression</th>
            </tr>
        </thead>
            <tbody>
        <c:forEach var="year" items="${data}">
            <tr>
                <td>${year.key}</td>
                <td class="currency">${year.value.sales}</td>
                <td>${year.value.quotations}</td>
                <td class="currency">${year.value.mean}</td>
                <td class="percent">${year.value.progression} %</td>
            </tr>
        </c:forEach>
            </tbody>
    </table>

  </div>
</div>

<div style="clear:both;"/>
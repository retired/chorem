<%--
  #%L
  Chorem :: webmotion
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2011 - 2014 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<ul class="list-group">
  <li class="list-group-item"><a href="<c:url value="/financial/report/billing"/>"><i class="icon-chevron-right"></i> Facturation</a></li>
  <li class="list-group-item"><a href="<c:url value="/financial/report/billingPerAccount"/>"><i class="icon-chevron-right"></i> Facturation par client</a></li>
  <li class="list-group-item"><a href="<c:url value="/financial/report/billingPerProject"/>"><i class="icon-chevron-right"></i> Facturation par projet</a></li>
</ul>
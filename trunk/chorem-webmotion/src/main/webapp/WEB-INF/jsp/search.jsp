<%--
  #%L
  Chorem webmotion
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2011 - 2012 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:forEach var="key" items="${result.keySet()}">
    <dl>
        <dt>
            <ul class="nav nav-pills">
                <li>
                    <a href="<c:url value="/wikitty/${key}/search"/>">${key}</a>
                </li>
                <li>
                    <a class="btn btn-success btn-xs" href="<c:url value="/wikitty/${key}/edit/new"/>"><i class="icon-plus icon-white"></i> Add</a>
                </li>
                <li>
                    <form class="form-search form-inline" action="<c:url value="/wikitty/${key}/search"/>">
                        <input type="text" class="search-query" placeholder="Search" name="query"/>
                    </form>
                </li>
            </ul>
        </dt>
        <c:forEach var="w" items="${result.get(key)}">
            <dd><a href="<c:url value="/wikitty/${key}/view/${w.id}"/>"><c:out value="${w.toString(key)}"/></a></dd>
        </c:forEach>
    </dl>
</c:forEach>

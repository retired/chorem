/*
 * #%L
 * Chorem :: webmotion
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


$(document).ready(function() {


	var editCounter = 0;
	var blockCounter = 0;

	$("#companySelect").change(function() {
		$("#companyForm").submit();
	});

	$(".employeeEdit").click(employeeEdit);
	$(".cjmRefresh").click(calculateAdc);
	$('.allCjmRefresh').click(calculateMultipleAdc);
	
	
	$('#editAllBtn').click(function() {
		$('.employeeEdit').click();	
	});
	
	$('#validateAllBtn').click(function() {
		$('.editOk').click();	
	});
	
	$('#cancelAllBtn').click(function() {
		$('.editCancel').click();	
	});
	
	$('#searchForm').submit(function() {
		$('.tableEdit tbody tr').each(function() {
			
			var name = $.trim($(this).find('.person').text().toLowerCase());
			//alert(name + "/" + name.indexOf($('#searchInput').val()));
			if(name.indexOf($('#searchInput').val().toLowerCase()) !== -1 ) {
				$(this).show();
			}
			else {
				$(this).hide();
			}
			
		});
		
		return false;
	});
	
	
	function employeeEdit() {
		var row = $(this).parent().parent();
		var salary = row.find(".salary");
		var other = row.find(".otherPayments");
		var prod = row.find(".productivityRate");
		var time = row.find(".partialTime");
		var cjm = row.find(".dailyReturn");
		
		salary.attr("oldVal", $.trim(salary.html()));
		salary.html("<input type = 'text' size='3' value='"+$.trim(salary.html())+"'/>");
		other.attr("oldVal", $.trim(other.html()));
		other.html("<input type = 'text' size='3' value='"+$.trim(other.html())+"'/>");
		prod.attr("oldVal", $.trim(prod.html()));
		prod.html("<input type = 'text' size='3' value='"+$.trim(prod.html()).replace('%','')+"'/>");
		time.attr("oldVal", $.trim(time.html()));
		time.html("<input type = 'text' size='3' value='"+$.trim(time.html()).replace('%','')+"'/>");
		cjm.attr("oldVal", $.trim(cjm.find('.adc').html()));
		cjm.html("<input type = 'text' size='3' value='"+$.trim(cjm.find('.adc').html()).replace('%','')+"'/>");

		displayConfirmation(row);
		editCounter++;
		if(editCounter === 1) 
			$('.editBtn').removeAttr('disabled');
			
		

	}

	function editCancel() {

		var row = $(this).parent().parent();

		var salary = row.find(".salary");
		var other = row.find(".otherPayments");
		var prod = row.find(".productivityRate");
		var time = row.find(".partialTime");
		var cjm = row.find('.dailyReturn');
		
		salary.html($.trim(salary.attr('oldVal')));
		other.html($.trim(other.attr('oldVal')));
		prod.html($.trim(prod.attr('oldVal')));
		time.html($.trim(time.attr('oldVal')));


		displayEdit(row);
		var refresh = false;
		if(cjm.attr("refresh") === "true")
			refresh = true;
		displayDailyReturn(row, $.trim(cjm.attr('oldVal')), refresh);
		editCounter--;
		if(editCounter === 0)
			$('.editBtn').attr('disabled', 'disabled');
		

	}

	function editOk() {
		var row = $(this).parent().parent();
		var cell = $(this).parent();

		var salary 	= row.find(".salary");
		var other = row.find(".otherPayments");
		var prod 	= row.find(".productivityRate");
		var time 	= row.find(".partialTime");
		var cjm 	= row.find(".dailyReturn");

		var salaryVal 	= $.trim(salary.find("input").attr('value'));
		var otherVal 	= $.trim(other.find("input").attr('value'));
		var prodVal 	= $.trim(prod.find("input").attr('value'));
		var timeVal 	= $.trim(time.find("input").attr('value'));
		var cjmVal 		= $.trim(cjm.find("input").attr('value'));
		
		blockEdit(row);
		blockCounter++;
		if(blockCounter === 1) {
			$('.editBtn').attr('disabled', 'disabled');
			$('#editAllBtn').attr('disabled', 'disabled');
			$('.spinner').show();
		}
		$.get(createUrl("/hr/employeeEdit/json/editEmployeeValues/",row.attr("id"), "?salaryStr=",
				salaryVal, "&productivityRateStr=", prodVal, "&partialTimeStr=", timeVal,
				"&dailyReturnStr=", cjmVal, "&otherPaymentsStr=", otherVal),
				function(ret){
			var data = ret['data'];

			if(data === "error") {
				row.find("input").css("background-color", "white");
				row.find(".errorMessage").remove();
				var errors = ret['errors'];
				for(var i = 0; i < errors.length; i++) {
					row.find("." + errors[i]["field"]+" input").css("background-color", "#FF5555");
					row.find("." + errors[i]["field"]).append(
							"<span class='errorMessage'>"+errors[i]["errorMessage"]+"</span>");
				}
				displayConfirmation(row);
				blockCounter--;
				if(blockCounter === 0) {
					$('.editBtn').removeAttr('disabled');
					$('.spinner').hide();
				}
			}
			else 
			{
				salary.html(data['salary']);
				other.html(data['otherPayments']);
				prod.html(data["productivityRate"]+"%");
				time.html(data['partialTime']+"%");
				displayDailyReturn(row, data['dailyReturn'], true);
				row.find('.dailyHoursWorked').html(data['dailyHoursWorked']);
				displayEdit(row);
				if(cell.attr("refresh") !== "true")
					cell.attr('refresh', 'true');
				editCounter--;
				blockCounter--;
				//Only case where the butotn should be reactivated
				if(editCounter !== 0 && blockCounter === 0)
					$('.editBtn').removeAttr('disabled');
				else
					$('.editBtn').attr('disabled', 'disabled');
				if(blockCounter === 0) {
					$('.spinner').hide();
					$('#editAllBtn').removeAttr('disabled');
				}
			}
			
			
		}
		);


	}
	
	function calculateAdc() {
		var row = $(this).parent().parent();
		var cjm = row.find(".dailyReturn");
		blockDailyReturn(row);
		blockEdit(row);
		$.get(createUrl("/hr/employeeEdit/json/requestAdc/",row.attr("id"),"?real=false"),
				function(ret){
			var adc = ret['adc'];
			displayDailyReturn(row, adc, true);
			displayEdit(row);
		});
		
	}
	
	function calculateMultipleAdc() {
		
		var cjmCells = $('.cjmRefresh').parent();
		var rows = cjmCells.parent();
		blockDailyReturn(rows);
		blockEdit(rows);
		var str = "";
		for(var i = 0; i < rows.length; i++) {
			str = str + $(rows[i]).attr('id');
			if(i != (rows.length -1)) {
				str= str + ',';
			}
			
		}
		$.get(createUrl("/hr/employeeEdit/json/requestMultipleAdc/",str, "?real=false&year=0"),
				function(ret){
			var adcs = ret['adcs'];
			for(var i in adcs) {
				var row = $('#'+i);
				displayDailyReturn(row, adcs[i], true);
				displayEdit(row);
			}
		});
		
	}
	
	function blockEdit(row) {
		row.find('.cellEdit').html('<img src="../img/ajax-loader.gif"/>');
	}
	
	function blockDailyReturn(row) {
		row.find('.dailyReturn').html('<img src="../img/ajax-loader.gif"/>');
	}
	
	function displayConfirmation(row) {
		var cell = row.find('.cellEdit');
		cell.html("<a class='editOk' style='cursor:pointer'><i class='icon icon-ok'></i></a>"
				+ "<a class='editCancel' style='cursor:pointer'><i class='icon icon-remove'></i></a>"
		);
		row.find(".editCancel").click(editCancel);
		row.find(".editOk").click(editOk);
		
	}
	
	function displayEdit(row) {
		var cell = row.find('.cellEdit');
		cell.html("<a class='employeeEdit' style='cursor:pointer'><i class='icon icon-edit'></i></a>");
		cell.find(".employeeEdit").click(employeeEdit);
	}
	
	function displayDailyReturn(row, val, icon) {
		var cell = row.find('.dailyReturn');
		var str = '<span class="adc">' + val + ' </span>'
		if(icon)
			str += '<a class="cjmRefresh" style="cursor:pointer"><i class="icon icon-refresh"></i></a>'
		cell.html(str);
		row.find(".cjmRefresh").click(calculateAdc);
	}
	
	


});
package org.chorem.webmotion;

/*
 * #%L
 * Chorem :: webmotion
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class PaginatedResult<E> implements Iterable<E>, Serializable {

    protected List<E> elements;
    protected int currentPage;
    protected int count;
    protected int nbPages;
    protected int total;

    public PaginatedResult(List<E> elements, int currentPage, int count, int total) {
        this.elements = elements;
        this.currentPage = currentPage;
        this.count = count;
        this.total = total;
        this.nbPages = total == 0 ? 1 : (int) Math.ceil((double) total / (double) count);
    }

    @Override
    public Iterator<E> iterator() {
        if (this.elements == null) {
            return null;
        } else {
            return elements.iterator();
        }
    }

    public List<E> getElements() {
        return elements;
    }

    public void setElements(List<E> elements) {
        this.elements = elements;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getNbPages() {
        return nbPages;
    }

    public void setNbPages(int nbPages) {
        this.nbPages = nbPages;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}

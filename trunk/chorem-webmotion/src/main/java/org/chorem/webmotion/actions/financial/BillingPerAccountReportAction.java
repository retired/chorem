package org.chorem.webmotion.actions.financial;

/*
 * #%L
 * Chorem :: webmotion
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.ChoremClient;
import org.chorem.entities.*;
import org.chorem.webmotion.actions.sales.SalesData;
import org.chorem.webmotion.actions.sales.SalesReportHelper;
import org.debux.webmotion.server.WebMotionController;
import org.debux.webmotion.server.render.Render;
import org.nuiton.wikitty.WikittyClient;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jcouteau <couteau@codelutin.com>
 */
public class BillingPerAccountReportAction extends WebMotionController {

    /**
     * Rend le graphe des devis envoyés par mois
     *
     * @param client
     * @return
     */
    public Render billing(ChoremClient client, String from, String to) {

        if (null == from) {
            from = String.valueOf(BillingReportHelper.getFirstYear(client));
        }

        if (null == to) {
            to = String.valueOf(BillingReportHelper.getLastYear());
        }

        Map<Company, SalesData> billingData = getBillingPerAccountData(from, to, client);

        List<Integer> listAllYearsInChorem = BillingReportHelper.listAllYears(client);

        return renderView("financial/reports/billingPerAccountReport.jsp",
                "data", billingData,
                "allYears", listAllYearsInChorem,
                "fromYear", from,
                "toYear", to);
    }

    protected Map<Company,SalesData> getBillingPerAccountData(String firstYear,
                                                            String lastYear,
                                                            WikittyClient client){

        Date last = BillingReportHelper.getLastDayOfYear(Integer.valueOf(lastYear));
        Date first = BillingReportHelper.getFirstDayOfYear(Integer.valueOf(firstYear));

        Map<Company,SalesData> salesData = new LinkedHashMap<Company, SalesData>();

        //a query to get all the accounts
        //FIXME JC 2012-01-26 Really bad to  find all employees and iterate on them :(
        WikittyQuery accountsQuery = new WikittyQueryMaker().and()
                .exteq(Company.EXT_COMPANY).end();

        List<Company> accounts = client.findAllByQuery(Company.class, accountsQuery).getAll();

        //iterate and two queries per account :(
        for (Company account:accounts){

            SalesData projectData = new SalesData();

            WikittyQuery accountQuery = new WikittyQueryMaker()
                    .select().sum("FinancialTransaction.amount").where().and()
                    .eq(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYER, account)
                    .bw(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EMITTEDDATE, first, last)
                    .end();

            Integer sales = client.findByQuery(Integer.class, accountQuery);

            //TODO JC 2012-01-26 Find a way to replace two queries into one.
            WikittyQuery quotationsQuery = new WikittyQueryMaker().and()
                    .eq(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYER, account)
                    .bw(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EMITTEDDATE, first, last)
                    .end();

            List<FinancialTransaction> quotations = client.findAllByQuery(FinancialTransaction.class,
                    quotationsQuery).getAll();

            //Rempli la map que si on a des valeurs
            if (null != sales && sales != 0) {
                projectData.setSales(sales);
                projectData.setQuotations(quotations.size());
                salesData.put(account, projectData);
            }
        }

        return salesData;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.chorem.webmotion;

/*
 * #%L
 * Chorem webmotion
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author ble
 */
public class ChoremDateUtil extends GregorianCalendar {
    

    // -- Constructor ----------------------------------------------------------
    
    public ChoremDateUtil() {
        super();
    }
    
    public ChoremDateUtil(Date d) {
        this.setTime(d);
    }
    
    public ChoremDateUtil(int jj, int mm, int aaaa) {
        this.set(aaaa, (mm-1), jj);
    }
    
    public ChoremDateUtil(String jour, String mois, String annee) {
        this.set(Integer.parseInt(jour),(Integer.parseInt(mois)-1),Integer.parseInt(annee));
    }
    
    
    // -- Methods --------------------------------------------------------------
    
    public Integer dayOfWeek() {  // retourne le numero de jour de la date...
	return this.get(DAY_OF_WEEK);
    }
    
}

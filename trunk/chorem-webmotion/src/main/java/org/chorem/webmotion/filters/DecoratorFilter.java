/*
 * #%L
 * Chorem webmotion
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem.webmotion.filters;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.debux.webmotion.server.WebMotionFilter;
import org.debux.webmotion.server.render.Render;

/**
 * Filtre pour decorer les pages. Il est possible de le configurer pour modifier
 * le fichier view par defaut ou le nom du slot principale
 * <p>
 * parametre possible
 * <li> wmDecoratorView: le nom du fichier jsp a utiliser comme decorateur (defaut: decorator.jsp)
 * <li> wmDecoratorMain: le nom du slot utilise pour mettre le contenu principal (defaut: slotContent)
 * 
 * Si dans la requete on trouve un attribut "wmDecoratorNo" qui vaut true alors
 * on ne fait pas la decoration.
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class DecoratorFilter extends WebMotionFilter {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(DecoratorFilter.class);

    public Render decorate(String wmDecoratorNo, String wmDecoratorView, String wmDecoratorMain) {
        Boolean nodecorator = (Boolean)getContext().getRequest().getAttribute("wmDecoratorNo");
        if (StringUtils.equalsIgnoreCase("true", wmDecoratorNo)
                || nodecorator != null && nodecorator) {
            log.debug("Decorator direct process");
            doProcess();
            return null;
        } else {
            getContext().getRequest().setAttribute("wmDecoratorNo", Boolean.TRUE);
            String url = getContext().getUrl();
            // pas besoin de mettre le query car, les arguments sont deja parse et stocke dans la request
            log.debug("Decorate page '"+ url +"'");

            if (StringUtils.isBlank(wmDecoratorView)) {
                wmDecoratorView = "decorator.jsp";
            }
            if (StringUtils.isBlank(wmDecoratorMain)) {
                wmDecoratorMain = "slotContent";
            }
            return renderView(wmDecoratorView, wmDecoratorMain, url);
        }
    }

}

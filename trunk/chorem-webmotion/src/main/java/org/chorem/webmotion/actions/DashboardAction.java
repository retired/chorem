/*
 * #%L
 * Chorem webmotion
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem.webmotion.actions;


import com.inamik.utils.SimpleTableFormatter;
import com.inamik.utils.TableFormatter;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.input.ReaderInputStream;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.ChoremClient;
import org.chorem.ChoremQueryHelper;
import org.chorem.ChoremQueryMaker;
import org.chorem.entities.Accepted;
import org.chorem.entities.Attachment;
import org.chorem.entities.Category;
import org.chorem.entities.Configuration;
import org.chorem.entities.Employee;
import org.chorem.entities.FinancialTransaction;
import org.chorem.entities.Interval;
import org.chorem.entities.Invoice;
import org.chorem.entities.InvoiceStatus;
import org.chorem.entities.Quotation;
import org.chorem.entities.Rejected;
import org.chorem.entities.Sent;
import org.chorem.entities.Started;
import org.chorem.entities.Task;
import org.chorem.entities.TaskStatus;
import org.chorem.entities.Time;
import org.chorem.entities.Touch;
import org.chorem.entities.Warranty;
import org.debux.webmotion.server.WebMotionController;
import org.debux.webmotion.server.render.Render;
import org.nuiton.util.DateUtil;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.Element;
import org.nuiton.wikitty.entities.ElementField;
import org.nuiton.wikitty.query.FacetSortType;
import org.nuiton.wikitty.query.FacetTopic;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.query.WikittyQueryResult;
import org.nuiton.wikitty.query.WikittyQueryResultTreeNode;
import org.nuiton.wikitty.query.conditions.Condition;
import org.nuiton.wikitty.query.conditions.Select;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class DashboardAction extends WebMotionController {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(DashboardAction.class);

    static final public String budgetDateFormat = "MM/yyyy";
    static final public String summaryDateFormat = "dd/MM/yyyy hh:mm";
    //static final public String solRDateFormat = "yyyy-MM-dd'T'hh:mm:ss'Z'";

    protected <E> Map<String, List<Attachment>> prepareAttachment(ChoremClient client, Collection<E> ids) {
        // recherche des attachments de chaque quotation trouvee
        WikittyQuery attachmentQuery = new WikittyQueryMaker().and()
                .exteq(Attachment.EXT_ATTACHMENT)
                .containsOne(Attachment.FQ_FIELD_ATTACHMENT_TARGET, ids)
                .end();

        WikittyQueryResult<Attachment> attachmentResult =
                client.findAllByQuery(Attachment.class, attachmentQuery);

        // preparation des attachements pour chaque quotation
        Map<String, List<Attachment>> attachments =
                new MapWithDefault<String, List<Attachment>>(
                new HashMap<String, List<Attachment>>(), new LinkedList<Attachment>());
        for (Attachment a : attachmentResult) {
            String id = a.getTarget();
            attachments.get(id).add(a);
        }

        return attachments;
    }

    public double computeWorkingDays(ChoremClient client, Time t) {
        Configuration config = client.getConfiguration();
        long workingTime = t.getEndDate().getTime() - t.getBeginDate().getTime();
        double result = workingTime / (1000.0 * 3600.0 * config.getDailyHoursWorked()); // N heures de travail par jour
        return result;
    }


    ////////////////////////////////////////////////////////////////////////////
    //
    // C O N T A C T
    //
    ////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////
    //
    // A C C U E I L
    //
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Cette requete comporte un select retournant un nombre nomme:
     * <li>debts: le montant TTC
     *
     * Le calcul se fait sur toutes les FinancialTransaction non annulee
     * 
     * @param first
     * @param last
     * @param companyId l'identifiant de la company dont elle ou un de ses employers
     * doit etre en Payer
     * @return
     */
    protected WikittyQuery getDebtQuery(Date first, Date last, String companyId) {
        // toutes les depenses depuis le debut de l'annee
        WikittyQuery debtQuery = new ChoremQueryMaker()
                .select()
                  .sum(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_AMOUNT, "debts")
                  .sum((String)null, "debtsTTC")
                    .function("#amountTTC")
                      .fieldValue(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_AMOUNT)
                      .fieldValue(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_VAT)
                .where()
                .and()
                    .exteq(FinancialTransaction.EXT_FINANCIALTRANSACTION)
                    .not().eq(Invoice.FQ_FIELD_INVOICE_STATUS, InvoiceStatus.CANCELED.name())
                    .bw(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EMITTEDDATE, first, last)
                    .containsOne(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYER)
                        .select(Element.ID).filterOnCompanyOrEmployee(companyId)
                .end().setLimit(WikittyQuery.MAX);

        return debtQuery;
    }

    /**
     * Cette requete comporte un select retournant deux nombre nomme:
     * <li>incomes: le montant total HT des factures
     * <li>incomesTTC: le montant total TTC des factures
     *
     * Le calcul se fait uniquement sur les Invoices
     *
     * @param first
     * @param last
     * @param companyId l'identifiant de la company dont elle ou un de ses employers
     * doit etre en Beneficiary
     * @return
     */
    protected WikittyQuery getIncomeInvoiceQuery(Date first, Date last, String companyId) {
        // toutes les factures emises depuis le debut de l'annee
        WikittyQuery incomeQuery = new ChoremQueryMaker()
                .select()
                  .sum(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_AMOUNT, "incomes")
                  .sum((String)null, "incomesTTC")
                    .function("#amountTTC")
                      .fieldValue(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_AMOUNT)
                      .fieldValue(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_VAT)
                .where()
                .and()
                    .exteq(Invoice.EXT_INVOICE)
                    .not().eq(Invoice.FQ_FIELD_INVOICE_STATUS, InvoiceStatus.CANCELED.name())
                    .bw(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EMITTEDDATE, first, last)
                    .containsOne(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_BENEFICIARY)
                        .select(Element.ID).filterOnCompanyOrEmployee(companyId)
                .end().setLimit(WikittyQuery.MAX);

        return incomeQuery;
    }

    /**
     * Cette requete comporte un select retournant deux nombre nomme:
     * <li>extraIncomes: le montant total HT des factures
     * <li>extraIncomesTTC: le montant total TTC des factures
     *
     * Le calcul se fait sur toutes les FinancialTransaction qui ne sont pas aussi Invoice
     *
     * @param first
     * @param last
     * @param companyId l'identifiant de la company dont elle ou un de ses employers
     * doit etre en Beneficiary
     * @return
     */
    protected WikittyQuery getExtraIncomeQuery(Date first, Date last, String companyId) {
        // toutes les factures emises depuis le debut de l'annee
        WikittyQuery incomeQuery = new ChoremQueryMaker()
                .select()
                  .sum(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_AMOUNT, "extraIncomes")
                  .sum((String)null, "extraIncomesTTC")
                    .function("#amountTTC")
                      .fieldValue(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_AMOUNT)
                      .fieldValue(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_VAT)
                .where()
                .and()
                    .exteq(FinancialTransaction.EXT_FINANCIALTRANSACTION)
                    .extne(Invoice.EXT_INVOICE)
                    .bw(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EMITTEDDATE, first, last)
                    .containsOne(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_BENEFICIARY)
                        .select(Element.ID).filterOnCompanyOrEmployee(companyId)
                .end().setLimit(WikittyQuery.MAX);

        return incomeQuery;
    }

    /**
     * Retourne le total des entree et sortie entre deux dates, les valeurs retournees sont:
     *
     * <li>debts: le montant HT (ne veut pas forcement dire grand chose, ex declaration TVA)
     * <li>debtsTTC: le montant TTC
     * <li>incomes: le montant total HT des factures
     * <li>incomesTTC: le montant total TTC des factures
     * <li>extraIncomes: le montant total HT des revenus hors facture
     * <li>extraIncomesTTC: le montant total TTC des revenus hors factures
     *
     * <li>debtsQuery: la condition sous forme texte qui a permit de calculer les depenses
     * <li>incomesQuery: la condition sous forme texte qui a permit de calculer les entrees
     * <li>extraIncomesQuery: la condition sous forme texte qui a permit de calculer les entrees supplementaire
     *
     * @param client
     * @param first
     * @param last
     * @param companyId
     * @return
     */
    protected Map<String, Object> getDebtIncome(ChoremClient client,
            Date first, Date last, String companyId) {
        WikittyQuery debt = getDebtQuery(first, last, companyId);
        WikittyQuery income = getIncomeInvoiceQuery(first, last, companyId);
        WikittyQuery extraIncome = getExtraIncomeQuery(first, last, companyId);

        // on recupere les condition qui ont permit de faire le calcule
        Condition[] cond = new Condition[3];
        cond[0] = ((Select)debt.getCondition()).getSubCondition();
        cond[1] = ((Select)income.getCondition()).getSubCondition();
        cond[2] = ((Select)extraIncome.getCondition()).getSubCondition();


        List<WikittyQuery> queries = new ArrayList<WikittyQuery>(3);
        queries.add(debt);
        queries.add(income);
        queries.add(extraIncome);

        List<Map<String, Double>> values = client.findByQueryAsMap(
                    Double.class, queries);
        Map aggregate = new HashMap<String, Object>();
        for (Map m : values) {
            aggregate.putAll(m);
        }

        aggregate.put("debtsQuery", debt.getWhere().toString());
        aggregate.put("incomesQuery", income.getWhere().toString());
        aggregate.put("extraIncomesQuery", extraIncome.getWhere().toString());

        return aggregate;
    }

    /**
     * Dashbord qui indique pour toutes les annees du systeme le CA et les depenses
     * @param client
     * @return
     */
    public Render annualProfit(ChoremClient client) {

        String companyId = client.getConfiguration().getDefaultCompany();

        long timeStart = System.currentTimeMillis();

        // Recuperation de la 1er et derniere annee, via les facets, c'est le plus rapide
        // via un select > 5s, via les facets ~0.150s
        WikittyQuery dateQueryFacet = new WikittyQueryMaker()
                .exteq(FinancialTransaction.EXT_FINANCIALTRANSACTION)
                .end().setLimit(0).addFacetField(
                Element.get(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EMITTEDDATE))
                .setFacetSort(FacetSortType.name)
                .setFacetLimit(WikittyQuery.MAX);

        WikittyQueryResult<String> resultFacet = client.findAllByQuery(dateQueryFacet);
        List<FacetTopic> topics = resultFacet.getFacets().get(
                FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EMITTEDDATE);

        Date firstDay = WikittyUtil.toDate(topics.get(0).getTopicName());
        Date lastDay = WikittyUtil.toDate(topics.get(topics.size()-1).getTopicName());

        firstDay = DateUtil.setFirstDayOfYear(firstDay);
        lastDay = DateUtil.setLastDayOfYear(lastDay);

        firstDay = DateUtil.setMinTimeOfDay(firstDay);
        lastDay = DateUtil.setMaxTimeOfDay(lastDay);

        log.debug(String.format("annualProfit: %s to %s", firstDay, lastDay));

        Map<Date, Map<String, Object>> result = new TreeMap<Date, Map<String, Object>>();

        Date currentFirst = firstDay;
        Date currentLast = DateUtil.setLastDayOfYear(firstDay);
        currentLast = DateUtil.setMaxTimeOfDay(currentLast);
        while (currentLast.compareTo(lastDay) <= 0) {
            Map<String, Object> values = getDebtIncome(client,
                    currentFirst, currentLast, companyId);
            result.put(currentFirst, values);
            currentFirst = DateUtils.addYears(currentFirst, 1);
            currentLast = DateUtils.addYears(currentLast, 1);
        }

        return renderView("dashboardAnnualProfit.jsp", "annualProfit", result);
    }

    /**
     * Dashbord pour la page d'accueil
     * @param client
     * @return
     */
    public Render summary(ChoremClient client) {
        Date now = new Date();
        Date inOneWeek = DateUtils.addDays(now, 7);
        Date firstDayYear = DateUtil.setFirstDayOfYear(now);
        Date lastYearNow = DateUtils.addYears(now, -1);
        Date lastYearFirstDay = DateUtil.setFirstDayOfYear(lastYearNow);

        firstDayYear = DateUtil.setMinTimeOfDay(firstDayYear);
        lastYearFirstDay = DateUtil.setMinTimeOfDay(lastYearFirstDay);

        String companyId = client.getConfiguration().getDefaultCompany();

        // toutes les depenses depuis le debut de l'annee
        Map<String, Object> annualDebtIncome = getDebtIncome(client,
                firstDayYear, now, companyId);
        Map<String, Object> pastAnnualDebtIncome = getDebtIncome(client,
                lastYearFirstDay, lastYearNow, companyId);

//        WikittyQuery[] annualDebtIncomeQuery = getDebtIncomeQuery(firstDayYear, now, companyId);
//        WikittyQuery[] pastAnnualDebtIncomeQuery = getDebtIncomeQuery(lastYearFirstDay, lastYearNow, companyId);

        // les factures que notre societe aura a payer dans le futur (facture non encore recu)
        WikittyQuery invoiceExpectedDebtQuery = new ChoremQueryMaker()
                .select().sum(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_AMOUNT)
                .and()
                    .exteq(FinancialTransaction.EXT_FINANCIALTRANSACTION)
                    .ne(Invoice.FQ_FIELD_INVOICE_STATUS, InvoiceStatus.CANCELED.name())
                    .or().isNull(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EMITTEDDATE)
                         .gt(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EMITTEDDATE, now)
                         .close()
                    .isNull(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYMENTDATE)
                    .containsOne(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYER)
                        .select(Element.ID).filterOnCompanyOrEmployee(companyId)
                .end().setLimit(WikittyQuery.MAX);

        // les factures que notre societe doit payer mais peu encore attendre (facture recu)
        WikittyQuery invoiceReceivedDebtQuery = new ChoremQueryMaker()
                .select().sum(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_AMOUNT)
                .and()
                    .exteq(FinancialTransaction.EXT_FINANCIALTRANSACTION)
                    .ne(Invoice.FQ_FIELD_INVOICE_STATUS, InvoiceStatus.CANCELED.name())
                    .lt(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EMITTEDDATE, now)
                    .or().isNull(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EXPECTEDDATE)
                         .gt(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EXPECTEDDATE, inOneWeek)
                         .close()
                    .isNull(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYMENTDATE)
                    .containsOne(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYER)
                        .select(Element.ID).filterOnCompanyOrEmployee(companyId)
                .end().setLimit(WikittyQuery.MAX);

        // les factures que notre societe doit payer au plus tard dans 7 jours
        WikittyQuery invoiceDueDebtQuery = new ChoremQueryMaker()
                .select().sum(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_AMOUNT)
                .and()
                    .exteq(FinancialTransaction.EXT_FINANCIALTRANSACTION)
                    .ne(Invoice.FQ_FIELD_INVOICE_STATUS, InvoiceStatus.CANCELED.name())
                    .lt(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EXPECTEDDATE, inOneWeek)
                    .isNull(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYMENTDATE)
                    .containsOne(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYER)
                        .select(Element.ID).filterOnCompanyOrEmployee(companyId)
                .end().setLimit(WikittyQuery.MAX);

        // les factures prevu mais pas encore envoyees
        WikittyQuery invoiceExpectedIncomeQuery = new ChoremQueryMaker()
                .select().sum(Invoice.FQ_FIELD_FINANCIALTRANSACTION_AMOUNT)
                .and()
                    .exteq(FinancialTransaction.EXT_FINANCIALTRANSACTION)
                    .ne(Invoice.FQ_FIELD_INVOICE_STATUS, InvoiceStatus.CANCELED.name())
                    .or().isNull(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EMITTEDDATE)
                         .gt(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EMITTEDDATE, now)
                         .close()
                    .isNull(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYMENTDATE)
                    .containsOne(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_BENEFICIARY)
                        .select(Element.ID).filterOnCompanyOrEmployee(companyId)
                .end().setLimit(WikittyQuery.MAX);

        // les factures envoyer mais pas encore en retard
        WikittyQuery invoiceEmittedIncomeQuery = new ChoremQueryMaker()
                .select().sum(Invoice.FQ_FIELD_FINANCIALTRANSACTION_AMOUNT)
                .and()
                    .exteq(FinancialTransaction.EXT_FINANCIALTRANSACTION)
                    .ne(Invoice.FQ_FIELD_INVOICE_STATUS, InvoiceStatus.CANCELED.name())
                    .lt(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EMITTEDDATE, now)
                    .gt(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EXPECTEDDATE, now)
                    .isNull(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYMENTDATE)
                    .containsOne(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_BENEFICIARY)
                        .select(Element.ID).filterOnCompanyOrEmployee(companyId)
                .end().setLimit(WikittyQuery.MAX);

        // les factures qu'auraient du payer nos clients (ils sont en retard)
        WikittyQuery invoiceDueIncomeQuery = new ChoremQueryMaker()
                .select().sum(Invoice.FQ_FIELD_FINANCIALTRANSACTION_AMOUNT)
                .and()
                    .exteq(FinancialTransaction.EXT_FINANCIALTRANSACTION)
                    .ne(Invoice.FQ_FIELD_INVOICE_STATUS, InvoiceStatus.CANCELED.name())
                    .lt(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EXPECTEDDATE, now)
                    .isNull(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYMENTDATE)
                    .containsOne(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_BENEFICIARY)
                        .select(Element.ID).filterOnCompanyOrEmployee(companyId)
                .end().setLimit(WikittyQuery.MAX);

        // les contacts qui doivent etre pris dans les 7 prochains jours
        WikittyQuery touchQuery = new WikittyQueryMaker().and()
                .exteq(Touch.EXT_TOUCH)
                .lt(Interval.FQ_FIELD_INTERVAL_BEGINDATE, inOneWeek)
                .isNull(Interval.FQ_FIELD_INTERVAL_ENDDATE)
                .end().setLimit(0);

        WikittyQueryResult<Double>[] invoices =
                client.findAllByQuery(Double.class,
                invoiceExpectedDebtQuery, invoiceReceivedDebtQuery, invoiceDueDebtQuery,
                invoiceExpectedIncomeQuery, invoiceEmittedIncomeQuery, invoiceDueIncomeQuery);

        WikittyQueryResult<String> touchs =
                client.findAllByQuery(touchQuery);

        // nombre de facture
        int invoiceExpectedDebtNb = invoices[0].getTotalResult();
        int invoiceReceivedDebtNb = invoices[1].getTotalResult();
        int invoiceDueDebtNb = invoices[2].getTotalResult();
        // montant total de toutes les factures
        double invoiceExpectedDebt = invoices[0].peek();
        double invoiceReceivedDebt = invoices[1].peek();
        double invoiceDueDebt = invoices[2].peek();

        // nombre de facture
        int invoiceExpectedIncomeNb = invoices[3].getTotalResult();
        int invoiceEmittedIncomeNb = invoices[4].getTotalResult();
        int invoiceDueIncomeNb = invoices[5].getTotalResult();
        // montant total de toutes les factures
        double invoiceExpectedIncome = invoices[3].peek();
        double invoiceEmittedIncome = invoices[4].peek();
        double invoiceDueIncome = invoices[5].peek();

        int touchNb = touchs.getTotalResult();

        //CA HT
        double income =  Double.valueOf(annualDebtIncome.get("incomes").toString()) +
                Double.valueOf(annualDebtIncome.get("extraIncomes").toString());

        //CA HT année précédente
        double pastIncome =  Double.valueOf(pastAnnualDebtIncome.get("incomes").toString()) +
                Double.valueOf(pastAnnualDebtIncome.get("extraIncomes").toString());

        //Evolution du CA HT
        double incomeEvolution = (income-pastIncome)/pastIncome;

        //CA TTC
        double incomeTTC =  Double.valueOf(annualDebtIncome.get("incomesTTC").toString()) +
                Double.valueOf(annualDebtIncome.get("extraIncomesTTC").toString());

        //CA TTC année précédente
        double pastIncomeTTC =  Double.valueOf(pastAnnualDebtIncome.get("incomesTTC").toString()) +
                Double.valueOf(pastAnnualDebtIncome.get("extraIncomesTTC").toString());

        //Evolution du CA TTC
        double incomeTTCEvolution = (incomeTTC-pastIncomeTTC)/pastIncomeTTC;

        //dépenses TTC
        double debtsTTC =  Double.valueOf(annualDebtIncome.get("debtsTTC").toString());

        //dépenses TTC année précédente
        double pastDebtsTTC =  Double.valueOf(pastAnnualDebtIncome.get("debtsTTC").toString());

        //Evolution des dépenses TTC
        double debtsTTCEvolution = (debtsTTC-pastDebtsTTC)/pastDebtsTTC;

        //profitTTC
        double profitTTC = incomeTTC - debtsTTC;

        //profit TTC année précédente
        double pastProfitTTC = pastIncomeTTC - pastDebtsTTC;

        //Evolution profit TTC
        double profitTTCEvolution = (profitTTC - pastProfitTTC) / pastProfitTTC;

        String touchQueryString = touchQuery.getCondition().toString();

        return renderView("dashboardSummary.jsp",
                "date", now,
                "now", "\"" + WikittyUtil.toString(now) + "\"",  //DateFormatUtils.format(now, solRDateFormat),
                "inOneWeek", DateFormatUtils.format(inOneWeek, summaryDateFormat),
                "invoiceExpectedDebtNb", invoiceExpectedDebtNb,
                "invoiceReceivedDebtNb", invoiceReceivedDebtNb,
                "invoiceDueDebtNb", invoiceDueDebtNb,
                "invoiceExpectedDebt", invoiceExpectedDebt,
                "invoiceReceivedDebt", invoiceReceivedDebt,
                "invoiceDueDebt", invoiceDueDebt,
                "invoiceExpectedIncomeNb", invoiceExpectedIncomeNb,
                "invoiceEmittedIncomeNb", invoiceEmittedIncomeNb,
                "invoiceDueIncomeNb", invoiceDueIncomeNb,
                "invoiceExpectedIncome", invoiceExpectedIncome,
                "invoiceEmittedIncome", invoiceEmittedIncome,
                "invoiceDueIncome", invoiceDueIncome,
                "annualDebtIncome", annualDebtIncome,
                "pastAnnualDebtIncome", pastAnnualDebtIncome,
                "income", income,
                "pastIncome", pastIncome,
                "incomeEvolution", incomeEvolution,
                "incomeTTC", incomeTTC,
                "pastIncomeTTC", pastIncomeTTC,
                "incomeTTCEvolution", incomeTTCEvolution,
                "debtsTTC", debtsTTC,
                "pastDebtsTTC", pastDebtsTTC,
                "debtsTTCEvolution", debtsTTCEvolution,
                "profitTTC", profitTTC,
                "pastProfitTTC", pastProfitTTC,
                "profitTTCEvolution", profitTTCEvolution,
                "touchNb", touchNb,
                "touchQueryString", touchQueryString
                );
    }

    ////////////////////////////////////////////////////////////////////////////
    //
    // P R O J E C T
    //
    ////////////////////////////////////////////////////////////////////////////

    /**
     *
     * @param client wikitty client injected by wm
     * @param query query to filter projet (can be null)
     * @param statusStart status interval start (extension name)
     * @param statusEnd status interval end (extension name). (Can be null)
     * @param jspRender jsp used to render result
     * @param title title of jsp page
     * @param computeTask true if we want to compute task info
     * (task info not needed for not stated project)
     * @return
     */
    protected Render quotationFilter(ChoremClient client,
            Date start, Date end, String query,
            String statusStart, String statusEnd, boolean excludeRejected, String jspRender,
            String title, boolean computeTask) {

        if (start == null) {
            start = new Date();
            start = DateUtils.setMonths(start, Calendar.JANUARY);
        }

        if (end == null) {
            end = DateUtils.setMonths(start, Calendar.DECEMBER);
        }

        // recherche de la configuration pour les calculs
        Configuration config = client.getConfiguration();

        // recuperation des quotations en fonction de leur status
        WikittyQueryMaker quotationQueryMaker = new WikittyQueryMaker().and()
                .parse(query)
                .exteq(statusStart);

        if (excludeRejected) {
            quotationQueryMaker = quotationQueryMaker.extne(Rejected.EXT_REJECTED);
        }

        if (statusEnd != null){
            quotationQueryMaker = quotationQueryMaker.extne(statusEnd);
        }

        WikittyQuery quotationQuery = quotationQueryMaker.or()
                  .bw(Interval.FQ_FIELD_INTERVAL_BEGINDATE, start, end)
                  .bw(Interval.FQ_FIELD_INTERVAL_ENDDATE, start, end)
                  .bw(Sent.FQ_FIELD_SENT_POSTEDDATE, start, end)
                   .and()
                    .isNull(Interval.FQ_FIELD_INTERVAL_BEGINDATE)
                    .isNull(Interval.FQ_FIELD_INTERVAL_ENDDATE)
                    .isNull(Sent.FQ_FIELD_SENT_POSTEDDATE)
                .end().setLimit(WikittyQuery.MAX);

        WikittyQueryResult<Quotation> result =
                client.findAllByQuery(Quotation.class, quotationQuery);

        // recherche des attachments de chaque quotation trouvee
        Map<String, List<Attachment>> attachments =
                prepareAttachment(client, result.getAll());

        // TODO poussin 20120523 ajouer la recherche des note sur la quotation

        // si besoin recherche le nombre de tache ouvert et ferme
        // key: quotation id
        Map<String, TaskInfo> taskInfos = new MapWithDefault<String, TaskInfo>(
                new HashMap<String, TaskInfo>(), TaskInfo.class);

        int opened = 0;
        int closed = 0;
        if (computeTask) {
            // recherche des taches
            WikittyQuery taskQuery = new WikittyQueryMaker().and()
                    .containsOne(Task.FQ_FIELD_TASK_QUOTATION, result.getAll())
                    .end();

            WikittyQueryResult<Task> taskResult =
                    client.findAllByQuery(Task.class, taskQuery);

            // recherche des temps sur les taches
            WikittyQuery timeQuery = new WikittyQueryMaker().and()
                    .containsOne(Time.FQ_FIELD_TIME_TASK, taskResult.getAll())
                    .end();

            WikittyQueryResult<Time> timeResult =
                    client.findAllByQuery(Time.class, timeQuery);

            for (Task t : taskResult) {
                String id = t.getQuotation();
                TaskInfo info = taskInfos.get(id);
                info.estimatedDays += t.getEstimatedDays();
                if (TaskStatus.SCHEDULED.toString().equalsIgnoreCase(t.getStatus())
                        || TaskStatus.STARTED.toString().equalsIgnoreCase(t.getStatus())) {
                    info.opened++;
                    opened++;
                } else {
                    info.closed++;
                    closed++;
                }
            }

            for (Time t : timeResult) {
                Task task = t.getTask(true);
                String id = task.getQuotation();
                TaskInfo info = taskInfos.get(id);
                info.workingDays += computeWorkingDays(client, t);
            }
        }

        // calcul des montants totaux
        double amount = 0;
        double amountHope = 0;
        double hopedProfit = 0;
        double realProfit = 0;
        for (Quotation q : result) {
            amount += q.getAmount();
            amountHope += q.getAmount() * q.getConversionHope() / 100.0;
            TaskInfo info = taskInfos.get(q.getWikittyId());

            info.hopedPriceDay = q.getAmount() / q.getEstimatedDays();
            info.realPriceDay = q.getAmount() / info.workingDays;
            info.hopedProfit = q.getAmount() - (q.getEstimatedDays() * config.getDailyReturn());
            info.realProfit = q.getAmount() - (info.workingDays * config.getDailyReturn());

            hopedProfit += info.hopedProfit;
            realProfit += info.realProfit;
        }

        return renderView(jspRender,
                "title", title,
                "quotations", result.getAll(),
                "amount", amount,
                "amountHope", amountHope,
                "hopedProfit", hopedProfit,
                "realProfit", realProfit,
                "opened", opened,
                "closed", closed,
                "attachments", attachments,
                "taskInfos", taskInfos);
    }

    public Render quotation(ChoremClient client,
            Date start, Date end, String query) {
        String jsp = "dashboardQuotation.jsp";
        String title = "Les propositions en attente de réponse";
        return quotationFilter(client, start, end, query, Quotation.EXT_QUOTATION, Accepted.EXT_ACCEPTED, true, jsp, title, false);
    }

    public Render projectOpen(ChoremClient client,
            Date start, Date end, String query) {
        String jsp = "dashboardProject.jsp";
        String title = "Les projets en cours";
        return quotationFilter(client, start, end, query, Accepted.EXT_ACCEPTED, Warranty.EXT_WARRANTY, true, jsp, title, true);
    }

    public Render projectClosed(ChoremClient client,
            Date start, Date end, String query) {
        String jsp = "dashboardProject.jsp";
        String title = "Les projets clos";
        return quotationFilter(client, start, end, query, Warranty.EXT_WARRANTY, null, true, jsp, title, true);
    }

    static public class TaskInfo implements Cloneable {
        /** nombre de tache ouverte (scheduled ou started) */
        public int opened;
        /** nombre de tache ferme (finished ou closed) */
        public int closed;
        /** nombre de jour estime pour toutes les taches */
        public double estimatedDays;
        /** nombre de jour passe reellement */
        public double workingDays;
        /** prix jour espere */
        public double hopedPriceDay;
        /** prix jour reel */
        public double realPriceDay;
        /** profit esperer pour ce projet */
        public double hopedProfit;
        /** profit reel pour ce projet */
        public double realProfit;

        @Override
        public Object clone() throws CloneNotSupportedException {
            return super.clone();
        }

        public int getOpened() {
            return opened;
        }

        public void setOpened(int opened) {
            this.opened = opened;
        }

        public int getClosed() {
            return closed;
        }

        public void setClosed(int closed) {
            this.closed = closed;
        }

        public double getEstimatedDays() {
            return estimatedDays;
        }

        public void setEstimatedDays(double estimatedDays) {
            this.estimatedDays = estimatedDays;
        }

        public double getWorkingDays() {
            return workingDays;
        }

        public void setWorkingDays(double workingDays) {
            this.workingDays = workingDays;
        }

        public double getHopedPriceDay() {
            return hopedPriceDay;
        }

        public void setHopedPriceDay(double hopedPriceDay) {
            this.hopedPriceDay = hopedPriceDay;
        }

        public double getRealPriceDay() {
            return realPriceDay;
        }

        public void setRealPriceDay(double realPriceDay) {
            this.realPriceDay = realPriceDay;
        }

        public double getHopedProfit() {
            return hopedProfit;
        }

        public void setHopedProfit(double hopedProfit) {
            this.hopedProfit = hopedProfit;
        }

        public double getRealProfit() {
            return realProfit;
        }

        public void setRealProfit(double realProfit) {
            this.realProfit = realProfit;
        }
        
    }

    ////////////////////////////////////////////////////////////////////////////
    //
    // F I N A N C I A L
    //
    ////////////////////////////////////////////////////////////////////////////

    /**
     *
     * @param client
     * @param title
     * @param filter le filtre de la method dashboard appelante
     * @param noUseDate indique de ne pas utiliser les dates, mais seulement filter et query
     * @param start si noUseDate est false est utilise pour filtrer suivant les dates
     * si start est null alors la 1 janvier de l'annee courante est pris
     * @param end si noUseDate est false est utilise pour filtrer suivant les dates
     * si end est null alors le 31 decembre de l'annee courante est pris
     * @param query le filtre de l'utilisateur
     * @return
     */
    protected Render invoiceFilter(ChoremClient client, String title,
            String filter, boolean useDate, Date start, Date end, String query) {
        // recuperation des factures
        WikittyQueryMaker maker = new WikittyQueryMaker().and()
                .parse(filter)
                .parse(query)
                .exteq(FinancialTransaction.EXT_FINANCIALTRANSACTION)
                .ne(Invoice.FQ_FIELD_INVOICE_STATUS, InvoiceStatus.CANCELED.name());

        if (useDate) {
            if (start == null) {
                start = new Date();
                start = DateUtils.setMonths(start, Calendar.JANUARY);
            }

            if (end == null) {
                end = DateUtils.setMonths(start, Calendar.DECEMBER);
            }

            start = DateUtil.setMinTimeOfDay(start);
            end = DateUtil.setMaxTimeOfDay(end);

            maker.or()
                    .bw(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EMITTEDDATE, start, end)
                    .isNull(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EMITTEDDATE)
                    .bw(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EXPECTEDDATE, start, end)
                    .bw(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYMENTDATE, start, end);
        }
        WikittyQuery invoiceQuery = maker.end().setLimit(WikittyQuery.MAX);

        ChoremQueryHelper.addSort(client, invoiceQuery, FinancialTransaction.EXT_FINANCIALTRANSACTION);

        WikittyQueryResult<FinancialTransaction> invoices =
                client.findAllByQuery(FinancialTransaction.class, invoiceQuery);

        log.debug(String.format("InvoiceFilter result(%s) query=%s", invoices.size(), invoiceQuery));

        double amount = 0;
        double amountPaid = 0;
        double amountExpected = 0;
        // calcul des amounts
        for (FinancialTransaction i : invoices) {
            double v = i.getAmount();
            amount += v;
            if (i.getPaymentDate() == null) {
                amountExpected += v;
            } else {
                amountPaid += v;
            }
        }

        // recherche des attachments de chaque quotation trouvee
        Map<String, List<Attachment>> attachments =
                prepareAttachment(client, invoices.getAll());

        return renderView("dashboardInvoice.jsp",
                "title", title,
                "invoices", invoices.getAll(),
                "amount", amount,
                "amountPaid", amountPaid,
                "amountExpected", amountExpected,
                "attachments", attachments
                );
    }

    public Render invoiceDebt(ChoremClient client, boolean useDate,
            Date start, Date end, String query) {
        String title = "Factures à payer par la société";
        String companyId = client.getConfiguration().getDefaultCompany();
        String filter = FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYER + "={SELECT id WHERE (" +
                Employee.FQ_FIELD_EMPLOYEE_COMPANY + "=" + companyId+" OR id=" + companyId + ")}";
        return invoiceFilter(client, title, filter, useDate, start, end, query);
    }

    public Render invoiceIncome(ChoremClient client, boolean useDate,
            Date start, Date end, String query) {
        String title = "Factures émises par la société";
        String companyId = client.getConfiguration().getDefaultCompany();
        String filter = FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_BENEFICIARY + "={SELECT id WHERE (" +
                Employee.FQ_FIELD_EMPLOYEE_COMPANY + "=" + companyId+" OR id=" + companyId + ")}";
        return invoiceFilter(client, title, filter, useDate, start, end, query);
    }

    public Render profitability(ChoremClient client,
            Date start, Date end, String query) {
        String jsp = "dashboardProfitability.jsp";
        String title = "Profitability";
        return quotationFilter(client, start, end, query, Started.EXT_STARTED, null, true, jsp, title, true);
    }

    /**
     * Prévisionnel entre deux dates en fonction des factures
     */
    public Render budget(ChoremClient client, Integer depth, Date start, Date end, String query, String type) {
        if (log.isDebugEnabled()) {
            log.debug(String.format(
                    "budget for period '%s' to '%s' with filter '%s'",
                    start, end, query));
        }
        String companyId = client.getConfiguration().getDefaultCompany();

        if (StringUtils.isBlank(companyId)) {
            getContext().addInfoMessage("message", "Vous devez définir une société par défaut");
            return renderURL("/admin/variables");
        }

        if (start == null) {
            start = new Date();
            start = DateUtils.addMonths(start, -2);
        }

        if (end == null) {
            end = DateUtils.addMonths(start, 6);
        }

        start = DateUtil.setFirstDayOfMonth(start);
        end = DateUtil.setLastDayOfMonth(end);

        start = DateUtil.setMinTimeOfDay(start);
        end = DateUtil.setMaxTimeOfDay(end);

        // La somme des factures que la company doit payer jusqu'a la date demandee
        WikittyQuery debt = new ChoremQueryMaker()
                .select()
                  .sum((String)null, "debts")
                    .function("#amountTTC")
                      .fieldValue(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_AMOUNT)
                      .fieldValue(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_VAT)
                .where()
                .and()
                  .exteq(FinancialTransaction.EXT_FINANCIALTRANSACTION)
                  .containsOne(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYER)
                    .select(Element.ID).filterOnCompanyOrEmployee(companyId)
                  .close()
                .not().eq(Invoice.FQ_FIELD_INVOICE_STATUS, InvoiceStatus.CANCELED)
                  .or()
                    .and()
                      .isNotNull(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYMENTDATE)
                      .lt(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYMENTDATE, start)
                    .close()
                    .and()
                      .isNull(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYMENTDATE)
                      .lt(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EXPECTEDDATE, start)
                .end()
                .setLimit(WikittyQuery.MAX);

        // La somme des factures que la company doit percevoir jusqu'a la date demandee
        WikittyQuery income = new ChoremQueryMaker()
                .select()
                  .sum((String)null, "incomes")
                    .function("#amountTTC")
                      .fieldValue(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_AMOUNT)
                      .fieldValue(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_VAT)
                .where()
                .and()
                  .exteq(FinancialTransaction.EXT_FINANCIALTRANSACTION)
                  .containsOne(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_BENEFICIARY)
                    .select(Element.ID).filterOnCompanyOrEmployee(companyId)
                  .close()
                  .not().eq(Invoice.FQ_FIELD_INVOICE_STATUS, InvoiceStatus.CANCELED)
                  .or()
                    .and()
                      .isNotNull(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYMENTDATE)
                      .lt(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYMENTDATE, start)
                    .close()
                    .and()
                      .isNull(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYMENTDATE)
                      .lt(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EXPECTEDDATE, start)
                .end()
                .setLimit(WikittyQuery.MAX);

        // La liste des factures entre les deux dates que l'on doit payer
        WikittyQuery invoiceDebt = new ChoremQueryMaker().and()
                .parse(query)
                  .exteq(FinancialTransaction.EXT_FINANCIALTRANSACTION)
                  .containsOne(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYER)
                    .select(Element.ID).filterOnCompanyOrEmployee(companyId)
                  .close()
                .not().eq(Invoice.FQ_FIELD_INVOICE_STATUS, InvoiceStatus.CANCELED)
                .or()
                  .and()
                    .isNotNull(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYMENTDATE)
                    .bw(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYMENTDATE, start, end)
                .close()
                  .and()
                    .isNull(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYMENTDATE)
                    .bw(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EXPECTEDDATE, start, end)
                .end()
                .setLimit(WikittyQuery.MAX);

        // La liste des factures entre les deux dates que l'on doit payer
        WikittyQuery invoiceIncome = new ChoremQueryMaker().and()
                .parse(query)
                  .exteq(FinancialTransaction.EXT_FINANCIALTRANSACTION)
                  .containsOne(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_BENEFICIARY)
                    .select(Element.ID).filterOnCompanyOrEmployee(companyId)
                  .close()
                .not().eq(Invoice.FQ_FIELD_INVOICE_STATUS, InvoiceStatus.CANCELED)
                .or()
                  .and()
                    .isNotNull(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYMENTDATE)
                    .bw(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYMENTDATE, start, end)
                .close()
                  .and()
                    .isNull(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYMENTDATE)
                    .bw(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EXPECTEDDATE, start, end)
                .end()
                .setLimit(WikittyQuery.MAX);

        // total des factures avant les dates selectionnes
        Double[] amounts = client.findByQuery(Double.class, debt, income);

        // toutes les factures des mois selectionnes
        WikittyQueryResult<FinancialTransaction>[] invoices =
                client.findAllByQuery(FinancialTransaction.class, invoiceDebt, invoiceIncome);

        // recuperation des noeuds root des categories
        WikittyQuery rootCategoriesQuery = new WikittyQueryMaker().and()
                .exteq(Category.EXT_CATEGORY)
                .isNull(Category.FQ_FIELD_WIKITTYTREENODE_PARENT)
                .end()
                .addSortAscending(new ElementField(Category.FQ_FIELD_CATEGORY_INDEX));

        WikittyQueryResult<Category> rootCategory =
                client.findAllByQuery(Category.class, rootCategoriesQuery);

        // les arbres de toutes les categories
        List<WikittyQueryResultTreeNode<Category>> categoriesTree =
                new LinkedList<WikittyQueryResultTreeNode<Category>>();
        for (Category c : rootCategory) {
            WikittyQueryResultTreeNode<Category> treeResults =
                    client.findTreeNode(Category.class, c.getWikittyId(), Integer.MAX_VALUE, false, null);
            if (treeResults != null) {
                CollectionUtils.addAll(categoriesTree, treeResults.preorderEnumeration());
            }
        }

        // beginningFinances contient l'etat du compte, au depart beginningFinances vaut "recette passee" - "depense passee"
        double finances = amounts[1] - amounts[0];

        BudgetData data = new BudgetData(start, end, categoriesTree, finances);

        // Debt
        for (FinancialTransaction i : invoices[0]) {
            data.add(true, i);
        }

        // Income
        for (FinancialTransaction i : invoices[1]) {
            data.add(false, i);
        }

        if (depth != null && depth > 0) {
            // on supprime les profondeurs non demandees
            Category lastCategory = null;
            for (Iterator<WikittyQueryResultTreeNode<Category>> i=data.getCategoriesTree().iterator(); i.hasNext();) {
                WikittyQueryResultTreeNode<Category> c = i.next();
                if (c.getLevel() < depth) {
                    lastCategory = c.getUserObject();
                } else {
                    i.remove();
                    for (String d : data.getDates()) {
                        Category currentCategory = c.getUserObject();
                        double s = data.getAmount(d, currentCategory);
                        data.addAmount(d, lastCategory, s);
                        for (FinancialTransaction invoice : data.getInvoices(d, currentCategory)) {
                            data.addInvoice(d, lastCategory, invoice);
                        }
                    }
                }
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("budget data\n\t dates: " + data.getDates() + "\n\t rootTree: " + rootCategory + "\n\t categoriesTree: " + categoriesTree);
        }

        if ("ascii".equalsIgnoreCase(type)) {
            InputStream out = computeAsciiTable(client, data);
            return renderDownload(out, "budget.txt", "text/plain");
        } else {
            return renderView("dashboardBudget.jsp", "companyId", companyId,
                    "data", data);
        }
    }

    protected InputStream computeAsciiTable(ChoremClient client, BudgetData data) {
        NumberFormat currency = NumberFormat.getCurrencyInstance(client.getUserLocale());

        TableFormatter tf = new SimpleTableFormatter(true);

        tf.nextRow();
        tf.nextCell(TableFormatter.ALIGN_CENTER, TableFormatter.VALIGN_DEFAULT)
                .addLine("Category");
        for (String d : data.getDates()) {
            tf.nextCell(TableFormatter.ALIGN_CENTER, TableFormatter.VALIGN_DEFAULT)
                    .addLine(d);
        }
        for (WikittyQueryResultTreeNode<Category> c : data.getCategoriesTree()) {

            tf.nextRow().nextCell(TableFormatter.ALIGN_LEFT, TableFormatter.VALIGN_DEFAULT)
                    .addLine(StringUtils.repeat("  ", c.getLevel()) + c.getUserObject());
            for (String d : data.getDates()) {
                tf.nextCell(TableFormatter.ALIGN_RIGHT, TableFormatter.VALIGN_DEFAULT)
                        .addLine(currency.format(data.getAmount(d, c.getUserObject())));
            }
        }
        tf.nextRow().nextCell().addLine("Sorties");
        for (String d : data.getDates()) {
            tf.nextCell(TableFormatter.ALIGN_RIGHT, TableFormatter.VALIGN_DEFAULT)
                    .addLine(currency.format(data.getDebt(d)));
        }
        tf.nextRow().nextCell().addLine("Entrées");
        for (String d : data.getDates()) {
            tf.nextCell(TableFormatter.ALIGN_RIGHT, TableFormatter.VALIGN_DEFAULT)
                    .addLine(currency.format(data.getIncome(d)));
        }
        tf.nextRow().nextCell().addLine("Total");
        for (String d : data.getDates()) {
            tf.nextCell(TableFormatter.ALIGN_RIGHT, TableFormatter.VALIGN_DEFAULT)
                    .addLine(currency.format(data.getTotal(d)));
        }
        tf.nextRow().nextCell().addLine("Total cumulé");
        for (String d : data.getDates()) {
            tf.nextCell(TableFormatter.ALIGN_RIGHT, TableFormatter.VALIGN_DEFAULT)
                    .addLine(currency.format(data.getFinances(d)));
        }

        String s = StringUtils.join(tf.getFormattedTable(), "\n");
        Reader reader = new StringReader(s);
        return new ReaderInputStream(reader);
    }

    /**
     * Permet de collecter et representer les donnees pour le budget
     */
    public static class BudgetData {
        final static private String DEBT_KEY = "debt";     // cle pour les depenses sur le mois
        final static private String INCOME_KEY = "income"; // cle pour les revenus sur le mois
        final static private String TOTAL_KEY = "total";   // cle pour la diffence entre revenus et depenses sur le mois

        protected List<String> dates;
        protected List<WikittyQueryResultTreeNode<Category>> categoriesTree;
        protected double beginningFinances;
        protected Map<String, Double> finances;
        protected Map<String, Map<Object, Double>> values;
        protected Map<String, Map<Category, List<FinancialTransaction>>> invoices;

        /**
         *
         * @param start la date de debut du budget
         * @param end la date de fin du budget
         * @param categoriesTree toutes les categories
         * @param beginningFinances l'etat des beginningFinances avant la periode a afficher
         */
        public BudgetData(Date start, Date end,
                List<WikittyQueryResultTreeNode<Category>> categoriesTree,
                double beginningFinances) {
            this.categoriesTree = categoriesTree;
            this.beginningFinances = beginningFinances;

            finances = new HashMap<String, Double>();

            // La liste de toutes les dates que l'on souhaite
            dates = new LinkedList<String>();
            do {
                dates.add(DateFormatUtils.format(start, budgetDateFormat));
                start = DateUtils.addMonths(start, 1);
            } while (!start.after(end));


            Map<Object, Double> capMap = new MapWithDefault<Object, Double>(
                    new HashMap<Object, Double>(), 0.0);
            values = new MapWithDefault<String, Map<Object, Double>>(
                    new HashMap<String, Map<Object, Double>>(), capMap);

            Map<Category, List<FinancialTransaction>> capMapInvoice = new MapWithDefault<Category, List<FinancialTransaction>>(
                    new HashMap<Category, List<FinancialTransaction>>(), new LinkedList<FinancialTransaction>());
            invoices = new MapWithDefault<String, Map<Category, List<FinancialTransaction>>>(
                    new HashMap<String, Map<Category, List<FinancialTransaction>>>(), capMapInvoice);

        }

        public List<String> getDates() {
            return dates;
        }

        public List<WikittyQueryResultTreeNode<Category>> getCategoriesTree() {
            return categoriesTree;
        }
        
        public void add(boolean debt, FinancialTransaction invoice) {
            // on modifie les invoices, donc on vide le cache de finances
            finances.clear();

            Date d = getDate(invoice);
            String date = DateFormatUtils.format(d, budgetDateFormat);
            Category c = invoice.getCategory(false);
            double amount = invoice.getAmount();
            double vat = invoice.getVAT();

            double ttc = ChoremQueryMaker.amountTTC(amount, vat);

            addAmount(date, c, ttc);
            addInvoice(date, c, invoice);

            if (debt) {
                addDebt(date, ttc);
                addTotal(date, -ttc);
            } else {
                addIncome(date, ttc);
                addTotal(date, ttc);
            }

        }

        protected Date getDate(FinancialTransaction invoice) {
            Date result;
            if (invoice.getPaymentDate() != null) {
                result = invoice.getPaymentDate();
            } else {
                result = invoice.getExpectedDate();
            }
            return result;
        }

        protected void addAmount(String month, Category c, double amount) {
            double v = getAmount(month, c);
            v += amount;
            values.get(month).put(c, v);
        }

        protected void addInvoice(String month, Category c, FinancialTransaction invoice) {
            invoices.get(month).get(c).add(invoice);
        }

        public void addDebt(String month, double amount) {
            double v = values.get(month).get(DEBT_KEY);
            v += amount;
            values.get(month).put(DEBT_KEY, v);
        }
        public void addIncome(String month, double amount) {
            double v = values.get(month).get(INCOME_KEY);
            v += amount;
            values.get(month).put(INCOME_KEY, v);
        }
        public void addTotal(String month, double amount) {
            double v = values.get(month).get(TOTAL_KEY);
            v += amount;
            values.get(month).put(TOTAL_KEY, v);
        }

        public double getAmount(String month, Category c) {
            double result = values.get(month).get(c);
            return result;
        }

        public List<FinancialTransaction> getInvoices(String month, Category c) {
            List<FinancialTransaction> result = invoices.get(month).get(c);
            return result;
        }

        public double getDebt(String month) {
            double result = values.get(month).get(DEBT_KEY);
            return result;
        }
        public double getIncome(String month) {
            double result = values.get(month).get(INCOME_KEY);
            return result;
        }
        public double getTotal(String month) {
            double result = values.get(month).get(TOTAL_KEY);
            return result;
        }
        public double getFinances(String month) {
            Double result = finances.get(month);
            if (result == null) {
                double value = beginningFinances;
                for (String d : getDates()) {
                    double total = getTotal(d);
                    value += total;
                    finances.put(d, value);
                }
                result = finances.get(month);
            }
            return result;
        }
    }


    /**
     * Retourne le total de la tva entree et sortie entre deux dates, les valeurs retournees sont:
     *
     * <li>(Double)vatDebt: le montant de la tva sur les depenses
     * <li>(Double)vatIncome: le montant de la tva sur les entrees
     * <li>(String)vatDebtQuery: la query permettant de retrouve les factures utilise
     * <li>(String)vatIncomeQuery: la query permettant de retrouve les factures utilise
     *
     * @param client
     * @param start
     * @param end
     * @param companyId
     * @return
     */
    protected Map<String, Object> getVAT(ChoremClient client,
            Date start, Date end, String query, String companyId) {

        // La liste des factures entre les deux dates que l'on doit payer
        WikittyQuery vatDebtQuery = new ChoremQueryMaker()
                .and()
                .parse(query)
                  .exteq(FinancialTransaction.EXT_FINANCIALTRANSACTION)
                  .containsOne(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYER)
                    .select(Element.ID).filterOnCompanyOrEmployee(companyId)
                  .close()
                .ne(Invoice.FQ_FIELD_INVOICE_STATUS, InvoiceStatus.CANCELED)
                .isNotNull(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_VAT)
                .ne(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_VAT, 0)
                .bw(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EMITTEDDATE, start, end)
                .end()
                .setLimit(WikittyQuery.MAX);

        WikittyQuery vatDebt = new ChoremQueryMaker()
                .select()
                  .sum((String)null, "vatDebt")
                    .function("#amountVAT")
                      .fieldValue(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_AMOUNT)
                      .fieldValue(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_VAT)
                .where()
                .condition(vatDebtQuery.getCondition())
                .end()
                .setLimit(WikittyQuery.MAX);

        // La liste des factures entre les deux dates que l'on doit payer
        WikittyQuery vatIncomeQuery = new ChoremQueryMaker()
                .and()
                .parse(query)
                  .exteq(FinancialTransaction.EXT_FINANCIALTRANSACTION)
                  .containsOne(Invoice.FQ_FIELD_FINANCIALTRANSACTION_BENEFICIARY)
                    .select(Element.ID).filterOnCompanyOrEmployee(companyId)
                  .close()
                .ne(Invoice.FQ_FIELD_INVOICE_STATUS, InvoiceStatus.CANCELED)
                .isNotNull(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_VAT)
                .ne(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_VAT, 0)
                .bw(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYMENTDATE, start, end)
                .end()
                .setLimit(WikittyQuery.MAX);

        // La liste des factures entre les deux dates que l'on doit payer
        WikittyQuery vatIncome = new ChoremQueryMaker()
                .select()
                  .sum((String)null, "vatIncome")
                    .function("#amountVAT")
                      .fieldValue(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_AMOUNT)
                      .fieldValue(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_VAT)
                .where()
                .condition(vatIncomeQuery.getCondition())
                .end()
                .setLimit(WikittyQuery.MAX);


        Double[] values = client.findByQuery(Double.class, vatDebt, vatIncome);
        Map<String, Object> aggregate = new HashMap<String, Object>();
        aggregate.put("vatDebt", values[0]);
        aggregate.put("vatIncome", values[1]);

        aggregate.put("vatDebtQuery", vatDebtQuery.getCondition().toString());
        aggregate.put("vatIncomeQuery", vatIncomeQuery.getCondition().toString());

        return aggregate;
    }

    /**
     * Prévisionnel entre deux dates en fonction des factures
     */
    public Render vat(ChoremClient client, Date start, Date end, String query) {
        if (log.isDebugEnabled()) {
            log.debug(String.format(
                    "vat for period '%s' to '%s' with filter '%s'",
                    start, end, query));
        }
        String companyId = client.getConfiguration().getDefaultCompany();

        if (StringUtils.isBlank(companyId)) {
            getContext().addInfoMessage("message", "Vous devez définir une société par défaut");
            return renderURL("/admin/variables");
        }

        if (start == null) {
            start = new Date();
            start = DateUtils.addMonths(start, -6);
        }

        if (end == null) {
            end = DateUtils.addMonths(start, 6);
        }

        start = DateUtil.setFirstDayOfMonth(start);
        end = DateUtil.setLastDayOfMonth(end);

        start = DateUtil.setMinTimeOfDay(start);
        end = DateUtil.setMaxTimeOfDay(end);


        Map<Date, Map<String, Object>> data = new LinkedHashMap<Date, Map<String, Object>>();

        Date currentFirst = start;
        Date currentLast = DateUtil.setLastDayOfMonth(start);
        while (currentLast.compareTo(end) <= 0) {
            Map<String, Object> vat = getVAT(client, currentFirst, currentLast, query, companyId);
            data.put(currentFirst, vat);
            currentFirst = DateUtils.addMonths(currentFirst, 1);
            currentLast = DateUtils.addMonths(currentLast, 1);
        }

        if (log.isDebugEnabled()) {
            log.debug(String.format(
                    "vat for period '%s' to '%s' with filter '%s = %s'",
                    start, end, query, data));
        }

        return renderView("dashboardVAT.jsp", "data", data);
    }


    ////////////////////////////////////////////////////////////////////////////
    //
    // H R
    //
    ////////////////////////////////////////////////////////////////////////////

//    public Render vacationFilter(ChoremClient client, String query, String jspRender, EnumSet<VacationStatus> status) {
//        // recuperation des vacances en fonction de leur status
//        WikittyQuery vacationQuery = new WikittyQueryMaker().and()
//                .parse(query)
//                .containsOne(Vacation.FQ_FIELD_VACATION_STATUS, status)
//                .end();
//
//        WikittyQueryResult<Vacation> vacations =
//                client.findAllByQuery(Vacation.class, vacationQuery);
//
//        Map<String, Double> days = new HashMap<String, Double>();
//        for (Vacation v : vacations) {
//            double d = computeVacationDays(v);
//            days.put(v.getWikittyId(), d);
//        }
//
//        return renderView(jspRender,
//                "vacations", vacations.getAll(),
//                "days", days
//                );
//
//    }
//
//    /**
//     * Les vacances demandees par des employes non encore validee.
//     * @param client
//     * @param query
//     * @return
//     */
//    public Render vacationAsked(ChoremClient client, String query) {
//        return vacationFilter(client, query, "dashboardVacationAsked.jsp",
//                EnumSet.of(VacationStatus.ASKED));
//    }
//
//    /**
//     * Les vacances validees
//     * @param client
//     * @param query
//     * @return
//     */
//    public Render vacation(ChoremClient client, String query) {
//        return vacationFilter(client, query, "dashboardVacation.jsp",
//                EnumSet.of(VacationStatus.ACCEPTED));
//    }
//
//    public Render vacationByEmployee(ChoremClient client, String query) {
//                WikittyQuery vacationQuery = new WikittyQueryMaker().and()
//                .parse(query)
//                .eq(Vacation.FQ_FIELD_VACATION_STATUS, VacationStatus.ACCEPTED)
//                .end();
//
//        WikittyQueryResult<Vacation> vacations =
//                client.findAllByQuery(Vacation.class, vacationQuery);
//
//        Map<Employee, Double> days = new MapWithDefault<Employee, Double>(
//                new HashMap<Employee, Double>(), Double.valueOf(0));
////        for(Vacation v : vacations) {
////            Employee e = v.getEmployee(false);
////            double d = days.get(e) + computeVacationDays(v);
////            days.put(e, d);
////        }
//
//        return renderView("dashboardVacationByEmployee.jsp",
//                "vacationByEmployee", days
//                );
//    }
//    
//    
//    /**
//     * Bilan du nombre de jour travaille sur des projets client depuis le 01/01 par employer de la
//     * societe par defaut (Configuration.defaultCompany)
//     */
//    public Render workingProjectDaysByEmployee(ChoremClient client, String query) {
//                WikittyQuery vacationQuery = new WikittyQueryMaker().and()
//                .parse(query)
//                .exteq(Time.EXT_TIME)
//                .end();
//
//        WikittyQueryResult<Time> times =
//                client.findAllByQuery(Time.class, vacationQuery);
//
//        Map<Employee, Double> days = new MapWithDefault<Employee, Double>(
//                new HashMap<Employee, Double>(), Double.valueOf(0));
//        for(Time t : times) {
//            Employee e = t.getEmployee(false);
//            double d = days.get(e) + computeWorkingDays(client, t);
//            days.put(e, d);
//        }
//
//        return renderView("dashboardWorkingProjectDaysByEmployee.jsp",
//                "workingProjectDaysByEmployee", days
//                );
//    }
    


}

package org.chorem.webmotion.actions.project;

/*
 * #%L
 * Chorem :: webmotion
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.ChoremClient;
import org.chorem.entities.Quotation;
import org.chorem.entities.Task;
import org.chorem.entities.TaskStatus;
import org.debux.webmotion.server.WebMotionController;
import org.debux.webmotion.server.render.Render;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.query.WikittyQueryResult;

/**
 * Manage the ajax actions for the gantt diagram on the single-project dashboard
 * @author gwenn
 *
 */
public class GanttAction extends WebMotionController {

	/** to use log facility, just put in your code: log.info(\"...\"); */
	static private Log log = LogFactory.getLog(GanttAction.class);


	/**
	 * Return gantt data under json format
	 * @param client
	 * @param id
	 * @return
	 */
	public Render getGanttInfo(ChoremClient client, String id) {
		Wikitty wikitty = client.restore(id);
		WikittyQuery quotationQuery = new WikittyQueryMaker()
		.ideq(id)
		.end();
		Quotation quotation = client.findByQuery(Quotation.class, quotationQuery);



		WikittyQuery taskQuery = new WikittyQueryMaker()
		.eq(Task.ELEMENT_FIELD_TASK_QUOTATION, quotation)
		.end();

		WikittyQueryResult<Task> taskResult = client.findAllByQuery(Task.class, taskQuery);
		String customClass = "";
		List<JTask> lTask = new ArrayList<JTask>();
		for(Task t : taskResult.getAll()) {

			if(t.getBeginDate() != null && t.getEndDate() != null) {
			    customClass= "gantt-" + t.getStatus().toLowerCase();
			
				Values[] v = null;
				if(t.getDayExtension() != 0)
					v= new Values[2];
				else
					v = new Values[1];
				v[0] = new Values(t.getBeginDate(), t.getEndDate(), t.getName(), customClass, t.getWikittyId());
				
				if(t.getDayExtension() != 0) {
					GregorianCalendar newEnd = new GregorianCalendar();
					newEnd.setTime(t.getEndDate());
					newEnd.add(Calendar.DAY_OF_YEAR, (int) t.getDayExtension());
					v[1]= new Values(t.getEndDate(), newEnd.getTime(), "Day extension", "ganttOrange", t.getWikittyId());
				}
				JTask jt = new JTask(t.getName(), t.getDescription(), t.getWikittyId(),t.getPrice(),
						t.getEstimatedDays(), t.getDayExtension(), v);

				lTask.add(jt);
			}
		}

		Collections.sort(lTask);
		String dateStart = "/Date(" + quotation.getBeginDate().getTime() + ")/";
		String dateEnd = "/Date(" + quotation.getEndDate().getTime() + ")/";

		HashMap<String,String> dateMap = new HashMap<String,String>();
		dateMap.put("sendingDate", "Draft");
		dateMap.put("postedDate", "Sent");
		dateMap.put("acceptedDate", "Accepted");
		dateMap.put("startedDate", "Started");
		dateMap.put("deliveryDate", "Delivered");
		dateMap.put("rsvStart", "RSV");
		dateMap.put("warrantyStart", "Warranty");
		dateMap.put("closedDate", "Closed");
		dateMap.put("rejectedDate", "Rejected");
		dateMap.put("cancelledDate", "Cancelled");

		Iterator<Map.Entry<String,String>> i = dateMap.entrySet().iterator();
		HashMap<String,String> extDate = new HashMap<String,String>();
		while(i.hasNext()) {
			Map.Entry<String, String> me = (Map.Entry<String, String>)i.next();
			if(wikitty.hasExtension(me.getValue())) {
				Date date = ((Date) (wikitty.getFieldAsObject(me.getValue(), me.getKey())));
				if(date != null)
					extDate.put(me.getValue() + " " + me.getKey(), "/Date(" + date.getTime() +")/");
			}
		}
		
		JRender data = new JRender(lTask, extDate, dateStart, dateEnd);
		return renderJSON("data", data);
	}


	private class JRender {
		private List<JTask> 			source;
		private String 					dateStart;
		private String 					dateEnd;
		private HashMap<String,String> 	extDate;
		public JRender(List<JTask> source,HashMap<String,String> extDate,String dateStart,String dateEnd) {
			this.source 	= source;
			this.dateStart 	= dateStart;
			this.dateEnd 	= dateEnd;
			this.extDate 	= extDate;
		}
	}

	private class JTask implements Comparable {
		private String 		name;
		private String 		desc;
		private Values[] 	values;
		private String 		status;
		private String 		wikittyId;
		private float 		price;
		private double 		estimatedDays;
		private double 		dayExtension;
		public JTask(String name, String desc, String wikittyId,float price,
				double estimatedDays, double dayExtension, Values[] values) {
			this.name 			= name;
			this.desc 			= desc;
			this.values 		= values;
			this.wikittyId 		= wikittyId;
			this.price 			= price;
			this.estimatedDays 	= estimatedDays;
			this.dayExtension 	= dayExtension;
		}
		@Override
		public int compareTo(Object task) {

			return values[0].compareTo(((JTask)task).values[0]);
		}

	}
	private class Values implements Comparable{
		private String 	from;
		private String 	to;
		private String 	label;
		private String 	customClass;
		private String 	dataObj;
		public Values(Date from, Date to, String label, String customClass, String dataObj) {
			this.from 			= "/Date(" + from.getTime() + ")/";
			this.to 			= "/Date(" + to.getTime() + ")/";
			this.label 			= label;
			this.customClass 	= customClass;
			this.dataObj 		= dataObj;
		}
		@Override
		public int compareTo(Object o) {

			return from.compareTo(((Values)o).from);
		}

	}
}

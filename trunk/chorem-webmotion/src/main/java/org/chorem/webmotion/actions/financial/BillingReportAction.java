package org.chorem.webmotion.actions.financial;

/*
 * #%L
 * Chorem :: webmotion
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.lang3.time.DateUtils;
import org.chorem.ChoremClient;
import org.chorem.entities.Accepted;
import org.chorem.entities.FinancialTransaction;
import org.chorem.webmotion.actions.sales.QuotationYearData;
import org.chorem.webmotion.actions.sales.SalesReportHelper;
import org.debux.webmotion.server.WebMotionController;
import org.debux.webmotion.server.render.Render;
import org.nuiton.util.DateUtil;
import org.nuiton.wikitty.WikittyClient;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jcouteau <couteau@codelutin.com>
 */
public class BillingReportAction extends WebMotionController {

    /**
     * Rend le graphe des factures par mois
     *
     * @param client
     * @return
     */
    public Render billing(ChoremClient client, String from, String to) {

        if (null == from) {
            from = String.valueOf(BillingReportHelper.getFirstYear(client));
        }

        if (null == to) {
            to = String.valueOf(BillingReportHelper.getLastYear());
        }

        Map<Integer, QuotationYearData> data = new LinkedHashMap<Integer, QuotationYearData>();

        List<Integer> listAllYears = BillingReportHelper.listAllYears(from, to);

        List<Integer> listAllYearsInChorem = BillingReportHelper.listAllYears(client);

        int previousYearValue = 0;

        for (Integer year:listAllYears){
            Date yearFirstDay = BillingReportHelper.getFirstDayOfYear(year);
            Date yearLastDay =  BillingReportHelper.getLastDayOfYear(year);

            QuotationYearData yearData = new QuotationYearData();

            //factures de l'année
            WikittyQuery billingQuery = new WikittyQueryMaker()
                    .select().sum("FinancialTransaction.amount").where().and()
                    .exteq(FinancialTransaction.EXT_FINANCIALTRANSACTION)
                    .bw(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EMITTEDDATE, yearFirstDay, yearLastDay)
                    .end();

            Integer billing = client.findByQuery(Integer.class, billingQuery);

            //Progression facturation
            int billingProgression = 0;
            if (previousYearValue != 0){
                billingProgression = 100 * (billing - previousYearValue) / previousYearValue;
            }

            previousYearValue = billing;

            //Graphe factures émises
            Map<String, Integer> billingData = getBillingData(year, client);

            yearData.setBaseValue(billing);
            yearData.setProgression(billingProgression);
            yearData.setPlotValues(billingData);

            data.put(year, yearData);
        }

        return renderView("financial/reports/billingReport.jsp",
                "data", data,
                "allYears", listAllYearsInChorem,
                "fromYear", from,
                "toYear", to);
    }

    protected Map<String,Integer> getBillingData(Integer year, WikittyClient client){

        Date first = BillingReportHelper.getFirstDayOfYear(year);

        Map<String,Integer> billingData = new LinkedHashMap<String, Integer>();

        //a query per month :(
        Date baseValue = first;
        for (int i=0;i<12;i++){
            Date lastDayOfMonth = DateUtil.setLastDayOfMonth(baseValue);
            Date firstDayOfMonth = DateUtil.setFirstDayOfMonth(baseValue);
            baseValue= DateUtils.addDays(lastDayOfMonth, 1);

            WikittyQuery monthQuery = new WikittyQueryMaker()
                    .select().sum("FinancialTransaction.amount").where().and()
                    .bw(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EMITTEDDATE, firstDayOfMonth, lastDayOfMonth)
                    .end();

            Integer billing = client.findByQuery(Integer.class, monthQuery);

            billingData.put(
                    DateUtil.getMonthLibelle(DateUtil.getMonth(lastDayOfMonth) + 1),
                    billing);
        }

        return billingData;
    }

}

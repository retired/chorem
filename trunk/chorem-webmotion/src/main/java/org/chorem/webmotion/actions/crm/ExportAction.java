package org.chorem.webmotion.actions.crm;

/*
 * #%L
 * Chorem :: webmotion
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.io.IOUtils;
import org.chorem.ChoremClient;
import org.chorem.entities.ContactDetails;
import org.chorem.entities.Employee;
import org.chorem.entities.Company;
import org.chorem.entities.Person;
import org.debux.webmotion.server.WebMotionController;
import org.debux.webmotion.server.render.Render;
import org.nuiton.csv.Export;
import org.nuiton.csv.ExportModel;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.query.WikittyQueryResult;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author couteau
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ExportAction extends WebMotionController {

    public Render exportContactBase(ChoremClient client) throws Exception {

        // création du modèle d'export csv
        ExportModel<ContactRowForExport> csvModel = new ContactRowForExportModel();

        // les données à exporter
        List<ContactRowForExport> datas = new ArrayList<ContactRowForExport>();

        // recuperation des quotations en statut lead
        WikittyQuery contactsQuery = new WikittyQueryMaker().and()
                .exteq(Employee.EXT_EMPLOYEE)
                .end().setLimit(WikittyQuery.MAX);

        WikittyQueryResult<Employee> contacts =
                client.findAllByQuery(Employee.class, contactsQuery);

        for(Employee contact:contacts){
            ContactRowForExport contactRow = new ContactRowForExport();
            //Contact company
            Company company = client.restore(Company.class, contact.getCompany(), "");
            contactRow.setCompany(company.getName());

            //contact names
            Person person = client.restore(Person.class, contact.getPerson());
            contactRow.setFirstName(person.getFirstName());
            contactRow.setLastName(person.getLastName());

            //start with person info
            WikittyQuery personContactDetailsQuery = new WikittyQueryMaker().and()
                    .exteq(ContactDetails.EXT_CONTACTDETAILS)
                    .eq(ContactDetails.ELEMENT_FIELD_CONTACTDETAILS_TARGET, person.getWikittyId())
                    .end().setLimit(WikittyQuery.MAX);
            WikittyQueryResult<ContactDetails> personContactDetails =
                    client.findAllByQuery(ContactDetails.class, personContactDetailsQuery);
            for (ContactDetails detail:personContactDetails) {
                addContactDetail(contactRow, detail);
            }

            //then company info (override person info if duplicate)
            WikittyQuery companyContactDetailsQuery = new WikittyQueryMaker().and()
                    .exteq(ContactDetails.EXT_CONTACTDETAILS)
                    .eq(ContactDetails.ELEMENT_FIELD_CONTACTDETAILS_TARGET, company.getWikittyId())
                    .end().setLimit(WikittyQuery.MAX);
            WikittyQueryResult<ContactDetails> companyContactDetails =
                    client.findAllByQuery(ContactDetails.class, companyContactDetailsQuery);
            for (ContactDetails detail:companyContactDetails) {
                addContactDetail(contactRow, detail);
            }

            //end with employee info (override previous info if duplicate)
            WikittyQuery employeeContactDetailsQuery = new WikittyQueryMaker().and()
                    .exteq(ContactDetails.EXT_CONTACTDETAILS)
                    .eq(ContactDetails.ELEMENT_FIELD_CONTACTDETAILS_TARGET, contact.getWikittyId())
                    .end().setLimit(WikittyQuery.MAX);
            WikittyQueryResult<ContactDetails> employeeContactDetails =
                    client.findAllByQuery(ContactDetails.class, employeeContactDetailsQuery);
            for (ContactDetails detail:companyContactDetails) {
                addContactDetail(contactRow, detail);
            }

            datas.add(contactRow);
        }



        // création d'un exporter
        Export<ContactRowForExport> exporter = Export.newExport(csvModel, datas);

        // lancement de l'export
        OutputStream stream = new ByteArrayOutputStream();
        exporter.write(stream);

        return renderDownload(IOUtils.toInputStream(stream.toString()), "contacts.csv", "text/csv");
    }

    protected void addContactDetail(ContactRowForExport contact,ContactDetails detail) {
        if ("email".equalsIgnoreCase(detail.getType())
                || "mail".equalsIgnoreCase(detail.getType())){
            contact.setEmail(detail.getValue());
        }
        if ("adresse".equalsIgnoreCase(detail.getType())){
            contact.setAddress(detail.getValue());
        }
        if ("téléphone".equalsIgnoreCase(detail.getType())
                || "telephone".equalsIgnoreCase(detail.getType())
                || "phone".equalsIgnoreCase(detail.getType())){
            if ("mobile".equalsIgnoreCase(detail.getName())){
                contact.setMobilePhone(detail.getValue());
            } else {
                contact.setFixPhone(detail.getValue());
            }
        }

    }
}

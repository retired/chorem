package org.chorem.webmotion.actions;

/*
 * #%L
 * Chorem :: webmotion
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.debux.webmotion.server.WebMotionController;
import org.chorem.ChoremClient;
import org.chorem.ChoremUtil;
import org.debux.webmotion.server.call.Call;
import org.debux.webmotion.server.render.Render;
import org.chorem.entities.ADC;
import org.chorem.entities.ADCImpl;
import org.chorem.entities.Company;
import org.chorem.entities.CompanyHR;
import org.chorem.entities.CompanyImpl;
import org.chorem.entities.Employee;
import org.chorem.entities.EmployeeHR;
import org.chorem.entities.EmployeeHRImpl;
import org.chorem.entities.EmployeeImpl;
import org.chorem.project.AdcCalculation;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.query.WikittyQueryResult;

/**
 * Generate a page to edit the calues of the employees
 * Also manages ajax requests for calculating the adc or 
 * @author meynier
 *
 */
public class EmployeeEditAction extends WebMotionController {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(EmployeeEditAction.class);

    /**
     * Return a page with an array with the employees of the specified company
     * @param client chorem client
     * @param company employees will be fetched from this company
     * @return
     */
    public Render employeeFilter(ChoremClient client, Company company) {
        //get all the empoyees from the default company

        List<Company> companies = client.findAllByQuery(Company.class,
                new WikittyQueryMaker().exteq("Company").end()).getAll();

        //Order by name
        WikittyQuery employeeQuery = new WikittyQueryMaker().eq(Employee.ELEMENT_FIELD_EMPLOYEE_COMPANY, company).end();

        WikittyQueryResult<Employee> employeeResult = client.findAllByQuery(Employee.class, employeeQuery);

        List<EmployeeData> employees = new ArrayList<EmployeeData>();
        for(Employee e : employeeResult.getAll()) {
            employees.add(new EmployeeData(e, client));
        }

        Collections.sort(employees);
        //simply return them
        return renderView("employeeEdit.jsp", 
                "employees",employees,
                "companies", companies,
                "company", new CompanyImpl(client.restore(company.getWikittyId())),
                "title", "Employee edit");
    }


    /**
     * generate a page to edit the employees
     * 
     * @param client choorem client
     * @param companyId
     * @param addExtension
     * @param call
     * @return
     */
    public Render requestEmployeeEdit(ChoremClient client, String companyId, String addExtension, Call call) {
        Company company = null;

        if(companyId == null)
            company = client.getDefaultCompany();
        else {
            company = new CompanyImpl(client.restore(companyId));
        }

        if(call != null) {
            Wikitty w = client.restore(company.getWikittyId());
            Map<String, Object> params = call.getExtractParameters();
            for(String key : params.keySet()) {
                //Prevent false data
                if((key.equals(CompanyHR.FQ_FIELD_COMPANYHR_DAILYRETURN)
                        || key.equals(CompanyHR.FQ_FIELD_COMPANYHR_DAILYHOURSWORKED))) {
                    try {
                        double value = Double.parseDouble(((String[])params.get(key))[0]);
                        w.setFqField(key, value);
                    }
                    catch(java.lang.NumberFormatException e) {
                        System.err.println(e.getMessage());//TODO : user output
                    }

                }
            }

            client.store(w);
        }

        if(addExtension != null && addExtension.equals("true")) {
            Wikitty w = client.restore(company.getWikittyId());
            w.addExtension(client.restoreExtensionLastVersion(CompanyHR.EXT_COMPANYHR));
            client.store(w);
        }

        return employeeFilter(client, company);
    }

    /**
     * Ajax request for the adc calculation
     * 
     * @param client chorme client
     * @param employeeId id of the employee that needs an adc calculation
     * @return adc
     */
    public Render requestAdc(ChoremClient client, String employeeId, boolean real) {
        Wikitty w = client.restore(employeeId);
        EmployeeHR e = new EmployeeHRImpl(w);

        double adc = (new AdcCalculation(client, e)).getAdc(real);

        w.setField(EmployeeHR.EXT_EMPLOYEEHR, EmployeeHR.FIELD_EMPLOYEEHR_DAILYRETURN, adc);
        client.store(w);

        int iadc = (int)(adc*100);
        return renderJSON("adc", (double)(iadc/100.0));


    }


    /**
     * Ajax request for the adc calculation of multiple employees of the same company
     * 
     * @param client chorme client
     * @param employeeId id of the employees that needs an adc calculation
     * @param real You must specify this parameter is true if you want the real values from the last year.
     * The value will be stored in a new ADC object.
     * @param year Year of the adc if it's the real one that needs to be calculated
     * @return adc
     */
    public Render requestMultipleAdc(ChoremClient client, String employeeId, boolean real, int year) {
        List<Wikitty> wemployees = client.restore(ChoremUtil.asList(",", employeeId));
        List<EmployeeHR> employees = new ArrayList<EmployeeHR>();
        System.out.println("real = " + real + ", year = " + year);
        for(int i = 0; i< wemployees.size(); i++) {
            employees.add(new EmployeeHRImpl(wemployees.get(i)));
        }

        Map<String, Double> adcs = (new AdcCalculation(client, employees)).getMultipleAdc(real);
        if(real) {
            for(int i = 0; i< wemployees.size(); i++) {
                Wikitty wi = wemployees.get(i);

                //Check if the ADC object already exists
                WikittyQuery adcQuery = new WikittyQueryMaker().and()
                        .eq(ADC.ELEMENT_FIELD_ADC_EMPLOYEEHR, employees.get(i))
                        .eq(ADC.ELEMENT_FIELD_ADC_YEAR, year)
                        .end();


                WikittyQueryResult<ADC> adcResult = client.findAllByQuery(
                        ADC.class, adcQuery);
                if(adcResult.size() == 0) {

                    ADC adc = new ADCImpl();
                    adc.setEmployeeHR(wi.getWikittyId());
                    adc.setYear(year);
                    adc.setValue(adcs.get(wi.getWikittyId()));
                    client.store(adc);
                }
                else {
                    ADC adc = adcResult.get(0); //There is only one (in theory)
                    Wikitty wadc =client.restore(adc.getWikittyId());
                    wadc.setField(ADC.EXT_ADC, ADC.FIELD_ADC_VALUE, adcs.get(wi.getWikittyId()));
                    client.store(wadc);

                }
            }
        }
        else {
            for(int i = 0; i< wemployees.size(); i++) {
                Wikitty wi = wemployees.get(i);
                wi.setField(EmployeeHR.EXT_EMPLOYEEHR, EmployeeHR.FIELD_EMPLOYEEHR_DAILYRETURN, adcs.get(wi.getWikittyId()));
                client.store(wi);
            }
        }
        for(String s : adcs.keySet()) {
            int iadc = (int)(adcs.get(s)*100);
            adcs.put(s, (double)(iadc/100.0));
        }


        return renderJSON("adcs", adcs);
    }



    /**
     * Ajax requets to modify the employee values
     * 
     * @param client chorme client
     * @param employeeId id of the employee to modify
     * @param salaryStr new salary in String format
     * @param productivityRateStr new prod rate in String format
     * @param partialTimeStr new partial time in String format
     * @param dailyReturnStr new daily return in String format
     * @return 
     */
    public Render editEmployeeValues(ChoremClient client, String employeeId, String salaryStr
            , String productivityRateStr, String partialTimeStr, String dailyReturnStr, String otherPaymentsStr) {
        Render render = null;
        Wikitty employeeWikitty = client.restore(employeeId);
        if(!employeeWikitty.hasExtension(EmployeeHR.EXT_EMPLOYEEHR)) {
            employeeWikitty.addExtension(client.restoreExtensionLastVersion(EmployeeHR.EXT_EMPLOYEEHR));
        }

        double salary = 0, productivityRate = 0, partialTime = 0, dailyReturn = 0, otherPayments = 0;

        //Try to parse the strings and generates if errors if it fails
        List<ErrorJson> errors = new ArrayList<ErrorJson>();
        try {
            salary = Double.parseDouble(salaryStr);
        }
        catch (java.lang.NumberFormatException e) {
            errors.add(new ErrorJson("salary", "Salary must be a real number"));
        }

        try{
            productivityRate = Double.parseDouble(productivityRateStr);
        }
        catch (java.lang.NumberFormatException e) {
            errors.add(new ErrorJson("productivityRate", "Productivity rate must be a real number"));
        }

        try {
            partialTime = Double.parseDouble(partialTimeStr);
        }
        catch (java.lang.NumberFormatException e) {
            errors.add(new ErrorJson("partialTime", "Partial time must be a real number"));
        }

        try {
            dailyReturn = Double.parseDouble(dailyReturnStr);
        }
        catch (java.lang.NumberFormatException e) {
            errors.add(new ErrorJson("dailyReturn", "Daily return must be a real number"));
        }

        try {
            otherPayments = Double.parseDouble(otherPaymentsStr);
        }
        catch (java.lang.NumberFormatException e) {
            errors.add(new ErrorJson("dailyReturn", "Other payments must be a real number"));
        }

        //Verify that the percentages are between 0 and 100
        if(productivityRate > 100 || productivityRate < 0) {
            errors.add(new ErrorJson("productivityRate", "Productivity rate must be between 0 and 100"));
        }
        if(partialTime > 100 || partialTime < 0) {
            errors.add(new ErrorJson("partialTime", "Partial time must be between 0 and 100"));
        }
        
        
        
        if(errors.size() != 0) {
            render = renderJSON("data", "error", "errors", errors);
        }
        else {

            if(salary != 0)
                employeeWikitty.setFqField(EmployeeHR.FQ_FIELD_EMPLOYEEHR_SALARY, salary);
            employeeWikitty.setFqField(EmployeeHR.FQ_FIELD_EMPLOYEEHR_PRODUCTIVITYRATE, productivityRate);
            employeeWikitty.setFqField(EmployeeHR.FQ_FIELD_EMPLOYEEHR_PARTIALTIME, partialTime);
            employeeWikitty.setFqField(EmployeeHR.FQ_FIELD_EMPLOYEEHR_DAILYRETURN, dailyReturn);
            employeeWikitty.setFqField(EmployeeHR.FQ_FIELD_EMPLOYEEHR_OTHERPAYMENTS, otherPayments);
            client.store(employeeWikitty);


            EmployeeData data = new EmployeeData(new EmployeeImpl(employeeWikitty), client);

            EmployeeJson json = new EmployeeJson(
                    data.getSalary() + "",
                    data.getProductivityRate(),
                    data.getPartialTime(),
                    data.getDailyReturn(),
                    data.getDailyHoursWorked(),
                    data.getOtherPayments()
                    );
            render = renderJSON("data", json);
        }
        return render;

    }

    /**
     * Class used to store an error
     * @author gwenn
     *
     */
    public class ErrorJson {
        private String errorMessage;
        private String field;

        public ErrorJson (String field, String message) {
            this.field = field;
            this.errorMessage = message;
        }
    }

    /**
     * Structure used to store information about an employee
     * Used for the AJAX request
     * 
     * @author gwenn
     *
     */
    public class EmployeeJson {

        private String salary;
        private double productivityRate;
        private double partialTime;
        private double dailyReturn;
        private double dailyHoursWorked;
        private double otherPayments;

        public EmployeeJson(String salary, double productivityRate, double partialTime,
                double dailyReturn, double dailyHoursWorked, double otherPayments) {
            this.salary = salary;
            this.productivityRate = productivityRate;
            this.partialTime = partialTime;
            this.dailyReturn = dailyReturn;
            this.dailyHoursWorked = dailyHoursWorked;
            this.otherPayments = otherPayments;
        }
    }

    /**
     * Structure used to store information about an employee
     * used for the standard http request
     * @author gwenn
     *
     */
    public class EmployeeData implements Comparable<EmployeeData>{

        private Employee e;
        private String salary;
        private double productivityRate;
        private double partialTime;
        private double dailyReturn;
        private double dailyHoursWorked;
        private double otherPayments;


        public EmployeeData(Employee e, ChoremClient client) {
            Wikitty w = client.restore(e.getWikittyId());
            if(w.hasExtension(EmployeeHR.EXT_EMPLOYEEHR)) {
                this.salary = w.getFieldAsDouble(EmployeeHR.EXT_EMPLOYEEHR, EmployeeHR.FIELD_EMPLOYEEHR_SALARY) + "";
                this.productivityRate =  w.getFieldAsDouble(EmployeeHR.EXT_EMPLOYEEHR, EmployeeHR.FIELD_EMPLOYEEHR_PRODUCTIVITYRATE);
                this.partialTime =  w.getFieldAsDouble(EmployeeHR.EXT_EMPLOYEEHR, EmployeeHR.FIELD_EMPLOYEEHR_PARTIALTIME);
                this.otherPayments = w.getFieldAsDouble(EmployeeHR.EXT_EMPLOYEEHR, EmployeeHR.FIELD_EMPLOYEEHR_OTHERPAYMENTS);
            }
            else {
                this.salary = "Non renseigné";
                this.productivityRate = 100;
                this.partialTime = 100;
                this.otherPayments = 0;
            }
            this.dailyReturn = client.getDailyReturn(e);
            //Simple trick to limit to 2 numbers after digit
            int x = (int)(client.getDailyHoursWorked(e)*100);
            double y = x/100.0;
            this.dailyHoursWorked = y;

            x = (int)(this.dailyReturn*100);
            y = x/100.0;
            this.dailyReturn = y;

            this.e = e;
        }

        public Employee getObject() {
            return e;
        }

        public String getSalary() {
            return salary;
        }

        public double getDailyReturn() {
            return dailyReturn;
        }

        public double getDailyHoursWorked() {
            return dailyHoursWorked;
        }
        public double getProductivityRate() {
            return productivityRate;
        }
        public double getPartialTime() {
            return partialTime;
        }
        public double getOtherPayments() {
            return otherPayments;
        }

        @Override
        public int compareTo(EmployeeData e) {
            return this.getObject().getPerson(false).getLastName().compareTo(e.getObject().getPerson(false).getLastName());
        }



    }

}

package org.chorem.webmotion.actions.crm;

/*
 * #%L
 * Chorem :: webmotion
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.chorem.ChoremClient;
import org.chorem.entities.Quotation;
import org.chorem.entities.QuotationImpl;
import org.debux.webmotion.server.WebMotionController;
import org.debux.webmotion.server.call.Call;
import org.debux.webmotion.server.render.Render;

import java.util.Map;

/**
 * @author jcouteau <couteau@codelutin.com>
 */
public class QuotationAction extends WebMotionController {

    public Render edit(ChoremClient client, String id, Call call) {
        // on preload rien, le preload sera fait a la fin
        Quotation quotation = client.restore(Quotation.class, id, "");

        // si on ne retrouve pas la quotation demandée, on en crée une nouvelle
        if (quotation == null) {
            quotation = new QuotationImpl();
        }

        //récupère les paramètres (customer ou autre) et on les ajoute au devis
        Map<String, Object> params = call.getExtractParameters();
        for (Map.Entry<String, Object> param : params.entrySet()) {

            String[] fqn = StringUtils.split(param.getKey(), ".");

            String[] values = (String[])param.getValue();

            if (values.length == 1) {
                quotation.setField(fqn[0], fqn[1], values[0]);
            } else {
                quotation.setField(fqn[0], fqn[1], values);
            }

            //client.preload(Collections.singleton(quotation), ".*");

        }

        return renderView("crm/editQuotation.jsp", "quotation", quotation);
    }

    //view
}

package org.chorem.webmotion.actions.project;

/*
 * #%L
 * Chorem :: webmotion
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.chorem.ChoremClient;
import org.chorem.entities.Task;
import org.chorem.entities.Quotation;
import org.chorem.entities.Time;
import org.chorem.project.QuotationCalculation;

/**
 * Represent a quotation to use it in a jsp page
 * @author gwenn
 *
 */
public class QuotationData extends QuotationCalculation {


    protected List<TaskData> tasksData;

    public QuotationData(Quotation q, ChoremClient client) {
        super(q, client);
        tasksData = new ArrayList<TaskData>();

        for(Task t : this.getTasks()) {
            tasksData.add(new TaskData(t, client));
        }
        this.calculate();
        addAlerts(tasksData);
    }

    /**
     * fetch the alerts of each task and stor it in the linked TaskData object
     * @param tasksData
     */
    protected void addAlerts(List<TaskData> tasksData) {
        Calendar now = new GregorianCalendar();
        for(TaskData td : tasksData) {
            Task t = td.getObject();
            if(t.getBeginDate() != null && t.getEndDate() != null) {
                String alertStr = "<h4>Warning</h4>";
                String infoStr = "<h4>Info</h4>";
                boolean alert = false;
                boolean info = false;
                //Test if the statuses are correct
                if("scheduled".equalsIgnoreCase(t.getStatus())) {
                    if(t.getBeginDate().before(now.getTime())) {
                        alert = true;
                        alertStr += "Task " + t.getName() + " should be started";
                    }
                }
                else if("started".equalsIgnoreCase(t.getStatus())) {

                    if(t.getEndDate().before(now.getTime())) {
                        alert = true;
                        alertStr += "Task " + t.getName() + " should have ended by now";
                    }
                }

                List<Time> times = client.getTimes(t);
                
                
                for(Time time : times) {
                    if((time.getBeginDate().before(t.getBeginDate()) || time.getEndDate().before(t.getBeginDate()))
                            || (times.size() != 0 && "scheduled".equalsIgnoreCase(t.getStatus()))) {
                        info=true;
                        infoStr = "Times have been added to the task but is is not started";
                    }
                    else if(time.getBeginDate().after(t.getEndDate()) || time.getEndDate().after(t.getEndDate())) {
                        info=true;
                        infoStr = "Times have been added to the task but it should be ended";
                    }
                }


                if(alert) {
                    td.setAlert(alertStr);
                }
                if(info) {
                    td.setInfo(infoStr);
                }

            }

        }

    }

    public TaskData getTaskData(Task t) {
        for(TaskData td : tasksData) {
            if(td.getObject() == t) {
                return td;
            }
        }
        return null;
    }



    public List<TaskData> getTasksData() {
        return tasksData;
    }


}

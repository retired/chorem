/*
 * #%L
 * Chorem webmotion
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem.webmotion.actions;


import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.Reader;
import java.text.NumberFormat;
import javassist.bytecode.ByteArray;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.ChoremClient;
import org.chorem.entities.Configuration;
import org.debux.webmotion.server.WebMotionController;
import org.debux.webmotion.server.call.UploadFile;
import org.debux.webmotion.server.render.Render;
import org.nuiton.wikitty.addons.WikittyImportExportService;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.query.WikittyQueryParser;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class AdminAction extends WebMotionController {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(AdminAction.class);

    /**
     * affiche les valeurs de configuration d'administration
     */
    public Render variables(ChoremClient client) {
        Configuration conf = client.getConfiguration();

        GenericAction g = new GenericAction();
        g.setContextable(contextable);
        return g.view(client, conf.getWikittyId(), null);
    }

    public Render reindex(ChoremClient client) {
        client.syncSearchEngine();
        getContext().addInfoMessage("message", "Database reindexed");

        return renderRedirect("/");
    }

    public Render doImport(ChoremClient client, UploadFile file) throws FileNotFoundException {
        WikittyImportExportService ies = new WikittyImportExportService(client);
        Reader in = new BufferedReader(new FileReader(file.getFile()));
        ies.syncImport(WikittyImportExportService.FORMAT.CSV, in);
        return renderRedirect("/admin/importExport");
    }

    public Render doExport(ChoremClient client, String query) {
        WikittyImportExportService ies = new WikittyImportExportService(client);
        WikittyQuery q = new WikittyQueryParser().parseQuery(query);
        String result = ies.syncExportAllByQuery(WikittyImportExportService.FORMAT.CSV, q);
        ByteArrayInputStream in = new ByteArrayInputStream(result.getBytes());
        return renderStream(in, "text/csv", "UTF-8");
    }

}

package org.chorem.webmotion.actions.sales;

/*
 * #%L
 * Chorem :: webmotion
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * @author jcouteau <couteau@codelutin.com>
 */
public class SalesData {

    protected Integer sales;

    protected Integer quotations;

    protected Integer progression;

    public Integer getSales() {
        return sales;
    }

    public void setSales(Integer sales) {
        this.sales = sales;
    }

    public Integer getQuotations() {
        return quotations;
    }

    public void setQuotations(Integer quotations) {
        this.quotations = quotations;
    }

    public Integer getMean() {

        Integer mean = 0;

        if (null != sales && null != quotations && quotations != 0) {
            mean = sales/quotations;
        }

        return mean;
    }

    public Integer getProgression() {
        return progression;
    }

    public void setProgression(Integer progression) {
        this.progression = progression;
    }
}

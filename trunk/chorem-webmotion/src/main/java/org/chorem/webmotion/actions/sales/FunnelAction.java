package org.chorem.webmotion.actions.sales;

/*
 * #%L
 * Chorem :: webmotion
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.ChoremClient;
import org.chorem.entities.*;
import org.debux.webmotion.server.WebMotionController;
import org.debux.webmotion.server.render.Render;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.query.WikittyQueryResult;

/**
 * @author jcouteau <couteau@codelutin.com>
 */
public class FunnelAction extends WebMotionController {

    /**
     * Rend le tunnel de commande
     *
     * @param client
     * @return
     */
    public Render funnel(ChoremClient client) {

        // recuperation des quotations en statut lead
        WikittyQuery leadQuotationQuery = new WikittyQueryMaker().and()
                .exteq(Quotation.EXT_QUOTATION)
                .extne(Draft.EXT_DRAFT)
                .extne(Cancelled.EXT_CANCELLED)
                .end().setLimit(WikittyQuery.MAX);

        WikittyQueryResult<Quotation> leads =
                client.findAllByQuery(Quotation.class, leadQuotationQuery);

        // calcul des montants totaux lead
        double leadAmount = 0;
        double leadAmountHope = 0;
        for (Quotation q : leads) {
            leadAmount += q.getAmount();
            leadAmountHope += q.getAmount() * q.getConversionHope() / 100.0;
        }

        // recuperation des quotations en statut draft
        WikittyQuery draftQuotationQuery = new WikittyQueryMaker().and()
                .exteq(Draft.EXT_DRAFT)
                .extne(Sent.EXT_SENT)
                .extne(Cancelled.EXT_CANCELLED)
                .end().setLimit(WikittyQuery.MAX);

        WikittyQueryResult<Quotation> drafts =
                client.findAllByQuery(Quotation.class, draftQuotationQuery);

        // calcul des montants totaux drafts
        double draftAmount = 0;
        double draftAmountHope = 0;
        for (Quotation q : drafts) {
            draftAmount += q.getAmount();
            draftAmountHope += q.getAmount() * q.getConversionHope() / 100.0;
        }

        // recuperation des quotations en statut sent
        WikittyQuery sentQuotationQuery = new WikittyQueryMaker().and()
                .exteq(Sent.EXT_SENT)
                .extne(Accepted.EXT_ACCEPTED)
                .extne(Rejected.EXT_REJECTED)
                .extne(Cancelled.EXT_CANCELLED)
                .end().setLimit(WikittyQuery.MAX);

        WikittyQueryResult<Quotation> sents =
                client.findAllByQuery(Quotation.class, sentQuotationQuery);

        // calcul des montants totaux
        double sentAmount = 0;
        double sentAmountHope = 0;
        for (Quotation q : sents) {
            sentAmount += q.getAmount();
            sentAmountHope += q.getAmount() * q.getConversionHope() / 100.0;
        }

        return renderView( "salesReports/salesFunnel.jsp",
                "leads", leads.getAll(),
                "leadAmount", leadAmount,
                "leadAmountHope", leadAmountHope,
                "drafts", drafts.getAll(),
                "draftAmount", draftAmount,
                "draftAmountHope", draftAmountHope,
                "sents", sents.getAll(),
                "sentAmount", sentAmount,
                "sentAmountHope", sentAmountHope);

    }
}

package org.chorem.webmotion.actions.crm;

/*
 * #%L
 * Chorem :: webmotion
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 *
 * @author couteau
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ContactRowForExportModel implements ExportModel<ContactRowForExport> {

    @Override

    public char getSeparator() {
        return ';';
    }

    @Override

    public Iterable<ExportableColumn<ContactRowForExport, Object>> getColumnsForExport() {

        ModelBuilder<ContactRowForExport> modelBuilder = new ModelBuilder<ContactRowForExport>();

        modelBuilder.newColumnForExport("FIRSTNAME", "firstName");

        modelBuilder.newColumnForExport("LASTNAME", "lastName");

        modelBuilder.newColumnForExport("COMPANY", "company");

        modelBuilder.newColumnForExport("ADDRESS", "address");

        modelBuilder.newColumnForExport("MOBILE", "mobilePhone");

        modelBuilder.newColumnForExport("FIX", "fixPhone");

        modelBuilder.newColumnForExport("EMAIL", "email");

        return (Iterable) modelBuilder.getColumnsForExport();

    }

}

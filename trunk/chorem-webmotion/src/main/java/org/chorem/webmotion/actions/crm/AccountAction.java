package org.chorem.webmotion.actions.crm;

/*
 * #%L
 * Chorem :: webmotion
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.ChoremClient;
import org.chorem.entities.Accepted;
import org.chorem.entities.Attachment;
import org.chorem.entities.ContactDetails;
import org.chorem.entities.Employee;
import org.chorem.entities.Note;
import org.chorem.entities.Quotation;
import org.chorem.entities.Rejected;
import org.debux.webmotion.server.WebMotionController;
import org.debux.webmotion.server.render.Render;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.query.WikittyQueryResult;

/**
 * @author jcouteau <couteau@codelutin.com>
 */
public class AccountAction extends WebMotionController {

    public Render view(ChoremClient client, String id) {
        //Recuperation du compte client
        Employee account = client.restore(Employee.class, id,
                "Employee.person");

        //recuperation des contact details
        WikittyQuery contactDetailsQuery = new WikittyQueryMaker().and()
                .exteq(ContactDetails.EXT_CONTACTDETAILS)
                .or()
                    .eq(ContactDetails.FQ_FIELD_CONTACTDETAILS_TARGET, account)
                    .eq(ContactDetails.FQ_FIELD_CONTACTDETAILS_TARGET, account.getPerson())
                    .eq(ContactDetails.FQ_FIELD_CONTACTDETAILS_TARGET, account.getCompany())
                .end().setLimit(WikittyQuery.MAX);
        WikittyQueryResult<ContactDetails> contacts =
                client.findAllByQuery(ContactDetails.class,
                        contactDetailsQuery);

        //recuperation des notes
        WikittyQuery notesQuery = new WikittyQueryMaker().and()
                .exteq(Note.EXT_NOTE)
                .eq(Note.FQ_FIELD_NOTE_TARGET, account)
                .end().setLimit(WikittyQuery.MAX)
                .addSortDescending(Note.ELEMENT_FIELD_NOTE_DATE);
        WikittyQueryResult<Note> notes =
                client.findAllByQuery(Note.class, notesQuery);

        //recuperation des collègues
        WikittyQuery colleaguesQuery = new WikittyQueryMaker().and()
                .exteq(Employee.EXT_EMPLOYEE)
                .eq(Employee.ELEMENT_FIELD_EMPLOYEE_COMPANY, account.getCompany())
                .ne(Employee.ELEMENT_FIELD_EMPLOYEE_PERSON, account.getPerson())
                .end().setLimit(WikittyQuery.MAX);
        WikittyQueryResult<Employee> colleagues =
                client.findAllByQuery(Employee.class, colleaguesQuery);


        //recuperation des documents
        WikittyQuery documentsQuery = new WikittyQueryMaker().and()
                .exteq(Attachment.EXT_ATTACHMENT)
                .eq(Attachment.ELEMENT_FIELD_ATTACHMENT_TARGET, account)
                .end().setLimit(WikittyQuery.MAX);
        WikittyQueryResult<Attachment> documents =
                client.findAllByQuery(Attachment.class, documentsQuery);

        //recuperation des devis en cours
        WikittyQuery quotationsQuery = new WikittyQueryMaker().and()
                .exteq(Quotation.EXT_QUOTATION)
                .extne(Accepted.EXT_ACCEPTED)
                .extne(Rejected.EXT_REJECTED)
                .eq(Quotation.FQ_FIELD_QUOTATION_CUSTOMER, account)
                .end().setLimit(WikittyQuery.MAX);
        WikittyQueryResult<Quotation> quotations =
                client.findAllByQuery(Quotation.class,
                        quotationsQuery);

        return renderView("crm/account.jsp",
                "account", account,
                "contacts", contacts.getAll(),
                "quotations", quotations.getAll(),
                "documents", documents.getAll(),
                "colleagues", colleagues.getAll(),
                "notes", notes.getAll()
        );
    }
}

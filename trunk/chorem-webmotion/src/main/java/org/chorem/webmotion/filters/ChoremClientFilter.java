/*
 * #%L
 * Chorem webmotion
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem.webmotion.filters;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.ChoremClient;
import org.chorem.webmotion.ChoremWebMotionUtil;
import org.debux.webmotion.server.WebMotionFilter;
import org.debux.webmotion.server.call.HttpContext;

/**
 * Inject le ChoremClient dans la request pour permettre au JSP de facilement
 * l'utiliser. Cela est necessaire car celui en session
 * est enregistrer avec le domain, ce qui empeche la recuperation dans
 * les jsp pour l'utiliser.
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ChoremClientFilter extends WebMotionFilter {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(ChoremClientFilter.class);

    public void inject(HttpContext context) {
        // recuperation du client courant
        ChoremClient client = ChoremWebMotionUtil.getClient(context);
        // ajout dans la request
        context.getRequest().setAttribute("client", client);
        doProcess();
    }
}

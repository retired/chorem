package org.chorem.webmotion.converters;

/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.beanutils.converters.AbstractConverter;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class JsonConverter<O> extends AbstractConverter {

    protected Class<O> objectType;

    protected final JsonHelper jsonHelper;

    public static <O> JsonConverter<O> newConverter(Class<O> objectType) {
        return new JsonConverter<>(objectType);
    }

    public JsonConverter(Class<O> objectType) {

        this.objectType = objectType;
        this.jsonHelper = new JsonHelper();
    }

    @Override
    protected String convertToString(Object value) throws Throwable {

        String result = jsonHelper.toJson(value);
        return result;
    }

    @Override
    protected <T> T convertToType(Class<T> type, Object value) throws Throwable {

        String stringValue;

        if (value instanceof String) {

            stringValue = (String) value;

        } else {

            stringValue = ((String[]) value)[0];
        }

        T result = (T) jsonHelper.fromJson(stringValue, type);

        return result;
    }

    @Override
    protected Class<O> getDefaultType() {
        return this.objectType;
    }
}

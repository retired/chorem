package org.chorem.webmotion.actions.sales;

/*
 * #%L
 * Chorem webmotion
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.ChoremClient;
import org.chorem.entities.Accepted;
import org.chorem.entities.Draft;
import org.chorem.entities.Rejected;
import org.chorem.entities.Sent;
import org.chorem.entities.Started;
import org.chorem.entities.Cancelled;
import org.chorem.webmotion.render.RenderWikitty;
import org.chorem.webmotion.render.RenderWikittyJson;
import org.debux.webmotion.server.WebMotionController;
import org.debux.webmotion.server.render.Render;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.Wikitty;

import java.text.ParseException;
import java.util.Date;

/**
 *
 * @author couteau
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class SalesAction  extends WebMotionController {

    static private Log log = LogFactory.getLog(SalesAction.class);

    public Render send(ChoremClient client, String id, String sendingDate) {
        Sent sent = client.restore(Sent.class, id);
        try {
            sent.setPostedDate(WikittyUtil.parseDate(sendingDate));
        } catch (ParseException e) {
            log.debug("Could not parse date : " + sendingDate);
            getContext().addInfoMessage("message", "Warning: Could not parse date " + sendingDate);
        }
        sent = client.store(sent);
        return new RenderWikittyJson(sent);
    }

    public Render accept(ChoremClient client, String id, String acceptedDate) {
        Accepted accepted = client.restore(Accepted.class, id);
        try {
            accepted.setAcceptedDate(WikittyUtil.parseDate(acceptedDate));
        } catch (ParseException e) {
            log.debug("Could not parse date : " + acceptedDate);
            getContext().addInfoMessage("message", "Warning: Could not parse date " + acceptedDate);
        }
        client.store(accepted);
        return new RenderWikittyJson(accepted);
    }

    public Render reject(ChoremClient client, String id, String rejectedDate) {
        Rejected rejected = client.restore(Rejected.class, id);
        try {
            rejected.setRejectedDate(WikittyUtil.parseDate(rejectedDate));
        } catch (ParseException e) {
            log.debug("Could not parse date : " + rejectedDate);
            getContext().addInfoMessage("message", "Warning: Could not parse date " + rejectedDate);
        }
        client.store(rejected);
        return new RenderWikittyJson(rejected);
    }

    public Render start(ChoremClient client, String id) {
        Started started = client.restore(Started.class, id);
        started.setStartedDate(new Date());
        client.store(started);
        return new RenderWikitty().setModelWikitty((Wikitty)started);
    }

    public Render answer(ChoremClient client, String id, String sendingDate,
                         String reference) {
        Draft draft = client.restore(Draft.class, id,".*");
        try {
            draft.setSendingDate(WikittyUtil.parseDate(sendingDate));
        } catch (ParseException e) {
            log.debug("Could not parse date : " + sendingDate);
            getContext().addInfoMessage("message", "Warning: Could not parse date " + sendingDate);
        }
        draft.setReference(reference);
        draft = client.store(draft);
        Wikitty w = client.castTo(Wikitty.class, draft);
        return new RenderWikitty().setModelWikitty(w);
    }

    public Render cancel(ChoremClient client, String id, String cancelledDate,
                         String reason) {
        Cancelled cancelled = client.restore(Cancelled.class, id, ".*");
        try {
            cancelled.setCancelledDate(WikittyUtil.parseDate(cancelledDate));
        } catch (ParseException e) {
            log.debug("Could not parse date : " + cancelledDate);
            getContext().addInfoMessage("message", "Warning: Could not parse date " + cancelledDate);
        }
        cancelled.setReason(reason);
        cancelled = client.store(cancelled);
        return new RenderWikittyJson(cancelled);
    }
}

package org.chorem.webmotion.actions.project;

/*
 * #%L
 * Chorem :: webmotion
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import org.chorem.ChoremClient;
import org.chorem.entities.Project;
import org.chorem.webmotion.PaginatedResult;
import org.debux.webmotion.server.WebMotionController;
import org.nuiton.wikitty.entities.ElementField;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.query.WikittyQueryResult;

import java.util.ArrayList;
import java.util.Map;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class ProjectsAction extends WebMotionController {

    /**
     * Rend le graphe des devis envoyés par mois
     *
     * @param client
     * @return
     */
    public PaginatedResult findAllProjects(ChoremClient client, int page, int count, Map<String, String> sorting) {

        //Ventes de l'année
        WikittyQuery totalProducts = new WikittyQueryMaker()
                .select().count(Project.ELEMENT_FIELD_PROJECT_NAME).where().and()
                .exteq(Project.EXT_PROJECT)
                .end();

        Integer nbProjects = client.findByQuery(Integer.class, totalProducts);

        PaginatedResult<Project> paginatedResult;
        if (nbProjects > 0) {

            if (sorting == null || sorting.isEmpty()) {
                sorting = ImmutableMap.of("name", "asc");
            }
            String orderBy = sorting.keySet().iterator().next();

            ElementField orderElement;
            if (Project.FIELD_PROJECT_DESCRIPTION.equals(orderBy)) {
                orderElement = Project.ELEMENT_FIELD_PROJECT_DESCRIPTION;
            } else {
                // by default, order by
                orderElement = Project.ELEMENT_FIELD_PROJECT_NAME;
            }

            WikittyQuery projectsQuery = new WikittyQueryMaker()
                .where().and()
                .exteq(Project.EXT_PROJECT)
                .end()
                .setOffset((page - 1) * count)
                .setLimit(page * count);

            if ("desc".equals(sorting.get(orderBy))) {
                projectsQuery.addSortDescending(orderElement);
            } else {
                //by default : ascending
                projectsQuery.addSortAscending(orderElement);
            }

            WikittyQueryResult<Project> projects = client.findAllByQuery(Project.class, projectsQuery);

            paginatedResult = new PaginatedResult<Project>(projects.getAll(), page, count, nbProjects);

        } else {
            paginatedResult = new PaginatedResult<Project>(new ArrayList<Project>(), 1, count, 0);
        }

        return paginatedResult;
    }

    public void updateProject(ChoremClient client, String id, String name, String description) {
        Project project = client.restore(Project.class, id);
        project.setName(name);
        project.setDescription(description);
        client.store(project);
    }

}

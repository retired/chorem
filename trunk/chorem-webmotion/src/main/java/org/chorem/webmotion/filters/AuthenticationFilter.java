/*
 * #%L
 * Chorem webmotion
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem.webmotion.filters;


import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.ChoremClient;
import org.chorem.ChoremConfigOption;
import org.chorem.webmotion.ChoremWebMotionUtil;
import org.debux.webmotion.server.WebMotionFilter;
import org.debux.webmotion.server.call.HttpContext;
import org.debux.webmotion.server.render.Render;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.wikitty.entities.WikittyUser;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;

/**
 * Inject dans les parametres des actions le client wikitty a utiliser pour les
 * actions (ChoremClient)
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 *
 */
public class AuthenticationFilter extends WebMotionFilter {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(AuthenticationFilter.class);

    public Render check(HttpContext context,
            String choremLogin, String choremPassword) {
        Render result = null;

        // injection de ChoremWebMotionUtil dans la session
        ChoremClient client = ChoremWebMotionUtil.getClient(context);

        String token = client.getSecurityToken();
        if (log.isDebugEnabled()) {
            // on affiche pas le token, car ca creerait un trou de securite
            // on indique donc juste s'il y en a un
            log.debug("SecurityTocken: " + StringUtils.isNotBlank(token));
        }
        if (token == null) {
            if (StringUtils.isBlank(choremLogin)) {
                // avant de mettre la page de login, on verifie s'il le faut vraiment
                ApplicationConfig config = ChoremWebMotionUtil.getConfig(context);
                boolean authActivate =
                        config.getOptionAsBoolean(ChoremConfigOption.CHOREM_AUTHENTICATION.getKey());
                if (authActivate) {
                    // la configuration demande une authentification, mais s'il
                    // n'y a aucun WikittyUser, on ne le fait pas pour permettre
                    // la creation du 1er user
                    WikittyQuery q = new WikittyQueryMaker()
                            .exteq(WikittyUser.EXT_WIKITTYUSER).end();
                    String userId = client.findByQuery(q);
                    if (userId != null) {
                        result = renderView("login.jsp");
                    }
                }
            } else {
                // phase d'authentification, l'utilisateur a soumis le formulaire de login
                try {
                    client.login(choremLogin, choremPassword);
                    String url = context.getUrl();
                    String query = context.getRequest().getQueryString();
                    if (StringUtils.isNotBlank(query)) {
                        url = StringUtils.join(url, "?", query);
                    }
                    
                    
                    result = renderURL(url);
                } catch (SecurityException eee) {
                    context.addErrorMessage("message", "bad login or password");
                    result = renderView("login.jsp");
                }
            }
        }

        if (result == null) {
            doProcess();
            return null;
        } else {
            return result;
        }
    }
}

package org.chorem.webmotion.actions.financial;

/*
 * #%L
 * Chorem :: webmotion
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.ChoremClient;
import org.chorem.entities.Accepted;
import org.chorem.entities.FinancialTransaction;
import org.chorem.entities.Quotation;
import org.chorem.webmotion.actions.sales.SalesData;
import org.chorem.webmotion.actions.sales.SalesReportHelper;
import org.debux.webmotion.server.WebMotionController;
import org.debux.webmotion.server.render.Render;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jcouteau <couteau@codelutin.com>
 */
public class ProjectBillingReportAction extends WebMotionController {

    /**
     * Rend le graphe des devis envoyés par mois
     *
     * @param client
     * @return
     */
    public Render billing(ChoremClient client, String project, String from,
                        String to) {

        if (null == from) {
            from = String.valueOf(BillingReportHelper.getFirstYear(client));
        }

        if (null == to) {
            to = String.valueOf(BillingReportHelper.getLastYear());
        }

        Map<Integer, SalesData> salesData = new LinkedHashMap<Integer, SalesData>();

        List<Integer> listAllYears = BillingReportHelper.listAllYears(from, to);

        List<Integer> listAllYearsInChorem = BillingReportHelper.listAllYears(client);

        int previousYearValue = 0;

        for (Integer year:listAllYears){
            Date yearFirstDay = BillingReportHelper.getFirstDayOfYear(year);
            Date yearLastDay =  BillingReportHelper.getLastDayOfYear(year);

            SalesData yearData = new SalesData();

            WikittyQuery projectQuery = new WikittyQueryMaker()
                    .select().sum("FinancialTransaction.amount").where().and()
                    .eq(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_TARGET, project)
                    .bw(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EMITTEDDATE, yearFirstDay,
                            yearLastDay)
                    .end();

            Integer billing = client.findByQuery(Integer.class, projectQuery);

            //TODO JC 2012-01-22 Find a way to replace two queries into one.
            WikittyQuery quotationsQuery = new WikittyQueryMaker().and()
                    .eq(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_TARGET, project)
                    .bw(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EMITTEDDATE, yearFirstDay,
                            yearLastDay)
                    .end();

            List<FinancialTransaction> quotations = client.findAllByQuery(FinancialTransaction.class,
                    quotationsQuery).getAll();

            //Progression devis envoyés
            int salesProgression = 0;
            if (previousYearValue != 0){
                salesProgression = 100 * (billing - previousYearValue) / previousYearValue;
            }

            previousYearValue = billing;

            yearData.setSales(billing);
            yearData.setProgression(salesProgression);
            yearData.setQuotations(quotations.size());

            salesData.put(year, yearData);
        }

        return renderView("financial/reports/projectBillingReport.jsp",
                "data", salesData,
                "allYears", listAllYearsInChorem,
                "fromYear", from,
                "toYear", to);
    }
}

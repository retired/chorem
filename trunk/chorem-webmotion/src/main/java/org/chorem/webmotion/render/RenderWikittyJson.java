package org.chorem.webmotion.render;

/*
 * #%L
 * Chorem :: webmotion
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.gson.Gson;
import org.apache.commons.lang3.StringUtils;
import org.debux.webmotion.server.call.Call;
import org.debux.webmotion.server.call.HttpContext;
import org.debux.webmotion.server.mapping.Mapping;
import org.debux.webmotion.server.render.Render;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.BusinessEntity;
import org.nuiton.wikitty.entities.Wikitty;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * @author jcouteau <couteau@codelutin.com>
 */
public class RenderWikittyJson extends Render {
    protected Map<String, Object> model;

    public RenderWikittyJson(Wikitty wikitty) {
        this.model = createModel(wikitty);
    }

    public RenderWikittyJson(BusinessEntity entity) {
        this(WikittyUtil.getWikitty(entity));
    }

    public Map<String, Object> getModel() {
        return model;
    }

    @Override
    public void create(Mapping mapping, Call call) throws IOException, ServletException {
        HttpContext context = call.getContext();
        HttpServletResponse response = context.getResponse();

        Object object = model;
        if (model != null && model.size() == 1
                && model.keySet().contains(DEFAULT_MODEL_NAME)) {
            object = model.values().toArray()[0];
        }

        response.setContentType("application/json");

        Gson gson = new Gson();
        String json = gson.toJson(object);
        PrintWriter out = context.getOut();
        out.print(json);
    }

    Map<String,Object> getMeta(Wikitty wikitty){
        Map<String,Object> meta = new HashMap<String,Object>();
        meta.put("id", wikitty.getWikittyId());
        meta.put("version", wikitty.getWikittyVersion());
        meta.put("extensions", StringUtils.join(wikitty.getExtensionNames(), ","));
        return meta;
    }

    Map<String,Object> getData(Wikitty wikitty){
        return wikitty.getFieldValue();
    }

    Map<String,Object> getPreloaded(Wikitty wikitty){
        Map<String,Object> preloadedModels = new HashMap<String, Object>();
        Map<String,Wikitty> preloaded = wikitty.getPreloaded();
        for (Map.Entry<String, Wikitty> entry : preloaded.entrySet()) {
            preloadedModels.put(entry.getKey(),createModel(entry.getValue()));
        }
        return preloadedModels;
    }

    Map<String,Object> createModel(Wikitty wikitty){
        Map<String,Object> model = new HashMap<String,Object>();
        model.put("meta", getMeta(wikitty));
        model.put("data", getData(wikitty));
        model.put("preloaded", getPreloaded(wikitty));
        return model;
    }

}

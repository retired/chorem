/*
 * #%L
 * Chorem webmotion
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem.webmotion.actions;


import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.ChoremClient;
import org.chorem.entities.EmployeeHR;
import org.chorem.entities.Interval;
import org.chorem.entities.Vacation;
import org.chorem.entities.VacationRequest;
import org.debux.webmotion.server.WebMotionController;
import org.debux.webmotion.server.render.Render;
import org.nuiton.wikitty.entities.ElementField;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.query.WikittyQueryResult;

/**
 *
 * @author ble
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class DashboardHRAction extends WebMotionController {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(DashboardHRAction.class);

    static final public String budgetDateFormat = "MM/yyyy";
    static final public String summaryDateFormat = "dd/MM/yyyy hh:mm";

    
    
    public Render vacationFilter(ChoremClient client, String query) {
        // recuperation des vacances
        WikittyQuery vacationQuery = new WikittyQueryMaker().and()
                .exteq(Interval.EXT_INTERVAL)
                .exteq(Vacation.EXT_VACATION)
                .end()
                .addSortAscending(new ElementField(VacationRequest.FQ_FIELD_VACATIONREQUEST_STATUSREQUEST));

        WikittyQueryResult<String> vacationIds =
                client.findAllByQuery(vacationQuery);
        
        List<Vacation> vacations =
                client.restore(Vacation.class, vacationIds.getAll(),
                "Vacation.vacationRequest;VacationRequest.employeeRequest");
               
        WikittyQuery employeeHRRequestQuery = new WikittyQueryMaker().and()
                .parse(query)
                .exteq(EmployeeHR.EXT_INTERVAL)
                .exteq(EmployeeHR.EXT_EMPLOYEE)
                .exteq(EmployeeHR.EXT_EMPLOYEEHR)
                .end();
                
        
        WikittyQueryResult<String> employeeHRIds =
                client.findAllByQuery(employeeHRRequestQuery);
        
        List<EmployeeHR> employeeHRRequests =
                client.restore(EmployeeHR.class, employeeHRIds.getAll());                


        return renderView("dashboardRequestVacation.jsp",
                "vacations", vacations, "employes", employeeHRRequests
                );

    }

        /**
     * Les vacances 
     * @param client
     * @param query
     * @return
     */
    public Render requestVacation(ChoremClient client, String query) {
        return vacationFilter(client, query);
    }
    

}

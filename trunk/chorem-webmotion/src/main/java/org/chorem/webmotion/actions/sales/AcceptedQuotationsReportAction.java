package org.chorem.webmotion.actions.sales;

/*
 * #%L
 * Chorem :: webmotion
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.lang3.time.DateUtils;
import org.chorem.ChoremClient;
import org.chorem.entities.Accepted;
import org.chorem.entities.Quotation;
import org.debux.webmotion.server.WebMotionController;
import org.debux.webmotion.server.render.Render;
import org.nuiton.util.DateUtil;
import org.nuiton.wikitty.WikittyClient;
import org.nuiton.wikitty.query.FacetTopic;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.query.WikittyQueryResult;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jcouteau <couteau@codelutin.com>
 */
public class AcceptedQuotationsReportAction extends WebMotionController {

    /**
     * Rend le graphe des devis acceptés par mois
     *
     * @param client
     * @return
     */
    public Render acceptedQuotationPerMonth(ChoremClient client, String from, String to) {

        if (null == from) {
            from = String.valueOf(SalesReportHelper.getFirstYear(client));
        }

        if (null == to) {
            to = String.valueOf(SalesReportHelper.getLastYear());
        }

        Map<Integer, QuotationYearData> data = new LinkedHashMap<Integer, QuotationYearData>();

        List<Integer> listAllYears = SalesReportHelper.listAllYears(from, to);

        List<Integer> listAllYearsInChorem = SalesReportHelper.listAllYears(client);

        int previousYearValue = 0;

        for (Integer year:listAllYears){
            Date yearFirstDay = SalesReportHelper.getFirstDayOfYear(year);
            Date yearLastDay =  SalesReportHelper.getLastDayOfYear(year);

            QuotationYearData yearData = new QuotationYearData();

            //Nombre de devis acceptés sur l'année
            WikittyQuery acceptedCurrentYearQuery = new WikittyQueryMaker().and()
                    .exteq(Accepted.EXT_ACCEPTED)
                    .bw(Accepted.FQ_FIELD_ACCEPTED_ACCEPTEDDATE, yearFirstDay, yearLastDay)
                    .end().setLimit(WikittyQuery.MAX);

            WikittyQueryResult<Quotation> acceptedCurrentYearResult =
                    client.findAllByQuery(Quotation.class, acceptedCurrentYearQuery);
            int yearAcceptedQuotation = acceptedCurrentYearResult.size();

            //Progression devis envoyés
            int acceptedQuotationProgression = 0;
            if (previousYearValue != 0){
                acceptedQuotationProgression = 100 * (yearAcceptedQuotation - previousYearValue) / previousYearValue;
            }

            previousYearValue = yearAcceptedQuotation;

            //Graphe devis envoyés
            Map<String, Integer> acceptedQuotationData = getAcceptedQuotationData(year, client);

            yearData.setBaseValue(yearAcceptedQuotation);
            yearData.setProgression(acceptedQuotationProgression);
            yearData.setPlotValues(acceptedQuotationData);

            data.put(year, yearData);
        }

        return renderView("salesReports/acceptedQuotation.jsp",
                "data", data,
                "allYears", listAllYearsInChorem,
                "fromYear", from,
                "toYear", to);
    }

    protected Map<String,Integer> getAcceptedQuotationData(Integer year, WikittyClient client){

        Date first = SalesReportHelper.getFirstDayOfYear(year);
        Date last = SalesReportHelper.getLastDayOfYear(year);

        //Create the basis wikitty query
        WikittyQuery acceptedQuotationDataQuery = new WikittyQueryMaker().and()
                .exteq(Accepted.EXT_ACCEPTED)
                .bw(Accepted.FQ_FIELD_ACCEPTED_ACCEPTEDDATE, first, last)
                .end().setLimit(WikittyQuery.MAX);

        //add a facet per month
        Map<Integer,String> acceptedQuotationLabels = new HashMap<Integer, String>();
        Date baseValue = DateUtils.addDays(last, 1);
        for (int i=0;i<12;i++){
            Date lastDayOfMonth = DateUtil.getYesterday(baseValue);
            Date firstDayOfMonth = DateUtil.setFirstDayOfMonth(lastDayOfMonth);
            baseValue=firstDayOfMonth;

            WikittyQuery monthFacet = new WikittyQueryMaker().and()
                    .bw(Accepted.FQ_FIELD_ACCEPTED_ACCEPTEDDATE, firstDayOfMonth, lastDayOfMonth)
                    .end();

            acceptedQuotationDataQuery = acceptedQuotationDataQuery.addFacetQuery(String.valueOf(i), monthFacet.getCondition());

            acceptedQuotationLabels.put(i, DateUtil.getMonthLibelle(DateUtil.getMonth(lastDayOfMonth) + 1));
        }

        WikittyQueryResult<Quotation> acceptedQuotationDataResult =
                client.findAllByQuery(Quotation.class, acceptedQuotationDataQuery.setFacetMinCount(0));

        Map<String,Integer> acceptedQuotationData = new LinkedHashMap<String, Integer>();

        Map<String,List<FacetTopic>> acceptedQuotationDataFacet = acceptedQuotationDataResult.getFacets();

        for (int i=11;i>=0;i--) {
            acceptedQuotationData.put(acceptedQuotationLabels.get(i), acceptedQuotationDataFacet.get(String.valueOf(i)).get(0).getCount());
        }

        return acceptedQuotationData;
    }


}

.. -
.. * #%L
.. * chorem
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2011 - 2012 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

=========================
Documentation développeur
=========================

Ajout d'une entité
==================

* Modifier le fichier ArgoUML chorem-model.zargo
* Modifier le fichier chorem-model.properties pour ajouter les informations de version et autre pour cette entité
* Ajouter cette entité dans la liste des entités qui doivent exister dans le système dans le fichier ChoremClient.java

Modification d'une list de choix possible pour un attribut
==========================================================

* Modifier le fichier chorem-model.properties pour ajouter l'enumeration sur le champs choisi
* Modifier ou créer le l'enumeration Java correspondante pour la manipuler dans le code Java (chorem-entities - org.chorem.entities.XXXX)

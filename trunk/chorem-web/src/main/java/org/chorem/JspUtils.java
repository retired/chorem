package org.chorem;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Convenient static methods to format output on jsp pages
 * 
 * @author vbriand
 */
public class JspUtils {
    /**
     * Formats a full date to something easily readable
     * 
     * @param date
     * @return the formatted date
     */
    public static String dateFormat(Date date) {
        String dateReturn;
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        
        dateReturn = formatter.format(date);
        return dateReturn;
    }
}

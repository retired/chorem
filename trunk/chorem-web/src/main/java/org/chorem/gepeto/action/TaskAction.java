package org.chorem.gepeto.action;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.ChoremProxy;
import org.chorem.action.ChoremBaseAction;
import org.chorem.entities.ProjectOrder;
import org.chorem.entities.TaskImpl;

import com.opensymphony.xwork2.ActionContext;

import static org.nuiton.i18n.I18n.n_;

/**
 * Task management class
 * 
 * @author vbriand
 */
public class TaskAction extends ChoremBaseAction {
    
    private static final long serialVersionUID = -3938480897148282089L;
    
    private static final Log log = LogFactory.getLog(TaskAction.class);
    
    static public TaskAction getAction() {
        return (TaskAction)ActionContext.getContext().get(CONTEXT_ACTION_KEY);
    }
    
    /**
     * Adds a new task
     * 
     * @return INPUT if the mandatory fields haven't all been filled in, 
     * SUCCESS if the task has been added successfully, 
     * ERROR if an error occurred
     */
    public String add() {
        String result = INPUT;
        ChoremProxy proxy = getChoremProxy();
        
        try {
            if (projectOrderId == null) {
                result = ERROR;
            } else {
                //If projectOrderId isn't a valid UUID, an exception is thrown
                UUID.fromString(projectOrderId);
                
                ProjectOrder projectOrder = proxy.restore(ProjectOrder.class, projectOrderId);
                
                //If the projectOrderId doesn't exist
                if (projectOrder == null) {
                    result = ERROR;
                } else {
                    if (name != null && description != null && price != null &&
                            estimatedDays != null && beginDate != null &&
                            estimatedEndDate != null) {
                        //If the task has been added successfully
                        if (addTask()) {
                            result = SUCCESS;
                        } else {
                            result = ERROR;
                        }
                    }
                }
            }
        } catch (IllegalArgumentException e) {
            //projectOrderId is invalid
            result = ERROR;
        }
        return result;
    }

    protected String name;
    protected String description;
    protected String price;
    protected String estimatedDays;
    protected String beginDate;
    protected String estimatedEndDate;
    protected String projectOrderId;

    /**
     * Stores the new task through the proxy 
     * 
     * @return true if the task has been stored properly, 
     * false if a problem occurred
     */
    protected boolean addTask() {
        boolean result = true;
        
        try {
            ChoremProxy proxy = getChoremProxy();
            TaskImpl newTask = new TaskImpl();
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            
            newTask.setDescription(description);
            newTask.setName(name);
            
            //Lenient mode disabled because results may be very odd
            formatter.setLenient(false);
            try {
                Calendar cal = Calendar.getInstance();
                
                newTask.setProjectOrder(projectOrderId);
                newTask.setBeginDate(formatter.parse(beginDate));
                newTask.setEndDate(formatter.parse(estimatedEndDate));
                
                //Subtracts 1 day to the current time so the next test willn't 
                //fail if the begin date is today's date, as the before() method
                //is exclusive
                cal.add(Calendar.DAY_OF_MONTH, -1);
                //If the begin date is set before the current date
                if (newTask.getBeginDate().before(cal.getTime())) {
                    result = false;
                    addFieldError("beginDate", getText(n_("chorem.beginDate.beforeToday")));
                }
                
                //If the estimated end date is anterior to the begin date (...)
                if (newTask.getEndDate().before(newTask.getBeginDate())) {
                    result = false;
                    addActionError(getText(n_("chorem.endDate.afterBegin")));
                }
            } catch (ParseException e) {
                //If the date doesn't match the format above
                result = false;
                addActionError(getText(n_("chorem.date.wrongFormat")));
            }
            
            newTask.setEstimatedDays(Integer.parseInt(estimatedDays));
            //Replaces the (possible) comma by a dot, so the string can be  
            //parsed successfully as a float
            price = price.replace(',', '.');
            newTask.setPrice(Float.parseFloat(price));
            
            //If everything went smoothly
            if (result) {
                proxy.store(newTask);
            }
        } catch (Exception e) {
            result = false;
            addActionError(getText(n_("chorem.create.error")));
            log.error("An error occurred while creating a new task", e);
        }
        return result;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the price
     */
    public String getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * @return the estimatedDays
     */
    public String getEstimatedDays() {
        return estimatedDays;
    }

    /**
     * @param estimatedDays the estimatedDays to set
     */
    public void setEstimatedDays(String estimatedDays) {
        this.estimatedDays = estimatedDays;
    }

    /**
     * @return the beginDate
     */
    public String getBeginDate() {
        return beginDate;
    }

    /**
     * @param beginDate the beginDate to set
     */
    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }

    /**
     * @return the estimatedEndDate
     */
    public String getEstimatedEndDate() {
        return estimatedEndDate;
    }

    /**
     * @param estimatedEndDate the estimatedEndDate to set
     */
    public void setEstimatedEndDate(String estimatedEndDate) {
        this.estimatedEndDate = estimatedEndDate;
    }

    /**
     * @return the projectOrderId
     */
    public String getProjectOrderId() {
        return projectOrderId;
    }

    /**
     * @param projectOrderId the projectOrderId to set
     */
    public void setProjectOrderId(String projectOrderId) {
        this.projectOrderId = projectOrderId;
    }
}

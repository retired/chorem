package org.chorem.gepeto.action;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.ChoremProxy;
import org.chorem.action.ChoremBaseAction;
import org.chorem.entities.Company;
import org.chorem.entities.ProjectOrder;
import org.chorem.entities.ProjectOrderImpl;
import org.chorem.entities.Quotation;
import org.chorem.entities.Task;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.search.PagedResult;
import org.nuiton.wikitty.search.Search;

import com.opensymphony.xwork2.ActionContext;

import static org.nuiton.i18n.I18n.n_;

/**
 * Project order management class
 * 
 * @author vbriand
 */
public class ProjectOrderAction extends ChoremBaseAction {

    private static final long serialVersionUID = -5469320345746533520L;

    private static final Log log = LogFactory.getLog(ProjectOrderAction.class);
    
    static public ProjectOrderAction getAction() {
        return (ProjectOrderAction)ActionContext.getContext().get(CONTEXT_ACTION_KEY);
    }
    
    /**
     * Adds a new project order
     * 
     * @return INPUT if the mandatory fields haven't all been filled in, 
     * SUCCESS if the project order has been added successfully, 
     * ERROR if an error occurred
     */
    public String add() {
        String result = INPUT;
        ChoremProxy proxy = getChoremProxy();

        try {
            if (quotationId == null) {
                result = ERROR;
            } else {
                //If quotationId isn't a valid UUID, an exception is thrown
                UUID.fromString(quotationId);
                
                Quotation quotation = proxy.restore(Quotation.class, quotationId);
                
                //If the quotationId doesn't exist or if the quotation already has a project order
                if (quotation == null || quotationHasProjectOrder(quotation)) {
                    result = ERROR;
                } else {
                    quotationReference = quotation.getReference();
                    if (type != null && description != null &&
                            beginDate != null && estimatedEndDate != null && 
                            companyId != null) {
                        //If the project order has been added successfully
                        if (addProjectOrder()) {
                            result = SUCCESS;
                        } else {
                            result = ERROR;
                        }
                    }
                }
            }
        } catch (IllegalArgumentException e) {
            //quotationId is invalid
            result = ERROR;
        }
        return result;
    }
    
    /**
     * Retrieves the information of the projectOrder referenced by the 
     * projectOrderId
     * 
     * @return SUCCESS if the information have been retrieved, 
     * ERROR either if the UUID is invalid or the projectOrderId doesn't exist
     */
    public String projectOrderDetails() {
        String result = SUCCESS;
        ChoremProxy proxy = getChoremProxy();
        
        try {
            if (projectOrderId == null) {
                result = ERROR;
            } else {
                //If projectOrderId isn't a valid UUID, an exception is thrown
                UUID.fromString(projectOrderId);
                
                ProjectOrder projectOrder = proxy.restore(ProjectOrder.class, 
                        projectOrderId);
                
                if (projectOrder != null) { //If the projectOrderId exists
                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                    
                    setBeginDate(formatter.format(projectOrder.getBeginDate()));
                    
                    Company company = proxy.restore(Company.class,
                            projectOrder.getCompany());
 
                    setCompany(company);
                    setDescription(projectOrder.getDescription());
                    setEstimatedEndDate(formatter.format(projectOrder.getEndDate()));
                    setStatus(projectOrder.getStatus());
                    setType(projectOrder.getType());
                    
                    Quotation quotation;
                    
                    quotation = proxy.restore(Quotation.class,
                            projectOrder.getQuotation());
                    setQuotationId(quotation.getWikittyId());
                    setQuotationReference(quotation.getReference());
                } else {
                    result = ERROR;
                }
            }
        } catch (IllegalArgumentException e) {
            //projectOrderId is invalid
            result = ERROR;
        }
        return result;
    }
    
    /**
     * Modifies the project order's information
     * 
     * @return SUCCESS if the project has been modified successfully,
     * INPUT if the date format is incorrect,
     * ERROR if the id is invalid
     */
    public String modify() {
        String result = SUCCESS;
        ProjectOrder projectOrder;
        ChoremProxy proxy = getChoremProxy();
        
        projectOrder = proxy.restore(ProjectOrder.class, projectOrderId);
         //If the id exists and the form has been submitted
        if (projectOrder != null && type != null && 
                beginDate != null && 
                estimatedEndDate != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date begin = null, end = null;
            
            //This is tested manually instead of using a validator because 
            //otherwise the company is not set (the validator returns INPUT and 
            //does not launch any method) and it leads to a null pointer 
            //exception
            if (type.isEmpty()) {
                result = INPUT;
                addFieldError("type", getText(n_("chorem.field.required")));
            }
            if (beginDate.isEmpty()) {
                result = INPUT;
                addFieldError("beginDate", getText(n_("chorem.field.required")));
            }
            if (estimatedEndDate.isEmpty()) {
                result = INPUT;
                addFieldError("endDate", getText(n_("chorem.field.required")));
            }
            
            //Lenient mode disabled because results may be very odd
            formatter.setLenient(false);
            try {
                begin = formatter.parse(beginDate);
                projectOrder.setBeginDate(begin);
            } catch (ParseException e) {
                result = INPUT;
                addFieldError("beginDate", getText(n_("chorem.date.wrongFormat")));
            }
            try {
                end = formatter.parse(estimatedEndDate);
                projectOrder.setEndDate(end);
            } catch (ParseException e) {
                result = INPUT;
                addFieldError("estimatedEndDate", getText(n_("chorem.date.wrongFormat")));
            }
            
            //If end and begin have been initialized
            if (!result.equals(INPUT)) {
                if (end.before(begin)) {
                    result = INPUT;
                    addFieldError("estimatedEndDate", getText(n_("chorem.endDate.afterBegin")));
                }
            }

            projectOrder.setType(type);
            if (result.equals(SUCCESS)) {
                proxy.store(projectOrder);
            } else if (result.equals(INPUT)) {
                Company company = proxy.restore(Company.class,
                        projectOrder.getCompany());
 
                setCompany(company);
            }
        } else {
            result = ERROR;
        }
        return result;
    }
    
    protected String type;
    protected String description;
    protected String status;
    protected String beginDate;
    protected String estimatedEndDate;
    protected String quotationId;
    protected String quotationReference;
    protected String companyId;
    protected Company company;
    protected String projectOrderId;

    /**
     * Gets the project order's tasks
     * 
     * @return the project order's tasks
     */
    public List<Task> getTasks() {
        ChoremProxy proxy = getChoremProxy();
        Search search = Search.query();
        Criteria criteria = search.eq(Task.FQ_FIELD_TASK_PROJECTORDER,
                projectOrderId).criteria();
        PagedResult<Task> result = proxy.findAllByCriteria(Task.class, criteria);
        List<Task> tasks = result.getAll();
        
        return tasks;
    }
    
    /**
     * Gets the list of all companies
     * 
     * @return a list containing all companies
     */
    public List<Company> getAllCompanies() {
        ChoremProxy proxy = getChoremProxy();
        Search search = Search.query().exteq(Company.EXT_COMPANY);
        Criteria criteria = search.criteria();
        PagedResult<Company> result = proxy.findAllByCriteria(Company.class, criteria);
        List<Company> companies = result.getAll();
        return companies;
    }
    
    /**
     * Stores the new project order through the proxy 
     * 
     * @return true if the project order has been stored properly, 
     * false if a problem occurred
     */
    protected boolean addProjectOrder() {
        boolean result = true;
        
        try {
            ChoremProxy proxy = getChoremProxy();
            ProjectOrderImpl newProjectOrder = new ProjectOrderImpl();
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            
            newProjectOrder.setType(type);
            newProjectOrder.setDescription(description);
            newProjectOrder.setQuotation(quotationId);
            newProjectOrder.setStatus(status);
            
            if (proxy.restore(Company.class, companyId) != null) {
                newProjectOrder.setCompany(companyId);
            } else { //If the id doesn't exist or is invalid
                addActionError(getText(n_("chorem.gepeto.projectOrder.invalidCompany")));
                result = false;
            }
            
            //Lenient mode disabled because results may be very odd
            formatter.setLenient(false);
            try {
                Calendar cal = Calendar.getInstance();
                
                newProjectOrder.setBeginDate(formatter.parse(beginDate));
                newProjectOrder.setEndDate(formatter.parse(estimatedEndDate));
                
                //Subtracts 1 day to the current time so the next test willn't 
                //fail if the begin date is today's date, as the before() method
                //is exclusive
                cal.add(Calendar.DAY_OF_MONTH, -1);
                //If the begin date is set before the current date
                if (newProjectOrder.getBeginDate().before(cal.getTime())) {
                    result = false;
                    addFieldError("beginDate", getText(n_("chorem.beginDate.beforeToday")));
                }
                
                //If the estimated end date is anterior to the begin date (...)
                if (newProjectOrder.getEndDate().before(newProjectOrder.getBeginDate())) {
                    result = false;
                    addActionError(getText(n_("chorem.endDate.afterBegin")));
                }
            } catch (ParseException e) {
                //If the date doesn't match the format above
                result = false;
                addActionError(getText(n_("chorem.date.wrongFormat")));
            }
            
            //If everything went smoothly
            if (result) {
                proxy.store(newProjectOrder);
            }
        } catch (Exception e) {
            result = false;
            addActionError(getText(n_("chorem.create.error")));
            log.error("An error occurred while creating a new project order", e);
        }
        return result;
    }
    
    /**
     * Tests if a quotation already has a project order
     * 
     * @param quotation
     * @return true if the quotation already has a project order, false otherwise
     */
    protected boolean quotationHasProjectOrder(Quotation quotation) {
        ChoremProxy proxy = getChoremProxy();
        Search search = Search.query();
        Criteria criteria = search.eq(ProjectOrder.FQ_FIELD_PROJECTORDER_QUOTATION,
                quotationId).criteria();
        ProjectOrder result = proxy.findByCriteria(ProjectOrder.class, criteria);
        boolean hasProjectOrder = (result != null);
        
        return hasProjectOrder;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }
    
    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }
    
    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }
    
    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the beginDate
     */
    public String getBeginDate() {
        return beginDate;
    }
    
    /**
     * @param beginDate the beginDate to set
     */
    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }
    
    /**
     * @return the estimatedEndDate
     */
    public String getEstimatedEndDate() {
        return estimatedEndDate;
    }
    
    /**
     * @param estimatedEndDate the estimatedEndDate to set
     */
    public void setEstimatedEndDate(String estimatedEndDate) {
        this.estimatedEndDate = estimatedEndDate;
    }

    /**
     * @return the quotationId
     */
    public String getQuotationId() {
        return quotationId;
    }

    /**
     * @param quotationId the quotationId to set
     */
    public void setQuotationId(String quotationId) {
        this.quotationId = quotationId;
    }

    /**
     * @return the quotationReference
     */
    public String getQuotationReference() {
        return quotationReference;
    }

    /**
     * @param quotationReference the quotationReference to set
     */
    public void setQuotationReference(String quotationReference) {
        this.quotationReference = quotationReference;
    }

    /**
     * @return the projectOrderId
     */
    public String getProjectOrderId() {
        return projectOrderId;
    }

    /**
     * @param projectOrderId the projectOrderId to set
     */
    public void setProjectOrderId(String projectOrderId) {
        this.projectOrderId = projectOrderId;
    }

    /**
     * @return the companyId
     */
    public String getCompanyId() {
        return companyId;
    }

    /**
     * @param companyId the companyId to set
     */
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    /**
     * @return the company
     */
    public Company getCompany() {
        return company;
    }

    /**
     * @param company the company to set
     */
    public void setCompany(Company company) {
        this.company = company;
    }
}
package org.chorem;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ChoremUtil {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(ChoremUtil.class);

    static public Collection<String> toCollection(String s) {
        Collection<String> result;
        if (s == null) {
            result = Collections.EMPTY_LIST;
        } else {
            result = Arrays.asList(StringUtils.split(s, " :,;|"));
        }
        return result;
    }

}

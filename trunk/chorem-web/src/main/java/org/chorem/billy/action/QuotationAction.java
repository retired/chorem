package org.chorem.billy.action;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.ChoremProxy;
import org.chorem.action.ChoremBaseAction;
//import org.chorem.bonzoms.EmployeeFull;
import org.chorem.entities.Employee;
import org.chorem.entities.Project;
import org.chorem.entities.ProjectOrder;
import org.chorem.entities.Quotation;
import org.chorem.entities.QuotationImpl;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.search.PagedResult;
import org.nuiton.wikitty.search.Search;

import com.opensymphony.xwork2.ActionContext;
import org.nuiton.wikitty.entities.Wikitty;

import static org.nuiton.i18n.I18n.n_;

/**
 * Quotation management class
 * 
 * @author vbriand
 */
public class QuotationAction extends ChoremBaseAction {
    
    private static final long serialVersionUID = -8773692389143447193L;

    private static final Log log = LogFactory.getLog(ChoremBaseAction.class);
    
    static public QuotationAction getAction() {
        return (QuotationAction)ActionContext.getContext().get(CONTEXT_ACTION_KEY);
    }
    
    protected String projectId;
    protected String projectName;
    protected String type;
    protected String reference;
    protected String description;
    protected String amount;
    protected String vat;
    protected String beginDate;
    protected String endDate;
    protected String postedDate;
    protected String conversionHope;
    protected String quotationId;
    protected String supplierId;
    protected String customerId;
//    protected EmployeeFull customer;
//    protected EmployeeFull supplier;

    /**
     * Adds a new quotation
     * 
     * @return INPUT if the mandatory fields haven't all been filled in, 
     * SUCCESS if the quotation has been added, 
     * ERROR if an error occurred
     */
    public String add() {
        String result = INPUT;
        ChoremProxy proxy = getChoremProxy();
        
        try {
            if (projectId == null) {
                result = ERROR;
            } else {
                //If projectId isn't a valid UUID, an exception is thrown
                UUID.fromString(projectId);
                
                Project project = proxy.restore(Project.class, projectId);
                
                if (project == null) { //If the projectId doesn't exist
                    result = ERROR;
                } else {
                    projectName = project.getName();
                    if (reference != null) {
                        //If the quotation has been created successfully
                        if (addOrModifyQuotation(null)) {
                            result = SUCCESS;
                        } else {
                            result = ERROR;
                        }
                    }
                }
            }
        } catch (IllegalArgumentException e) {
            result = ERROR;
        }
        return result;        
    }
    
    /**
     * Retrieves the information of the quotation referenced by the 
     * quotationId
     * 
     * @return SUCCESS if the information have been retrieved, 
     * ERROR either if the UUID is invalid or the quotationId doesn't exist
     */
    public String quotationDetails() {
        String result = SUCCESS;
        ChoremProxy proxy = getChoremProxy();
        
        try {
            if (quotationId == null) {
                result = ERROR;
            } else {
                //If quotationId isn't a valid UUID, an exception is thrown
                UUID.fromString(quotationId);
                
                Quotation quotation = proxy.restore(Quotation.class, quotationId);
                log.debug(String.format("Restored quotation: ", quotation));
                
                if (quotation != null) { //If the quotationId exists
                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

                    setType(quotation.getType());
                    setReference(quotation.getReference());
                    setDescription(quotation.getDescription());
                    setAmount(String.valueOf(quotation.getAmount()));
                    setVat(String.valueOf(quotation.getVAT()));
                    if (quotation.getBeginDate() != null) {
                        setBeginDate(formatter.format(quotation.getBeginDate()));
                    }
                    if (quotation.getEndDate() != null) {
                        setEndDate(formatter.format(quotation.getEndDate()));
                    }
                    if (quotation.getPostedDate() != null) {
                        setPostedDate(formatter.format(quotation.getPostedDate()));
                    }
                    setConversionHope(String.valueOf(quotation.getConversionHope()));
                    setProjectId(quotation.getProject());
//                    setSupplier(EmployeeFull.initEmployee(quotation.getSupplier(),
//                            proxy));
//                    setCustomer(EmployeeFull.initEmployee(quotation.getCustomer(),
//                            proxy));
                } else {
                    result = ERROR;
                }
            }
        } catch (IllegalArgumentException e) {
            //quotationId is invalid
            result = ERROR;
        }
        return result;
    }
    
    /**
     * Modifies the quotation's information
     * 
     * @return SUCCESS if the quotation has been successfully modified,
     * INPUT if the date format is incorrect,
     * ERROR if the id is invalid
     */
    public String modify() {
        String result = SUCCESS;
        Quotation quotation;
        ChoremProxy proxy = getChoremProxy();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        
        quotation = proxy.restore(Quotation.class, quotationId);
        if (quotation != null) { //If the id exists
            if (addOrModifyQuotation(quotation)) {
                result = SUCCESS;
            } else {
                result = INPUT;
            }
        } else {
            result = ERROR;
        }
        return result;
    }
    
    /**
     * Returns the project order linked with the quotation (if it exists)
     * 
     * @return the project order if it exists, null otherwise
     */
    public ProjectOrder getProjectOrder() {
        ChoremProxy proxy = getChoremProxy();
        Search search = Search.query();
        Criteria criteria = search.eq(ProjectOrder.FQ_FIELD_PROJECTORDER_QUOTATION, 
                quotationId).criteria();
        ProjectOrder projectOrder = proxy.findByCriteria(ProjectOrder.class,
                criteria);

        return projectOrder;
    }
    
//    public List<EmployeeFull> getAllEmployees() {
//        ChoremProxy proxy = getChoremProxy();
//        Search search = Search.query();
//        Criteria criteria = search.exteq(Employee.EXT_EMPLOYEE).criteria();
//        PagedResult<Employee> result = proxy.findAllByCriteria(Employee.class,
//                criteria);
//        List<Employee> employees = result.getAll();
//        List<EmployeeFull> employeesFullList = EmployeeFull.
//        initEmployeeFullList(employees, proxy);
//        return employeesFullList;
//    }
    
    /**
     * Stores the new quotation through the proxy 
     *
     * @param newQuotation if null new quotation is created, else argument is used
     * @return true if the quotation has been stored properly, 
     * false if a problem occurred
     */
    protected boolean addOrModifyQuotation(Quotation newQuotation) {
        boolean result = true;
        
        try {
            ChoremProxy proxy = getChoremProxy();
            if (newQuotation == null) {
                newQuotation = new QuotationImpl();
            }

            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            if (getType() != null) {
                newQuotation.setType(getType());
            }
            if (getReference() != null) {
                newQuotation.setReference(getReference());
            }
            if (getDescription() != null) {
                newQuotation.setDescription(getDescription());
            }
            if (getAmount() != null) {
                String val = getAmount();
                val = val.replace(',', '.');
                newQuotation.setAmount(Double.parseDouble(val));
            }

            if (getVat() != null) {
                String val = getVat();
                //Replaces the (possible) comma by a dot, so the string can be
                //parsed successfully as a double
                val = val.replace(',', '.');
                newQuotation.setVAT(Double.parseDouble(val));
            }
            if (getProjectId() != null) {
                newQuotation.setProject(getProjectId());
            }

            //Lenient mode disabled because results may be very odd
            formatter.setLenient(false);

            if (getBeginDate() != null) {
                try {
                    newQuotation.setBeginDate(formatter.parse(getBeginDate()));
                } catch (ParseException e) {
                    result = false;
                    addActionError(getText(n_("chorem.date.wrongFormat")));
                }
            }
            if (getEndDate() != null) {
                try {
                    newQuotation.setEndDate(formatter.parse(getEndDate()));
                } catch (ParseException e) {
                    result = false;
                    addActionError(getText(n_("chorem.date.wrongFormat")));
                }
            }
            if (getPostedDate() != null) {
                try {
                    newQuotation.setPostedDate(formatter.parse(getPostedDate()));
                } catch (ParseException e) {
                    result = false;
                    addActionError(getText(n_("chorem.date.wrongFormat")));
                }
            }
            if (getConversionHope() != null) {
                int val = Integer.parseInt(getConversionHope());
                newQuotation.setConversionHope(val);
            }


            if (supplierId != null) {
                Wikitty wsupplier = proxy.restore(supplierId);
                if (wsupplier != null) {
                    newQuotation.setSupplier(supplierId);
                } else { //If the id doesn't exist or is invalid
                    addActionError(getText(n_("chorem.billy.quotation.invalidSupplier")));
                    result = false;
                }
            }

            if (customerId != null) {
                Wikitty wcustomer = proxy.restore(customerId);
                if (wcustomer != null) {
                    newQuotation.setCustomer(customerId);
                } else { //If the id doesn't exist or is invalid
                    addActionError(getText(n_("chorem.billy.quotation.invalidCustomer")));
                    result = false;
                }
            }

            //If everything went smoothly
            if (result) {
                proxy.store(newQuotation);
            }
        } catch (Exception e) {
            result = false;
            addActionError(getText(n_("chorem.create.error")));
            log.error("An error occurred while creating a new quotation", e);
        }
        return result;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the reference
     */
    public String getReference() {
        return reference;
    }
    
    /**
     * @param reference the reference to set
     */
    public void setReference(String reference) {
        this.reference = reference;
    }
    
    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }
    
    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
    /**
     * @return the amount
     */
    public String getAmount() {
        return amount;
    }
    
    /**
     * @param amount the amount to set
     */
    public void setAmount(String amount) {
        this.amount = amount;
    }
    
    /**
     * @return the vat
     */
    public String getVat() {
        return vat;
    }
    
    /**
     * @param vat the vat to set
     */
    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
    
    /**
     * @return the postedDate
     */
    public String getPostedDate() {
        return postedDate;
    }
    
    /**
     * @param postedDate the postedDate to set
     */
    public void setPostedDate(String postedDate) {
        this.postedDate = postedDate;
    }

    public String getConversionHope() {
        return conversionHope;
    }

    public void setConversionHope(String conversionHope) {
        this.conversionHope = conversionHope;
    }

    /**
     * @return the projectId
     */
    public String getProjectId() {
        return projectId;
    }

    /**
     * @param projectId the projectId to set
     */
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    /**
     * @return the projectName
     */
    public String getProjectName() {
        return projectName;
    }

    /**
     * @param projectName the projectName to set
     */
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    /**
     * @return the quotationId
     */
    public String getQuotationId() {
        return quotationId;
    }

    /**
     * @param quotationId the quotationId to set
     */
    public void setQuotationId(String quotationId) {
        this.quotationId = quotationId;
    }

    /**
     * @return the supplierId
     */
    public String getSupplierId() {
        return supplierId;
    }

    /**
     * @param supplierId the supplierId to set
     */
    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    /**
     * @return the customerId
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * @param customerId the customerId to set
     */
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
    
    /**
     * Returns the current date
     * 
     * @return the current date in a string
     */
    public String getDayDate() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String dayDate;
        
        dayDate = formatter.format(calendar.getTime());
        return dayDate;
    }

    /**
     * @return the customer
     */
//    public EmployeeFull getCustomer() {
//        return customer;
//    }
//
//    /**
//     * @param customer the customer to set
//     */
//    public void setCustomer(EmployeeFull customer) {
//        this.customer = customer;
//    }
//
//    /**
//     * @return the supplier
//     */
//    public EmployeeFull getSupplier() {
//        return supplier;
//    }
//
//    /**
//     * @param supplier the supplier to set
//     */
//    public void setSupplier(EmployeeFull supplier) {
//        this.supplier = supplier;
//    }
}

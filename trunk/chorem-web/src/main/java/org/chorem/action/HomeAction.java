package org.chorem.action;

import com.opensymphony.xwork2.ActionContext;
import java.text.ParseException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import static org.nuiton.i18n.I18n.n_;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.entities.Company;
import org.chorem.entities.Invoice;
import org.chorem.entities.Invoiceable;
import org.chorem.entities.InvoiceableImpl;
import org.chorem.entities.InvoiceableUtil;
import org.chorem.entities.Person;
import org.chorem.entities.ProjectOrder;
import org.chorem.entities.Task;
import org.chorem.entities.Touch;
import org.chorem.entities.Worker;
import org.nuiton.wikitty.WikittyClient;
import org.nuiton.wikitty.WikittyProxy;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyUser;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.search.FacetTopic;
import org.nuiton.wikitty.search.PagedResult;
import org.nuiton.wikitty.search.Search;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class HomeAction extends ChoremBaseAction {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(HomeAction.class);

    static public HomeAction getAction() {
        return (HomeAction)ActionContext.getContext().get(CONTEXT_ACTION_KEY);
    }

    @Override
    public String execute() {
        try {
            WikittyClient proxy = getChoremProxy();
            load(proxy);
        } catch (Exception eee) {
            addActionError(getText(n_("chorem.error.internal")));
            log.error("Can't load data for home page", eee);
        }
        return SUCCESS;
    }

    /** nombre d'objet company */
    protected int nbCompany = 0;
    /** nombre d'objet person */
    protected int nbPerson = 0;
    /** nombre de contact pour les 7 derniers jours jusqu'à maintenant */
    protected int nbTouchForLast7Days = 0;
    protected int nbTouchForLast7DaysForMe = 0;
    /** nombre de contact pour les 7 prochains jours depuis maintenant */
    protected int nbTouchForNext7Days = 0;
    protected int nbTouchForNext7DaysForMe = 0;

    /** nombre de projet en cours */
    protected List<FacetTopic> nbProjectOrderStatus = Collections.EMPTY_LIST;
    /** nombre de tache a faire ou finir */
    protected int nbTaskOpen = 0;
    protected int nbTaskOpenForMe = 0;
    /** nombre de facture a envoyer */
    protected int nbInvoiceToSend = 0;
    /** montant des factures a envoyer*/
    protected double invoiceAmountToSend = 0;

    /** cash flow pour les 3 prochains moins */
    protected double[] cashFlow = new double[3];

    /** nombre de facture envoyer cette annee jusqu'a maintenant */
    protected int nbInvoiceSendThisYear = 0;
    /** nombre de facture envoyer l'annee derniere jusqu'a maintenant (meme periode)*/
    protected int nbInvoiceSendLastYear = 0;
    /** nombre de facture impayer */
    protected int nbInvoiceNotPaid = 0;
    /** nombre de facture impayer et en retard */
    protected int nbInvoiceNotPaidAndLate = 0;
    /** CA cette annee jusqu'a maintenant */
    protected double salesTurnoverThisYear = 0;
    /** CA l'annee derniere jusqu'a maintenant (meme periode)*/
    protected double salesTurnoverLastYear = 0;

    public void load(WikittyClient proxy) throws ParseException {

        String now = WikittyUtil.formatDate(new Date());

        WikittyUser user = proxy.getUser();
        String userId = "nobody";
        if (user != null) {
            userId = user.getWikittyId();
        }

        Criteria company = Search.query().exteq(Company.EXT_COMPANY).criteria().setEndIndex(0);
        Criteria person = Search.query().exteq(Person.EXT_PERSON).criteria().setEndIndex(0);

        Criteria touchLast7days = Search.query()
                .exteq(Touch.EXT_TOUCH)
                .and()
                .le(Touch.FQ_FIELD_INTERVAL_BEGINDATE, now)
                .criteria().addFacetField(Touch.FQ_FIELD_TOUCH_PARTICIPANT).setEndIndex(0);

        Criteria touchNext7days = Search.query()
                .exteq(Touch.EXT_TOUCH)
                .and()
                .gt(Touch.FQ_FIELD_INTERVAL_BEGINDATE, now)
                .criteria().addFacetField(Touch.FQ_FIELD_TOUCH_PARTICIPANT).setEndIndex(0);

        Criteria projectOrder = Search.query().exteq(ProjectOrder.EXT_PROJECTORDER)
                .criteria().addFacetField(ProjectOrder.FQ_FIELD_PROJECTORDER_STATUS).setEndIndex(0);

        Criteria taskOpen = Search.query().exteq(Task.EXT_TASK)
                .neq(Task.FQ_FIELD_TASK_STATUS, "closed")
                .criteria().setEndIndex(0);


        Search taskOpenForMeSearch = Search.query().exteq(Worker.EXT_WORKER)
                .eq(Worker.FQ_FIELD_WORKER_PERSON, userId);
        taskOpenForMeSearch.associated(Worker.FQ_FIELD_WORKER_TASK)
                .neq(Task.FQ_FIELD_TASK_STATUS, "closed");
        Criteria taskOpenForMe = taskOpenForMeSearch.criteria().setEndIndex(0);


        Criteria invoiceToSend = Search.query().exteq(Invoiceable.EXT_INVOICEABLE)
                .extneq(Invoice.EXT_INVOICE)
                .criteria().setEndIndex(0);
        
        PagedResult<Wikitty>[] results = proxy.findAllByCriteria(
                company,
                person,
                touchLast7days,
                touchNext7days,
                projectOrder,
                taskOpen,
                taskOpenForMe,
                invoiceToSend
                );

        nbCompany = results[0].getNumFound();
        nbPerson = results[1].getNumFound();

        nbTouchForLast7Days = results[2].getNumFound();
        nbTouchForLast7DaysForMe =
                results[2].getTopicCount(Touch.FQ_FIELD_TOUCH_PARTICIPANT, userId);
        nbTouchForNext7Days = results[3].getNumFound();
        nbTouchForNext7DaysForMe =
                results[3].getTopicCount(Touch.FQ_FIELD_TOUCH_PARTICIPANT, userId);

        nbProjectOrderStatus = results[4].getTopic(ProjectOrder.FQ_FIELD_PROJECTORDER_STATUS);

        nbTaskOpen = results[5].getNumFound();
        nbTaskOpenForMe = results[6].getNumFound();

        // On evalue les conditions pour savoir
        // lesquels sont reellement a facturer. idem pour les values
        List<InvoiceableImpl> invoiceable = 
                results[7].cast(proxy, InvoiceableImpl.class).getAll();
        
        int invoiceableCount = 0;
        double invoiceableAmount = 0;
        for(InvoiceableImpl i : invoiceable) {
            boolean cond = InvoiceableUtil.evalCondition(proxy, i);
            if (cond) {
                invoiceableCount ++;
                invoiceableAmount += InvoiceableUtil.evalValue(proxy, i);
            }
        }
        
        nbInvoiceToSend = invoiceableCount;
        invoiceAmountToSend = invoiceableAmount;
        
        
//
//        cashFlow = new double[]{
//                    results[11].getNumFound(),
//                    results[12].getNumFound(),
//                    results[13].getNumFound()
//                };
//
//        nbInvoiceSendThisYear = results[14].getNumFound();
//        nbInvoiceSendLastYear = results[15].getNumFound();
//        nbInvoiceNotPaid = results[16].getNumFound();
//        nbInvoiceNotPaidAndLate = results[17].getNumFound();
//        salesTurnoverThisYear = results[18].getNumFound();
//        salesTurnoverLastYear = results[19].getNumFound();

    }

    public double[] getCashFlow() {
        return cashFlow;
    }

    public int getNbInvoiceNotPaid() {
        return nbInvoiceNotPaid;
    }

    public int getNbInvoiceNotPaidAndLate() {
        return nbInvoiceNotPaidAndLate;
    }

    public int getNbInvoiceSendLastYear() {
        return nbInvoiceSendLastYear;
    }

    public int getNbInvoiceSendThisYear() {
        return nbInvoiceSendThisYear;
    }

    public int getNbInvoiceToSend() {
        return nbInvoiceToSend;
    }

    public int getNbCompany() {
        return nbCompany;
    }

    public int getNbPerson() {
        return nbPerson;
    }

    public List<FacetTopic> getNbProjectOrderStatus() {
        return nbProjectOrderStatus;
    }

    public int getNbTaskOpen() {
        return nbTaskOpen;
    }

    public int getNbTaskOpenForMe() {
        return nbTaskOpenForMe;
    }

    public int getNbTouchForNext7Days() {
        return nbTouchForNext7Days;
    }

    public int getNbTouchForNext7DaysForMe() {
        return nbTouchForNext7DaysForMe;
    }

    public int getNbTouchForLast7Days() {
        return nbTouchForLast7Days;
    }

    public int getNbTouchForLast7DaysForMe() {
        return nbTouchForLast7DaysForMe;
    }

    public double getInvoiceAmountToSend() {
        return invoiceAmountToSend;
    }

    public double getSalesTurnoverLastYear() {
        return salesTurnoverLastYear;
    }

    public double getSalesTurnoverThisYear() {
        return salesTurnoverThisYear;
    }

}

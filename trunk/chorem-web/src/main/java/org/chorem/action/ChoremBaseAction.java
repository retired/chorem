package org.chorem.action;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.nuiton.web.struts2.BaseAction;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
import org.chorem.ChoremConfig;
import org.chorem.ChoremProxy;
import org.chorem.ChoremSession;

/**
 * Base class which must be extended by every action
 * Overrides the Struts2 methods to render text in order to explicitly show 
 * the missing i18n translations
 */
public class ChoremBaseAction extends BaseAction implements SessionAware {

    private static final long serialVersionUID = 6360393466153765988L;

    private static final Log log = LogFactory.getLog(ChoremBaseAction.class);

    final static protected String CONTEXT_ACTION_KEY = "action";
    
    public static final String UNTRANSLATED_MARKER = "???";
    
    protected static final String MISSING_PARAM = "param";
    
    protected Map<String, Object> session;

    /** Configuration, default null for lazy loading */
    protected transient ChoremConfig config;

    public ChoremConfig getConfig() {
        if (config == null) {
            config = new ChoremConfig();
        }
        return config;
    }

    public ChoremSession getChoremSession() {
        ChoremSession result = ChoremSession.getChoremSession(session);
        return result;
    }

    public ChoremProxy getChoremProxy() {
        ChoremProxy result = getChoremSession().getProxy();
        return result;
    }

    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }

}

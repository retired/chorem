<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.List" %>
<%@page import="org.chorem.entities.Company" %>
<%@page import="org.chorem.bonzoms.action.CompanyAction" %>
<%@page import="org.chorem.gepeto.action.ProjectOrderAction" %>
<%@taglib prefix="s" uri="/struts-tags" %>

<html xmlns:s="http://java.sun.com/xml/ns/j2ee/web-jsptaglibrary_2_0.xsd"
      xmlns:jsp="http://java.sun.com/JSP/Page">
	<head>
		<title><s:text name="chorem.gepeto.projectOrder.add" /></title>
		<s:head />
	</head>
	<body>
	    <h2>
	        <s:text name="chorem.gepeto.projectOrder.addToQuotation">
	            <s:param><%= ProjectOrderAction.getAction().getQuotationReference() %></s:param>
	        </s:text>
	    </h2>
	    <s:actionerror />
	    <s:url action="addProjectOrder" var="addProjectOrder">
	        <s:param name="quotationId"><%= ProjectOrderAction.getAction().getQuotationId() %></s:param>
	    </s:url>
	    <form action="${addProjectOrder}" method="post">
	        <fieldset>
	            <legend><s:text name="chorem.gepeto.projectOrder" /></legend>
	            <s:textfield key="chorem.gepeto.projectOrder.type" name="type" labelSeparator=": " />
	            <br />
	            <br />
	            <s:textarea key="chorem.gepeto.projectOrder.description" name="description" cols="50" rows="10" labelposition="top" />
	            <br />
	            <br />
	            <s:textfield key="chorem.gepeto.projectOrder.status" name="status" labelSeparator=": " />
	            <br />
	            <s:textfield key="chorem.gepeto.projectOrder.beginDate" name="beginDate" labelSeparator=": " />
	            <br />
	            <s:textfield key="chorem.gepeto.projectOrder.estimatedEndDate" name="estimatedEndDate" labelSeparator=": " />
	            <br />
	            <s:select label="%{getText('chorem.gepeto.projectOrder.company')}" list="allCompanies" name="companyId" listKey="wikittyId" listValue="name" />
	            <br />
	            <s:submit key="chorem.gepeto.projectOrder.add" name="submit" />
	        </fieldset>
	    </form>
	    <s:url namespace="/billy" action="quotationDetails" var="quotationDetails">
	        <s:param name="quotationId"><%= ProjectOrderAction.getAction().getQuotationId() %></s:param>
	    </s:url>
	    <a href="${quotationDetails}"><s:text name="chorem.gepeto.projectOrder.backToQuotationDetails" /></a>
	</body>
</html>

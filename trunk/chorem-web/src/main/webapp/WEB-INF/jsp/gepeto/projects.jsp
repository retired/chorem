<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj"  uri="/struts-jquery-tags"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<html xmlns:s="http://java.sun.com/xml/ns/j2ee/web-jsptaglibrary_2_0.xsd"
      xmlns:jsp="http://java.sun.com/JSP/Page">
    <head>
        <title><s:text name="chorem.gepeto.projects" /></title>
    </head>
    <body>

        <s:url id="selectEmployeeUrl" namespace="/ajax" action="selectWikittyComponent" escapeAmp="false">
            <s:param name="wikittyExtension">Employee</s:param>
            <s:param name="sortField">Employee.person</s:param>
        </s:url>

        <s:url id="selectCompanyUrl" namespace="/ajax" action="selectWikittyComponent" escapeAmp="false">
            <s:param name="wikittyExtension">Company</s:param>
            <s:param name="sortField">Company.name</s:param>
        </s:url>

        <s:url id="listProjectUrl" namespace="/ajax" action="EntitySearchJson" escapeAmp="false">
            <s:param name="wikittyExtension">Project</s:param>
        </s:url>

        <s:url id="editProjectUrl" namespace="/ajax" action="EntitySearchJson" escapeAmp="false">
            <s:param name="wikittyExtension">Project</s:param>
        </s:url>

        <s:url id="listQuotationUrl" namespace="/ajax" action="EntitySearchJson" escapeAmp="false">
            <s:param name="wikittyExtension">Quotation</s:param>
            <s:param name="wikittyLinkToLoad">Quotation.customer,Employee.person,Employee.company</s:param>
        </s:url>

        <s:url id="editQuotationUrl" namespace="/ajax" action="EntitySearchJson" escapeAmp="false">
            <s:param name="wikittyExtension">Quotation</s:param>
            <s:param name="wikittyLinkFieldForRowId">Quotation.project</s:param>
        </s:url>

        <s:url id="listProjectOrderUrl" namespace="/ajax" action="EntitySearchJson" escapeAmp="false">
            <s:param name="wikittyExtension">ProjectOrder</s:param>
            <s:param name="wikittyLinkToLoad">ProjectOrder.Quotation,Quotation.project,Quotation.customer,Employee.person,Employee.company</s:param>
        </s:url>

        <s:url id="editProjectOrderUrl" namespace="/ajax" action="EntitySearchJson" escapeAmp="false">
            <s:param name="wikittyExtension">ProjectOrder</s:param>
        </s:url>

        <script type="text/javascript">
            // event de la grille des companies qui selection seulement les employers
            // la companie selectionnee
            $.subscribe('rowselect', function(event, data) {
                // alert('Selected Row : ' + event.originalEvent.id + ' data: ' + event.originalEvent.status);
                var gridUrl = "<s:property value="listProjectOrderUrl" escapeHtml="false"/>&fulltextSearch=" + event.originalEvent.id;
                jQuery("#gridProjectOrder").jqGrid('setGridParam',{url:gridUrl,page:1})
                .trigger('reloadGrid');
            });
        </script>

        <h2><s:text name="chorem.bonzoms.projects" /></h2>

        <s:form>
            <s:textfield id="fulltextSearchProject" name="fulltextSearchProject"
                         onkeydown="doSearch('#gridProject', '#fulltextSearchProject', '%{listProjectUrl}')"></s:textfield>
        </s:form>

        <sjg:grid
            id="gridProject"
            dataType="json"
            href="%{listProjectUrl}"
            gridModel="gridModel"
            rowList="10,20,50,100"
            rowNum="20"
            rownumbers="true"
            autowidth="true"
            sortname="Project.name"
            sortorder="asc"
            pager="true"
            viewrecords="true"

            multiselect="false"

            editurl="%{editProjectUrl}"
            editinline="false"

            onSelectRowTopics="rowselect"

            navigator="true"
            navigatorAdd="true"
            navigatorEdit="true"
            navigatorDelete="true"
            navigatorView="true"
            navigatorSearch="true"
            navigatorAddOptions="{height:280,reloadAfterSubmit:true}"
            navigatorEditOptions="{height:280,reloadAfterSubmit:true}"
            navigatorDeleteOptions="{height:280,reloadAfterSubmit:true}"
            navigatorSearchOptions="{multipleGroup:true,showQuery:true,multipleSearch:true,sopt:['eq','ne','cn','nc','bw','bn','ew','en','lt','le','gt','ge','nu','nn','in','ni']}"
            >

            <sjg:gridColumn name="id" hidden="true" key="true" title="id"/>
            <sjg:gridColumn name="Project.name" index="Project.name" title="Nom"
                            editable="true" sortable="true"/>
            <sjg:gridColumn name="Project.description" index="Project.description"
                            title="Description" editable="true" sortable="true"
                            edittype="textarea"/>


            <sjg:grid
                id="gridQuotation"
                dataType="json"
                href="%{listQuotationUrl}"
                gridModel="gridModel"
                rowList="10,20,50,100"
                rowNum="20"
                rownumbers="true"
                autowidth="true"
                sortname="Quotation.reference"
                sortorder="asc"
                pager="true"
                viewrecords="true"

                multiselect="false"

                editurl="%{editQuotationUrl}"
                editinline="false"

                navigator="true"
                navigatorAdd="true"
                navigatorEdit="true"
                navigatorDelete="true"
                navigatorView="true"
                navigatorSearch="true"
                navigatorAddOptions="{height:280,reloadAfterSubmit:true}"
                navigatorEditOptions="{height:280,reloadAfterSubmit:true}"
                navigatorDeleteOptions="{height:280,reloadAfterSubmit:true}"
                navigatorSearchOptions="{multipleGroup:true,showQuery:true,multipleSearch:true,sopt:['eq','ne','cn','nc','bw','bn','ew','en','lt','le','gt','ge','nu','nn','in','ni']}"
                >

                <sjg:gridColumn name="id" hidden="true" key="true" title="id"/>

                <sjg:gridColumn name="Quotation.customer" index="Quotation.customer"
                                title="Acheteur" editable="true" edittype="select"
                                hidden="true" editrules="{edithidden:true,required:true}"
                                editoptions="{dataUrl:'%{selectEmployeeUrl}'}"/>
                <sjg:gridColumn name="Quotation.supplier" index="Quotation.supplier"
                                title="Vendeur" editable="true" edittype="select"
                                hidden="true" editrules="{edithidden:true}"
                                editoptions="{dataUrl:'%{selectEmployeeUrl}'}"/>


                <sjg:gridColumn name="Company.name" index="Company.name"
                                title="Société" editable="false" sortable="false"/>
                <sjg:gridColumn name="Person.name" index="Person.name"
                                title="Acheteur" editable="false" sortable="false"/>

                <sjg:gridColumn name="Quotation.reference" index="Quotation.reference"
                                title="Référence" editable="true" sortable="true"
                                editrules="{required:true}"/>
                <sjg:gridColumn name="Quotation.type" index="Quotation.type"
                                title="Type" editable="true" sortable="true"
                                edittype="select" editoptions="{value:'Development:Development;TMA:TMA;Regie:Regie;System:Sytem;Conseil:Conseil;Audit:Audit;Support:Support;Other:Other'}"/>
                <sjg:gridColumn name="Quotation.description" index="Quotation.description"
                                title="Description" editable="true" sortable="true"
                                edittype="textarea"/>
                <sjg:gridColumn name="Quotation.amount" index="Quotation.amount"
                                title="Montant" editable="true" sortable="true"
                                editrules="{number:true}"/>
                <sjg:gridColumn name="Quotation.VAT" index="Quotation.VAT"
                                title="TVA" editable="true" sortable="true"
                                editrules="{number:true}"/>
                <sjg:gridColumn name="Interval.beginDate" index="Interval.beginDate"
                                title="Date d'envoi" editable="true" sortable="true"
                                editrules="{date:true}"/>
                <sjg:gridColumn name="Interval.endDate" index="Interval.endDate"
                                title="Date de validité" editable="true" sortable="true"
                                editrules="{date:true}"/>
                <sjg:gridColumn name="Quotation.conversionHope" index="Quotation.conversionHope"
                                title="Convertibilité" editable="true" sortable="true"
                                editrules="{integer:true,minValue:0,maxValue:100}"/>
            </sjg:grid>
        </sjg:grid>

        <h2><s:text name="chorem.bonzoms.employees" /></h2>

        <s:form>
            <s:textfield id="fulltextSearchProjectOrder" name="fulltextSearchProjectOrder"
                         onkeydown="doSearch('#gridProjectOrder', '#fulltextSearchProjectOrder', '%{listProjectOrderUrl}')"></s:textfield>
        </s:form>

        <sjg:grid
            id="gridProjectOrder"
            dataType="json"
            href="%{listProjectOrderUrl}"
            gridModel="gridModel"
            rowList="10,20,50,100"
            rowNum="20"
            rownumbers="true"
            autowidth="true"
            sortname="ProjectOrder.status"
            sortorder="asc"
            pager="true"
            viewrecords="true"

            multiselect="false"

            editurl="%{editProjectOrderUrl}"
            editinline="false"

            navigator="true"
            navigatorAdd="true"
            navigatorEdit="true"
            navigatorDelete="true"
            navigatorView="true"
            navigatorSearch="true"
            navigatorAddOptions="{height:280,reloadAfterSubmit:true}"
            navigatorEditOptions="{height:280,reloadAfterSubmit:true}"
            navigatorDeleteOptions="{height:280,reloadAfterSubmit:true}"
            navigatorSearchOptions="{multipleGroup:true,showQuery:true,multipleSearch:true,sopt:['eq','ne','cn','nc','bw','bn','ew','en','lt','le','gt','ge','nu','nn','in','ni']}"
            >

            <sjg:gridColumn name="id" hidden="true" key="true" title="id"/>
            
            <sjg:gridColumn name="ProjectOrder.company" index="ProjectOrder.company"
                            title="Société" editable="true" edittype="select"
                            hidden="true" editrules="{edithidden:true}"
                            editoptions="{dataUrl:'%{selectCompanyUrl}'}"/>

            <sjg:gridColumn name="Company.name" index="Company.name"
                            title="Société" sortable="false" editable="false"/>

            <sjg:gridColumn name="ProjectOrder.status" index="ProjectOrder.status"
                            title="Type" edittype="select" editable="true"
                            editoptions="{value:'NotStarted;NotStarted;WorkInProgress:WorkInProgress;Finished:Finished;Other:Other'}"/>
            <sjg:gridColumn name="ProjectOrder.type" index="ProjectOrder.type"
                            title="Type" edittype="select" editable="true"
                            editoptions="{value:'Development;Development;Regie:Regie;System:Sytem;Conseil:Conseil;Audit:Audit;Support:Support;Other:Other'}"/>
            <sjg:gridColumn name="ProjectOrder.description" index="ProjectOrder.description"
                            title="Description" edittype="textarea" editable="true"/>
            <sjg:gridColumn name="Interval.beginDate" index="Interval.beginDate"
                            title="Date d'envoi" editable="true" sortable="true"
                            editrules="{date:true}"/>
            <sjg:gridColumn name="Interval.endDate" index="Interval.endDate"
                            title="Date de validité" editable="true" sortable="true"
                            editrules="{date:true}"/>
            <sjg:gridColumn name="Quotation.conversionHope" index="Quotation.conversionHope"
                            title="Convertibilité" editable="true" sortable="true"
                            editrules="{integer:true,minValue:0,maxValue:100}"/>

            <%-- TODO poussin 20111123 put task grid here --%>
        </sjg:grid>

    </body>
</html>

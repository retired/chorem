<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="java.util.List" %>
<%@page import="org.chorem.entities.Task" %>
<%@page import="org.chorem.gepeto.action.ProjectOrderAction" %>
<%@page import="org.chorem.JspUtils" %>
<%@taglib prefix="s" uri="/struts-tags" %>

<html xmlns:s="http://java.sun.com/xml/ns/j2ee/web-jsptaglibrary_2_0.xsd"
      xmlns:jsp="http://java.sun.com/JSP/Page">
	<head>
		<title>
		    <s:text name="chorem.gepeto.projectOrderDetails.title" />
		</title>
		<s:head />
	</head>
	<body>
	    <p>
	        <s:url namespace="/bonzoms" action="companyDetails" var="companyDetails">
    	        <s:param name="companyId"><%= ProjectOrderAction.getAction().getCompany().getWikittyId() %></s:param>
    	    </s:url>
            <s:text name="chorem.gepeto.projectOrderDetails.companyName">
                <s:param><a href="${companyDetails}"><%= ProjectOrderAction.getAction().getCompany().getName() %></a></s:param>
            </s:text>
            <br />
            <s:text name="chorem.gepeto.projectOrderDetails.quotation">
                <s:param>
                    <s:url namespace="/billy" action="quotationDetails" var="quotationDetails">
                        <s:param name="quotationId">
                            <%= ProjectOrderAction.getAction().getQuotationId() %>
                        </s:param>
                    </s:url>
                    <a href="${quotationDetails}"><%= ProjectOrderAction.getAction().getQuotationReference() %></a>
                </s:param>
            </s:text>
	        <s:url action="modifyProjectOrder" var="modifyProjectOrder">
	            <s:param name="projectOrderId"><%= ProjectOrderAction.getAction().getProjectOrderId() %></s:param>
	        </s:url>
	        <form action="${modifyProjectOrder}" method="post">
	            <p>
	                <s:textfield name="beginDate" key="chorem.gepeto.projectOrderDetails.beginDate" labelSeparator=": " size="7" />
	                <br />
	                <s:textfield name="estimatedEndDate" key="chorem.gepeto.projectOrderDetails.endDate" labelSeparator=": " size="7" />
	                <br />
	                <s:textfield name="type" key="chorem.gepeto.projectOrderDetails.type" labelSeparator=": " size="4" />
	                <br />
	                <s:submit key="chorem.misc.modify" name="modify" />
	            </p>
	        </form>
            <s:url namespace="/gepeto" action="addTaskInput" var="addTask">
                <s:param name="projectOrderId"><%= ProjectOrderAction.getAction().getProjectOrderId() %></s:param>
            </s:url>
            <a href="${addTask}">
                <s:text name="chorem.gepeto.task.add" />
            </a>
	    </p>
	    <p>
            <s:text name="chorem.gepeto.projectOrderDetails.tasks" />
            <br />
            <br />
            <%
            List<Task> tasks = ProjectOrderAction.getAction().getTasks();
    
            for (Task task : tasks) {
            %>
                <s:text name="chorem.gepeto.projectOrderDetails.task.name">
                    <s:param><%= task.getName() %></s:param>
                </s:text>
                <br />
                <s:text name="chorem.gepeto.projectOrderDetails.task.status">
                    <s:param><%= task.getStatus() %></s:param>
                </s:text>
                <br />
                <s:text name="chorem.gepeto.projectOrderDetails.task.price">
                    <s:param><%= task.getPrice() %></s:param>
                </s:text>
                <br />
                <s:text name="chorem.gepeto.projectOrderDetails.task.beginDate">
                    <s:param><%= JspUtils.dateFormat(task.getBeginDate()) %></s:param>
                </s:text>
                <br />
                <s:text name="chorem.gepeto.projectOrderDetails.task.endDate">
                    <s:param><%= JspUtils.dateFormat(task.getEndDate()) %></s:param>
                </s:text>
                <br />
                <br />
            <% } %>
	    </p>
	</body>
</html>

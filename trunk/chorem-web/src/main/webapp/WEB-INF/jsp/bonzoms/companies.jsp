<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj"  uri="/struts-jquery-tags"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<html xmlns:s="http://java.sun.com/xml/ns/j2ee/web-jsptaglibrary_2_0.xsd"
      xmlns:jsp="http://java.sun.com/JSP/Page">
    <head>
        <title><s:text name="chorem.bonzoms.companies" /></title>
    </head>
    <body>

        <!--
         |
         | Definition des URL
         |
         +-->
        <s:url id="selectPersonUrl" namespace="/ajax" action="selectWikittyComponent" escapeAmp="false">
            <s:param name="wikittyExtension">Person</s:param>
            <s:param name="sortField">Person.lastName</s:param>
        </s:url>

        <s:url id="selectCompanyUrl" namespace="/ajax" action="selectWikittyComponent" escapeAmp="false">
            <s:param name="wikittyExtension">Company</s:param>
            <s:param name="sortField">Company.name</s:param>
        </s:url>

        <s:url id="listCompanyUrl" namespace="/ajax" action="EntitySearchJson" escapeAmp="false">
            <s:param name="wikittyExtension">Company</s:param>
        </s:url>

        <s:url id="listContactDetailsUrl" namespace="/ajax" action="EntitySearchJson" escapeAmp="false">
            <s:param name="wikittyExtension">ContactDetails</s:param>
        </s:url>

        <s:url id="listEmployeeUrl" namespace="/ajax" action="EntitySearchJson" escapeAmp="false">
            <s:param name="wikittyExtension">Employee</s:param>
            <s:param name="wikittyLinkToLoad">Employee.person,Employee.company</s:param>
        </s:url>

        <s:url id="editCompanyUrl" namespace="/ajax" action="EntitySearchJson" escapeAmp="false">
            <s:param name="wikittyExtension">Company</s:param>
        </s:url>

        <s:url id="editEmployeeUrl" namespace="/ajax" action="EntitySearchJson" escapeAmp="false">
            <s:param name="wikittyExtension">Employee</s:param>
        </s:url>

        <s:url id="editContactDetailsUrl" namespace="/ajax" action="EntitySearchJson" escapeAmp="false">
            <s:param name="wikittyExtension">ContactDetails</s:param>
            <s:param name="wikittyLinkFieldForRowId">ContactDetails.target</s:param>
        </s:url>

        <script type="text/javascript">
            // event de la grille des companies qui selection seulement les employers
            // la companie selectionnee
            $.subscribe('rowselect', function(event, data) {
                // alert('Selected Row : ' + event.originalEvent.id + ' data: ' + event.originalEvent.status);
                var gridUrl = "<s:property value="listEmployeeUrl" escapeHtml="false"/>&fulltextSearch=" + event.originalEvent.id;
                jQuery("#gridEmployee").jqGrid('setGridParam',{url:gridUrl,page:1})
                .trigger('reloadGrid');
            });
        </script>

        <h2><s:text name="chorem.bonzoms.companies" /></h2>

        <s:form>
            <s:textfield id="fulltextSearchCompany" name="fulltextSearchCompany"
                         onkeydown="doSearch('#gridCompany', '#fulltextSearchCompany', '%{listCompanyUrl}')"></s:textfield>
        </s:form>

        <sjg:grid
            id="gridCompany"
            dataType="json"
            href="%{listCompanyUrl}"
            gridModel="gridModel"
            rowList="10,20,50,100"
            rowNum="10"
            rownumbers="true"
            autowidth="true"
            sortname="Company.name"
            sortorder="asc"
            pager="true"
            viewrecords="true"

            multiselect="false"

            editurl="%{editCompanyUrl}"
            editinline="false"

            onSelectRowTopics="rowselect"

            navigator="true"
            navigatorAdd="true"
            navigatorEdit="true"
            navigatorDelete="true"
            navigatorView="true"
            navigatorSearch="true"
            navigatorAddOptions="{height:280,reloadAfterSubmit:true}"
            navigatorEditOptions="{height:280,reloadAfterSubmit:true}"
            navigatorDeleteOptions="{height:280,reloadAfterSubmit:true}"
            navigatorSearchOptions="{multipleGroup:true,showQuery:true,multipleSearch:true,sopt:['eq','ne','cn','nc','bw','bn','ew','en','lt','le','gt','ge','nu','nn','in','ni']}"
            >

            <sjg:gridColumn name="id" hidden="true" title="id" key="true"/>
            <sjg:gridColumn name="Company.name" index="Company.name" title="Société" editable="true" sortable="true"/>
            <sjg:gridColumn name="Company.type" index="Company.type" title="type" editable="true" sortable="true"/>

            <sjg:grid
                id="gridCompanyContact"
                dataType="json"
                href="%{listContactDetailsUrl}"
                gridModel="gridModel"
                rowList="10,20,50,100"
                rowNum="10"
                rownumbers="true"
                autowidth="true"
                sortname="ContactDetails.type"
                sortorder="asc"
                pager="true"
                viewrecords="true"

                multiselect="false"

                editurl="%{editContactDetailsUrl}"
                editinline="false"

                navigator="true"
                navigatorAdd="true"
                navigatorEdit="true"
                navigatorDelete="true"
                navigatorView="true"
                navigatorSearch="true"
                navigatorAddOptions="{height:280,reloadAfterSubmit:true}"
                navigatorEditOptions="{height:280,reloadAfterSubmit:true}"
                navigatorDeleteOptions="{height:280,reloadAfterSubmit:true}"
                navigatorSearchOptions="{multipleGroup:true,showQuery:true,multipleSearch:true,sopt:['eq','ne','cn','nc','bw','bn','ew','en','lt','le','gt','ge','nu','nn','in','ni']}"
                >

                <sjg:gridColumn name="id" hidden="true" key="true" title="id"/>
                <sjg:gridColumn name="ContactDetails.type" index="ContactDetails.type"
                                edittype="select"
                                editoptions="{value:'Phone:Phone;Address:Address;Email:Email;Jabber:Jabber;Other:Other'}"
                                title="Type" editable="true" sortable="true"/>
                <sjg:gridColumn name="ContactDetails.name" index="ContactDetails.name" title="Name" editable="true" sortable="true"/>
                <sjg:gridColumn name="ContactDetails.value" index="ContactDetails.value" title="Value" editable="true" sortable="true"/>
            </sjg:grid>

        </sjg:grid>

        <h2><s:text name="chorem.bonzoms.employees" /></h2>

        <s:form>
            <s:textfield id="fulltextSearchEmployee" name="fulltextSearchEmployee"
                         onkeydown="doSearch('#gridEmployee', '#fulltextSearchEmployee', '%{listEmployeeUrl}')"></s:textfield>
        </s:form>

        <sjg:grid
            id="gridEmployee"
            dataType="json"
            href="%{listEmployeeUrl}"
            gridModel="gridModel"
            rowList="10,20,50,100"
            rowNum="10"
            rownumbers="true"
            autowidth="true"
            sortname="Person.lastName"
            sortorder="asc"
            pager="true"
            viewrecords="true"

            multiselect="false"

            editurl="%{editEmployeeUrl}"
            editinline="false"

            navigator="true"
            navigatorAdd="true"
            navigatorEdit="true"
            navigatorDelete="true"
            navigatorView="true"
            navigatorSearch="true"
            navigatorAddOptions="{height:280,reloadAfterSubmit:true}"
            navigatorEditOptions="{height:280,reloadAfterSubmit:true}"
            navigatorDeleteOptions="{height:280,reloadAfterSubmit:true}"
            navigatorSearchOptions="{multipleGroup:true,showQuery:true,multipleSearch:true,sopt:['eq','ne','cn','nc','bw','bn','ew','en','lt','le','gt','ge','nu','nn','in','ni']}"
            >

            <sjg:gridColumn name="id" hidden="true" key="true" title="id"/>
            
            <sjg:gridColumn name="Employee.company" index="Employee.company"
                            title="Société" editable="true" edittype="select"
                            hidden="true" editrules="{edithidden:true}"
                            editoptions="{dataUrl:'%{selectCompanyUrl}'}"/>
            <sjg:gridColumn name="Employee.person" index="Employee.person"
                            title="Personne" editable="true" edittype="select"
                            hidden="true" editrules="{edithidden:true}"
                            editoptions="{dataUrl:'%{selectPersonUrl}'}"/>

            <sjg:gridColumn name="Company.name" index="Company.name" title="Société" sortable="false" editable="false"/>
            <sjg:gridColumn name="Person.lastName" index="Person.lastName" title="Nom" sortable="false" editable="false"/>
            <sjg:gridColumn name="Person.firstName" index="Person.firstName" title="Prénom" sortable="false" editable="false"/>
            <sjg:gridColumn name="Person.diploma" index="Person.diploma" title="Diplôme" sortable="false" editable="false"/>

            <sjg:gridColumn name="Employee.type" index="Employee.type" title="Type" edittype="select" editoptions="{value:'CDI:CDI;CDD:CDD;Stage:Stage;TNS:TNS'}" editable="true"/>
            <sjg:gridColumn name="Employee.paidLeave" index="Employee.paidLeave" title="Congé" editrules="{number:true}" editable="true"/>
            <sjg:gridColumn name="Employee.rtt" index="Employee.rtt" title="RTT" editrules="{number:true}" editable="true"/>
            <sjg:gridColumn name="Employee.salary" index="Employee.salary" title="Salaire" editrules="{number:true}" editable="true"/>
            <sjg:gridColumn name="Employee.workingTime" index="Employee.workingTime" title="Temps de travail" editrules="{number:true}" editable="true"/>
            <sjg:gridColumn name="Employee.description" index="Employee.description" title="Description" edittype="textarea" editable="true"/>

            <sjg:grid
                id="gridEmployeeContact"
                dataType="json"
                href="%{listContactDetailsUrl}"
                gridModel="gridModel"
                rowList="10,20,50,100"
                rowNum="10"
                rownumbers="true"
                autowidth="true"
                sortname="ContactDetails.type"
                sortorder="asc"
                pager="true"
                viewrecords="true"

                multiselect="false"

                editurl="%{editContactDetailsUrl}"
                editinline="false"

                navigator="true"
                navigatorAdd="true"
                navigatorEdit="true"
                navigatorDelete="true"
                navigatorView="true"
                navigatorSearch="true"
                navigatorAddOptions="{height:280,reloadAfterSubmit:true}"
                navigatorEditOptions="{height:280,reloadAfterSubmit:true}"
                navigatorDeleteOptions="{height:280,reloadAfterSubmit:true}"
                navigatorSearchOptions="{multipleGroup:true,showQuery:true,multipleSearch:true,sopt:['eq','ne','cn','nc','bw','bn','ew','en','lt','le','gt','ge','nu','nn','in','ni']}"
                >

                <sjg:gridColumn name="id" hidden="true" key="true" title="id"/>
                <sjg:gridColumn name="ContactDetails.type" index="ContactDetails.type"
                                edittype="select"
                                editoptions="{value:'Phone:Phone;Address:Address;Email:Email;Jabber:Jabber;Other:Other'}"
                                title="Type" editable="true" sortable="true"/>
                <sjg:gridColumn name="ContactDetails.name" index="ContactDetails.name" title="Name" editable="true" sortable="true"/>
                <sjg:gridColumn name="ContactDetails.value" index="ContactDetails.value" title="Value" editable="true" sortable="true"/>
            </sjg:grid>

        </sjg:grid>

    </body>
</html>

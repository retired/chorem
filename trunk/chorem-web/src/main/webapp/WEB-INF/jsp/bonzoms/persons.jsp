<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj"  uri="/struts-jquery-tags"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<html xmlns:s="http://java.sun.com/xml/ns/j2ee/web-jsptaglibrary_2_0.xsd"
      xmlns:jsp="http://java.sun.com/JSP/Page">
    <head>
        <title><s:text name="chorem.bonzoms.persons" /></title>
    </head>
    <body>

        <h2><s:text name="chorem.bonzoms.persons" /></h2>

        <s:url id="listPersonUrl"  namespace="/ajax" action="EntitySearchJson" escapeAmp="false">
            <s:param name="wikittyExtension">Person</s:param>
        </s:url>

        <s:url id="editPersonUrl"  namespace="/ajax" action="EntitySearchJson" escapeAmp="false">
            <s:param name="wikittyExtension">Person</s:param>
        </s:url>

        <s:url id="listContactDetailsUrl" namespace="/ajax" action="EntitySearchJson" escapeAmp="false">
            <s:param name="wikittyExtension">ContactDetails</s:param>
        </s:url>

         <s:url id="editContactDetailsUrl" namespace="/ajax" action="EntitySearchJson" escapeAmp="false">
            <s:param name="wikittyExtension">ContactDetails</s:param>
            <s:param name="wikittyLinkFieldForRowId">ContactDetails.target</s:param>
        </s:url>

        <s:form>
            <s:textfield id="fulltextSearch" name="fulltextSearch"
                         onkeydown="doSearch('#gridtable', '#fulltextSearch', '%{listPersonUrl}')"></s:textfield>
        </s:form>

        <sjg:grid
            id="gridtable"
            dataType="json"
            href="%{listPersonUrl}"
            gridModel="gridModel"
            rowList="10,20,50,100"
            rowNum="10"
            rownumbers="true"
            autowidth="true"
            sortname="Person.lastName"
            sortorder="asc"
            pager="true"
            viewrecords="true"

            multiselect="false"

            editurl="%{editPersonUrl}"
            editinline="false"

            navigator="true"
            navigatorAdd="true"
            navigatorEdit="true"
            navigatorDelete="true"
            navigatorView="true"
            navigatorSearch="true"
            navigatorAddOptions="{height:280,reloadAfterSubmit:true}"
            navigatorEditOptions="{height:280,reloadAfterSubmit:true}"
            navigatorDeleteOptions="{height:280,reloadAfterSubmit:true}"
            navigatorSearchOptions="{multipleSearch:true,sopt:['eq','ne','cn','nc','bw','bn','ew','en','lt','le','gt','ge','nu','nn','in','ni']}"
            >

            <sjg:gridColumn name="id" hidden="true" key="true" title="id"/>
            <sjg:gridColumn name="Person.lastName" index="Person.lastName" title="Nom" editable="true"/>
            <sjg:gridColumn name="Person.firstName" index="Person.firstName" title="Prénom" editable="true"/>
            <sjg:gridColumn name="Person.diploma" index="Person.diploma" title="Diplôme" editable="true"/>

            <sjg:grid
                id="gridCompanyContact"
                dataType="json"
                href="%{listContactDetailsUrl}"
                gridModel="gridModel"
                rowList="10,20,50,100"
                rowNum="10"
                rownumbers="true"
                autowidth="true"
                sortname="ContactDetails.type"
                sortorder="asc"
                pager="true"
                viewrecords="true"

                multiselect="false"

                editurl="%{editContactDetailsUrl}"
                editinline="false"

                navigator="true"
                navigatorAdd="true"
                navigatorEdit="true"
                navigatorDelete="true"
                navigatorView="true"
                navigatorSearch="true"
                navigatorAddOptions="{height:280,reloadAfterSubmit:true}"
                navigatorEditOptions="{height:280,reloadAfterSubmit:true}"
                navigatorDeleteOptions="{height:280,reloadAfterSubmit:true}"
                navigatorSearchOptions="{multipleGroup:true,showQuery:true,multipleSearch:true,sopt:['eq','ne','cn','nc','bw','bn','ew','en','lt','le','gt','ge','nu','nn','in','ni']}"
                >

                <sjg:gridColumn name="id" hidden="true" key="true" title="id"/>
                <sjg:gridColumn name="ContactDetails.type" index="ContactDetails.type"
                                edittype="select"
                                editoptions="{value:'Phone:Phone;Address:Address;Email:Email;Jabber:Jabber;Other:Other'}"
                                title="Type" editable="true" sortable="true"/>
                <sjg:gridColumn name="ContactDetails.name" index="ContactDetails.name" title="Name" editable="true" sortable="true"/>
                <sjg:gridColumn name="ContactDetails.value" index="ContactDetails.value"
                                edittype="textarea"
                                title="Value" editable="true" sortable="true"/>
            </sjg:grid>

        </sjg:grid>

    </body>
</html>

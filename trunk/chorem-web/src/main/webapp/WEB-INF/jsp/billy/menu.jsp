<%@taglib prefix="s" uri="/struts-tags" %>

<div id="menu">
    <ul>
	    <li><s:a namespace="/gepeto" action="addProjectInput"><s:text name="chorem.gepeto.project.add" /></s:a></li>
	    <s:url namespace="/gepeto" action="projectsByYear" var="projectsByYear">
	        <s:param name="year">2011</s:param>
	    </s:url>
	    <li><a href="${projectsByYear}"><s:text name="chorem.gepeto.projects" /></a></li>
	    <li><s:a namespace="/gepeto" action="projectsWithoutQuotation"><s:text name="chorem.gepeto.projectsWithoutQuotation" /></s:a></li>
    </ul>
</div>

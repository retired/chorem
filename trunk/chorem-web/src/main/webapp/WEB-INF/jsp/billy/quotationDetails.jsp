<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="org.chorem.billy.action.QuotationAction" %>
<%@page import="org.chorem.entities.ProjectOrder" %>
<%@page import="org.chorem.JspUtils" %>
<%@page import="org.chorem.bonzoms.EmployeeFull" %>
<%@taglib prefix="s" uri="/struts-tags" %>

<html xmlns:s="http://java.sun.com/xml/ns/j2ee/web-jsptaglibrary_2_0.xsd"
      xmlns:jsp="http://java.sun.com/JSP/Page">
    <head>
        <title>
            <s:text name="chorem.billy.quotationDetails.title">
                <s:param><%= QuotationAction.getAction().getReference() %></s:param>
            </s:text>
        </title>
        <s:head />
    </head>
    <body>
        <h2>
            <s:text name="chorem.billy.quotation">
                <s:param><%= QuotationAction.getAction().getReference() %></s:param>
            </s:text>
        </h2>
        <s:url action="modifyQuotation" var="modifyQuotation">
            <s:param name="quotationId"><%= QuotationAction.getAction().getQuotationId() %></s:param>
        </s:url>
        <form action="${modifyQuotation}" method="post">
            <p>
                <s:textfield name="type" key="chorem.billy.quotation.type" labelSeparator=": "/>
                <br />
                <s:textarea name="description" key="chorem.billy.quotation.description" cols="50" rows="10" labelposition="top" labelSeparator=": " />
                <br />
                <br />
                <s:textfield name="amount" key="chorem.billy.quotation.amount" labelSeparator=": " size="5" />
                <br />
                <s:textfield name="vat" key="chorem.billy.quotation.vat" labelSeparator=": " size="2" />%
                <br />
                <s:textfield name="beginDate" key="chorem.billy.quotation.beginDate" labelSeparator=": " size="7" />
                <br />
                <s:textfield name="endDate" key="chorem.billy.quotation.endDate" labelSeparator=": " size="7" />
                <br />
                <s:textfield name="postedDate" key="chorem.billy.quotation.postedDate" labelSeparator=": " size="7" />
                <br />
                <s:textfield name="conversionHope" key="chorem.billy.quotation.conversionHope" labelSeparator=": " size="2" />
                <br />
                <% EmployeeFull supplier = QuotationAction.getAction().getSupplier();
                   if (supplier != null) {
                %>
                <s:url namespace="/bonzoms" action="companyDetails" var="companyDetails">
                    <s:param name="companyId"><%= supplier.getCompany().getWikittyId() %></s:param>
                </s:url>
                <s:text name="chorem.billy.quotation.supplier" />: <a href="${companyDetails}"><%= supplier.getCompany().getName() %></a> - <%= supplier.getPerson().getLastName() %>&nbsp;<%= supplier.getPerson().getFirstName() %> (<%= JspUtils.dateFormat(supplier.getPerson().getBirthDate()) %>)
                <br />
                <%
                   }
                   EmployeeFull customer = QuotationAction.getAction().getCustomer();
                   if (customer != null) {
                %>
                <s:url namespace="/bonzoms" action="companyDetails" var="companyDetails">
                    <s:param name="companyId"><%= customer.getCompany().getWikittyId() %></s:param>
                </s:url>
                <s:text name="chorem.billy.quotation.customer" />: <a href="${companyDetails}"><%= customer.getCompany().getName() %></a> - <%= customer.getPerson().getLastName() %>&nbsp;<%= customer.getPerson().getFirstName() %> (<%= JspUtils.dateFormat(customer.getPerson().getBirthDate()) %>)
                <br /> 
                <%
                   }
                %>
                <s:submit key="chorem.misc.modify" name="modify" />
            </p>
        </form>
        <p>
            <br />
            <br />
            <%
            ProjectOrder projectOrder = QuotationAction.getAction().getProjectOrder();

            if (projectOrder != null) {
            %>
            <s:text name="chorem.billy.quotation.projectOrder" />
            <br />
            <s:url namespace="/gepeto" action="projectOrderDetails" var="projectOrderDetails">
                <s:param name="projectOrderId"><%= projectOrder.getWikittyId() %></s:param>
            </s:url>
            <a href="${projectOrderDetails}">
                <s:text name="chorem.gepeto.projectDetails.projectOrder">
                    <s:param>
                        <%= JspUtils.dateFormat(projectOrder.getBeginDate()) %>
                    </s:param>
                    <s:param>
                        <%= JspUtils.dateFormat(projectOrder.getEndDate()) %>
                    </s:param>
                </s:text>
            </a>
            <br />
            <% } else { %>
            <s:url namespace="/gepeto" action="addProjectOrderInput" var="addProjectOrder">
                <s:param name="quotationId"><%= QuotationAction.getAction().getQuotationId() %></s:param>
            </s:url>
            <a href="${addProjectOrder}"><s:text name="chorem.billy.quotation.addProjectOrder" /></a>
            <br />
            <% } %>
            <br />
            <s:url namespace="/gepeto" action="projectDetails" var="projectDetails">
                <s:param name="projectId"><%= QuotationAction.getAction().getProjectId() %></s:param>
            </s:url>
            <a href="${projectDetails}"><s:text name="chorem.billy.quotation.backToProjectDetails" /></a>
        </p>
    </body>
</html>

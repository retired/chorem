/*
 * #%L
 * Chorem entities
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem.entities;

import org.junit.Test;
import org.nuiton.wikitty.WikittyClient;

import static org.junit.Assert.*;

/**
 *
 * @author poussin
 */
public class InvoiceableUtilTest {

//    /**
//     * Test of evalCondition method, of class InvoiceableUtil.
//     */
//    @Test
//    public void testEvalCondition() {
//        System.out.println("evalCondition");
//
//        WikittyClient proxy = null;
//        Invoiceable invoiceable = new InvoiceableImpl();
//        invoiceable.setMimetype("application/javascript");
//        invoiceable.setCondition("'coucou'.equals(invoiceable.getName())");
//
//        {
//            invoiceable.setName("coucou");
//            boolean expResult = true;
//            boolean result = InvoiceableUtil.evalCondition(proxy, invoiceable);
//            assertEquals(expResult, result);
//        }
//        {
//            invoiceable.setName("not coucou");
//            boolean expResult = false;
//            boolean result = InvoiceableUtil.evalCondition(proxy, invoiceable);
//            assertEquals(expResult, result);
//        }
//    }
//
//    /**
//     * Test of evalValue method, of class InvoiceableUtil.
//     */
//    @Test
//    public void testEvalValue() {
//        System.out.println("evalValue");
//
//        WikittyClient proxy = null;
//        Invoiceable invoiceable = new InvoiceableImpl();
//        invoiceable.setMimetype("application/javascript");
//        invoiceable.setValue("10*10");
//
//        double expResult = 100;
//        double result = InvoiceableUtil.evalValue(proxy, invoiceable);
//        assertEquals(expResult, result, 0.0);
//    }
}

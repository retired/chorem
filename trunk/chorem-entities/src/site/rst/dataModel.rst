.. -
.. * #%L
.. * Chorem entities
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2011 - 2012 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -
========================
Modele de données Chorem
========================

Général
=======

- Configuration: cet objet ne doit avoir qu'une instance, il permet de stocker
  de valeur de configuration pour les calculs. Il sera bon d'avoir une page qui
  permette de mettre a jour cet objet soit par calcul soit manuellement.
  (peut-etre que dans le futur il pourrait y avoir une instance par company/type de contrat/année)
- Attachment: permet de mettre un fichier (pdf, image, odt, ...)
  Peut par exemple être lié avec un projet pour y attacher le cahier des  
  charges

Gepeto
======

- EmploymentContract: Le contrat de travail de l'employé. Un employé peut avoir 
  de 0 à n contrats de travail
- Employee: Description d'un employé (diplôme, congés)
- Person: Description d'une personne (nom, prénom, email, date de naissance)
- Worker: Pourcentage de travail d'une personne sur une tâche
- Task: Description d'une tâche (description, date de fin réelle, prix, nombre 
  de jours estimés). Si une tâche n'est pas liée à un contrat, utiliser un 
  contrat "Hors contrat" qui a un coût de 0 (la tâche a par contre un prix)
  une tache a un etat, ex: notstarted, started, delivered, closed
- Quotation: Contrat de commande d'un projet si il a au moins le status accepted.
  Plusieurs contrats peuvent être liés à un même projet
  un contrat a un etat, ex: draft, send, refused, accepted, started, delivered, vrs, warranty, closed
- Company: Description d'une société (nom, type)
- Project: Description d'un projet
- ContactDetails: Méthode de contact de n'importe quel type (site internet, 
  adresse email, adresse postale, etc.)
  
Plusieurs contrats de travail peuvent être liés à un employé qui travaille dans 
une seule et même entreprise (Company). Si une personne (Person) travaille dans 
plusieurs entreprises différentes, chaque Employee (qui sera lié à chaque 
entreprise dans laquelle la personne travaille) aura un lien avec Person. Par 
exemple, si une personne travaille dans trois entreprises différentes, trois 
Employee pointeront vers une seule et même personne (Person), et chacun de ces 
Employee aura un lien vers Company.

Bonzoms
=======

- Evaluation: permet d'évaluer n'importe quel type de Wikitty par une
  personne (faudrait-il mettre Employee ?). Cela permet de demander l'avis
  de gens sur une Mission ou un PersonSkill (ex: eval 360)
- Skill: une compétence (permet une représentation arbre des compétences)
- Mission: description d'une activité d'un salarié (commercial, régie,
  forfait, ...) si la personne n'est pas employée, on créera une pseudo company
  qui regroupera toutes les personnes sans emploi (ANPE?)
  Cela permettra de générer les missions d'un CV
- PersonSkill: lie une compétence à une personne, et permet de l'évaluer
  Cette evaluation est le niveau officiel de cette personne
- Goal: un objectif fixé à un employé sur une période donnée. À la fin de la
  période on évalue si l'objectif est atteint et dans quelle proportion. La
  prime réelle est calculée à partir de cette proportion et la prime prévue.
- Interview: entretien entre un groupe d'employés (1-*) et un employé
- Touch: une rencontre, future ou passée, entre plusieurs employés de la même
  société ou non (participant). La rencontre a un but(goal) et une fois
  faite, un résumé doit en être fait (digest). Cette rencontre peut avoir
  comme but de discuter, d'un contrat, d'un projet, ... on utilise alors
  link pour le lier à cet élément. Il faut préciser aussi de quel façon a eu
  lieu la rencontre (téléphone, visio, mail, rendez-vous, salon, ...)
- Vacation: les demandes de congés avec une petite description, et type
  (congé payé, rtt, sans solde, ...) et un statut (brouillon, demandé, validé)

Billy
=====

- Quotation: devis envoyé à un client en rapport avec un projet, un devis a
  un type (dev, admin, formation, ...), et un beginDate, endDate qui
  détermine approximativement le moment ou la réalisation devra être faite.
  Tant que le devis n'a pas de date d'envoi, il s'agit d'un brouillon.
  L'espoire de conversion (conversionHope) indique si l'on pense convertir
  facilement en contrat ce devis (0 = non, 100 = quasi sur)
- Invoiceable: indique les conditions de facturation. Chaque facture a
  emettre doit avoir une condition et une valeur associee. La condition et la
  valeur associee sont evaluees en fonction du type mime indique. De meme il
  est possible d'indique un script de recurrence qui permettra de generer
  automatiquement le Invoiceable suivant lorsque le Invoiceable aura dans
  le même Wikitty un Invoice. Ceci ce fera via l'enregistrement d'un Hook
  (post-store). Le nouveau Invoiceable ainsi genere aura le meme ID de
  depart que le wikitty courant avec un suffixe qui s'incremente. Si la
  recurrence doit generer 3 Invoiceable, on aura donc le premier Invoiceable
  avec l'id UUID puis les suivant avec UUID_1, UUID_2, UUID_3. De cette
  facon, il est simple de savoir si l'on a deja genere le Invoiceable
  suivant.
- Invoice: facture envoyée à un client en rapport avec un Invoiceable

Un devis accepté engendre un ProjectOrder.
Lors de la création du ProjectOrder, on crée aussi les conditions de
facturation via la creation de 1 ou N Invoiceable en indiquant via son
attribut target sur quel objet il porte. De cette facon il est possible
de mettre un Invoiceable sur tout type d'objet: Quotation, ProjectOrder,
Task, ...

Par exemple pour trouver ce qu'il y a a facturer, il faut:
- rechercher tous les Invoiceable n'ayant pas l'extension Invoice
- si la condition est vrai alors il y a une facture a emettre du montant
  evalué par value.

Quelques exemples de condition:
- status du target egal a "a facturer" (lot de forfait terminé)
- jour/mois/annee > 01/03/2011 (facture par mois pour de la TMA)

Quelques exemples de value:
- 30% du montant du target
- 2000 euros
- 450 euros par jour de travail trouvé dans time sur le dernier mois
- 25 euros par quart d'heure commencé dans time sur le dernier mois

À une facture et un devis sont liés deux ContactDetails qui représentent les 
adresses du client et du prestataire. Ainsi, si une personne change d'adresse 
ou quitte la société par exemple, les adresses d'origine sont sauvegardées afin 
de garantir une bonne traçabilité
À la base, un projet (Project) est défini. Un ou plusieurs devis (Quotation) 
peuvent être réalisés selon les remarques formulées par le client. Ensuite, un 
contrat (ProjectOrder) est signé et une ou plusieurs factures (Invoice) sont 
liées à ce contrat via un Invoiceable sur le contrat lui meme ou sur ses
Taches.

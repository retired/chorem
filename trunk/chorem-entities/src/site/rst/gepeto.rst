.. -
.. * #%L
.. * Chorem :: entities
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2011 - 2014 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -
========================
Gestion de projet Chorem
========================
--------------------------------------------------
Outil de gestion et de suivi de projet dans Chorem
--------------------------------------------------

.. contents::

1. Bilan projet
===============

Le bilan de projet présente divers calculs financiers liés aux projets.

Un certain nombre d'éléments sont à saisir manuellement, les champs en italique sont nécéssaires pour les calculs présent dans le bilan/tableau de bord  :

1.1. Quotation
--------------

:*Interval*:        l'intervalle de temps sur lequel se déroule le devis
:description:       description du devis
:*estimatedDays*:   nombre de jours-homme estimés
:*amount*:          montant facturé au client
:VAT:               taxe à appliquer au montant
:conversionHope:    pourcentage de réussite du projet
:supplier:          personne chargée du dossier dans l'entreprise
:customer:          personne chargée du dossier coté client
:project:           projet lié au devis
:category:          catégorie du devis

Pour chaque devis, un ensemble de tâche est associé. Les calculs effectués sur les tâches sont les mêmes que ceux des devis, seul le calcul de certains éléments diffèrent.
Les champs à saisir manuellement des tâches sont :

1.2. Task
---------

:*Interval*:        l'intervalle de temps sur lequel se déroule la tâche
:description:       description de la tâche
:*dayExtension*:    jours supplémentaire nécéssaires pour finir la tâche
:*price*:           prix facturé pour la tâche
:*estimatedDays*:   nombre de jours-homme estimés
:name:              nom de la tâche
:*status*:          statut de la tâche, devant être actualisé à chaque modification de statut
:*quotation*:       devis lié à la tâche
:forgeRef:          identifiant extérieur, numero de la tache dans une forge par exemple

Les tâches peuvent être reliées à des objets Time qui permettent d'avoir le temps passé par chaque Employee sur la tâches
&

1.3. Calculs
------------

Les calculs sont effectués grâce à la classe **Calculation** présente dans le package *org.chorem.project*. C'est une classe abstraite qui factorise les calculs des Task et des Quotation. Les classes **QuotationCalculation** et **TaskCalculation** définissent les méthodes spécifiques au type d'objet.

1.3.1. TJM [1]_ estimé
~~~~~~~~~~~~~~~~~~~~~~

Le TJM (ou ADR) est ce qui facturé au client par jour::

 adr = amount / estimatedDays

1.3.2. TJM réel
~~~~~~~~~~~~~~~

Le TJM réel est calculé en fonction du nombre de `jours réels`_ ::

 adr = amount / realDays

.. _`jours réels`:

1.3.3. Jours réels
~~~~~~~~~~~~~~~~~~
La méthode realDays() permet de calculer le nombre de jours réels qu'a pris le projet. Cette méthode est abstraite dans **Calculation**. La durée réelle d'un devis est la somme de la durée de ses tâches.
Le calcul de la durée réelle est effectuée de deux manières différentes, si le devis est terminé ou non.

a. Devis non terminé
____________________

Si le devis n'est pas terminé, le nombre de jours réels est égal au nombre de jours éstimé plus le nombre de jours supplémentaire::

 realDays = estimatedDays + dayExtension

b. Devis terminé
________________

Si le devis est terminé, le nombre de jours réels est égal à la somme des temps passés sur la tâche (objets Time)::

 realDays = Σ time

Le temps est divisé en jours, en prenant comme diviseur l'horaire journalier de l'employé concerné.::

 time = hours / employee.dailyHoursWorked

1.3.4. Différence estimation/réel
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Simple soustraction entre le nombre de jours estimés et `jours réels`_::

 deltaDays = realDays - estimatedDays

.. _`CJM estimé`:

1.3.5. CJM [2]_ estimé
~~~~~~~~~~~~~~~~~~~~~~

Moyenne des CJM (ou ADC) des employés en fonction du pourcentage estimé (pour un devis, un calcul du pourcentage moyen par employé est effectué au préalable)::

 estimatedAdc = Σ employee.adc*employee.estimatedPercentage

.. _`CJM réel`:

1.3.6. CJM réel
~~~~~~~~~~~~~~~

Moyenne des CJM (ou ADC) des employés en fonction du temps réel passé::

 estimatedAdc = Σ employee.adc*employee.timeSpent

1.3.7. Gain attendu
~~~~~~~~~~~~~~~~~~~

Le gain attendu est calculé à partir du montant, du nombre de jours estimés et du `CJM estimé`_ ::

 expectedProfit = amount - (estimatedDays * estimatedAdc)

1.3.7. Gain ou perte réelle
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Gain ou perte réalisée à la fin du projet, calculée à partir du nombre de `jours réels`_ et du `CJM réel`_ ::

 lossOrProfit = amount - (realDays * realAdc)

1.4. Alertes des tâches
-----------------------

En cas de dépassement des délais ou autre déroulement non prévu, des alertes sont affichées sur le tableau de bord. Voici la liste des évenements déclenchant, pour l'instant, des alertes :

 * La tâche devrait avoir commencé (statut "SCHEDULED" alors que la date de début est passée)
 * La tâche a été commencé en avance (statut "STARTED" alors que la date de début n'est pas passée)
 * La tâche devrait être finie (statut "STARTED" alors que la date de fin est passée)
 * La tâche a été finie en avance (statut "FINISHED" alors que la date de début n'est pas passée)

2. Gestion des employés
=======================

2.1 Calcul du CJM
-----------------

La classe **AdcCalculation** dans le package *org.chorem.project* gère le calcul du CJM [2]_ (ou ADC).
Le coût journalier moyen d'un employé est calculé à partir des données sur un an::

 adc = ( (totalExpenses / totalSalaries) /totalTimes) * employee.dailyHoursWorked * employee.dailySalary

2.1.1 Dépenses totales
~~~~~~~~~~~~~~~~~~~~~~

Les dépenses totales sont la somme des paiements (FinancialTransaction) effectués par l'entreprise sur l'année. Une simple requête Wikitty est effectué afin de récupérer ces objets FinancialTransaction afin d'effectuer la somme des montants HT::

 totalExpenses = Σ expense

2.1.1 Salaires totaux
~~~~~~~~~~~~~~~~~~~~~

Somme des salaires des employés productifs ayant travaillé durant l'année, pondéré par le taux de productivité.::

 totalSalaries = Σ ( employee.salary * employee.productivityRate )

2.1.1 Total des temps
~~~~~~~~~~~~~~~~~~~~~

Somme des temps travaillé durant l'année pour les employés de l'entreprise::

 totalTimes = Σ employee.times

.. [1] Taux Journalier Moyen (ADR : Average Daily Rate)
.. [2] Coût Journalier Moyen (ADC : Average Daily Cost)



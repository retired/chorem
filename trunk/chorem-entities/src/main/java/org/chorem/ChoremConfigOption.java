/*
 * #%L
 * Chorem entities
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem;

import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ConfigOptionDef;

import static org.nuiton.i18n.I18n.t;

/**
 * Chorem option definition.
 *
 * @author poussin
 */
public enum ChoremConfigOption implements ConfigOptionDef {
    CONFIG_FILE(ApplicationConfig.CONFIG_FILE_NAME,
            t("chorem.config.configFileName.description"),
            "chorem.properties", String.class, false, false),
//    CHOREM_MIGRATION_INVOICE(
//            WikittyConfigOption.WIKITTY_MIGRATION_CLASS + Invoice.EXT_INVOICE,
//            t("Classe de migration de l'extension Invoice"),
//            InvoiceMigration.class.getName(),
//            Class.class, false, false),
//    CHOREM_MIGRATION_QUOTATION(
//            WikittyConfigOption.WIKITTY_MIGRATION_CLASS + Quotation.EXT_QUOTATION,
//            t("Classe de migration de l'extension Quotation"),
//            QuotationMigration.class.getName(),
//            Class.class, false, false),
    CHOREM_EXTENSION_SEARCH_EXCLUSION(
            "chorem.extension.search.exclusion",
            t("Liste des extensions qui ne doivent pas apparaitre dans les resultats"),
            "Configuration,Interval,WikittyTreeNode,WikittyToken,WikittyUser",
            String.class, false, false),
    CHOREM_AUTHENTICATION(
            "chorem.authentication",
            t("If true authentication is used to acces page (default: false)"),
            "false",
            Boolean.class, false, false),
    CHOREM_DOMAIN(
            "chorem.domain",
            t("Domain qui permet de trouver la base de donnees a utiliser"),
            "default",
            String.class, false, false),
    CHOREM_LOGIN(
            "chorem.admin.login",
            t("Login as default login for action on command line"),
            "",
            String.class, false, false),
    CHOREM_PASSWORD(
            "chorem.import.csv.format",
            t("Password associated with login used"),
            "",
            String.class, false, false);

    public final String key;
    public final String description;
    public String defaultValue;
    public final Class<?> type;
    public boolean isTransient;
    public boolean isFinal;

    ChoremConfigOption(String key, String description, String defaultValue,
            Class<?> type, boolean isTransient, boolean isFinal) {
        this.key = key;
        this.description = description;
        this.defaultValue = defaultValue;
        this.type = type;
        this.isFinal = isFinal;
        this.isTransient = isTransient;
    }

    public String getKey() {
        return key;
    }

    public Class<?> getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public boolean isTransient() {
        return isTransient;
    }

    public boolean isFinal() {
        return isFinal;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public void setTransient(boolean isTransient) {
        this.isTransient = isTransient;
    }

    public void setFinal(boolean isFinal) {
        this.isFinal = isFinal;
    }

}

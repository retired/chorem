/*
 * #%L
 * Chorem entities
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem;

import java.util.Arrays;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ChoremMain {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(ChoremMain.class);

    static public void main(String[] args) throws Exception {
//        args = "--clear -ic /tmp/chorem-company.csv -ip /tmp/chorem-person.csv -ie /tmp/chorem-employee.csv --import-contract-type /tmp/chorem-contracttype.csv --import-contract /tmp/chorem-contract.csv --commit".split(" ");
//        args = "-ic /tmp/chorem-company.csv --commit".split(" ");
//        args = "--removeObject Invoice".split(" ");
//        args = "--removeObject Invoice -ii /tmp/bill.csv --import-costs /tmp/cost.csv --commit".split(" ");
//        args = "--reindex".split(" ");
        System.out.println(String.format("Launching ChoremMain ... (args: %s)", Arrays.toString(args)));
        // getConfig do all: parse and doAllAction
        //XXX ymartel 2014/03/12 : not used.
//        ApplicationConfig config = ChoremConfig.getConfig(null, args);
        
        System.exit(0);
    }
}

/*
 * #%L
 * Chorem entities
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.wikitty.WikittyConfigOption;

/**
 * Inits chorem configuration
 */
public class ChoremConfig {
    
    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(ChoremConfig.class);

    static protected Map<String, ApplicationConfig> config = new HashMap<String, ApplicationConfig>();

    /**
     * constructeur public seulement pour pouvoir mettre une variable de ce
     * type dans BowBaseAction et acceder facilement au donnees dans les jsp
     * en ognl
     */
    public ChoremConfig() {
    }

    /**
     * Get config for specified domain, if domain is null, configuration domain
     * is used
     * @param domain
     * @param args
     * @return
     */
    public static ApplicationConfig getConfig(String domain, String... args) {
        ApplicationConfig result = config.get(domain);
        if (result == null) {
            synchronized (config) {
                result = config.get(domain);
                if (result == null) {
                    try {
                        result = new ApplicationConfig(
                                ChoremConfigOption.CONFIG_FILE.getDefaultValue());

                        // Load wikitty options
                        result.loadDefaultOptions(WikittyConfigOption.values());

                        // Load chorem options
                        result.loadDefaultOptions(ChoremConfigOption.values());

                        // Load chorem action
                        result.loadActions(ChoremConfigAction.values());

                        // set domain before parse command line but after load default
                        if (StringUtils.isNotBlank(domain)) {
                            result.setOption(
                                    ChoremConfigOption.CHOREM_DOMAIN.getKey(),
                                    domain);
                        }

                        // Parse args
                        result.parse(args);

                        // on fait toutes les actions
                        result.doAllAction();
                        config.put(domain, result);
                    } catch (Exception eee) {
                        log.error("Can't create chorem configuration", eee);
                        throw new UnhandledException(eee);
                    }
                }
            }
        }
        return result;
    }

}

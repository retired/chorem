/*
 * #%L
 * Chorem entities
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem;

import org.nuiton.config.ConfigActionDef;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public enum ChoremConfigAction implements ConfigActionDef {

    HELP(ChoremAction.class.getName() + "#help", "-h", "--help"),
    INFO(ChoremAction.class.getName() + "#configInfo", "-i", "--config-info" , "--configInfo"),
    CLEAN(ChoremAction.class.getName() + "#clean", "--clean"),
    CLEAR(ChoremAction.class.getName() + "#clear", "--clear"),
    REMOVE_OBJECT(ChoremAction.class.getName() + "#removeObject", "--removeObject"),
    REINDEX(ChoremAction.class.getName() + "#reindex", "--reindex"),
    IMPORT_PERSON(ImportChoremTopia.class.getName() + "#importPerson",
            "-ip", "--import-person", "--importPerson"),
    IMPORT_COMPANY(ImportChoremTopia.class.getName() + "#importCompany",
            "-ic", "--import-company", "--importCompany"),
    IMPORT_EMPLOYEE(ImportChoremTopia.class.getName() + "#importEmployee",
            "-ie", "--import-employee", "--importEmployee"),
    IMPORT_CONTRACT(ImportChoremTopia.class.getName() + "#importContract",
            "--import-contract", "--importContract"),
    IMPORT_CONTRACT_TYPE(ImportChoremTopia.class.getName() + "#importContractType",
            "--import-contract-type", "--importContractType"),
    IMPORT_INVOICE(ImportChoremTopia.class.getName() + "#importInvoices",
            "-ii", "--import-invoices", "--importInvoices"),
    IMPORT_COST(ImportChoremTopia.class.getName() + "#importCosts",
            "--import-costs", "--importCosts"),
    IMPORT_COMMIT(ImportChoremTopia.class.getName() + "#commit", "--commit");

    public String action;
    public String[] aliases;

    private ChoremConfigAction(String action, String... aliases) {
        this.action = action;
        this.aliases = aliases;
    }

    @Override
    public String getAction() {
        return action;
    }

    @Override
    public String[] getAliases() {
        return aliases;
    }
}

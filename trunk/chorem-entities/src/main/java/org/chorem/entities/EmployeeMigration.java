/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.chorem.entities;

/*
 * #%L
 * Chorem entities
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.beans.PropertyChangeListener;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.ChoremClient;
import org.nuiton.util.VersionUtil;
import org.nuiton.wikitty.WikittyService;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.FieldType;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.services.WikittyExtensionMigrationRename;

/**
 *
 * @author mble
 */
public class EmployeeMigration extends WikittyExtensionMigrationRename {
    
   static private Log log = LogFactory.getLog(EmployeeMigration.class);
    
   /**
    * Passage de la v9 à la v10 : Suppression de 5 champs
    * (ces 5 champs sont migrés dans un entitée nommée employeeRH)
    
    */ 
   protected void clearFieldType(Wikitty old, Wikitty result) {
       String fType = WikittyUtil.getFQFieldName(Employee.EXT_EMPLOYEE, "type");
       result.clearField(fType);
   }
   protected void clearFieldPaidLeave(Wikitty old, Wikitty result) {
       String fPaidLeave = WikittyUtil.getFQFieldName(Employee.EXT_EMPLOYEE, "paidLeave");
       result.clearField(fPaidLeave);
   }
   protected void clearFieldRtt(Wikitty old, Wikitty result) {
       String fRtt = WikittyUtil.getFQFieldName(Employee.EXT_EMPLOYEE, "rtt");
       result.clearField(fRtt);
   }
   protected void clearFieldSalary(Wikitty old, Wikitty result) {
       String fSalary = WikittyUtil.getFQFieldName(Employee.EXT_EMPLOYEE, "salary");
       result.clearField(fSalary);
   }
   protected void clearFieldWorkingTime(Wikitty old, Wikitty result) {
       String fWorkingTime = WikittyUtil.getFQFieldName(Employee.EXT_EMPLOYEE, "workingTime");
       result.clearField(fWorkingTime);
   }
   
  
    
    @Override
    public Wikitty migrate(WikittyService service, Wikitty oldWikitty,Wikitty wikitty,
        WikittyExtension oldExt, WikittyExtension newExt) {
        Wikitty result = super.migrate(service,oldWikitty, wikitty, oldExt, newExt);
                
        if (Employee.EXT_EMPLOYEE.equals(newExt.getName())) {
            String oldVersion = oldExt.getVersion();
            String newVersion = newExt.getVersion();
            if (!VersionUtil.greaterThan("9.0", oldVersion)
                    && !VersionUtil.smallerThan(newVersion, "10.0")) {
                clearFieldType(wikitty, result);
                clearFieldPaidLeave(wikitty, result);
                clearFieldRtt(wikitty, result);
                clearFieldSalary(wikitty, result);
                clearFieldWorkingTime(wikitty, result);
            }
        }
       
        
        return result;
    }

    
}

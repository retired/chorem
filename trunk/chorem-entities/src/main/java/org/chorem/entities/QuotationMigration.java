/*
 * #%L
 * Chorem entities
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package org.chorem.entities;

import org.nuiton.util.VersionUtil;
import org.nuiton.wikitty.WikittyService;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.services.WikittyExtensionMigrationRename;

/**
 *
 * @author mble
 */
public class QuotationMigration extends WikittyExtensionMigrationRename {
    
    
   /**
    * Migration du champ VTA vers VAT
    
    */ 
    protected void migrateFieldVTA(Wikitty old, Wikitty result) {
        
        String oldFieldName =  WikittyUtil.getFQFieldName(
                Quotation.EXT_QUOTATION, "VTA");
        String newFieldName = WikittyUtil.getFQFieldName(
                Quotation.EXT_QUOTATION, Quotation.FIELD_QUOTATION_VAT);
        Object v = old.getFqField(oldFieldName);
        result.setFqField(newFieldName, v);
        
    }
   
    
    @Override
    public Wikitty migrate(WikittyService service, Wikitty oldWikitty, Wikitty wikitty,
        WikittyExtension oldExt, WikittyExtension newExt) {
        Wikitty result = super.migrate(service, oldWikitty, wikitty, oldExt, newExt);
        
        if (Quotation.EXT_QUOTATION.equals(newExt.getName())) {
            String oldVersion = oldExt.getVersion();
            String newVersion = newExt.getVersion();
            if (!VersionUtil.greaterThan("17.0", oldVersion)
                    && !VersionUtil.smallerThan(newVersion, "18.0")) {
                migrateFieldVTA(wikitty, result);
            }
        }
        return result;
    }
    

    
}

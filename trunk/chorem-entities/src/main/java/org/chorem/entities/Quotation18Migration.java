package org.chorem.entities;

/*
 * #%L
 * Chorem :: entities
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.nuiton.util.VersionUtil;
import org.nuiton.wikitty.WikittyService;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.services.WikittyExtensionMigrationRename;

/**
 * Migrate Quotation from version 18.0 to version 19.0
 *
 * Quotation is now split into several extensions (one per status)
 *
 * @author jcouteau
 * @since 0.3
 */
public class Quotation18Migration  extends WikittyExtensionMigrationRename {

    @Override
    public Wikitty migrate(WikittyService service, Wikitty oldWikitty, Wikitty wikitty,
                           WikittyExtension oldExt, WikittyExtension newExt) {
        Wikitty result = super.migrate(service, oldWikitty, wikitty, oldExt, newExt);

        if (Quotation.EXT_QUOTATION.equals(newExt.getName())) {
            String oldVersion = oldExt.getVersion();
            String newVersion = newExt.getVersion();
            if (!VersionUtil.greaterThan("18.0", oldVersion)
                    && !VersionUtil.smallerThan(newVersion, "19.0")) {



                //get status and switch case on it
                String oldFieldName =  WikittyUtil.getFQFieldName(
                        Quotation.EXT_QUOTATION, "status");
                String status = (String)wikitty.getFqField(oldFieldName);
                if ("DRAFT".equals(status)){
                    migrateToDraft(wikitty, result, service);
                }
                if ("SENT".equals(status)){
                    migrateToSent(wikitty, result, service);
                }
                if ("ACCEPTED".equals(status)){
                    migrateToAccepted(wikitty, result, service);
                }
                if ("REJECTED".equals(status)){
                    migrateToRejected(wikitty, result, service);
                }
                if ("STARTED".equals(status)){
                    migrateToStarted(wikitty, result, service);
                }
                if ("DELIVERED".equals(status)){
                    migrateToDelivered(wikitty, result, service);
                }
                if ("RSV".equals(status)){
                    migrateToRsv(wikitty, result, service);
                }
                if ("WARRANTY".equals(status)){
                    migrateToWarranty(wikitty, result, service);
                }
                if ("CLOSED".equals(status)){
                    migrateToClosed(wikitty, result, service);
                }
            }
        }

        return result;
    }

    /**
     * Draft
     *
     * Add Draft extension
     * Migrate Quotation.reference to Draft.reference
     *
     */
    protected void migrateToDraft(Wikitty old, Wikitty result,
                                  WikittyService service){

        // Add Draft extension
        WikittyExtension draft = service.restoreExtensionLastVersion(null, Draft.EXT_DRAFT);
        result.addExtension(draft);

        // Migrate Quotation.reference to Draft.reference
        String oldFieldName =  WikittyUtil.getFQFieldName(
                Quotation.EXT_QUOTATION, "reference");
        String newFieldName = WikittyUtil.getFQFieldName(
                Draft.EXT_DRAFT, Draft.FIELD_DRAFT_REFERENCE);
        Object v = old.getFqField(oldFieldName);
        result.setFqField(newFieldName, v);
    }

    /**
     * Sent
     *
     * Migrate Draft first
     * Add Sent extension
     * Migrate Quotation.postedDate to Sent.postedDate
     *
     */
    protected void migrateToSent(Wikitty old, Wikitty result,
                                 WikittyService service){

        migrateToDraft(old, result, service);

        // Add Sent extension
        WikittyExtension sent = service.restoreExtensionLastVersion(null,
                Sent.EXT_SENT);
        result.addExtension(sent);

        // Migrate Quotation.postedDate to Sent.postedDate
        String oldFieldName =  WikittyUtil.getFQFieldName(
                Quotation.EXT_QUOTATION, "postedDate");
        String newFieldName = WikittyUtil.getFQFieldName(
                Sent.EXT_SENT, Sent.FIELD_SENT_POSTEDDATE);
        Object v = old.getFqField(oldFieldName);
        result.setFqField(newFieldName, v);
    }

    /**
     * Accepted
     *
     * Migrate Sent first
     * Add Accepted extension
     *
     */
    protected void migrateToAccepted(Wikitty old, Wikitty result,
                                 WikittyService service){

        migrateToSent(old, result, service);

        //Add Accepted extension
        WikittyExtension accepted = service.restoreExtensionLastVersion(null,
                Accepted.EXT_ACCEPTED);
        result.addExtension(accepted);

    }

    /**
     * Rejected
     *
     * Migrate Sent first
     * Add Rejected extension
     *
     */
    protected void migrateToRejected(Wikitty old, Wikitty result,
                                     WikittyService service){

        migrateToSent(old, result, service);

        //Add Rejected extension
        WikittyExtension rejected = service.restoreExtensionLastVersion(null,
                Rejected.EXT_REJECTED);
        result.addExtension(rejected);

    }

    /**
     * Started
     *
     * Migrate Accepted first
     * Add Started extension
     *
     */
    protected void migrateToStarted(Wikitty old, Wikitty result,
                                     WikittyService service){

        migrateToAccepted(old, result, service);

        //Add Started extension
        WikittyExtension started = service.restoreExtensionLastVersion(null,
                Started.EXT_STARTED);
        result.addExtension(started);

    }

    /**
     * Delivered
     *
     * Migrate Started
     * Add Delivered extension
     */
    protected void migrateToDelivered(Wikitty old, Wikitty result,
                                    WikittyService service){

        migrateToStarted(old, result, service);

        //Add Delivered extension
        WikittyExtension delivered = service.restoreExtensionLastVersion(null,
                Delivered.EXT_DELIVERED);
        result.addExtension(delivered);

    }

    /**
     * RSV
     *
     * Migrate Delivered
     * Add RSV extension
     * Migrate Quotation.vrsStart to RSV.rsvStart
     * Migrate Quotation.vrsPeriod to RSV.rsvPeriod
     */
    protected void migrateToRsv(Wikitty old, Wikitty result,
                                    WikittyService service){

        migrateToDelivered(old, result, service);

        //Add RSV extension
        WikittyExtension rsv = service.restoreExtensionLastVersion(null,
                RSV.EXT_RSV);
        result.addExtension(rsv);

        // Migrate Quotation.vrsStart to RSV.rsvStart
        String oldRsvStartFieldName =  WikittyUtil.getFQFieldName(
                Quotation.EXT_QUOTATION, "vrsStart");
        String newRsvStartFieldName = WikittyUtil.getFQFieldName(
                RSV.EXT_RSV, RSV.FIELD_RSV_RSVSTART);
        Object rsvStart = old.getFqField(oldRsvStartFieldName);
        result.setFqField(newRsvStartFieldName, rsvStart);

        // Migrate Quotation.vrsPeriod to RSV.rsvPeriod
        String oldRsvPeriodFieldName =  WikittyUtil.getFQFieldName(
                Quotation.EXT_QUOTATION, "vrsPeriod");
        String newRsvPeriodFieldName = WikittyUtil.getFQFieldName(
                RSV.EXT_RSV, RSV.FIELD_RSV_RSVPERIOD);
        Object rsvPeriod = old.getFqField(oldRsvPeriodFieldName);
        result.setFqField(newRsvPeriodFieldName, rsvPeriod);

    }

    /**
     * Warranty
     *
     * Migrate Rsv
     * Add Warranty extension
     * Migrate Quotation.warrantyStart to Warranty.warrantyStart
     * Migrate Quotation.warrantyPeriod to Warranty.warrantyPeriod
     */
    protected void migrateToWarranty(Wikitty old, Wikitty result,
                                    WikittyService service){

        migrateToRsv(old, result, service);

        //Add Warranty extension
        WikittyExtension warranty = service.restoreExtensionLastVersion(null,
                Warranty.EXT_WARRANTY);
        result.addExtension(warranty);

        // Migrate Quotation.warrantyStart to Warranty.warrantyStart
        String oldWarrantyStartFieldName =  WikittyUtil.getFQFieldName(
                Quotation.EXT_QUOTATION, "warrantyStart");
        String newWarrantyStartFieldName = WikittyUtil.getFQFieldName(
                Warranty.EXT_WARRANTY, Warranty.FIELD_WARRANTY_WARRANTYSTART);
        Object warrantyStart = old.getFqField(oldWarrantyStartFieldName);
        result.setFqField(newWarrantyStartFieldName, warrantyStart);

        // Migrate Quotation.warrantyPeriod to Warranty.warrantyPeriod
        String oldWarrantyPeriodFieldName =  WikittyUtil.getFQFieldName(
                Quotation.EXT_QUOTATION, "warrantyPeriod");
        String newWarrantyPeriodFieldName = WikittyUtil.getFQFieldName(
                Warranty.EXT_WARRANTY, Warranty.FIELD_WARRANTY_WARRANTYPERIOD);
        Object warrantyPeriod = old.getFqField(oldWarrantyPeriodFieldName);
        result.setFqField(newWarrantyPeriodFieldName, warrantyPeriod);

    }

    /**
     * Closed
     *
     * Migrate warranty
     * Add Closed extension
     */
    protected void migrateToClosed(Wikitty old, Wikitty result,
                                    WikittyService service){

        migrateToWarranty(old, result, service);

        //Add Closed extension
        WikittyExtension closed = service.restoreExtensionLastVersion(null,
                Closed.EXT_CLOSED);
        result.addExtension(closed);

    }
}

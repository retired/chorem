/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.chorem.entities;

/*
 * #%L
 * Chorem entities
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.VersionUtil;
import org.nuiton.wikitty.WikittyService;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.services.WikittyExtensionMigrationRename;

/**
 *
 * @author mble
 */
public class InvoiceMigration extends WikittyExtensionMigrationRename {
    
   static private Log log = LogFactory.getLog(InvoiceMigration.class);
    
   /**
    * Migration du champ VTA vers VAT
    
    */ 
    protected void migrateFieldVTA(Wikitty old, Wikitty result) {
        FinancialTransactionHelper.addExtension(result);
        
        String oldFieldName =  WikittyUtil.getFQFieldName(
                Invoice.EXT_INVOICE, "VTA");
        String newFieldName = WikittyUtil.getFQFieldName(
                FinancialTransaction.EXT_FINANCIALTRANSACTION, "VAT");
        Object v = old.getFqField(oldFieldName);
        result.setFqField(newFieldName, v);
        
    }

    protected void migrateToFinancialTransactional(Wikitty old, Wikitty result) {
        result.removeExtension("Invoiceable");

        FinancialTransactionHelper.addExtension(result);

        result.setFqField(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_REFERENCE,
                old.getFieldAsObject("Invoice", "reference"));
        result.setFqField(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_DESCRIPTION,
                old.getFieldAsObject("Invoice", "description"));
        result.setFqField(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_AMOUNT,
                old.getFieldAsObject("Invoice", "amount"));

        // VAT peut etre null si on avait un tres vieux objet avec 'VTA'
        if (old.hasField("Invoice", "VAT")) {
            result.setFqField(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_VAT,
                    old.getFieldAsObject("Invoice", "VAT"));
        }

        result.setFqField(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EMITTEDDATE,
                old.getFieldAsObject("Invoice", "postedDate"));
        result.setFqField(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EXPECTEDDATE,
                old.getFieldAsObject("Invoice", "expectedDate"));
        result.setFqField(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYMENTDATE,
                old.getFieldAsObject("Invoice", "paymentDate"));
        result.setFqField(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYER,
                old.getFieldAsObject("Invoice", "customer"));
        result.setFqField(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_BENEFICIARY,
                old.getFieldAsObject("Invoice", "supplier"));
        result.setFqField(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_CATEGORY,
                old.getFieldAsObject("Invoiceable", "category"));
        result.setFqField(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_TARGET,
                old.getFieldAsObject("Invoiceable", "target"));
    }
    
    @Override
    public Wikitty migrate(WikittyService service, Wikitty oldWikitty, Wikitty wikitty,
        WikittyExtension oldExt, WikittyExtension newExt) {
        Wikitty result = super.migrate(service,oldWikitty, wikitty, oldExt, newExt);

        if (Invoice.EXT_INVOICE.equals(newExt.getName())) {
            String oldVersion = oldExt.getVersion();
            if (VersionUtil.smallerThan(oldVersion, "16.0")) {
                migrateFieldVTA(wikitty, result);
            }

            if (VersionUtil.smallerThan(oldVersion, "17.0")) {
                migrateToFinancialTransactional(wikitty, result);
            }
        }
       
        
        return result;
    }

    
}

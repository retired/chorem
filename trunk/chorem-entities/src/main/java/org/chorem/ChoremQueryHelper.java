package org.chorem;

/*
 * #%L
 * Chorem entities
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Collection;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.entities.ElementField;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.query.WikittyQuery;

/**
 * Classe contenant des methodes permettant d'ajouter des elements a une Query.
 * Ceci permet de centraliser les modification de query et de les faire evoluer
 * pour toutes les requetes facilement.
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ChoremQueryHelper {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(ChoremQueryHelper.class);

    /**
     * Ajoute les tries comme declarer sur les extensions passer en parametre.
     * Si extensions est vide ou null, alors on ajoute les tries de toutes les
     * extensions connues
     *
     * @param client
     * @param query
     * @param extensions les noms des extensions sur lequel il faut faire les tries
     * @return query qui etait en parametre
     */
    static public WikittyQuery addSort(ChoremClient client, WikittyQuery query, String ... extensions) {
        // on essai de trier les resultats au mieux tout en ne faisant qu'un requete
        Collection<WikittyExtension> exts = client.restoreExtensionLastVersion(extensions);
        for (WikittyExtension ext : exts) {
            query.addSortAscending(ext.getSortAscending().toArray(new ElementField[0]));
            query.addSortDescending(ext.getSortDescending().toArray(new ElementField[0]));
        }
        
        return query;
    }

}

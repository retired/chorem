package org.chorem.project;

/*
 * #%L
 * Chorem :: entities
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.chorem.ChoremClient;
import org.chorem.ChoremUtil;
import org.chorem.entities.Employee;
import org.chorem.entities.EmployeeHR;
import org.chorem.entities.FinancialTransaction;
import org.chorem.entities.Interval;
import org.chorem.entities.Time;
import org.chorem.entities.Company;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.query.WikittyQueryResult;

/**
 * Makes the calculation for the adc.
 * It can calculate the adc of one or more employee by using the method getAdc or getMultipleAdc
 * Usage : construct the object with an employee or a list of employees and call the method
 * getAdc() or getMultipleAdc()
 * 
 * @author gwenn
 *
 */
public class AdcCalculation {

    /** Chorme Client*/
    protected ChoremClient client;
    /** Company of the employee(s) */
    protected Company company;
    /** Employee */
    protected EmployeeHR employee;
    /** List of employees */
    protected List<EmployeeHR> employees;

    /** start date of the year*/
    protected Date start;
    /** end date of the year*/
    protected Date end;

    /**
     * Construct the object for calculating the adc of an employee
     * @param client chorem client
     * @param employee employee withthe adc to calculate
     */
    public AdcCalculation(ChoremClient client, EmployeeHR employee) {
        this.employee = employee;
        this.company = employee.getCompany(false);
        this.client = client;
        this.employees = null;
        initDate();
    }

    /**
     * Construct the object for calculating the adc of a list of employees
     * @param client chorem client
     * @param employees employees with the adc to calculate
     */
    public AdcCalculation(ChoremClient client, List<EmployeeHR> employees) {
        this.employees = employees;
        this.company = employees.get(0).getCompany(false);
        this.client = client;
        this.employee = null;
        initDate();
    }

    /**
     * Initiate the start and end date to the beginning and end of the year
     * TODO : Manage this in the admin configuration of chorem
     */
    protected void initDate() {
        Calendar cstart = new GregorianCalendar();
        cstart.add(Calendar.YEAR, -1);
        cstart.set(Calendar.MONTH, Calendar.JANUARY);
        cstart.set(Calendar.DAY_OF_MONTH, 1);
        cstart.set(Calendar.HOUR_OF_DAY, 0);
        cstart.set(Calendar.MINUTE, 0);
        cstart.set(Calendar.SECOND, 0);
        cstart.set(Calendar.MILLISECOND, 0);
        
        Calendar cend = new GregorianCalendar();
        cend.add(Calendar.YEAR, -1);
        cend.set(Calendar.MONTH, Calendar.DECEMBER);
        cend.set(Calendar.DAY_OF_MONTH, 31);
        cend.set(Calendar.HOUR_OF_DAY, 23);
        cend.set(Calendar.MINUTE, 59);
        cend.set(Calendar.SECOND, 59);
        cend.set(Calendar.MILLISECOND, 999);

        

        start = cstart.getTime();
        end = cend.getTime();
    }


    /**
     * Calculates the total gains made by the company in one year
     * TODO : Use wikitty function for sum.
     * @return total expenses
     */
    public double getTotalExpenses() {

        //Construct the query
        
        WikittyQuery expenseQuery = new WikittyQueryMaker()
        .select()
        .sum(FinancialTransaction.ELEMENT_FIELD_FINANCIALTRANSACTION_AMOUNT)
        .and()
        .exteq(FinancialTransaction.EXT_FINANCIALTRANSACTION) //of type financial transaction
        .or()
        //The payer is an employee of the company or the company itself
        .containsOne(FinancialTransaction.ELEMENT_FIELD_FINANCIALTRANSACTION_PAYER, getEmployeesHR())
        .eq(FinancialTransaction.ELEMENT_FIELD_FINANCIALTRANSACTION_PAYER, company)
        .close()
        //The payment hapened during the year
        .bw(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EMITTEDDATE, start, end)	
        .end();
        System.out.println("QUERY = " + expenseQuery);

        //WikittyQueryResult<FinancialTransaction> result = client.findAllByQuery(FinancialTransaction.class, expenseQuery);
        Double result = client.findByQuery(Double.class, expenseQuery);
        //Calculates the total
        double total = result;
        System.out.println("RESULT = " + result);
        /*for(FinancialTransaction ft : result.getAll()) {
            total += ft.getAmount();
        }*/

        return total;
    }

    /**
     * Calculates the employee cost at full time.
     * 
     * @param e employee concerned
     * @return employee cost at full time
     */
    public double getMonthlyFullTimeCost(EmployeeHR e) {
        double salary = e.getSalary();
        double otherPayments = e.getOtherPayments();
        double partialTime = e.getPartialTime();

        return ( ( salary + otherPayments) * 100  ) / partialTime;
    }


    /**
     * calculates the estimated number of productive days when the employee will work
     * 
     * It takes the average number of days in a year (218) and multiplicates it by the partial time
     * and the productivity rate
     * @param e employee concerned
     * @return estimated number of productive days
     */
    public double getEstimatedNumberOfProductiveDays(EmployeeHR e) {
        int daysInYear = 218; //It would be *much*  better to calculate this or at least get it from input
        double partialTime = e.getPartialTime();
        double productivityRate = e.getProductivityRate();

        return (daysInYear * (partialTime/100) * (productivityRate/100));
    }

    /**
     * Returns the real number of procuctive days in year, which is the sum of all the time object by the employee
     * @param e employee concerned
     * @return
     */
    public double getRealNumberOfProductiveDays(EmployeeHR e) {

        //fetch the times of all the employees of the company that have worked between $start an $end
        WikittyQuery timeQuery = new WikittyQueryMaker()
        .and()
        .exteq(Time.EXT_TIME)
        .eq(Time.ELEMENT_FIELD_TIME_EMPLOYEE, e)
        .or()
        .bw(Interval.FQ_FIELD_INTERVAL_BEGINDATE, start, end)
        .bw(Interval.FQ_FIELD_INTERVAL_ENDDATE, start, end)
        .end();
        WikittyQueryResult<Time> result = client.findAllByQuery(Time.class, timeQuery);

        //Calculates the total
        double total = 0;
        for(Time t : result.getAll()) {
            total +=  ChoremUtil.getPeriodInHours(t.getBeginDate(), t.getEndDate());

        }

        return (total/ client.getDailyHoursWorked(e) )
                * (e.getProductivityRate()/100);
    }

    /**
     * calculates the full time cost in a year
     * @param e
     * @param real true if the real time is needed
     * @return
     */
    public double getYearCost(EmployeeHR e, boolean real) {
        double result = 0;
        if(real) {
            result = getMonthlyFullTimeCost(e) * getRealNumberOfProductiveDays(e);
        }
        else {
            result = getMonthlyFullTimeCost(e) * getEstimatedNumberOfProductiveDays(e);
        }
        return result;
    }

    /**
     * calculates the full time cost in a year for all the employees in the company
     * @param real true if the real time is needed
     * @return
     */
    public double getTotalYearCost(boolean real) {

        double total = 0;
        for(EmployeeHR e : getEmployeesHR()) {
            total += getYearCost(e, real);
            System.out.println("total = " + total);
        }
        return total;
    }



    /**
     * Get all the employees of the company having the EmployeeHR extension
     * @return
     */
    protected List<EmployeeHR> getEmployeesHR() {

        WikittyQuery employeeQuery = new WikittyQueryMaker().and()
                .exteq(EmployeeHR.EXT_EMPLOYEEHR)
                .eq(Employee.ELEMENT_FIELD_EMPLOYEE_COMPANY, company)
                .end();
        WikittyQueryResult<EmployeeHR> result = client.findAllByQuery(EmployeeHR.class, employeeQuery);
        return result.getAll();

    }

    /**
     * Calculates the sum of all the times object linked to the company
     * @return
     */
    public double getTotalTimes() {

        WikittyQuery empQuery = new WikittyQueryMaker()
        .eq(Employee.ELEMENT_FIELD_EMPLOYEE_COMPANY, company).end();
        WikittyQueryResult<Employee> empResult = client.findAllByQuery(Employee.class, empQuery);

        //fetch the times of all the employees of the company that have worked between $start an $end
        WikittyQuery timeQuery = new WikittyQueryMaker()
        .and()
        .exteq(Time.EXT_TIME)
        .containsOne(Time.ELEMENT_FIELD_TIME_EMPLOYEE, empResult.getAll())
        .or()
        .bw(Interval.FQ_FIELD_INTERVAL_BEGINDATE, start, end)
        .bw(Interval.FQ_FIELD_INTERVAL_ENDDATE, start, end)
        .end();
        WikittyQueryResult<Time> result = client.findAllByQuery(Time.class, timeQuery);

        //Calculates the total
        double total = 0;
        for(Time t : result.getAll()) {
            total += ChoremUtil.getPeriodInHours(t.getBeginDate(), t.getEndDate());
        }

        return total;
    }

    /**
     * Returns the employee's adc.
     * See the documentation for a complete description of the operations made during this calculation.
     * @return the employee's adc
     */
    public double getAdc(boolean real) {


        double expenses = getTotalExpenses();
        double productiveDays = 0;
        if(real) {
            productiveDays = getRealNumberOfProductiveDays(employee);
        }
        else {
            productiveDays = getEstimatedNumberOfProductiveDays(employee);
        }

        double cost = getYearCost(employee, real);
        double totalCost = getTotalYearCost(real);

        double adc = ( (expenses * cost) / totalCost ) / productiveDays;


        return adc;
    }


    /**
     * Returns multiple adc form the given employee list.
     * @param real
     * @return adc of the employees
     */
    public Map<String, Double> getMultipleAdc(boolean real) {
        if(employees.size() == 0)
            return null;

        //Calculate all the "static" values before

        double expenses = getTotalExpenses();

        double totalCost = getTotalYearCost(real);

        //Then, for each employee

        HashMap<String, Double> adcs = new HashMap<String, Double>();
        for(EmployeeHR e : employees) {
            this.employee = e;

            double productiveDays = 0;
            if(real) {
                productiveDays = getRealNumberOfProductiveDays(e);
            }
            else {
                productiveDays = getEstimatedNumberOfProductiveDays(e);
            }
            double cost = getYearCost(e, real);

            Double adc = ( ( (expenses * cost) / totalCost ) / productiveDays);
            if(adc.isNaN()) {
                adc = 0.0;
            }
            adcs.put(e.getWikittyId(),adc);
        }

        return adcs;

    } 


}

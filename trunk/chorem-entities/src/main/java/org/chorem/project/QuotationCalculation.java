package org.chorem.project;

/*
 * #%L
 * Chorem :: entities
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.chorem.ChoremClient;
import org.chorem.entities.Employee;
import org.chorem.entities.Quotation;
import org.chorem.entities.Task;

public class QuotationCalculation extends Calculation<Quotation> {


    protected List<Task> tasks = null;

    /**
     * Initiate the calculation class
     * @param q quotation
     * @param client chorem client
     */
    public QuotationCalculation(Quotation q, ChoremClient client) {
        super(q, q.getAmount(), q.getEstimatedDays(), client);
    }

    @Override
    public double realDays() {
        double totalTime = 0;
        //For each task
        for(Task t : client.getTasks(e)) {
            //Sum the real days of the tasks objects
            totalTime+=new TaskCalculation(t, client).getRealDays();
        }
        return totalTime;

    }

    @Override
    public Map<Employee, Double> getPercentages() {
        Map<Employee, Double> percentages = new HashMap<Employee, Double>();
        double totalDays = 0;
        for(Task t : getTasks()) 
            totalDays += t.getEstimatedDays();

        List<Task> tasks  = getTasks();
        for(Task t : tasks) {

            Map<Employee, Double> taskPercentages = new TaskCalculation(t, client).getPercentages();
            for (Map.Entry<Employee, Double> employeeTime : taskPercentages.entrySet()) {
                double rate = (t.getEstimatedDays()/totalDays);
                Employee emp = employeeTime.getKey();
                Double employeePercentage = percentages.get(emp);
                if (employeePercentage == null) {
                    // the value was not found : init to 0
                    employeePercentage = 0d;
                }
                percentages.put(emp, employeePercentage + employeeTime.getValue() * rate);
            }
        }

        
        return percentages;
    }


    @Override
    public Map<Employee, Double> getTimes() {
        HashMap<Employee, Double> times = new HashMap<Employee, Double>();

        for(Task t : getTasks()) {

            Map<Employee, Double> taskPercentages = new TaskCalculation(t, client).getTimes();
            for(Employee emp : taskPercentages.keySet()) {
                if(times.containsKey(emp)) {
                    times.put(emp, times.get(emp) + taskPercentages.get(emp));
                }
                else {
                    times.put(emp, taskPercentages.get(emp));
                }

            }

        }

        return times;
    }

    /**
     * Fetch the tasks of the quotation and keep a reference
     * @return lits of the quotation's task
     */
    public List<Task> getTasks() {
        if(tasks == null)
            tasks = client.getTasks(e);
        return tasks;

    }
}

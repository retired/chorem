.. -
.. * #%L
.. * Chorem entities
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2011 - 2012 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -
EmployeeHR est une nouvelle entité définissant l'employé interne à la société (Default Company).
On a donc passé les attributs types "ressources humaines" (salary, workingtime, paidleave) de l'entité Employee vers l'entité EmployeeHR.
Trois attributs pour les congés :
	- paidLeave : solde de jours de congés payés dans l'année
	- rtt : solde de jours de récupération du temps de travail
	- otherLeave : solde de jours de congés de diverses natures éventuellements attribués dans l'année

VacationRequest :
Pour effectuer une demande de congés, l'employé doit passer par la saisie d'une "demande" de congés (VacationRequest).
Une demande de congé peut contenir plusieurs périodes de congés définies dans l'entité Vacation.
Les attributs :
	- reqDate : c'est la date de dernière modification de la demande effectuée par l'employé demandeur
	- reqComment : commentaires saisis par l'employé demandeur
	- ansDate : c'est la date à laquelle l'employé décideur a statué sur la demande (acceptation ou refus)
	- ansComment : commentaires de l'employé décideur
	- reqStatus (fieldset) : 
		. EN PREVISION : status par défaut à la création de la demande (permet au salarié de saisir un "prévisionnel" des congés)
		. EN DEMANDE : le salarié demandeur sélectionne ce statut pour soumettre la demande au décideur
		. ACCEPTEE : seul le décideur peut attribué ce statut à la demande
		. REFUSEE : idem ACCEPTEE
		. FERMETURE ANNUELLE : statut particulier (pas vraiment une demande), c'est un congé forcé par le décideur
	- employeeRequest     : objet EmployeeHR définissant l'employé demandeur
	- employeeWriter : objet EmployeeHR définissant l'employé qui saisit la demande pour le compte du demandeur (peut être identique)
	- employeeAnswer : objet EmployeeHR définissant l'employé décideur

Vacation :
Entité définissant une période de congé.
Les attributs :
	- amount : nombre de jours réels, saisis par l'employé, de congés dans cette période (permet de gérer, par exemple, les jours fériés)
	- typeLeave (fieldset) : type de congé (CONGES ANNUELS,RTT,SANS-SOLDE,MATERNITE,DIVERS)
	- vacationRequest : objet demande auquel la période est rattachée

ReferenceYear :
Entité définissant une année de référence (ou, en France, année fiscale).
Les attributs :
	- leavePaidDays : nombre de jours par défaut de congés payés par an attribué aux employés
	- differLeave : (booléen) report des congés payés oui/non

WorkingDays :
Entité qui référencie les demi-journées travaillées
 

	
		. 



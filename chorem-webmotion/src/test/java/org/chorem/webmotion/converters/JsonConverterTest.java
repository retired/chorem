package org.chorem.webmotion.converters;

/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.Date;
import java.util.Set;

import org.chorem.webmotion.bean.financial.ExpenseAccountBean;
import org.chorem.webmotion.bean.financial.ExpenseAccountEntryBean;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.util.DateUtil;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class JsonConverterTest {

    @Test
    public void testConvertExpenseAccountBeanFromJson() throws Exception {

        String employeeName = "Lucius Fox (WayneCorp)";
        String employeeId = "f44720a5-f003-4b3f-bfb9-a9189d0ab7d6";
        Date startDate = DateUtil.createDate(01, 07, 2014);
        Date endDate = DateUtil.createDate(31, 07, 2014);
        Date emittedDate = DateUtil.createDate(05, 07, 2014);

        String json = "{"
                        + "\"id\":\"f44720a5-f003-4b3f-bfb9-a9189d0ab7d6\""
                        + ",\"employeeName\":\"" + employeeName + "\""
                        + ",\"employeeId\":\"" + employeeId + "\""
                        + ",\"startDate\":\"" + startDate.getTime() + "\""
                        + ",\"endDate\":\"" + endDate.getTime() + "\""
                        + ",\"expenseAccountEntries\":[{\"justificationNumber\":\"Depl-0507\",\"emittedDate\":\"" + emittedDate.getTime() + "\",\"project\":\"\",\"description\":\"Deplacement\",\"category\":\"\",\"amount\":16,\"VAT\":4,\"total\":20,\"categoryName\":\"Train Hotel Repas Km\",\"categoryId\":\"d6cd6f26-44cb-4a45-a1cf-f41c6dd6fe7f\"}]"
                        + "}";
        JsonConverter<ExpenseAccountBean> expenseAccountJsonConverter = JsonConverter.newConverter(ExpenseAccountBean.class);
        ExpenseAccountBean expenseAccount = expenseAccountJsonConverter.convert(ExpenseAccountBean.class, json);
        Assert.assertNotNull(expenseAccount);
        Assert.assertNotNull(expenseAccount.getId());
        Assert.assertEquals(employeeId, expenseAccount.getEmployeeId());
        Assert.assertEquals(employeeName, expenseAccount.getEmployeeName());
        Assert.assertEquals(startDate.getTime(), expenseAccount.getStartDate());
        Assert.assertEquals(endDate.getTime(), expenseAccount.getEndDate());
        Set<ExpenseAccountEntryBean> expenseAccountEntries = expenseAccount.getExpenseAccountEntries();
        Assert.assertNotNull(expenseAccountEntries);
        Assert.assertEquals(1, expenseAccountEntries.size());
        ExpenseAccountEntryBean expenseAccountEntryBean = expenseAccountEntries.iterator().next();
        Assert.assertEquals(emittedDate.getTime(), expenseAccountEntryBean.getEmittedDate());

    }

}

/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.wikitty.web.jsptag;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.FieldType;
import org.nuiton.wikitty.entities.Wikitty;

import javax.servlet.ServletContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.nuiton.wikitty.entities.BusinessEntityImpl;

/**
 * Tag Support to display Wikitty fields.
 *
 * attribute:
 * <li>wikitty: wikitty object or BusinessEntityImpl
 * <li>wikitties: Collection of wikitty object or BusinessEntityImpl (used if wikitty is null)
 * <li>fqfield: fully qualified field to display
 * <li>label: if not presente use field name, if empty or blank don't show label
 * <li>toString: String used to transforme Wikitty to string representation (overwrite default toString)
 * <li>pattern: pattern used to display field (Date, Number) (overwrite default format)
 * <li>subtype: subtype used for display field (overwrite default subtype)
 *
 * @author ymartel <martel@codelutin.com>
 * @since 04/26/2012
 */
public class WikittyDisplay extends SimpleTagSupport {

    protected Wikitty wikitty;
    
    protected Collection<Wikitty> wikitties;
    protected String listStart = "<ul>";
    protected String listEnd = "</ul>";
    protected String listItemStart = "<li>";
    protected String listItemEnd = "</li>";

    protected String fqfield = "";
    protected String label = null;
    protected String toString = null;
    protected String pattern = null;
    protected String subtype = null;

    public void setWikitty(Object o) {
        this.wikitty = WikittyUtil.getWikitty(o);
    }

    public void setWikitties(Collection<Object> os) {
        wikitties = new ArrayList<Wikitty>(os.size());
        for (Object o : os) {
            wikitties.add(WikittyUtil.getWikitty(o));
        }
    }

    public void setListStart(String listStart) {
        this.listStart = listStart;
    }

    public void setListEnd(String listEnd) {
        this.listEnd = listEnd;
    }

    public void setListItemStart(String listItemStart) {
        this.listItemStart = listItemStart;
    }

    public void setListItemEnd(String listItemEnd) {
        this.listItemEnd = listItemEnd;
    }

    public void setFqfield(String fqfield) {
        this.fqfield = fqfield;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setToString(String toString) {
        this.toString = toString;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    @Override
    public void doTag() throws JspException, IOException {

        JspWriter output = getJspContext().getOut();
        PageContext pageContext = (PageContext)getJspContext();
        ServletContext servletContext = pageContext.getServletContext();
        String contextPath = servletContext.getContextPath();

        if (wikitty != null) {
            renderOneWikitty(output, contextPath, wikitty);
        } else if (wikitties != null) {
            /*{<%=listStart%>}*/
            for (Wikitty wikitty : wikitties) {
                /*{<%=listItemStart%>}*/
                renderOneWikitty(output, contextPath, wikitty);
                /*{<%=listItemEnd%>}*/
            }
            /*{<%=listEnd%>}*/
        }
        // si pas de wikitty, on a rien a faire
    }

    protected void renderOneWikitty(JspWriter output, String contextPath, Wikitty wikitty) throws JspException, IOException {
        if (StringUtils.isBlank(fqfield)) {
            // pas de champs precisse, on doit afficher le wikitty lui meme
            if (StringUtils.isNotBlank(label)) {
                /*{<%=label%> : }*/
            }
            renderWikitty(output, contextPath, wikitty.getWikittyId(), wikitty, toString);
        } else {
            String extName = WikittyUtil.getExtensionNameFromFQFieldName(fqfield);
            String fieldName = WikittyUtil.getFieldNameFromFQFieldName(fqfield);
            FieldType field = wikitty.getFieldType(fqfield);

            String visible = field.getTagValue("visible");
            if ("false".equals(visible)) {
                // on affiche rien pour ce champs
                return;
            }

            // par defaut si pas de label on utilise le nom du field
            if (null == label) {
                label = fieldName;
            }

            if (field != null) {
                if (StringUtils.isNotBlank(label)) {
                    /*{<%=label%> : }*/
                }

                switch(field.getType()) {
                    case BINARY:
                        viewBinary(output, contextPath, label, wikitty, extName, fieldName);
                        break;
                    case BOOLEAN:
                        viewBoolean(output, contextPath, label, wikitty, extName, fieldName);
                        break;
                    case DATE:
                        viewDate(output, contextPath, label, wikitty, field, extName, fieldName);
                        break;
                    case NUMERIC:
                        viewNumeric(output, contextPath, label, wikitty, field, extName, fieldName);
                        break;
                    case STRING:
                        if (field.isCollection()) {
                            viewCollectionString(output, contextPath, label, wikitty, field, extName, fieldName);
                        } else {
                            viewString(output, label, wikitty, field, extName, fieldName);
                        }
                        break;
                    case WIKITTY:
                        if (field.isCollection()) {
                            viewCollectionWikitty(output, contextPath, label, wikitty, field, extName, fieldName);
                        } else {
                            viewWikitty(output, contextPath, label, wikitty, field, extName, fieldName);
                        }
                        break;
                }
            } else {
                /*{<div class="alert alert-error"><%=wikitty.getWikittyId()%> doesn't have field '<%=fqfield%>'</div>}*/
            }
        }
    }

    protected void viewBinary(JspWriter output, String contextPath, String label,
            Wikitty wikitty, String extName , String fieldName)
            throws JspException, IOException {
        byte[] fieldAsBytes = wikitty.getFieldAsBytes(extName, fieldName);
        if (fieldAsBytes != null) {
            String url = contextPath + "/wikitty/view/" + wikitty.getWikittyId() + "/" + extName + "/" + fieldName;
            /*{<a href="<%=url%>">Download</a>}*/
        } else {
            // nothing
        }
    }

    protected void viewBoolean(JspWriter output, String contextPath, String label,
            Wikitty wikitty, String extName , String fieldName)
            throws JspException, IOException {
        boolean value = wikitty.getFieldAsBoolean(extName, fieldName);
        /*{<%=value%>}*/
    }

    protected void viewDate(JspWriter output, String contextPath, String label,
            Wikitty wikitty, FieldType field, String extName , String fieldName)
            throws JspException, IOException {
        Date date = wikitty.getFieldAsDate(extName, fieldName);

        String value = "";
        if (date != null) {

            pattern = StringUtils.defaultIfBlank(pattern, field.getPattern());

            if (StringUtils.isBlank(pattern)) {
                subtype = StringUtils.defaultIfBlank(subtype, field.getSubtype());
                if ("month".equalsIgnoreCase(subtype)) {
                    pattern = "MM/yyyy";
                } else if ("time".equalsIgnoreCase(subtype)) {
                    pattern = "hh:mm";
                } else if ("datetime".equalsIgnoreCase(subtype)) {
                    pattern = "dd/MM/yyyy hh:mm";
                } else {
                    pattern = "dd/MM/yyyy";
                }
            }
            value = DateFormatUtils.format(date, pattern);
        }
        /*{<%=value%> }*/
    }

    protected void viewNumeric(JspWriter output, String contextPath, String label,
            Wikitty wikitty, FieldType field, String extName , String fieldName)
            throws JspException, IOException {
        BigDecimal value = wikitty.getFieldAsBigDecimal(extName, fieldName);

        String formattedValue = "";
        if (value != null) {

            pattern = StringUtils.defaultIfBlank(pattern, field.getPattern());
            NumberFormat formatter;

            if (StringUtils.isNotBlank(pattern)) {
                formatter = new DecimalFormat(pattern);
            } else {
                subtype = StringUtils.defaultIfBlank(subtype, field.getSubtype());

                if ("currency".equalsIgnoreCase(subtype)) {
                    formatter =  NumberFormat.getCurrencyInstance();
                } else if ("percent".equalsIgnoreCase(subtype)) {
                     // on utilise pas NumberFormat.getPercentInstance();
                    // car il divise tout seul par 100, ce qu'on ne souhaite pas
                    // on veut affichage=stockage
                    formatter = new DecimalFormat("#,##0 '%'");
                } else if ("integer".equalsIgnoreCase(subtype)) {
                    formatter = NumberFormat.getIntegerInstance();
                } else {
                    formatter = NumberFormat.getNumberInstance();
                }
            }
            formattedValue = formatter.format(value);
        }
        /*{<%=formattedValue%> }*/
    }

    protected void viewCollectionString(JspWriter output, String contextPath, String label,
            Wikitty wikitty, FieldType field, String extName , String fieldName)
            throws JspException, IOException {
        List<String> values = wikitty.getFieldAsList(extName, fieldName, String.class);
        subtype = StringUtils.defaultIfBlank(subtype, field.getSubtype());

        /*{<ul>}*/
        if (values != null) {
            for (String value : values) {
                if (value != null) {
                    value = transformString(subtype, value);
                    /*{<li><%=value%></li>}*/
                }
            }
        }
        /*{</ul>}*/
    }

    protected void viewString(JspWriter output, String label, Wikitty wikitty,
            FieldType field, String extName , String fieldName)
            throws JspException, IOException {
        String value = wikitty.getFieldAsString(extName, fieldName);
        subtype = StringUtils.defaultIfBlank(subtype, field.getSubtype());

        if (StringUtils.isNotBlank(value)) {
            value = transformString(subtype, value);
        } else {
            value = "";
        }
        /*{<%=value%>}*/
    }

    protected void viewCollectionWikitty(JspWriter output, String contextPath, String label,
            Wikitty wikitty, FieldType field, String extName , String fieldName)
            throws JspException, IOException {
        List<Wikitty> values = wikitty.getFieldAsWikittyList(extName, fieldName, false);
        toString = StringUtils.defaultIfBlank(toString, field.getToString());

        /*{<ul>}*/
        if (values != null) {
            for (Wikitty wikittyValue : values) {
                if (wikittyValue != null) {
                    String id = wikittyValue.getWikittyId();
                    renderWikitty(output, contextPath, id, wikittyValue, toString);
                }
            }
        }
        /*{</ul>}*/

    }

    /**
     * Si l'objet de ce champs est non preloaded alors utilise l'id comme representation texte.
     * Si l'objet est preloaded alors on utilise le toString du tag s'il existe
     * sinon le toString du champs et en dernier le toString du wikitty
     *
     * @param output
     * @param contextPath
     * @param label
     * @param wikitty
     * @param field
     * @param extName
     * @param fieldName
     * @throws JspException
     * @throws IOException
     */
    protected void viewWikitty(JspWriter output, String contextPath, String label,
            Wikitty wikitty, FieldType field, String extName , String fieldName)
            throws JspException, IOException {
        String id = wikitty.getFieldAsWikitty(extName, fieldName);
        Wikitty wikittyValue = wikitty.getFieldAsWikitty(extName, fieldName, false);
        String value = id; // par defaut, si on a pas mieux on met l'id de l'objet
        toString = StringUtils.defaultIfBlank(toString, field.getToString());

        renderWikitty(output, contextPath, id, wikittyValue, toString);
    }

    protected void renderWikitty(JspWriter output, String contextPath,
            String id, Wikitty wikittyValue, String toString)
            throws JspException, IOException {
        if (StringUtils.isNotBlank(id)) {
            String url = contextPath + "/wikitty/view/" + id;
            String value = id; // par defaut, si on a pas mieux on met l'id de l'objet

            if (wikittyValue != null) {
                if (StringUtils.isNotBlank(toString)) {
                    value = WikittyUtil.format(toString, wikittyValue);
                } else {
                    value = wikittyValue.toString();
                }
            }
            value = StringUtils.replace(value, "\"", "&quot;");
            value = StringUtils.replace(value, "'", "&apos;");
            /*{<a href="<%=url%>"><%=value%></a>}*/
        }
    }

    /**
     * Transform String in specifique format to other format (ex: HTML).
     *
     * @param subtype mimetype or other used to choice output format
     * @param value value to transform
     * @return
     */
    protected String transformString(String subtype, String value) {
        String result = value;
        if ("text/rst".equalsIgnoreCase(subtype)) {
            // TODO poussin 20120807 use jrst to transforme string to html
        } else if ("text/dbkx".equalsIgnoreCase(subtype)) {
            // TODO poussin 20120807 use docbook jrst xsl to transforme string to html
        }
        return result;
    }

}//WikittyDisplay

/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.wikitty.web.jsptag;


import java.io.IOException;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.DynamicAttributes;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.FieldType;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyInput extends SimpleTagSupport implements DynamicAttributes {

	/** to use log facility, just put in your code: log.info(\"...\"); */
	static private Log log = LogFactory.getLog(WikittyInput.class);

	protected Map<String, Object> dynamicAttribute = new HashMap<String, Object>();

	protected String name="";
	protected Object defaultValue="";
	protected boolean emptyField = false;
	protected Wikitty wikitty;
	protected WikittyExtension extension = null;
	protected String fqfield="";

	public void setDynamicAttribute(String uri, String localName, Object value) throws JspException {
		dynamicAttribute.put(localName, value);
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDefaultValue(Object defaultValue) {
		this.defaultValue = defaultValue;
	}

	public void setWikitty(Wikitty wikitty) {
		this.wikitty = wikitty;
	}

	public void setExtension(WikittyExtension extension) {
		this.extension = extension;
	}

	public void setFqfield(String fqfield) {
		this.fqfield = fqfield;
	}

	@Override
	public void doTag() throws JspException, IOException {
		// par defaut si pas de name on utilise celui du field
		if (StringUtils.isBlank(name)) {
			name = fqfield;
		}

		JspWriter output = getJspContext().getOut();
		PageContext pageContext = (PageContext)getJspContext();
		ServletContext servletContext = pageContext.getServletContext();
		String contextPath = servletContext.getContextPath();

		String extName = WikittyUtil.getExtensionNameFromFQFieldName(fqfield);
		String fieldName = WikittyUtil.getFieldNameFromFQFieldName(fqfield);
		FieldType field = null;
		try {
			field = wikitty.getFieldType(fqfield);
		}
		catch(Exception e) {
			if(extension == null) {
				throw new JspException(e);
			}
			else {
				field = extension.getFieldType(WikittyUtil.getFieldNameFromFQFieldName(fqfield));
				emptyField = true;
			}
		}
		


		

		if (field != null) {
			String visible = field.getTagValue("visible");
			if ("false".equals(visible)) {
				// on affiche rien pour ce champs
				return;
			}
			
			switch(field.getType()) {
			case BINARY:
				inputBinary(output, contextPath, name, wikitty, field, extName, fieldName);
				break;
			case BOOLEAN:
				inputBoolean(output, contextPath, name, wikitty, field, extName, fieldName);
				break;
			case DATE:
				inputDate(output, contextPath, name, wikitty, field, extName, fieldName);
				break;
			case NUMERIC:
				inputNumeric(output, contextPath, name, wikitty, field, extName, fieldName);
				break;
			case STRING:
				if (field.hasAllowed()) {
					inputSelectString(output, contextPath, name, wikitty, field, extName, fieldName);
				} else if (field.isCollection()){
					inputCollectionString(output, contextPath, name, wikitty, field, extName, fieldName);
				} else {
					inputString(output, contextPath, name, wikitty, field, extName, fieldName);
				}
				break;
			case WIKITTY:
				if (field.isCollection()) {
					inputCollectionWikitty(output, contextPath, name, wikitty, field, extName, fieldName);
				} else {
					inputWikitty(output, contextPath, name, wikitty, field, extName, fieldName);
				}
				break;
			}
			String help = field.getTagValue("help");
			if (StringUtils.isNotBlank(help)) {
				/*{ <i class="icon-question-sign" title="<%=help%>"></i>}*/
			}
		} else {
			/*{<div class="alert alert-error"><%=wikitty.getWikittyId()%> doesn't have field '<%=fqfield%>'</div>}*/
		}
	}

	protected String getDynamicAttribute() {
		StringBuilder result = new StringBuilder();
		for (Map.Entry<String, Object> e : dynamicAttribute.entrySet()) {
			result.append(" " + e.getKey() + "=\"" + e.getValue() + "\"");
		}
		return result.toString();
	}



	protected void inputBinary(JspWriter output, String contextPath, String name,
			Wikitty wikitty, FieldType field, String extName , String fieldName)
					throws JspException, IOException {
		/*{<input type="file" class="form-control" name="<%=name%>" <%=getDynamicAttribute()%>/>}*/
	}

	protected void inputBoolean(JspWriter output, String contextPath, String name,
			Wikitty wikitty, FieldType field, String extName , String fieldName)
					throws JspException, IOException {
		boolean value = false;
		if(!emptyField)
			value = wikitty.getFieldAsBoolean(extName, fieldName);
		String checked="";
		if (value) {
			checked=" checked";
		}
		/*{<input type="checkbox" name="<%=name%>"<%=checked%><%=getDynamicAttribute()%>/>}*/
	}

	protected void inputDate(JspWriter output, String contextPath, String name,
			Wikitty wikitty, FieldType field, String extName , String fieldName)
					throws JspException, IOException {
		Date date = null;
		if(!emptyField)
			date = wikitty.getFieldAsDate(extName, fieldName);
		String value = "";
		if ("month".equals(field.getTagValue("subtype"))) {
			if (date != null) {
				value = DateFormatUtils.format(date, "MM/yyyy");
			}
			/*{<input class="monthpicker form-control" type="text" name="<%=name%>" value="<%=value%>" <%=getDynamicAttribute()%>/>}*/
		} else if ("time".equals(field.getTagValue("subtype"))) {
			if (date != null) {
				value = DateFormatUtils.format(date, "HH:mm");
			}
			/*{<input class="timepicker form-control" type="text" name="<%=name%>" value="<%=value%>" <%=getDynamicAttribute()%>/>}*/
		} else if ("datetime".equals(field.getTagValue("subtype"))) {
			if (date != null) {
				value = DateFormatUtils.format(date, "dd/MM/yyyy HH:mm");
			}
			/*{<input class="datetimepicker form-control" type="text" name="<%=name%>" value="<%=value%>" <%=getDynamicAttribute()%>/>}*/
		} else {
			if (date != null) {
				value = DateFormatUtils.format(date, "dd/MM/yyyy");
			}
			/*{<input class="datepicker form-control" type="text" name="<%=name%>" value="<%=value%>"<%=getDynamicAttribute()%>/>}*/
		}
	}

	protected void inputNumeric(JspWriter output, String contextPath, String name,
			Wikitty wikitty, FieldType field, String extName , String fieldName)
					throws JspException, IOException {
		BigDecimal value = new BigDecimal(0);
		if(!emptyField)
			value= wikitty.getFieldAsBigDecimal(extName, fieldName);
		NumberFormat formatter = NumberFormat.getNumberInstance();
		String formattedValue = formatter.format(value);
		/*{<input type="text" class="form-control" name="<%=name%>" value="<%=formattedValue%>"<%=getDynamicAttribute()%>/>}*/
	}

	protected void inputString(JspWriter output, String contextPath, String name,
			Wikitty wikitty, FieldType field, String extName , String fieldName)
					throws JspException, IOException {
		String id = wikitty.getWikittyId() + "-" + extName + "-" + fieldName;

		String fieldValue = "";
		if(!emptyField)
			fieldValue = wikitty.getFieldAsString(extName, fieldName);
		String value = fieldValue !=null ? fieldValue : "";
		if ("monoline".equalsIgnoreCase(field.getSubtype())) {
			/*{<input id="text-<%=id%>" class="form-control" type="text" name="<%=name%>" value="<%=value%>"<%=getDynamicAttribute()%>/>}*/
			if (field.hasChoiceQuery()) {
				String url = contextPath + "/wikitty-json/searchField?q=" + field.getChoiceQuery();
				/*{
        <script>
            $(function() {
                $( "#text-<%=id%>" ).autocomplete({
                    source: "<%=url%>",
                    minLength: 1
                });
            });
        </script>
}*/
			}
		} else {
			/*{<textarea  class="form-control" name="<%=name%>"<%=getDynamicAttribute()%>><%=value%></textarea>}*/
		}
	}

	protected void inputSelectString(JspWriter output, String contextPath, String name,
			Wikitty wikitty, FieldType field, String extName , String fieldName)
					throws JspException, IOException {
		String id = wikitty.getWikittyId() + "-" + extName + "-" + fieldName;
		String value = "";
		if(!emptyField)
			value = wikitty.getFieldAsString(extName, fieldName);
		/*{<select  class="form-control" id="text-<%=id%>" name="<%=name%>" <%=getDynamicAttribute()%> }*/
		if (field.isCollection()) {
			/*{ multiple="multiple" }*/
		}
		/*{ >}*/
		List<String> allowedValues = field.getAllowedAsList();
		for (String allowedValue : allowedValues) {
			String selected = "";
			if (allowedValue.equals(value)) {
				selected = "selected";
			}
			/*{<option value="<%=allowedValue%>" <%=selected%>><%=allowedValue%></option>}*/
		}
		/*{</select>}*/
	}

	protected void inputCollectionString(JspWriter output, String contextPath, String name,
			Wikitty wikitty, FieldType field, String extName , String fieldName)
					throws JspException, IOException {
		String id = wikitty.getWikittyId() + "-" + extName + "-" + fieldName;
		List<String> value = null;
		if(!emptyField)
			value = wikitty.getFieldAsList(extName, fieldName, String.class);

		if ("monoline".equalsIgnoreCase(field.getSubtype())) {
			/*{<input  class="form-control" id="text-<%=id%>" type="text" name="<%=name%>" value="<%=value%>"<%=getDynamicAttribute()%>/>}*/
			if (field.hasChoiceQuery()) {
				String url = contextPath + "/wikitty-json/searchField?q=" + field.getChoiceQuery();
				/*{
        <script>
            $(function() {
                $( "#text-<%=id%>" ).autocomplete({
                    source: "<%=url%>",
                    minLength: 1
                });
            });
        </script>
}*/
			}
		} else {
			/*{<textarea name="<%=name%>"<%=getDynamicAttribute()%>><%=value%></textarea>}*/
		}
	}

	protected void inputWikitty(JspWriter output, String contextPath, String name,
			Wikitty wikitty, FieldType field, String extName , String fieldName)
					throws JspException, IOException {
		String id = wikitty.getWikittyId() + "-" + extName + "-" + fieldName;
		String value = "";
		if(!emptyField)
			value = wikitty.getFieldAsWikitty(extName, fieldName);
		
		Wikitty wikittyValue = null;
		if(!emptyField)
			wikittyValue = wikitty.getFieldAsWikitty(extName, fieldName, false);

		String wlabel = "";
		if (wikittyValue != null) {
			wlabel = wikittyValue.toString();
		}

		String wid = "";
		if (value != null) {
			wid = value;
		}

		String url = "";
		if (field.hasAllowed()) {
			url = contextPath + "/wikitty-json/search?extension=" + field.getAllowed();
		} else if (field.hasAllowedQuery()) {
			url = contextPath + "/wikitty-json/search?query=" + field.getAllowedQuery();
		} else {
			url = contextPath + "/wikitty-json/search?query=";
		}
		/*{
        <script>
            $(function() {
                $( "#text-<%=id%>" ).autocompleteByExtension({
                    source: "<%=url%>",
                    minLength: 2,
                    select: function( event, ui ) {
                        $( "#text-<%=id%>" ).val( ui.item.label );
                        $( "#hidden-<%=id%>" ).val( ui.item.id );

                        return false;
                    }
                });
            });
        </script>

<input type="text" class="form-control" id="text-<%=id%>" value="<%=wlabel%>"<%=getDynamicAttribute()%>/>
<input type="hidden" id="hidden-<%=id%>" name="<%=name%>" value="<%=wid%>"/>
}*/
	}

	protected void inputCollectionWikitty(JspWriter output, String contextPath, String name,
			Wikitty wikitty, FieldType field, String extName , String fieldName)
					throws JspException, IOException {
		String id = wikitty.getWikittyId() + "-" + extName + "-" + fieldName;
		List<Wikitty> values = null;
		if(!emptyField)
			values = wikitty.getFieldAsWikittyList(extName, fieldName, false);

		//        String url = contextPath + "/wikitty-json/search?extension=" + field.getAllowed();
		String url = "";
		if (field.hasAllowed()) {
			url = contextPath + "/wikitty-json/search?extension=" + field.getAllowed();
		} else if (field.hasAllowedQuery()) {
			url = contextPath + "/wikitty-json/search?query=" + field.getAllowedQuery();
		} else {
			url = contextPath + "/wikitty-json/search?query=";
		}

		String sep = "";
        StringBuilder builder = new StringBuilder("[");
		if (values != null) {
			for (Wikitty w :values) {
				if (w != null) {
					builder.append(String.format("%s{id:'%s', label:'%s'}", sep,
							StringEscapeUtils.escapeEcmaScript(w.getWikittyId()),
							StringEscapeUtils.escapeEcmaScript(w.toString())));
					sep = ",";
				}
			}
		}
        builder.append("]");
		String prePopulate = builder.toString();
		/*{
<script type="text/javascript">
$(document).ready(function () {
    $("#text-<%=id%>").tokenInput("<%=url%>", {
         prePopulate: <%=prePopulate%>,
         preventDuplicates: true,
         propertyToSearch: "label"

     });
});
</script>

<input type="text" class="form-control" id="text-<%=id%>" name="<%=name%>"<%=getDynamicAttribute()%>/>
}*/
	}


}

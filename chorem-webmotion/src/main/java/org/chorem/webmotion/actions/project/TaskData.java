package org.chorem.webmotion.actions.project;

/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import org.chorem.ChoremClient;
import org.chorem.entities.Task;
import org.chorem.project.TaskCalculation;

/**
 * Represents a task withe the alert and the info linked to it.
 * Used in a jsp page.
 * @author gwenn
 *
 */
public class TaskData extends TaskCalculation {


    private String alert;
    private String info;

    public TaskData(Task t, ChoremClient client) {
        super(t, client);
        this.calculate();
    }

    public void setAlert(String a) {
        alert = a;
    }

    public String getAlert() {
        return alert;
    }

    public void setInfo(String a) {
        info = a;
    }

    public String getInfo() {
        return info;
    }
}

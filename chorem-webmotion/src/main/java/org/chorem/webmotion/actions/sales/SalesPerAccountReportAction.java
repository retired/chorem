package org.chorem.webmotion.actions.sales;

/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.ChoremClient;
import org.chorem.entities.Accepted;
import org.chorem.entities.Employee;
import org.chorem.entities.Quotation;
import org.debux.webmotion.server.WebMotionController;
import org.debux.webmotion.server.render.Render;
import org.nuiton.wikitty.WikittyClient;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.query.conditions.Aggregate;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jcouteau <couteau@codelutin.com>
 */
public class SalesPerAccountReportAction extends WebMotionController {

    /**
     * Rend le graphe des devis envoyés par mois
     *
     * @param client
     * @return
     */
    public Render sales(ChoremClient client, String from, String to) {

        if (null == from) {
            from = String.valueOf(SalesReportHelper.getFirstYear(client));
        }

        if (null == to) {
            to = String.valueOf(SalesReportHelper.getLastYear());
        }

        Map<Employee, SalesData> salesData = getSalesPerProjectData(from, to, client);

        List<Integer> listAllYearsInChorem = SalesReportHelper.listAllYears(client);

        return renderView("salesReports/salesPerAccountReport.jsp",
                "data", salesData,
                "allYears", listAllYearsInChorem,
                "fromYear", from,
                "toYear", to);
    }

    protected Map<Employee,SalesData> getSalesPerProjectData(String firstYear,
                                                            String lastYear,
                                                            WikittyClient client){

        Date last = SalesReportHelper.getLastDayOfYear(Integer.valueOf(lastYear));
        Date first = SalesReportHelper.getFirstDayOfYear(Integer.valueOf(firstYear));

        Map<Employee,SalesData> salesData = new LinkedHashMap<Employee, SalesData>();

        //a query to get all the accounts
        //FIXME JC 2012-01-26 Really bad to  find all employees and iterate on them :(
        WikittyQuery accountsQuery = new WikittyQueryMaker().and()
                .exteq(Employee.EXT_EMPLOYEE).end();

        List<Employee> accounts = client.findAllByQuery(Employee.class, accountsQuery).getAll();

        //iterate and two queries per account :(
        for (Employee account:accounts){

            SalesData projectData = new SalesData();

            WikittyQuery accountQuery = new WikittyQueryMaker()
                    .select().sum("Quotation.amount").where().and()
                    .eq(Quotation.FQ_FIELD_QUOTATION_CUSTOMER, account)
                    .bw(Accepted.FQ_FIELD_ACCEPTED_ACCEPTEDDATE, first, last)
                    .end();

            Double sales = client.findByQuery(Double.class, accountQuery);

            //TODO JC 2012-01-26 Find a way to replace two queries into one.
            WikittyQuery quotationsQuery = new WikittyQueryMaker().and()
                    .eq(Quotation.FQ_FIELD_QUOTATION_CUSTOMER, account)
                    .bw(Accepted.FQ_FIELD_ACCEPTED_ACCEPTEDDATE, first, last)
                    .end();

            List<Quotation> quotations = client.findAllByQuery(Quotation.class,
                    quotationsQuery).getAll();

            //Rempli la map que si on a des valeurs
            if (null != sales && sales != 0) {
                projectData.setSales(sales);
                projectData.setQuotations(quotations.size());
                salesData.put(account, projectData);
            }
        }

        return salesData;
    }
}

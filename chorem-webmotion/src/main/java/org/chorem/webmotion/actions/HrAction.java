
package org.chorem.webmotion.actions;

/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.ChoremClient;
import org.chorem.entities.EmployeeHR;
import org.chorem.entities.Vacation;
import org.chorem.entities.VacationRequest;
import org.debux.webmotion.server.call.Call;
import org.debux.webmotion.server.render.Render;
import org.nuiton.wikitty.WikittyUtil;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.entities.WikittyImpl;

/**
 *
 * @author ble
 */
public class HrAction extends GenericAction {

    /**
     * to use log facility, just put in your code: log.info(\"...\");
     */
    static private Log log = LogFactory.getLog(HrAction.class);
    private Map<String, Object> params = new HashMap<String, Object>();
    //  mettre isAnswer à true si on arrive en tant qu'administrateur pour saisir une réponse
    private boolean isAnswer = true;

    public Render editVacationRequest(ChoremClient client, String id, String[] extension, Call call) {

        String ids = "";
        String separator = ",";
        boolean newVR = false;
        Wikitty w = client.restore(id);

        // si on ne retrouve pas l'objet demandé, on en edit un nouveau
        if (w == null) {
            w = new WikittyImpl();
            newVR = true;
            ids = w.getWikittyId();
            List<String> extensionNames = Arrays.asList(VacationRequest.EXT_VACATIONREQUEST);
            List<WikittyExtension> newExts =
                    client.restoreExtensionAndDependenciesLastVesion(extensionNames);
            w.addExtension(newExts);            
            client.store(w);
        }

        String[] extVac = StringUtils.split(Vacation.EXT_VACATION);
        // Stocke dans une Map les Wikitty de type Vacation liés à la VacationRequest
        Map<String, List<Wikitty>> wRelated = super.searchAsMap(client, extVac, id, 1);

        // edition VacationRequest existante (pas new)
        if (!newVR) {
            // ajout de l'employeeHR (Request) dans les ids
            ids = w.getFieldAsString(VacationRequest.EXT_VACATIONREQUEST,
                    VacationRequest.FIELD_VACATIONREQUEST_EMPLOYEEREQUEST)
                    + separator + id;
            
        }
        else {
            //extension = extVac;
            Wikitty wVac = new WikittyImpl();
            ids += separator + wVac.getWikittyId();
//            wVac.addToField(Vacation.FQ_FIELD_VACATION_VACATIONREQUEST, id);
//            client.store(wVac); 
        }

        // ajout des id Vacation liés à la VacationRequest
        for (Map.Entry<String, List<Wikitty>> relatedEntry : wRelated.entrySet()) {
            List<Wikitty> relatedWikitties = relatedEntry.getValue();
            int size = relatedWikitties.size();
            for (int ii = 0; ii < size; ii++) {
                String wid = relatedWikitties.get(ii).getWikittyId();
                ids += separator + wid;
            }
        }
        for (String ws : wRelated.keySet()) {
            int size = wRelated.get(ws).size();
            for (int ii = 0; ii < size; ii++) {
                String wid = wRelated.get(ws).get(ii).getWikittyId();
                ids += separator + wid;
            }

        }
        return editMultiRender(client, ids, extension, call, "editVacationRequest.jsp");
    }

    public Render editVacationDiv(ChoremClient client, String ids, String[] extension, Call call) {
        return editMultiRender(client, ids, extension, call, "vacationDiv.jsp");
    }

    public Render saveVacationRequest(ChoremClient client, String[] ids, Call call) {
        log.debug("saveVacationRequest: " + ids);

        String idVR = "";
        String oldReqStatus = "";
        String newReqStatus = "";
        Double vacAmount = 0.0;
        String typeLeave = "";
        Wikitty wEmployeeReq = null;

        String dateDuJour = WikittyUtil.toString((new java.util.Date()));
        this.params = call.getExtractParameters();
        String keyParams = "";

        for (String id : ids) {
            Wikitty w = client.restore(id);
            if (w == null) {
                w = new WikittyImpl();
            }

            if (w.hasExtension(VacationRequest.EXT_VACATIONREQUEST)) {
                oldReqStatus = w.getFieldAsString(VacationRequest.EXT_VACATIONREQUEST, 
                        VacationRequest.FIELD_VACATIONREQUEST_STATUSREQUEST);
                keyParams = id + "." + VacationRequest.FQ_FIELD_VACATIONREQUEST_STATUSREQUEST;
                if (this.params.containsKey(keyParams)) {
                    newReqStatus = StringUtils.join((String[]) this.params.get(keyParams));
                }
            }

            if (w.hasExtension(EmployeeHR.EXT_EMPLOYEEHR)) {
                wEmployeeReq = w;
            }

            // si ce n'est pas une réponse c'est une demande
            if (!this.isAnswer) {
                // on enregistre la date de demande
                updateVacationRequestField(w, VacationRequest.EXT_VACATIONREQUEST,
                        VacationRequest.FIELD_VACATIONREQUEST_DATEREQUEST, dateDuJour);

                // pour chaque période (Vacation) on enregistre le champ vacationRequest pour lier la période à la demande
                updateVacationRequestField(w, Vacation.EXT_VACATION, Vacation.FIELD_VACATION_VACATIONREQUEST, idVR);

            } else {
                // on enregistre la date de réponse
                updateVacationRequestField(w, VacationRequest.EXT_VACATIONREQUEST,
                        VacationRequest.FIELD_VACATIONREQUEST_DATEANSWER, dateDuJour);
                
                // si l'ancien status de la demande est différent de "ACCEPTEE" ou "FERMETURE ANNUELLE"
                // si le nouveau status (réponse) est "ACCEPTEE" : pour chaque période demandée on met à jour les soldes de l'Employé
                if (w.hasExtension(Vacation.EXT_VACATION)) {
                    if ((!oldReqStatus.equals("ACCEPTEE")) && (!oldReqStatus.equals("FERMETURE ANNUELLE"))) {
                    if ((newReqStatus.equals("ACCEPTEE") || (newReqStatus.equals("FERMETURE ANNUELLE"))) && (wEmployeeReq != null)) {

                        keyParams = id + "." + Vacation.FQ_FIELD_VACATION_AMOUNT;
                        vacAmount = 0.0;
                        if (this.params.containsKey(keyParams)) {
                            vacAmount = Double.parseDouble(StringUtils.join((String[]) this.params.get(keyParams)));
                        }

                        keyParams = id + "." + Vacation.FQ_FIELD_VACATION_TYPELEAVE;
                        typeLeave = "";
                        if (this.params.containsKey(keyParams)) {
                            typeLeave = StringUtils.join((String[]) this.params.get(keyParams));
                        }

                        updateAmounts(wEmployeeReq, vacAmount, typeLeave);
                    }
                }
                }
            }
        }
        call.setExtractParameters(this.params);
        return saveMulti(client, ids, call);
    }
    
    // cette méthode est spécifique à VacationRequest : 
    // elle ne s'appuie pas sur la méthode delete de GenericAction (non terminée)
    public Render deleteVacationRequest(ChoremClient client, String id, Call call) {
        log.debug("deleteVacationRequest: " + id); 
        String separator = ",";
        
        Wikitty w = client.restore(id);
        if (w != null) {

            String[] extVac = StringUtils.split(Vacation.EXT_VACATION);
            // Stocke dans une Map les Wikitty de type Vacation liés à la VacationRequest
            Map<String, List<Wikitty>> wRelated = super.searchAsMap(client, extVac, id, 1);
        
            // Parcours les Vacations liées et les supprime
            for (String ws : wRelated.keySet()) {
                int size = wRelated.get(ws).size();
                for (int ii = 0; ii < size; ii++) {
                    String wid = wRelated.get(ws).get(ii).getWikittyId();
                    client.delete(wid);
                }
            }
            // supprime la VacationRequest
            client.delete(id);
        }
        return renderURL("report?report=requestVacation");
        
    }

    // écriture d'une "value" dans un champ "fieldname"
    private void updateVacationRequestField(Wikitty w, String extensionName, String fieldName, String value) {
        if ((w != null) && (w.hasExtension(extensionName))) {
            String cleParams = w.getWikittyId() + "." + extensionName + "." + fieldName;
            this.params.put(cleParams, StringUtils.split(value));
        }
    }

    //
    private void updateAmounts(Wikitty wEmployeeReq, Double amount, String typeLeave) {
        Double eAmount = 0.0;

        if (typeLeave.equals("CONGES ANNUELS")) {
            eAmount = wEmployeeReq.getFieldAsDouble(EmployeeHR.EXT_EMPLOYEEHR,
                    EmployeeHR.FIELD_EMPLOYEEHR_PAIDLEAVE) - amount;
            updateVacationRequestField(wEmployeeReq, EmployeeHR.EXT_EMPLOYEEHR,
                    EmployeeHR.FIELD_EMPLOYEEHR_PAIDLEAVE, Double.toString(eAmount));

        } else if (typeLeave.equals("RTT")) {
            eAmount = wEmployeeReq.getFieldAsDouble(EmployeeHR.EXT_EMPLOYEEHR,
                    EmployeeHR.FIELD_EMPLOYEEHR_RTT) - amount;
            updateVacationRequestField(wEmployeeReq, EmployeeHR.EXT_EMPLOYEEHR,
                    EmployeeHR.FIELD_EMPLOYEEHR_RTT, Double.toString(eAmount));

        } else {
            eAmount = wEmployeeReq.getFieldAsDouble(EmployeeHR.EXT_EMPLOYEEHR,
                    EmployeeHR.FIELD_EMPLOYEEHR_OTHERLEAVE) - amount;
            updateVacationRequestField(wEmployeeReq, EmployeeHR.EXT_EMPLOYEEHR,
                    EmployeeHR.FIELD_EMPLOYEEHR_OTHERLEAVE, Double.toString(eAmount));
        }
    }
}

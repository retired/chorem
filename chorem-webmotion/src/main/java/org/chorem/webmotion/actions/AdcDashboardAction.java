package org.chorem.webmotion.actions;

/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.debux.webmotion.server.WebMotionController;
import org.chorem.ChoremClient;
import org.debux.webmotion.server.render.Render;
import org.chorem.entities.ADC;
import org.chorem.entities.Company;
import org.chorem.entities.CompanyImpl;
import org.chorem.entities.Employee;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.query.WikittyQueryResult;

/**
 * 
 * @author meynier
 * 
 */
public class AdcDashboardAction extends WebMotionController {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(AdcDashboardAction.class);

    /**
     * Return a page with an array with the employees of the specified company
     * 
     * @param client
     *            chorem client
     * @param company
     *            employees will be fetched from this company
     * @return
     */
    public Render employeeFilter(ChoremClient client, Company company, int from, int to) {
        // get all the empoyees from the default company

        List<Company> companies = client.findAllByQuery(Company.class,
                new WikittyQueryMaker().exteq("Company").end()).getAll();

        // Order by name
        WikittyQuery employeeQuery = new WikittyQueryMaker().eq(
                Employee.ELEMENT_FIELD_EMPLOYEE_COMPANY, company).end();

        WikittyQueryResult<Employee> employeeResult = client.findAllByQuery(
                Employee.class, employeeQuery);

        List<EmployeeData> employees = new ArrayList<EmployeeData>();
        Set<Integer> years = new HashSet<Integer>();
        for (Employee e : employeeResult.getAll()) {
            EmployeeData eData = new EmployeeData(e, client, from, to);
            employees.add(eData);
            years.addAll(eData.getAdcPerYear().keySet());
        }
        
        Collections.sort(employees);
        
        
        
        return renderView("dashboardAdc.jsp",
                "employees", employees,
                "from", from,
                "to", to,
                "lastYear", (new GregorianCalendar()).get(Calendar.YEAR) +1,
                "companies", companies, 
                "company",
                new CompanyImpl(client.restore(company.getWikittyId())),
                "years", years,
                "title", "ADC Dashboard");
    }

    /**
     * generate a page to manage the adcs
     * 
     * @param client chorem client
     * @param companyId
     * @param addExtension
     * @param call
     * @return
     */
    public Render requestDashboardAdc(ChoremClient client, String companyId, Integer from, Integer to) {
        Company company = null;
        
        if(from == null) {
            from = (new GregorianCalendar()).get(Calendar.YEAR);
            from--;
        }
        if(to == null) {
            to = (new GregorianCalendar()).get(Calendar.YEAR);
        }

        if (companyId == null)
            company = client.getDefaultCompany();
        else {
            company = new CompanyImpl(client.restore(companyId));
        }
        
        

        return employeeFilter(client, company, from , to);
    }

    
    
  

    /**
     * Structure used to store information about an employee. Used for the AJAX
     * request
     * 
     * @author gwenn
     * 
     */
    public class EmployeeJson {

        private String salary;
        private double productivityRate;
        private double partialTime;
        private double dailyReturn;
        private double dailyHoursWorked;
        private double otherPayments;

        public EmployeeJson(String salary, double productivityRate,
                double partialTime, double dailyReturn,
                double dailyHoursWorked, double otherPayments) {
            this.salary = salary;
            this.productivityRate = productivityRate;
            this.partialTime = partialTime;
            this.dailyReturn = dailyReturn;
            this.dailyHoursWorked = dailyHoursWorked;
            this.otherPayments = otherPayments;
        }
    }

    /**
     * Structure used to store information about an employee. used for the
     * standard http request
     * 
     * @author gwenn
     * 
     */
    public class EmployeeData implements Comparable {

        private Employee e;
        private double estimatedAdc;
        private Map<Integer, Double> adcPerYear;

        public EmployeeData(Employee e, ChoremClient client, int from, int to) {
            Wikitty w = client.restore(e.getWikittyId());

            this.estimatedAdc = client.getDailyReturn(e);
            //Simple trick to limit to 2 numbers after digit
            int x = (int)(client.getDailyHoursWorked(e)*100);
            double y = x/100.0;

            x = (int)(this.estimatedAdc*100);
            y = x/100.0;
            this.estimatedAdc = y;


             WikittyQuery adcQuery = new WikittyQueryMaker().and()
            .eq(ADC.ELEMENT_FIELD_ADC_EMPLOYEEHR, e)
            .bw(ADC.ELEMENT_FIELD_ADC_YEAR, from, to)
            .end();
             adcPerYear = new HashMap<Integer, Double>();
             
             WikittyQueryResult<ADC> adcResult = client.findAllByQuery(
                     ADC.class, adcQuery);
             
             for(ADC a : adcResult.getAll()) {
                 adcPerYear.put(a.getYear(), a.getValue());
             }
             System.out.println("ADC FOR " + e.getPerson(false) + " : " + adcPerYear);
              this.e = e;
        }

        public Employee getObject() {
            return e;
        }

        public double getEstimatedAdc() {
            return estimatedAdc;
        }

        public Map<Integer, Double> getAdcPerYear() {
            return adcPerYear;
        }

        @Override
        public int compareTo(Object o) {
            EmployeeData e = (EmployeeData) o;
            return this.getObject().getPerson(false).getLastName()
                    .compareTo(e.getObject().getPerson(false).getLastName());
        }

    }

}

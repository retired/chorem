package org.chorem.webmotion.actions.sales;

/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.entities.Draft;
import org.nuiton.util.DateUtil;
import org.nuiton.wikitty.WikittyClient;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.query.conditions.Aggregate;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * @author jcouteau <couteau@codelutin.com>
 */
public class SalesReportHelper {

    /**
     * This method return the first day of the year string given in parameter.
     *
     * @param year the year of the expected Date
     * @return a Date object representing the first day of the year given in
     * parameter and the minimum time on this day.
     */
    public static Date getFirstDayOfYear(Integer year){
        Date today = new Date();

        Calendar c = Calendar.getInstance();
        c.setTime(today);
        c.set(Calendar.YEAR, year);

        Date result = c.getTime();
        result = DateUtil.setFirstDayOfYear(result);
        result = DateUtil.setMinTimeOfDay(result);

        return result;

    }

    /**
     * This method return the last day of the year string given in parameter.
     *
     * @param year the year of the expected Date
     * @return a Date object representing the last day of the year given in
     * parameter and the maximum time on this day.
     */
    public static Date getLastDayOfYear(Integer year){
        Date today = new Date();

        Calendar c = Calendar.getInstance();
        c.setTime(today);
        c.set(Calendar.YEAR, year);

        Date result = c.getTime();
        result = DateUtil.setLastDayOfYear(result);
        result = DateUtil.setMaxTimeOfDay(result);

        return result;

    }

    public static List<Integer> listAllYears(WikittyClient client) {

        List<Integer> years = new LinkedList<Integer>();

        for (int i=getFirstYear(client);i<=getLastYear();i++) {
            years.add(i);
        }

        return years;

    }

    public static Integer getLastYear() {
        Calendar c = Calendar.getInstance();
        return c.get(Calendar.YEAR);
    }

    public static Integer getFirstYear(WikittyClient client) {

        //Ventes de l'année
        WikittyQuery firstYearQuery = new WikittyQueryMaker()
                .select().min("Draft.sendingDate").where().and()
                .exteq(Draft.EXT_DRAFT)
                .end();

        Date firstYear = client.findByQuery(Date.class, firstYearQuery);

        Calendar c = Calendar.getInstance();
        if (firstYear != null) { // if no DRAFT available
            c.setTime(firstYear);
        }
        return c.get(Calendar.YEAR);
    }

    public static List<Integer> listAllYears(String begin, String end) {

        int beginYear = Integer.parseInt(begin);
        int lastYear = Integer.parseInt(end);

        List<Integer> years = new LinkedList<Integer>();

        for (int i = beginYear; i<= lastYear; i++) {
            years.add(i);
        }

        return years;

    }
}

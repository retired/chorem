package org.chorem.webmotion.actions;

/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.apache.commons.lang3.ArrayUtils;
import org.nuiton.util.ObjectUtil;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
/**
 * Permet de decore une map dont toutes les valeurs manquant lorsqu'on les
 * demandent sont initialise avec l'objet passe dans le constructeur.
 * L'objet initialise est pousse dans la map avant d'etre retourne.
 *
 * TODO poussin 20120526 move this class to nuiton-utils
 *
 * @param <K>
 * @param <V>
 */
public class MapWithDefault<K, V> implements Map<K, V>, Cloneable {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    protected Map<K, V> map;
    protected Object defaultValue;
    protected Collection args;

    @Override
    public Object clone() throws CloneNotSupportedException {
        MapWithDefault result = (MapWithDefault) super.clone();
        // class cast exception if map is not cloneable
        result.map = (Map) ObjectUtil.clone((Cloneable) map);
        return result;
    }

    /**
     * L'objet par default utilise est l'objet passe en parametre s'il n'est
     * pas clonable ou un clone de l'objet s'il est clonable.
     * @param map la map a decore
     * @param defaultValue la valeur par default
     */
    public MapWithDefault(Map<K, V> map, V defaultValue) {
        this.map = map;
        this.defaultValue = defaultValue;
    }

    /**
     * L'objet par defaut est construit via le constructeur de la classe
     * passe en parametre prenant les arguments args.
     * @param map la map a decore
     * @param defaultValue la class de la valeur par defaut
     * @param args les arguments du construteur pour la classe
     */
    public MapWithDefault(Map<K, V> map, Class<V> defaultValue, Object... args) {
        this.map = map;
        this.defaultValue = defaultValue;
        if (ArrayUtils.isNotEmpty(args)) {
            this.args = Arrays.asList(args);
        }
    }

    /**
     * Get new instance of defaultValue if defaultValue is clonable or
     * return directly defaultValue otherwize.
     * @return
     */
    protected V getDefaultValue() {
        try {
            Object result = defaultValue;
            if (defaultValue instanceof Class) {
                if (args == null) {
                    result = ((Class) defaultValue).newInstance();
                } else {
                    result = ObjectUtil.newInstance((Class) defaultValue, args, true);
                }
            } else if (defaultValue instanceof Cloneable) {
                result = ObjectUtil.clone((Cloneable) defaultValue);
            }
            return (V) result;
        } catch (Exception eee) {
            throw new RuntimeException(eee);
        }
    }

    // on surcharge le get pour creer automatiquement les objets
    @Override
    public V get(Object key) {
        V result = map.get(key);
        if (result == null) {
            result = getDefaultValue();
            put((K) key, result);
        }
        return result;
    }

    public int size() {
        return map.size();
    }

    public boolean isEmpty() {
        return map.isEmpty();
    }

    public boolean containsKey(Object key) {
        return map.containsKey(key);
    }

    public boolean containsValue(Object value) {
        return map.containsValue(value);
    }

    public V put(K key, V value) {
        return map.put(key, value);
    }

    public V remove(Object key) {
        return map.remove(key);
    }

    public void putAll(Map<? extends K, ? extends V> m) {
        map.putAll(m);
    }

    public void clear() {
        map.clear();
    }

    public Set<K> keySet() {
        return map.keySet();
    }

    public Collection<V> values() {
        return map.values();
    }

    public Set<Entry<K, V>> entrySet() {
        return map.entrySet();
    }

}

package org.chorem.webmotion.actions.sales;

/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.time.DateUtils;
import org.chorem.ChoremClient;
import org.chorem.entities.Accepted;
import org.debux.webmotion.server.WebMotionController;
import org.debux.webmotion.server.render.Render;
import org.nuiton.util.DateUtil;
import org.nuiton.wikitty.WikittyClient;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;

/**
 * @author jcouteau <couteau@codelutin.com>
 */
public class SalesReportAction extends WebMotionController {

    /**
     * Rend le graphe des devis envoyés par mois
     *
     * @param client
     * @return
     */
    public Render sales(ChoremClient client, String from, String to) {

        if (null == from) {
            from = String.valueOf(SalesReportHelper.getFirstYear(client));
        }

        if (null == to) {
            to = String.valueOf(SalesReportHelper.getLastYear());
        }

        Map<Integer, QuotationYearData> data = new LinkedHashMap<Integer, QuotationYearData>();

        List<Integer> listAllYears = SalesReportHelper.listAllYears(from, to);

        List<Integer> listAllYearsInChorem = SalesReportHelper.listAllYears(client);

        int previousYearValue = 0;

        for (Integer year:listAllYears){
            Date yearFirstDay = SalesReportHelper.getFirstDayOfYear(year);
            Date yearLastDay =  SalesReportHelper.getLastDayOfYear(year);

            QuotationYearData yearData = new QuotationYearData();

            //Ventes de l'année
            WikittyQuery salesQuery = new WikittyQueryMaker()
                    .select().sum("Quotation.amount").where().and()
                    .exteq(Accepted.EXT_ACCEPTED)
                    .bw(Accepted.FQ_FIELD_ACCEPTED_ACCEPTEDDATE, yearFirstDay, yearLastDay)
                    .end();

            Integer sales = client.findByQuery(Integer.class, salesQuery);

            //Progression devis envoyés
            int salesProgression = 0;
            if (previousYearValue != 0){
                salesProgression = 100 * (sales - previousYearValue) / previousYearValue;
            }

            previousYearValue = sales;

            //Graphe devis envoyés
            Map<String, Integer> sentQuotationData = getSalesData(year, client);

            yearData.setBaseValue(sales);
            yearData.setProgression(salesProgression);
            yearData.setPlotValues(sentQuotationData);

            data.put(year, yearData);
        }

        return renderView("salesReports/salesReport.jsp",
                "data", data,
                "allYears", listAllYearsInChorem,
                "fromYear", from,
                "toYear", to);
    }

    protected Map<String,Integer> getSalesData(Integer year, WikittyClient client){

        Date first = SalesReportHelper.getFirstDayOfYear(year);

        Map<String,Integer> salesData = new LinkedHashMap<String, Integer>();

        //a query per month :(
        Date baseValue = first;
        for (int i=0;i<12;i++){
            Date lastDayOfMonth = DateUtil.setLastDayOfMonth(baseValue);
            Date firstDayOfMonth = DateUtil.setFirstDayOfMonth(baseValue);
            baseValue= DateUtils.addDays(lastDayOfMonth, 1);

            WikittyQuery monthQuery = new WikittyQueryMaker()
                    .select().sum("Quotation.amount").where().and()
                    .bw(Accepted.FQ_FIELD_ACCEPTED_ACCEPTEDDATE, firstDayOfMonth, lastDayOfMonth)
                    .end();

            Integer sales = client.findByQuery(Integer.class, monthQuery);

            salesData.put(
                    DateUtil.getMonthLibelle(DateUtil.getMonth(lastDayOfMonth) + 1),
                    sales);
        }

        return salesData;
    }

}

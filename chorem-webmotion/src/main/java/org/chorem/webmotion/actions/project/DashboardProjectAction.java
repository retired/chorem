package org.chorem.webmotion.actions.project;

/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.debux.webmotion.server.WebMotionController;
import org.chorem.ChoremClient;
import org.chorem.ChoremUtil;
import org.debux.webmotion.server.render.Render;
import org.chorem.entities.Closed;
import org.chorem.entities.Employee;
import org.chorem.entities.Interval;
import org.chorem.entities.Quotation;
import org.chorem.entities.Project;
import org.chorem.entities.Task;
import org.chorem.entities.Time;
import org.chorem.project.QuotationCalculation;
import org.chorem.project.TotalQuotationCalculation;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.query.WikittyQueryResult;

/**
 * Displays a page that shows one or many quotations, with financial tables and a gantt diagram.
 * @author meynier
 *
 */
public class DashboardProjectAction extends WebMotionController {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(DashboardProjectAction.class);

    /**
     * Return the view for a single quotation
     * @param client Chorem client
     * @param id Project ID (useless here)
     * @param quotationFilter Quotation id
     * @return
     */
    public Render singleQuotationFilter(ChoremClient client, String id, String quotationFilter) {
        Render render = null;
        //Fetch the quotation from the filter
        WikittyQuery quotationQuery =  new WikittyQueryMaker().ideq(quotationFilter).end();
        WikittyQueryResult<Quotation> quotationResult = client.findAllByQuery(Quotation.class, quotationQuery);

        Map<Project, List<QuotationData>> projectData = new HashMap<Project, List<QuotationData>>();

        //If some quotation has been found
        if(quotationResult != null && quotationResult.size() != 0) {
            Quotation quotation = quotationResult.get(0);

            //Fetch the quotation's project
            WikittyQuery projectQuery =  new WikittyQueryMaker().ideq(quotation.getProject()).end();
            WikittyQueryResult<Project> projectResult = client.findAllByQuery(Project.class, projectQuery);

            List<QuotationData> q = new ArrayList<QuotationData>();
            q.add(new QuotationData(quotation, client));
            projectData.put(projectResult.get(0), q);

            render = renderView("dashboardSingleProject.jsp",
                    "locale", client.getUserLocale(),
                    "title", "Tableau de bord projet",
                    "projects", projectData);

        }
        else {
            render = renderView("dashboardSingleProject.jsp");
        }
        return render;
    }

    /**
     * Display multiple quotation for one or more projects
     * @param client chorem client
     * @param id Project id
     * @param quotationFilter filter (open quotation or all)
     * @return
     */
    public Render projectFilter(ChoremClient client, String id, String quotationFilter) {

        WikittyQueryResult<Project> projectResult = null;
        WikittyQueryResult<Quotation> quotationResult = null;


        Map<Project, List<QuotationData>> projectData = new HashMap<Project, List<QuotationData>>();

        //Fetch the projects from the id or the filter
        WikittyQueryMaker projectQueryMaker = new WikittyQueryMaker();
        if(id != null && !id.equals(""))
            projectQueryMaker.ideq(id);
        else
            projectQueryMaker.exteq(Project.EXT_PROJECT);
        WikittyQuery projectQuery = projectQueryMaker.end();

        projectResult =  client.findAllByQuery(Project.class, projectQuery);

        //Fetch the quotations from the projects
        if(projectResult.size() != 0) {
            for(Project project : projectResult.getAll()) {
                WikittyQuery quotationQuery = null;
                WikittyQueryMaker wqm = new WikittyQueryMaker();


                if(quotationFilter.equals("open")) {
                    wqm.and()
                    .eq(Quotation.ELEMENT_FIELD_QUOTATION_PROJECT, project)
                    .extne(Closed.EXT_CLOSED);
                }
                else if (quotationFilter.equals("all")){
                    wqm.eq(Quotation.ELEMENT_FIELD_QUOTATION_PROJECT, project);
                }


                quotationQuery = wqm.end();
                quotationQuery.setLimit(20);
                quotationQuery.addSortDescending(Quotation.ELEMENT_FIELD_INTERVAL_BEGINDATE);
                quotationResult = client.findAllByQuery(Quotation.class, quotationQuery);

                //Fetch the tasks form the quotations (if there are some)
                if(quotationResult != null && quotationResult.size() != 0) {
                    List<QuotationData> quotation = new ArrayList<QuotationData>();
                    for(Quotation quote : quotationResult.getAll()) {
                        quotation.add(new QuotationData(quote, client));
                    }
                    projectData.put(project, quotation);
                }
            }
        }

        return renderView("dashboardSingleProject.jsp",
                "locale", client.getUserLocale(),
                "title", "Tableau de bord projet",
                "projects", projectData);

    }




    /**
     * Generates the multi-project page 
     * @param client chorem client
     * @param from begin date for the interval
     * @param to end date for the interval
     * @return
     */
    public Render multiProjectFilter(ChoremClient client, Date from, Date to) {
        System.out.println(from  + "," + to);
        if(from == null || to == null) {
            Calendar now = new GregorianCalendar();
            Calendar gFrom = new GregorianCalendar(now.get(Calendar.YEAR), now.get(Calendar.MONTH)
                    , now.getActualMinimum(Calendar.DAY_OF_MONTH));
            Calendar gTo = new GregorianCalendar(now.get(Calendar.YEAR), now.get(Calendar.MONTH)
                    , now.getActualMaximum(Calendar.DAY_OF_MONTH));
            from = gFrom.getTime();
            to = gTo.getTime();

        }


        WikittyQueryMaker quotationQueryMaker = new WikittyQueryMaker();
        WikittyQuery quotationQuery = quotationQueryMaker.or()
                .bw(Interval.FQ_FIELD_INTERVAL_BEGINDATE, from, to)
                .bw(Interval.FQ_FIELD_INTERVAL_ENDDATE, from, to)
                .and()
                .le(Interval.FQ_FIELD_INTERVAL_BEGINDATE, from)
                .ge(Interval.FQ_FIELD_INTERVAL_ENDDATE, to)
                .end();
        WikittyQueryResult<Quotation> result =
                client.findAllByQuery(Quotation.class, quotationQuery);

        Collection<Quotation> quotations = result.getAll();

        Map<Quotation, QuotationCalculation> calculations = new HashMap<Quotation, QuotationCalculation>();
        for(Quotation q : quotations) {
            QuotationCalculation calc = new QuotationCalculation(q, client);
            calc.calculate();
            calculations.put(q, calc);
        }

        TotalQuotationCalculation total = new TotalQuotationCalculation(
                new ArrayList<QuotationCalculation>(calculations.values()));



        return renderView("dashboardMultiProject.jsp",
                "title", "Tableau de bord projets",
                "locale", client.getUserLocale(),
                "quotations", quotations,
                "calculations", calculations,
                "total", total);


    }

    /**
     * Generate the employee page
     * @param client
     * @param id
     * @param from
     * @param to
     * @param quotationFilters
     * @return
     */
    public Render employeeFilter(ChoremClient client, String id, Date from, Date to, String[] quotationFilters) {

        //If no field has been set, sets the date to the actual month
        if(from == null || to == null) {
            Calendar now = new GregorianCalendar();
            Calendar gFrom = new GregorianCalendar(now.get(Calendar.YEAR), now.get(Calendar.MONTH)
                    , now.getActualMinimum(Calendar.DAY_OF_MONTH));
            Calendar gTo = new GregorianCalendar(now.get(Calendar.YEAR), now.get(Calendar.MONTH)
                    , now.getActualMaximum(Calendar.DAY_OF_MONTH));
            from = gFrom.getTime();
            to = gTo.getTime();
        }

        //Fetch the total list of employee (or just the chosen one
        WikittyQueryMaker employeeQueryMaker =  new WikittyQueryMaker();
        if(id != null && !id.equals(""))
            employeeQueryMaker.ideq(id);
        else {
            employeeQueryMaker.exteq(Employee.EXT_EMPLOYEE);
        }
        WikittyQueryResult<Employee> employeeResult = client.findAllByQuery(Employee.class,employeeQueryMaker.end());
        List<Employee> employeeList = employeeResult.getAll();

        //Fetch all the tasks on the interval
        WikittyQueryMaker taskQueryMaker = new WikittyQueryMaker()
        .and()
        .exteq(Task.EXT_TASK);

        if(quotationFilters!=null) {
            taskQueryMaker.or();
            for(String q : quotationFilters) {
                taskQueryMaker		.eq(Task.FQ_FIELD_TASK_QUOTATION,q);
                System.out.println(client.restore(q));}
            taskQueryMaker.close();
        }

        taskQueryMaker		.or()
        .bw(Interval.FQ_FIELD_INTERVAL_BEGINDATE, from, to)
        .bw(Interval.FQ_FIELD_INTERVAL_ENDDATE, from, to)
        .and()
        .le(Interval.FQ_FIELD_INTERVAL_BEGINDATE, from)
        .ge(Interval.FQ_FIELD_INTERVAL_ENDDATE, to);

        WikittyQuery taskQuery = taskQueryMaker.end();
        System.out.println(taskQuery);
        List<Task> taskList = client.findAllByQuery(Task.class,taskQuery).getAll();
        System.out.println("RESULT : " + taskList);
        //Fetch the employee's workers on those tasks



        //----------------------------------------
        //Fetch the real time spent on each project/quotation
        HashMap<Employee, Double> timeTotals = new HashMap<Employee, Double>();
        HashMap<Employee, HashMap<Quotation, Double>> timePerQuotation = new HashMap<Employee, HashMap<Quotation, Double>>();
        for(Task task : taskList) {

            WikittyQueryMaker timeQueryMaker = new WikittyQueryMaker().and()
                    .eq(Time.ELEMENT_FIELD_TIME_TASK, task);
            if(employeeList.size() == 1)
                timeQueryMaker.eq(Time.ELEMENT_FIELD_TIME_EMPLOYEE, employeeList.get(0));
            WikittyQuery timeQuery = timeQueryMaker.end();
            WikittyQueryResult<Time> timeResult = client.findAllByQuery(Time.class, timeQuery);



            for(Time time : timeResult.getAll()) {
                Employee emp = time.getEmployee(false);

                //TOTAL TIMES
                if(timeTotals.containsKey(emp))
                    timeTotals.put(emp, timeTotals.get(emp) + ChoremUtil.getPeriodInHours(time.getBeginDate(), time.getEndDate()));
                else
                    timeTotals.put(emp, ChoremUtil.getPeriodInHours(time.getBeginDate(), time.getEndDate()));

                HashMap<Quotation, Double> timeQ = null;
                //TIME PER QUOTATION
                if(timePerQuotation.containsKey(emp)) {
                    timeQ = timePerQuotation.get(emp);
                }
                else {
                    timeQ = new HashMap<Quotation, Double>();
                    timePerQuotation.put(emp, timeQ);
                }
                Quotation qKey = task.getQuotation(false);
                if(timeQ.containsKey(qKey)) {
                    timeQ.put(qKey, timeQ.get(qKey) + 
                            ChoremUtil.getPeriodInHours(time.getBeginDate(), time.getEndDate()));
                }
                else {
                    timeQ.put(qKey,
                            ChoremUtil.getPeriodInHours(time.getBeginDate(), time.getEndDate()));
                }

            }
        }
        Set<Employee> kSet = timePerQuotation.keySet();
        Set<Quotation> quotations = new HashSet<Quotation>();
        for(Employee e : kSet) {
            quotations.addAll(timePerQuotation.get(e).keySet());
        }
        List<Quotation> allQuotations = new ArrayList<Quotation>();
        WikittyQuery quotationQuery = new WikittyQueryMaker().or()
                .bw(Interval.FQ_FIELD_INTERVAL_BEGINDATE, from, to)
                .bw(Interval.FQ_FIELD_INTERVAL_ENDDATE, from, to)
                .and()
                .le(Interval.FQ_FIELD_INTERVAL_BEGINDATE, from)
                .ge(Interval.FQ_FIELD_INTERVAL_ENDDATE, to).end();
        allQuotations = client.findAllByQuery(Quotation.class, quotationQuery).getAll();

        HashMap<Employee, HashMap<Quotation, Double>> percentages = new HashMap<Employee, HashMap<Quotation, Double>>();

        for(Employee e : kSet) {
            Set<Quotation> qSet = timePerQuotation.get(e).keySet();
            HashMap<Quotation, Double> p = new HashMap<Quotation, Double>();
            for(Quotation q : qSet) {

                p.put(q, ( timePerQuotation.get(e).get(q) / timeTotals.get(e) ) *100  );

            }
            percentages.put(e, p);
        }


        //Calculates the average working time

        Calendar gFrom = new GregorianCalendar();
        Calendar gTo = new GregorianCalendar();
        gFrom.setTime(from);
        gTo.setTime(to);

        return renderView("dashboardEmployee.jsp",
                "title", "Tableau de bord employé",
                "locale", client.getUserLocale(),
                "timePerQuotation", timePerQuotation,
                "quotations", quotations,
                "percentages", percentages,
                "timeTotals", timeTotals,
                "allQuotations", allQuotations);

    }

    /**
     * Request the generation of the single-projetc page
     * @param client chorem client
     * @param project_name project name
     * @param project_id project id
     * @param quotationFilter quotation filter
     * @return the project page
     */
    public Render requestProject(ChoremClient client, String project_name, String project_id, String quotationFilter) {
        if(quotationFilter == null)
            quotationFilter = "open";
        if(project_name == null || project_name.equals(""))
            project_id = "";
        Render render = null;
        if(quotationFilter.equals("open") || quotationFilter.equals("all")) {
            render = projectFilter(client, project_id, quotationFilter);
        }
        else {
            render = singleQuotationFilter(client, project_id, quotationFilter);
        }
        return render;
    }
    /**
     * Request the generation of the multi project page
     * @param client chorem client
     * @param from start date
     * @param to end date
     * @return the multi project page
     */
    public Render requestMultiProject(ChoremClient client, Date from, Date to) {
        return multiProjectFilter(client, from, to);
    }

    /**
     * request the generation of the employee page
     * @param client chorem client
     * @param id employee id
     * @param from start date
     * @param to end date
     * @param quotations quotation filter
     * @return the employee page
     */
    public Render requestEmployee(ChoremClient client, String id, Date from, Date to, String[] quotations) {
        System.out.println("QUOTATIONS : " + Arrays.toString(quotations));
        return employeeFilter(client, id, from, to, quotations);
    }


    private class IntervalSorter<T extends Interval> implements Comparator {

        @Override
        public int compare(Object q1, Object q2) {

            return ((T)q1).getBeginDate().compareTo(((T)q2).getBeginDate());
        }

    }


}

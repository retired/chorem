package org.chorem.webmotion.actions.financial;

/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.ChoremClient;
import org.chorem.ChoremQueryMaker;
import org.chorem.entities.FinancialTransaction;
import org.chorem.entities.Project;
import org.chorem.entities.Quotation;
import org.chorem.webmotion.actions.sales.SalesData;
import org.debux.webmotion.server.WebMotionController;
import org.debux.webmotion.server.render.Render;
import org.nuiton.wikitty.entities.Element;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.query.WikittyQuery;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * @author jcouteau <couteau@codelutin.com>
 */
public class BillingPerProjectReportAction extends WebMotionController {

    /**
     * Rend le graphe des devis envoyés par mois
     *
     * @param client
     * @return
     */
    public Render billing(ChoremClient client, String from, String to) {

        if (null == from) {
            from = String.valueOf(BillingReportHelper.getFirstYear(client));
        }

        if (null == to) {
            to = String.valueOf(BillingReportHelper.getLastYear());
        }

        Map<Project, SalesData> billingData = getBillingPerProjectData(from, to, client);

        Double total = 0.0;

        for (Map.Entry<Project, SalesData> entry : billingData.entrySet()) {
            total += entry.getValue().getSales();
        }

        List<Integer> listAllYearsInChorem = BillingReportHelper.listAllYears(client);

        return renderView("financial/reports/billingPerProjectReport.jsp",
                "data", billingData,
                "total", total,
                "allYears", listAllYearsInChorem,
                "fromYear", from,
                "toYear", to);
    }

    protected Map<Project,SalesData> getBillingPerProjectData(String firstYear,
                                                         String lastYear,
                                                         ChoremClient client){

        Date last = BillingReportHelper.getLastDayOfYear(Integer.valueOf(lastYear));
        Date first = BillingReportHelper.getFirstDayOfYear(Integer.valueOf(firstYear));

        Map<Project,SalesData> billingData = new LinkedHashMap<>();

        String companyId = client.getConfiguration().getDefaultCompany();

        //get all the bills between first and last
        WikittyQuery billsQuery = new ChoremQueryMaker().and()
                .bw(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EMITTEDDATE, first, last)
                .containsOne(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_BENEFICIARY)
                .select(Element.ID).filterOnCompanyOrEmployee(companyId)
                .end().setLimit(WikittyQuery.MAX);

        List<FinancialTransaction> bills = client.findAllByQuery(FinancialTransaction.class,
                billsQuery.setLimit(WikittyQuery.MAX)).getAll();

        Map<String,SalesData> billingDataPerId = new LinkedHashMap<>();

        Set<String> targetsId = new HashSet<>();
        for (FinancialTransaction bill:bills){
            String target = bill.getTarget();
            if (target != null) {
                targetsId.add(target);
            }
        }

        List<Wikitty> targetsWikitty = client.restore(new ArrayList<>(targetsId));

        Map<String,Project> projectsById = new HashMap<>();

        List<String> projectsId = new ArrayList<>();

        Map <String, Wikitty> wikittyById = new HashMap<>();

        for (Wikitty wkt:targetsWikitty){
            if (wkt.hasExtension(Project.EXT_PROJECT)){
                projectsById.put(wkt.getWikittyId(), client.castTo(Project.class, wkt));
            } else if (wkt.hasExtension(Quotation.EXT_QUOTATION)){
                projectsId.add(wkt.getFieldAsWikitty(Quotation.EXT_QUOTATION, Quotation.FIELD_QUOTATION_PROJECT));
                wikittyById.put(wkt.getWikittyId(), wkt);
            }
        }

        List<Project> projects = client.restore(Project.class, projectsId, false);
        for (Project project:projects){
            if (project != null) {
                projectsById.put(project.getWikittyId(), project);
            }
        }

        //group bills by project and sum values
        for (FinancialTransaction bill:bills){
            String targetId = bill.getTarget();
            SalesData projectData;

            //if target is a quotation, replace targetId with the quotation project
            if (!projectsById.containsKey(targetId)) {
                Wikitty wkt = wikittyById.get(targetId);
                if (wkt != null){
                    targetId = wkt.getFieldAsWikitty(Quotation.EXT_QUOTATION, Quotation.FIELD_QUOTATION_PROJECT);
                }
            }

            if (billingDataPerId.containsKey(targetId)) {
                projectData = billingDataPerId.get(targetId);
            } else {
                projectData = new SalesData();
            }

            projectData.setSales(projectData.getSales() + bill.getAmount());
            projectData.setQuotations(projectData.getQuotations() + 1);
            billingDataPerId.put(targetId, projectData);

        }

        //populate the output
        for (Map.Entry<String, SalesData> entry : billingDataPerId.entrySet()) {
            Project project = projectsById.get(entry.getKey());
            if (project != null) {
                billingData.put(project, entry.getValue());
            }
        }

        return billingData;
    }
}

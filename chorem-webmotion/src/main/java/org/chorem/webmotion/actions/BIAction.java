package org.chorem.webmotion.actions;

/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.LinkedHashSet;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.ChoremClient;
import org.chorem.ChoremUtil;
import org.debux.webmotion.server.WebMotionController;
import org.debux.webmotion.server.render.Render;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class BIAction extends WebMotionController {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(BIAction.class);

    public Render reportDefinition(ChoremClient client, String id, String[] extension) {
        log.debug("reportDefinition: " + id);
        Wikitty w = client.restore(id, ".*");

        LinkedHashSet<WikittyExtension> exts = new LinkedHashSet<WikittyExtension>();
        if (w != null) {
            if (extension == null) {
                exts.addAll(w.getExtensions());
            } else {
                // Display the extensions and required ones for them
                List<String> extensionNames = ChoremUtil.asList(null, extension);
                List<WikittyExtension> wikittyExtensions = client.restoreExtensionAndDependenciesLastVesion(extensionNames);
                exts.addAll(wikittyExtensions);
            }
        }


        return renderView("bi/reportDefinition.jsp", "wikitty", w, "extensions", exts);
    }

}

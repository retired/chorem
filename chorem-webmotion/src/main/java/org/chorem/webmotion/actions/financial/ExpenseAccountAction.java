package org.chorem.webmotion.actions.financial;

/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.chorem.ChoremClient;
import org.chorem.entities.Category;
import org.chorem.entities.Employee;
import org.chorem.entities.ExpenseAccount;
import org.chorem.entities.ExpenseAccountEntry;
import org.chorem.entities.ExpenseAccountEntryImpl;
import org.chorem.entities.ExpenseAccountImpl;
import org.chorem.entities.Project;
import org.chorem.entities.Quotation;
import org.chorem.webmotion.PaginatedResult;
import org.chorem.webmotion.bean.financial.ExpenseAccountBean;
import org.chorem.webmotion.bean.financial.ExpenseAccountEntryBean;
import org.debux.webmotion.server.WebMotionController;
import org.debux.webmotion.server.render.Render;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.query.WikittyQueryResult;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class ExpenseAccountAction extends WebMotionController {

    /**
     * Find all {@link org.chorem.entities.ExpenseAccount} and return their as a {@link org.chorem.webmotion.PaginatedResult} of {@code ExpenseAccountBean}.
     *
     * The result is paginated and can be filtered as following :
     * <ul>
     * <li>corresponding to a specific month/year : all expense accounts covering this month are returned ;</li>
     * <li>corresponding to a specific year : in this case, all the ExpenseAccount covering a part of the year are returned;</li>
     * <li>no year and no month given : in this case, all the ExpenseAccount covering current month are returned;</li>
     * </ul>
     *
     * @param client
     * @param startTime
     * @param endTime
     * @param page
     * @param count
     * @return
     */
    public PaginatedResult<ExpenseAccountBean> findAllExpenseAccounts(ChoremClient client, Long startTime, Long endTime, int page, int count) {

        WikittyQueryMaker totalQueryMaker = new WikittyQueryMaker()
            .select().count(ExpenseAccount.ELEMENT_FIELD_INTERVAL_BEGINDATE).where().and()
            .exteq(ExpenseAccount.EXT_EXPENSEACCOUNT);

        if (endTime != null) {
            Date refEndDate = new Date(endTime);
            totalQueryMaker.le(ExpenseAccount.ELEMENT_FIELD_INTERVAL_BEGINDATE, refEndDate);
        }
        if (startTime != null) {
            Date refStartDate = new Date(startTime);
            totalQueryMaker.ge(ExpenseAccount.ELEMENT_FIELD_INTERVAL_ENDDATE, refStartDate);
        }

        WikittyQuery totalQuery = totalQueryMaker.end();

        Integer nbExpenseAccounts = client.findByQuery(Integer.class, totalQuery);

        PaginatedResult<ExpenseAccountBean> paginatedResult;
        if (nbExpenseAccounts > 0) {

            WikittyQueryMaker listingQueryMaker = new WikittyQueryMaker()
                .where().and()
                .exteq(ExpenseAccount.EXT_EXPENSEACCOUNT);

            if (endTime != null) {
                Date refEndDate = new Date(endTime);
                listingQueryMaker.le(ExpenseAccount.ELEMENT_FIELD_INTERVAL_BEGINDATE, refEndDate);
            }
            if (startTime != null) {
                Date refStartDate = new Date(startTime);
                listingQueryMaker.ge(ExpenseAccount.ELEMENT_FIELD_INTERVAL_ENDDATE, refStartDate);
            }

            WikittyQuery expAccountsQuery = listingQueryMaker.end()
                .setOffset((page - 1) * count)
                .setLimit(page * count);

            WikittyQueryResult<ExpenseAccount> expenseAccounts = client.findAllByQuery(ExpenseAccount.class, expAccountsQuery);

            List<ExpenseAccount> expenseAccountList = expenseAccounts.getAll();
            Collection<ExpenseAccountBean> expenseAccountBeans = Collections2.transform(expenseAccountList, TRANSFORM_EXPENSE_ACCOUNT_TO_BEAN);
            paginatedResult = new PaginatedResult<>(Lists.newArrayList(expenseAccountBeans), page, count, nbExpenseAccounts);

        } else {
            paginatedResult = new PaginatedResult<>(Collections.EMPTY_LIST, 1, 0, 0);
        }

        return paginatedResult;
    }

    public void saveExpenseAccount(ChoremClient client, String expenseAccountId, ExpenseAccountBean expenseAccountBean) {

        ExpenseAccountImpl expenseAccountWikitty = client.restore(ExpenseAccountImpl.class, expenseAccountId);

        if (expenseAccountWikitty == null) {
            expenseAccountWikitty = new ExpenseAccountImpl();
        }

        // manage employee
        String employeeId = expenseAccountBean.getEmployeeId();
        Employee employee = client.restore(Employee.class, employeeId);
        expenseAccountWikitty.setEmployee(employee);

        expenseAccountWikitty.setBeginDate(new Date(expenseAccountBean.getStartDate()));
        expenseAccountWikitty.setEndDate(new Date(expenseAccountBean.getEndDate()));

        Set<String> existingExpenseEntryIds = expenseAccountWikitty.getExpenseAccountEntry();
        Set<String> missingAccountEntryIds;
        if (existingExpenseEntryIds != null) {
            missingAccountEntryIds = Sets.newHashSet(existingExpenseEntryIds);
        } else {
            missingAccountEntryIds = new HashSet<>();
        }

        expenseAccountWikitty.clearExpenseAccountEntry();

        //manage each Expense Account Entry : try to find them or create new one
        Set<ExpenseAccountEntryBean> expenseAccountEntryBeans = expenseAccountBean.getExpenseAccountEntries();
        if (expenseAccountEntryBeans != null) {
            List<ExpenseAccountEntry> expenseAccountEntries = new ArrayList<>(expenseAccountEntryBeans.size());

            for (ExpenseAccountEntryBean expenseAccountEntryBean : expenseAccountEntryBeans) {
                String expenseAccountEntryBeanId = expenseAccountEntryBean.getId();
                ExpenseAccountEntry expenseAccountEntry = client.restore(ExpenseAccountEntry.class, expenseAccountEntryBeanId);
                if (expenseAccountEntry == null) {
                    expenseAccountEntry = new ExpenseAccountEntryImpl();
                } else {
                    // remove the Id from the list of missing entries
                    missingAccountEntryIds.remove(expenseAccountEntryBeanId);
                }

                expenseAccountEntry.setReference(expenseAccountEntryBean.getJustificationNumber());
                expenseAccountEntry.setEmittedDate(new Date(expenseAccountEntryBean.getEmittedDate()));
                expenseAccountEntry.setDescription(expenseAccountEntryBean.getDescription());
                expenseAccountEntry.setAmount(expenseAccountEntryBean.getAmount());
                expenseAccountEntry.setVAT(expenseAccountEntryBean.getVAT());

                String projectId = expenseAccountEntryBean.getProjectId();
                if (StringUtils.isNotBlank(projectId)) {
                    Wikitty project = client.restore(projectId);
                    expenseAccountEntry.setTarget(project);
                }

                String categoryId = expenseAccountEntryBean.getCategoryId();
                if (StringUtils.isNotBlank(categoryId)) {
                    Category category = client.restore(Category.class, categoryId);
                    expenseAccountEntry.setCategory(category);
                }
                expenseAccountEntries.add(expenseAccountEntry);

            }
            // Store the entries & put them in Expense Account
            client.store(expenseAccountEntries);
            expenseAccountWikitty.addAllExpenseAccountEntryEntity(expenseAccountEntries);
        }
        // Store the Expense Account
        client.store(expenseAccountWikitty);

        // Delete old entries
        client.delete(missingAccountEntryIds);
    }

    public Render findExpenseAccount(ChoremClient client, String expenseAccountId) {
        ExpenseAccount expenseAccount = client.restore(ExpenseAccount.class, expenseAccountId);

        if (expenseAccount == null) {
            expenseAccount = new ExpenseAccountImpl();
        }

        ExpenseAccountBean expenseAccountBean = TRANSFORM_EXPENSE_ACCOUNT_TO_BEAN.apply(expenseAccount);

        return renderView("financial/expenseAccountView.jsp", "expenseAccount", expenseAccountBean);
    }


    protected Function<ExpenseAccount, ExpenseAccountBean> TRANSFORM_EXPENSE_ACCOUNT_TO_BEAN = new Function<ExpenseAccount, ExpenseAccountBean>() {
        @Override
        public ExpenseAccountBean apply(ExpenseAccount expenseAccount) {
            ExpenseAccountBean expenseAccountBean = new ExpenseAccountBean();
            expenseAccountBean.setId(expenseAccount.getWikittyId());
            Employee employee = expenseAccount.getEmployee(false);
            expenseAccountBean.setEmployeeName(employee != null ? employee.toString() : null);
            expenseAccountBean.setEmployeeId(employee != null ? employee.getWikittyId() : null);
            expenseAccountBean.setStartDate(expenseAccount.getBeginDate());
            expenseAccountBean.setEndDate(expenseAccount.getEndDate());
            expenseAccountBean.setEndDate(expenseAccount.getEndDate());
            Set<ExpenseAccountEntry> expenseAccountEntries = expenseAccount.getExpenseAccountEntry(false);
            if (expenseAccountEntries == null) {
                expenseAccountBean.setExpenseAccountEntries(new HashSet<ExpenseAccountEntryBean>());
            } else {
                for (ExpenseAccountEntry expenseAccountEntry : expenseAccountEntries) {
                    if (expenseAccountEntry != null) {
                        ExpenseAccountEntryBean expenseAccountEntryBean = TRANSFORM_EXPENSE_ACCOUNT_ENTRY_TO_BEAN.apply(expenseAccountEntry);
                        expenseAccountBean.addExpenseAccountEntry(expenseAccountEntryBean);
                    }
                }
            }
            return expenseAccountBean;
        }
    };

    protected Function<ExpenseAccountEntry, ExpenseAccountEntryBean> TRANSFORM_EXPENSE_ACCOUNT_ENTRY_TO_BEAN = new Function<ExpenseAccountEntry, ExpenseAccountEntryBean>() {

        @Override
        public ExpenseAccountEntryBean apply(ExpenseAccountEntry expenseAccountEntry) {
            ExpenseAccountEntryBean expenseAccountEntryBean = new ExpenseAccountEntryBean();
            expenseAccountEntryBean.setId(expenseAccountEntry.getWikittyId());

            Category category = expenseAccountEntry.getCategory(false);
            if (category != null) {
                expenseAccountEntryBean.setCategoryId(category.getWikittyId());
                expenseAccountEntryBean.setCategoryName(category.getName());
            }

            Wikitty project = expenseAccountEntry.getTarget(false);
            if (project != null) {
                expenseAccountEntryBean.setProjectId(project.getWikittyId());
                expenseAccountEntryBean.setProjectName(project.getFieldAsWikitty(Project.EXT_PROJECT, Project.FIELD_PROJECT_NAME));
            }

            Date emittedDate = expenseAccountEntry.getEmittedDate();
            if (emittedDate != null) {
                expenseAccountEntryBean.setEmittedDate(new Date(emittedDate.getTime()));
            }

            Date paymentDate = expenseAccountEntry.getPaymentDate();
            if (paymentDate != null) {
                expenseAccountEntryBean.setPaymentDate(new Date(paymentDate.getTime()));
            }

            expenseAccountEntryBean.setDescription(expenseAccountEntry.getDescription());
            expenseAccountEntryBean.setJustificationNumber(expenseAccountEntry.getReference());

            double amount = expenseAccountEntry.getAmount();
            double vat = expenseAccountEntry.getVAT();
            expenseAccountEntryBean.setAmount(amount);
            expenseAccountEntryBean.setVAT(vat);
            expenseAccountEntryBean.setTotal(amount + vat);

            Quotation quotation = expenseAccountEntry.getQuotation(false);
            if (quotation != null) {
                expenseAccountEntryBean.setProjectId(quotation.getWikittyId());
                expenseAccountEntryBean.setProjectName(quotation.toString());
            }
            return expenseAccountEntryBean;
        }
    };
}

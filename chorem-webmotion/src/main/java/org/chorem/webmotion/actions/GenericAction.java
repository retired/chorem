/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem.webmotion.actions;

import com.google.common.collect.LinkedHashMultimap;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.ChoremClient;
import org.chorem.ChoremConfigOption;
import org.chorem.ChoremQueryHelper;
import org.debux.webmotion.server.WebMotionController;
import org.debux.webmotion.server.call.Call;
import org.debux.webmotion.server.call.UploadFile;
import org.debux.webmotion.server.mapping.Config;
import org.debux.webmotion.server.render.Render;
import org.nuiton.wikitty.entities.Element;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.entities.WikittyImpl;
import org.nuiton.wikitty.entities.WikittyTypes;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.query.WikittyQueryParser;
import org.nuiton.wikitty.query.WikittyQueryResult;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import java.io.ByteArrayInputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.chorem.ChoremUtil;
import org.nuiton.wikitty.WikittyUtil;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class GenericAction extends WebMotionController {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(GenericAction.class);

    public Render logout(HttpSession session) {
        session.invalidate();
        return renderURL("/");
    }

    // FIXME poussin 20120418 cette methode est-elle vraiment utilise ?
    // si oui searchJson ne peut-elle pas la remplacer ?
    public Render searchFieldJson(ChoremClient client, String query) {
        List<Map<String, String>> result;
        try {
            WikittyQuery q = WikittyQueryParser.parse(query);
            q.setOffset(0).setLimit(WikittyQuery.MAX); // FIXME poussin 20120416 peut-etre toujours avoir un max et non pas l'infini (100?)
            WikittyQueryResult<String> queryResult = client.findAllByQuery(q);

            result = new ArrayList<Map<String, String>>(queryResult.size());
            for (String s : queryResult) {
                String id = s;
                String label = s;
                Map<String, String> item = new LinkedHashMap<String, String>();
                item.put("label", label);
                item.put("value", label);
                item.put("name", label);
                item.put("id", id);
                result.add(item);
            }
        } catch (Exception eee) {
            log.error(String.format("Can't evaluate query: '%s'", query), eee);
            result = Collections.EMPTY_LIST;
        }

        return renderJSON(result);

    }

    public Render getById(ChoremClient client, String id) {
        Map<String, String> result = new LinkedHashMap<String, String>();

        Wikitty w = client.restore(id);
        if (w != null) {
            result.put("id", id);
            result.put("version", w.getWikittyVersion());
            result.put("extensions", StringUtils.join(w.getExtensionNames(), ","));
            for (WikittyExtension ext : w.getExtensions()) {
                result.put(ext.getName() + ".version", ext.getVersion());
                for (String tag : ext.getTagNames()) {
                    result.put(ext.getName() + ".tag." + tag, ext.getTagValue(tag));
                }
                for (String field : ext.getFieldNames()){
                    result.put(ext.getName() + "." + field, w.getFieldAsString(ext.getName(), field));
                }
            }
        }

        return renderJSON(result);

    }

    /**
     * Fait une recherche et retourne le resultat dans une map avec comme cle
     * le nom de l'extension et comme valeur une liste de Wikitty qui correspond
     * aux criteres de recherche.
     *
     * Recherche avec la possilite d'indiquer le niveau de profondeur de recherche
     * pour les champs de type wikitty.
     *
     * @param client objet injecte par le framework webmotion/chorem
     * @param extension la liste des extensions que peut/doit avoir l'objet resultat
     * Si vide alors on fait la recherche sur toutes les extensions disponibles
     * dans la base.
     * @param query la restriction a appliquer en plus des extensions
     * @param depth le niveau de profondeur de recherche
     * @return une Map{extensionName: liste de wikitty}
     */
    // depth est un Integer car si c'est un int et qu'il n'existe pas dans les parametres
    // l'appel plante au lieu de passer 0 comme valeur par defaut
    public Map<String, List<Wikitty>> searchAsMap(ChoremClient client, String[] extension, String query, Integer depth) {
        if (depth == null) {
            depth = 0;
        }

        WikittyQueryMaker maker;

        // S'il y a des extensions on force la requete sur ces types d'extension
        if (extension != null) {
            maker = new WikittyQueryMaker().and()
                    .extContainsOne(ChoremUtil.asList(null, extension));
        } else {
            maker = new WikittyQueryMaker();
        }

        // s'il y a un filtre on l'utilise, et on utilisera aussi #offset et #limit
        // de ce filtre s'ils sont positionne
        WikittyQuery filter = WikittyQueryParser.parse(query);
        if (filter != null) { // filter peut-etre null si la requete a une mauvaise syntaxe
            maker.condition(filter.getCondition());
            // si l'utilisateur a force le depth dans son filtre on l'utilise
            if (StringUtils.containsIgnoreCase(query, WikittyQueryParser.DEPTH)) {
                depth = filter.getWikittyFieldSearchDepth();
            }
        }

        // on recupere la requete
        WikittyQuery q = maker.end();

        q.setWikittyFieldSearchDepth(depth);
        if (filter != null) {
            q.setOffset(filter.getOffset());
            q.setLimit(filter.getLimit());
        }
        
        // on essai de trier les resultats au mieux tout en ne faisant qu'un requete
        ChoremQueryHelper.addSort(client, q, extension);

        WikittyQueryResult<Wikitty> results =
                client.findAllByQuery(Wikitty.class, q);

        // on classe par extension demande les resultats
        Map<String, List<Wikitty>> map =
                new MapWithDefault<String, List<Wikitty>>(
                new TreeMap<String, List<Wikitty>>(), new LinkedList<Wikitty>());

        Set<String> exts = null;
        if (extension != null && extension.length > 0) {
            exts = new HashSet<String>(ChoremUtil.asList(null, extension));
        }

        Set<String> extExcluded = new HashSet<String>(client.getConfig().getOptionAsList(
                ChoremConfigOption.CHOREM_EXTENSION_SEARCH_EXCLUSION.key).getOption());

        for (Wikitty w : results) {
            for (WikittyExtension ext : w.getExtensions()) {
                // on ne garde que les extensions demandees et qui ne sont pas exclus sauf si demandee explicitement
                if ((exts == null && !extExcluded.contains(ext.getName()))
                        || (exts != null && exts.contains(ext.getName()))) {
                    map.get(ext.getName()).add(w);
                }
            }
        }

        if (extension != null) {
            for (String ext : extension) {
                // The wanted extensions should be present, even without wikitties...
                // just get, create entry
                map.get(ext);
            }
        }
        return map;

    }

    public Render search(ChoremClient client, String[] extension, String query) {
        Map<String, List<Wikitty>> map = searchAsMap(client, extension, query, 1);
        return renderView("search.jsp", "result", map);

    }
    
    /**
     * Execute une recherche et retourne le resultat sous un format json
     * <pre>
     * [{extension: XXXX, // nom de l'extension
     *   label: XXXX      // toString pour l'extension pour l'objet
     *   value: XXXX      // toString pour l'extension pour l'objet
     *   name: XXXX       // toString pour l'extension pour l'objet
     *   id: XXXX         // id de l'objet
     *  },
     *  {...}
     *  ,...
     * ]
     * </pre>
     *
     * @param client objet injecte par le framework webmotion/chorem
     * @param extension la liste des extensions que peut/doit avoir l'objet resultat
     * @param query la restriction a appliquer en plus des extensions
     * @return une reponse JSON
     */
    public Render searchJson(ChoremClient client, String[] extension, String query) {
//        // extensionRestriction est de la forme "ext1,ext2;ext3;ext4,ext5,ext6" ...
//        String extensionRestriction = StringUtils.join(extension, ";");
//        if (StringUtils.isNotBlank(extensionRestriction)) {
//            // et il faut avoir "extension=ext1 AND extension=ext2 OR extension=ext3 OR extension=ext4 AND extension=ext5 AND extension=ext6"
//            extensionRestriction = extensionRestriction.replaceAll("\\s", "");
//            extensionRestriction = "extension=" + extensionRestriction;
//            extensionRestriction = extensionRestriction.replaceAll(";", " OR extension=");
//            extensionRestriction = extensionRestriction.replaceAll(",", " AND extension=");
//
//            query += " AND ("+extensionRestriction+")";
//        }

        Map<String, List<Wikitty>> map = searchAsMap(client, extension, query, 1);
        List<Map<String, String>> result = new ArrayList<Map<String, String>>(map.size());
        for (Map.Entry<String, List<Wikitty>> e : map.entrySet()) {
            String extName = e.getKey();
            for (Wikitty value : e.getValue()) {
                // Avoid potential null value
                if (value != null) {
                    String id = value.getWikittyId();
                    String label = value.toString(extName);
                    Map<String, String> item = new LinkedHashMap<String, String>();
                    item.put("extension", extName);
                    item.put("label", label);
                    item.put("value", label);
                    item.put("name", label);
                    item.put("id", id);
                    result.add(item);
                }
            }
        }
        return renderJSON(result);
    }

    /**
     * Fait une recherche et retourne le resultat dans une map avec comme cle
     * le nom de l'extension et comme valeur une liste de Wikitty qui correspond
     * aux criteres de recherche
     *
     * @param client objet injecte par le framework webmotion/chorem
     * @param extension la liste des extensions que peut/doit avoir l'objet resultat
     * Si vide alors on fait la recherche sur toutes les extensions disponibles
     * dans la base.
     * @param query la restriction a appliquer en plus des extensions
     * @return une Map{extensionName: liste de wikitty}
     */
    // FIXME poussin 20120531 a supprimer lorsque le searchAsMap fonctionnera bien
    protected LinkedHashMultimap<String, Wikitty> searchAsMapOld(
            ChoremClient client, String[] extension, String query) {
        boolean expliciteExtension = false;
        Collection<String> extNames;
        if (ArrayUtils.isEmpty(extension)) {
            // recuperation de toutes les extensions
            extNames = new LinkedHashSet<String>();
            Collection<String> extIds = client.getAllExtensionIds();
            for (String id : extIds) {
                String name = WikittyExtension.computeName(id);
                extNames.add(name);
            }
        } else {
            // utilisation des extensions demande en parametre
            extNames = ChoremUtil.asList(null, extension);
            expliciteExtension = true;
        }

        Map<String, WikittyExtension> extensionMaps = new LinkedHashMap<String, WikittyExtension>();
        // chargement de la definition de toutes les extensions
        List<WikittyExtension> exts = client.restoreExtensionAndDependenciesLastVesion(extNames);
        for (WikittyExtension ext : exts) {
            // on ne garde que les extensions qui on un toString ou qui ont ete
            // explicitement demandee
            if (expliciteExtension || ext.hasTagValueToString()) {
                extensionMaps.put(ext.getName(), ext);
            }
        }

        // suppression des extensions non souhaitee
        // le mieux serait d'avoir une method sur le client 'restoreExtensionLastVesion'
        extensionMaps.keySet().retainAll(extNames);

        // creation de la parti de la condition commune a toutes les requetes
        WikittyQuery q = null;
        if (StringUtils.isNotBlank(query)) {
            q = new WikittyQueryParser().parseQuery(query);
        }

        // creation d'une requete par extension
        List<WikittyQuery> queries = new ArrayList<WikittyQuery>(extensionMaps.size());
        for (WikittyExtension ext : extensionMaps.values()) {
            WikittyQueryMaker queryMaker = new WikittyQueryMaker().and();
            if (q != null) {
                // on ajoute pour chaque champs precharge, la meme condition
                // par exemple cela permet lorsqu'on recherche un 'Employee' de
                // recherche aussi sur le nom de la personne ou de la societe
                String [] preload = null;//FIXME test StringUtils.split(ext.getTagValue(WikittyTagValue.TAG_PRELOAD), ";");
                if (preload == null) {
                    // si pas de preload, on ajoute directement la restriction
                    queryMaker.condition(q.getCondition());
                } else {
                    // sinon, il faut faire un "or" entre tous
                    queryMaker.or();
                    // on ajoute la restriction dans le or
                    queryMaker.condition(q.getCondition());
                    for (String e : preload) {
//                        String preloadQuery = String.format("%s={SELECT ID WHERE %s}",
//                                e, query);
//                        WikittyQuery wq = WikittyQueryParser.parse(preloadQuery);
//                        queryMaker.condition(wq.getCondition());
                        queryMaker.containsOne(Element.get(e))
                                .select(Element.ID).condition(q.getCondition())
                                .close()  // select
                                .close(); // containsOne
                    }
                    // on ferme le or
                    queryMaker.close();
                }
            }


            WikittyQuery extQuery = queryMaker.exteq(ext.getName()).end();
            extQuery.setName(ext.getName());
            if (q != null) {
                extQuery.setOffset(q.getOffset());
                extQuery.setLimit(q.getLimit());
            }
            // ajout de l'ordre de tri de l'extension
            extQuery.setSortAscending(ext.getSortAscending());
            extQuery.setSortDescending(ext.getSortDescending());

            queries.add(extQuery);
        }

        if (log.isDebugEnabled()) {
            log.debug("Queries: " + queries);
        }

        List<WikittyQueryResult<Wikitty>> results = 
                client.findAllByQuery(Wikitty.class, queries);

        // on classe par extension demande les resultats
        LinkedHashMultimap<String, Wikitty> map = LinkedHashMultimap.create();
        for (WikittyQueryResult<Wikitty> result : results) {
            String ext = result.getQueryName();
            for (Wikitty w : result) {
                map.put(ext, w);
            }
        }

        return map;
    }

    public Render view(ChoremClient client, String id, String[] extension) {
        Config config = getContext().getServerContext().getMapping().getConfig();
        ServletContext servletContext = getContext().getServletContext();

        log.debug("view: " + id);
        Wikitty w = client.restore(id, ".*");

        LinkedHashSet<WikittyExtension> exts = new LinkedHashSet<WikittyExtension>();
        if (w != null) {
            if (extension == null) {
                exts.addAll(w.getExtensions());
            } else {
                // Display the extensions and required ones for them
                List<String> extensionNames = ChoremUtil.asList(null, extension);
                List<WikittyExtension> wikittyExtensions = client.restoreExtensionAndDependenciesLastVesion(extensionNames);
                exts.addAll(wikittyExtensions);
            }
        }
        
        // recherche de footer specifique aux extensions
        String pathView = config.getPackageViews();
        if (pathView == null) {
            pathView = "";
        }
        pathView = "/" + pathView + "/";
        pathView = StringUtils.replace(pathView, "\\", "/");
        pathView = StringUtils.replace(pathView, "//", "/");

        // recherche des JSP d'action specifique pour les extensions
        List<String> viewActions = new LinkedList<String>();
        for (WikittyExtension e : exts) {
            String viewAction = pathView + "viewAction" + e.getName() + ".jsp";
            try {
                if (servletContext.getResource(viewAction) != null) {
                    viewActions.add(viewAction);
                }
            } catch (MalformedURLException eee) {
                log.warn(String.format("Bad url '%s'", viewAction), eee);
            }
        }

        return renderView("view.jsp", "wikitty", w, "extensions", exts, "viewActions", viewActions);
    }

    public Render edit(ChoremClient client, String id, Boolean copy,
            String[] extension, Call call) {
        log.debug("edit: " + id);
        // on preload rien, le preload sera fait a la fin
        Wikitty w = client.restore(id, "");

        // si on ne retrouve pas l'objet demande, on en edit un nouveau
        if (w == null) {
            w = new WikittyImpl();
        } else if (copy != null && copy) {
            // on copie le wikitty trouve pour qu'a la sauvegarde ce soit un nouveau qui soit creer
            WikittyImpl newW = new WikittyImpl();
            newW.replaceWith(w, true);
            w = newW;
        }

        LinkedHashSet<WikittyExtension> exts = new LinkedHashSet<WikittyExtension>();

        if (extension == null) {
            // Show all wikitty if no extension were mentioned
            exts.addAll(w.getExtensions());
        } else {
            // Be sure that the wikitty has good extension if wanted and their requirements
            List<String> extensionNames = ChoremUtil.asList(null, extension);
            List<WikittyExtension> newExts =
                    client.restoreExtensionAndDependenciesLastVesion(extensionNames);
            w.addExtension(newExts);

            // As we want to edit a wikitty by its extensions, we should edit the required extensions too
            // Add the extension and required ones in the list of extensions to display
            exts.addAll(newExts);
        }

        // si des nouvelles valeurs sont en parametre, on change les valeurs
        // actuel du wikitty
        Map<String, Object> params = call.getExtractParameters();
        String error = setWikittyField(w, "", params);
        // on recharge des entites qui ont pu apparaitre, pour pouvoir les afficher comme il faut
        client.preload(Collections.singleton(w), ".*");

        getContext().addInfoMessage("message", "Warning: Can't put some value in object" + error);

        return renderView("edit.jsp", "wikitty", w, "extensions", exts);
    }
    
       
    public Render editMultiRender(ChoremClient client, String ids, String[] extension, Call call, String jspRender) {
        log.debug("editMulti: " + ids);

        LinkedHashSet<WikittyExtension> exts = new LinkedHashSet<WikittyExtension>(); 
        List<WikittyExtension> newExts = null;
        
        if (extension != null) {
        // Be sure that the wikitty has good extension if wanted and their requirements
                List<String> extensionNames = ChoremUtil.asList(null, extension);
                newExts = client.restoreExtensionAndDependenciesLastVesion(extensionNames);
                exts.addAll(newExts);
        }
        
        List<Wikitty> ws = client.restore(ChoremUtil.asList(null, ids));
        ArrayList<Integer> newW = new ArrayList<Integer>();
        int index = 0;
        
        for (Wikitty w : ws) {
            
            String error="";
            
            // si on ne retrouve pas l'objet demande, on en edit un nouveau
            if (w == null) {
                if (extension != null) {
                    newW.add(index);
                }
            } else {
                
                // si des nouvelles valeurs sont en parametre, on change les valeurs
                // actuel du wikitty
                Map<String, Object> params = call.getExtractParameters();
                error += setWikittyField(w, "", params);

                // on recharge des entites qui ont pu apparaitre, pour pouvoir les afficher comme il faut
                client.preload(Collections.singleton(w), ".*");
            }
            index++;
            
            getContext().addInfoMessage("message", "Warning: Can't put some value in object" + error + " ! ");
        }
        
        // ajout des nouveaux Wikitty dans ws
        for (int ii=0;ii<newW.size();ii++) {
            Wikitty w = new WikittyImpl();
            w.addExtension(newExts);
            // on ajoute le nouveau wikitty au bon endroit
            ws.add(newW.get(ii+1), w); 
        }
        return renderView(jspRender, "ws", ws, "extensions", exts);
    }
    
    // les nouveaux wikittyId passés en paramètre dans l'URL doivent avoir des noms différents :
    // exemple /wikitty/{extension}/editMulti/new1,new2,new3...
    public Render editMulti(ChoremClient client, String ids, String[] extension, Call call) {
            return editMultiRender(client, ids, extension, call, "editMulti.jsp");
    }

    
    
    public Render save(ChoremClient client, String id, String[] extension, Call call) {
        Wikitty w = saveInternal(client, id, extension, call);
        getContext().addInfoMessage("message", "Object saved");
        return renderURL("/wikitty/view/"+w.getWikittyId());
    }

    public Render saveJson(ChoremClient client, String id, String[] extension, Call call) {
        Wikitty w = saveInternal(client, id, extension, call);
        return renderJSON(w);
    }

    /**
     * Method used by save and saveJson to actually perform the save action
     * @param client
     * @param id
     * @param extension
     * @param call
     * @return the saved wikitty
     */
    protected Wikitty saveInternal(ChoremClient client, String id, String[] extension, Call call) {

        log.debug("save: " + id);

        Wikitty w = client.restore(id);
        if (w == null) {
            w = new WikittyImpl();
        }
        List<WikittyExtension> exts =
                client.restoreExtensionAndDependenciesLastVesion(ChoremUtil.asList(null, extension));
        w.addExtension(exts);
        Map<String, Object> params = call.getExtractParameters();
        String error = setWikittyField(w, "", params);
        client.store(w);

        return w;
    }


    
    public Render saveMulti(ChoremClient client, String[] ids, Call call) {
        log.debug("save: " + ids);

        List<Wikitty> toSave = new LinkedList<Wikitty>();
        List<String> newIds = new LinkedList<String>();
        Map<String, Object> params = call.getExtractParameters();
        for (String id : ids) {
            Wikitty w = client.restore(id);
            if (w == null) {
                w = new WikittyImpl();
            }

            String[] extension = (String[]) params.get(id + ".extension");
            
            List<WikittyExtension> exts =
                    client.restoreExtensionAndDependenciesLastVesion(ChoremUtil.asList(null, extension));
            w.addExtension(exts);

            String error = setWikittyField(w, id + ".", params);
            getContext().addInfoMessage("message", "Object saved" + error);
            toSave.add(w);
            newIds.add(w.getWikittyId());
        }
        client.storeWikitty(toSave);
        return renderURL("/wikitty/search?query=id={" + StringUtils.join(newIds, ",") + "}");
    }
    

    /**
     * Modifie les valeurs de l'objet passe en parametre avec les valeurs de
     * champs que l'on arrive a retrouver dans le params.
     *
     * @param w Le wikitty a mettre a jour
     * @param params les parametres dans lequel on peut trouver des valeurs
     * @return une chaine representant toutes les erreurs rencontrees
     */
    protected String setWikittyField(Wikitty w, String prefix, Map<String, Object> params) {
        String error = "";
        for (String fqfield : params.keySet()) {
            String fqfieldPre = fqfield;
            if (StringUtils.startsWith(fqfield, prefix)) {
                fqfield = StringUtils.removeStart(fqfield, prefix);
                if (w.hasField(fqfield)) {
                    Object v;
                    if (w.getFieldType(fqfield).getType() == WikittyTypes.BINARY) {
                        //TODO : from file, get the binary, maybe configure mimeType field ?
                        UploadFile file = (UploadFile) params.get(fqfieldPre);
                        v = file.getFile();
                    } else {
                        String[] values = (String[]) params.get(fqfieldPre);
                        if (log.isDebugEnabled()) {
                            log.debug("ZZZZ fqfield:" + fqfieldPre + " values: " + Arrays.toString(values) + " size:" + values.length);
                        }
                        if (w.getFieldType(fqfield).isCollection()) {
                            if (values.length == 1 && w.getFieldType(fqfield).getType() == WikittyTypes.WIKITTY) {
                                // petit hack si l'editeur utilise n'est pas un select,
                                // mais un input (et donc des valeurs separees par des ','
                                values = StringUtils.split(values[0], ",");
                                if (log.isDebugEnabled()) {
                                    log.debug("YYYY " + Arrays.toString(values) + " size: " + values.length);
                                }
                            }
                            v = values;
                        } else {
                            v = values[0];
                        }
                    }
                    try {
                        w.setFqField(fqfield, v);
                        if (log.isDebugEnabled()) {
                            log.debug("XXX after field: " + w.getFqField(fqfield)
                                    + "(" + ClassUtils.getSimpleName(w.getFqField(fqfield), "NULLclasse!!!") + ")");
                        }
                    } catch (Exception eee) {
                        String msg = String.format("Can't put value '%s' in field '%s'", v, fqfield);
                        error += "\n<li>" + msg + "</li>";
                        log.error(msg, eee);
                    }
                }
            }
        }
        return error;
    }

    public Render delete(ChoremClient client, String id, String[] extension) {
        log.debug("delete: " + id);
        if (extension == null) {
            client.delete(id);
            getContext().addInfoMessage("message", "Object deleted");
        } else {
            getContext().addInfoMessage("message", "Delete extension not yet implemented");
//            Wikitty w = client.restore(id);
//            for (String extName : extension) {
//                // todo remove extension field and extension
//            }
        }
        return renderURL("/wikitty/view/"+id);
    }

    /**
     * Search the objects in relation with one given.
     * The method {@link this#searchAsMap(org.chorem.ChoremClient, String[], String, Integer)}
     * is called with a query created from the given id.
     *
     * @param client objet injecte par le framework webmotion/chorem
     * @param id     Id of the object for which we search the related others
     */
    public Render searchRelated(ChoremClient client, String id) {
        String query = id + " AND id != " + id;
        Map<String, List<Wikitty>> map = searchAsMap(client, null, query, 0);
        return renderView("search.jsp", "result", map);
    }

    public Render download(ChoremClient client, String id, String extension, String field) {
        Wikitty wikitty = client.restore(id);
        byte[] fieldValue = wikitty.getFieldAsBytes(extension, field);
        ByteArrayInputStream stream = new ByteArrayInputStream(fieldValue);
        //TODO ymartel
        String name = wikitty.getFieldAsString(extension, "name");
        return renderDownload(stream, name, "application/octet-stream");
    }

}

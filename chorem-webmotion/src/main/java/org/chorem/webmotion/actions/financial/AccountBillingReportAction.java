package org.chorem.webmotion.actions.financial;

/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.ChoremClient;
import org.chorem.ChoremQueryMaker;
import org.chorem.entities.Company;
import org.chorem.entities.FinancialTransaction;
import org.chorem.webmotion.actions.sales.SalesData;
import org.debux.webmotion.server.WebMotionController;
import org.debux.webmotion.server.render.Render;
import org.nuiton.wikitty.entities.Element;
import org.nuiton.wikitty.query.WikittyQuery;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jcouteau <couteau@codelutin.com>
 */
public class AccountBillingReportAction extends WebMotionController {

    /**
     * Rend le graphe des devis envoyés par mois
     *
     * @param client
     * @return
     */
    public Render billing(ChoremClient client, String account, String from,
                        String to) {

        if (null == from) {
            from = String.valueOf(BillingReportHelper.getFirstYear(client));
        }

        if (null == to) {
            to = String.valueOf(BillingReportHelper.getLastYear());
        }

        Map<Integer, SalesData> salesData = new LinkedHashMap<>();

        List<Integer> listAllYears = BillingReportHelper.listAllYears(from, to);

        List<Integer> listAllYearsInChorem = BillingReportHelper.listAllYears(client);

        double previousYearValue = 0.0;

        Company company = client.restore(Company.class, account);

        for (Integer year:listAllYears){

            Date yearFirstDay = BillingReportHelper.getFirstDayOfYear(year);
            Date yearLastDay =  BillingReportHelper.getLastDayOfYear(year);

            SalesData yearData = new SalesData();

            WikittyQuery projectQuery = new ChoremQueryMaker()
                    .select().sum(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_AMOUNT).where().and()
                    .bw(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EMITTEDDATE, yearFirstDay,
                            yearLastDay)
                    .containsOne(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYER)
                    .select(Element.ID).filterOnCompanyOrEmployee(account)
                    .end();

            Double sales = client.findByQuery(Double.class, projectQuery);

            //TODO JC 2012-01-26 Find a way to replace two queries into one.
            WikittyQuery quotationsQuery = new ChoremQueryMaker().and()
                    .bw(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EMITTEDDATE, yearFirstDay,
                            yearLastDay)
                    .containsOne(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_PAYER)
                    .select(Element.ID).filterOnCompanyOrEmployee(account)
                    .end();

            List<FinancialTransaction> quotations = client.findAllByQuery(FinancialTransaction.class,
                    quotationsQuery).getAll();

            //Progression ventes
            double salesProgression = 0;
            if (previousYearValue != 0){
                salesProgression = 100 * (sales - previousYearValue) / previousYearValue;
            }

            previousYearValue = sales;

            yearData.setSales(sales);
            yearData.setProgression(salesProgression);
            yearData.setQuotations(quotations.size());

            salesData.put(year, yearData);
        }

        Double total = 0.0;

        for (Map.Entry<Integer, SalesData> entry : salesData.entrySet()) {
            total += entry.getValue().getSales();
        }

        return renderView("financial/reports/accountBillingReport.jsp",
                "account", company.getName(),
                "data", salesData,
                "total", total,
                "allYears", listAllYearsInChorem,
                "fromYear", from,
                "toYear", to);
    }
}

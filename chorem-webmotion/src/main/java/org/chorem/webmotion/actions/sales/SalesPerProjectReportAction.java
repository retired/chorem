package org.chorem.webmotion.actions.sales;

/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.ChoremClient;
import org.chorem.entities.Accepted;
import org.chorem.entities.Project;
import org.chorem.entities.Quotation;
import org.debux.webmotion.server.WebMotionController;
import org.debux.webmotion.server.render.Render;
import org.nuiton.wikitty.WikittyClient;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.query.conditions.Aggregate;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jcouteau <couteau@codelutin.com>
 */
public class SalesPerProjectReportAction extends WebMotionController {

    /**
     * Rend le graphe des devis envoyés par mois
     *
     * @param client
     * @return
     */
    public Render sales(ChoremClient client, String from, String to) {

        if (null == from) {
            from = String.valueOf(SalesReportHelper.getFirstYear(client));
        }

        if (null == to) {
            to = String.valueOf(SalesReportHelper.getLastYear());
        }

        Map<Project, SalesData> salesData = getSalesPerProjectData(from, to, client);

        List<Integer> listAllYearsInChorem = SalesReportHelper.listAllYears(client);

        return renderView("salesReports/salesPerProjectReport.jsp",
                "data", salesData,
                "allYears", listAllYearsInChorem,
                "fromYear", from,
                "toYear", to);
    }

    protected Map<Project,SalesData> getSalesPerProjectData(String firstYear,
                                                         String lastYear,
                                                         WikittyClient client){

        Date last = SalesReportHelper.getLastDayOfYear(Integer.valueOf(lastYear));
        Date first = SalesReportHelper.getFirstDayOfYear(Integer.valueOf(firstYear));

        Map<Project,SalesData> salesData = new LinkedHashMap<Project, SalesData>();

        //a query to get all the projects
        WikittyQuery projectsQuery = new WikittyQueryMaker().and()
                .exteq(Project.EXT_PROJECT).end();

        List<Project> projects = client.findAllByQuery(Project.class, projectsQuery).getAll();

        //a query per project :(
        for (Project project:projects){

            SalesData projectData = new SalesData();

            WikittyQuery projectQuery = new WikittyQueryMaker()
                    .select().sum("Quotation.amount").where().and()
                    .eq(Quotation.FQ_FIELD_QUOTATION_PROJECT, project)
                    .bw(Accepted.FQ_FIELD_ACCEPTED_ACCEPTEDDATE, first, last)
                    .end();

            Double sales = client.findByQuery(Double.class, projectQuery);

            //TODO JC 2012-01-22 Find a way to replace two queries into one.
            WikittyQuery quotationsQuery = new WikittyQueryMaker().and()
                    .eq(Quotation.FQ_FIELD_QUOTATION_PROJECT, project)
                    .bw(Accepted.FQ_FIELD_ACCEPTED_ACCEPTEDDATE, first, last)
                    .end();

            List<Quotation> quotations = client.findAllByQuery(Quotation.class,
                    quotationsQuery).getAll();

            //Rempli la map que si on a des valeurs
            if (null != sales && sales != 0) {
                projectData.setSales(sales);
                projectData.setQuotations(quotations.size());
                salesData.put(project, projectData);
            }
        }

        return salesData;
    }
}

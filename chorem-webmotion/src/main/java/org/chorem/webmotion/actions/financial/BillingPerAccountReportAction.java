package org.chorem.webmotion.actions.financial;

/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.ChoremClient;
import org.chorem.ChoremQueryMaker;
import org.chorem.entities.*;
import org.chorem.webmotion.actions.sales.SalesData;
import org.debux.webmotion.server.WebMotionController;
import org.debux.webmotion.server.render.Render;
import org.nuiton.wikitty.entities.Element;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.query.WikittyQuery;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * @author jcouteau <couteau@codelutin.com>
 */
public class BillingPerAccountReportAction extends WebMotionController {

    /**
     * Rend le graphe des devis envoyés par mois
     *
     * @param client
     * @return
     */
    public Render billing(ChoremClient client, String from, String to) {

        if (null == from) {
            from = String.valueOf(BillingReportHelper.getFirstYear(client));
        }

        if (null == to) {
            to = String.valueOf(BillingReportHelper.getLastYear());
        }

        Map<Company, SalesData> billingData = getBillingPerAccountData(from, to, client);

        Double total = 0.0;

        for (Map.Entry<Company, SalesData> entry : billingData.entrySet()) {
            total += entry.getValue().getSales();
        }

        List<Integer> listAllYearsInChorem = BillingReportHelper.listAllYears(client);

        return renderView("financial/reports/billingPerAccountReport.jsp",
                "data", billingData,
                "total", total,
                "allYears", listAllYearsInChorem,
                "fromYear", from,
                "toYear", to);
    }

    protected Map<Company,SalesData> getBillingPerAccountData(String firstYear,
                                                            String lastYear,
                                                            ChoremClient client){

        Date last = BillingReportHelper.getLastDayOfYear(Integer.valueOf(lastYear));
        Date first = BillingReportHelper.getFirstDayOfYear(Integer.valueOf(firstYear));

        Map<Company,SalesData> salesData = new LinkedHashMap<>();

        String companyId = client.getConfiguration().getDefaultCompany();

        //get all the bills between first and last
        WikittyQuery billsQuery = new ChoremQueryMaker().and()
                .bw(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_EMITTEDDATE, first, last)
                .containsOne(FinancialTransaction.FQ_FIELD_FINANCIALTRANSACTION_BENEFICIARY)
                .select(Element.ID).filterOnCompanyOrEmployee(companyId)
                .end().setLimit(WikittyQuery.MAX);

        List<FinancialTransaction> bills = client.findAllByQuery(FinancialTransaction.class,
                billsQuery).getAll();

        Map<String,SalesData> salesDataPerId = new LinkedHashMap<>();

        Set<String> payersId = new HashSet<>();
        for (FinancialTransaction bill:bills){
            String payer = bill.getPayer();
            if (payer != null) {
                payersId.add(payer);
            }
        }

        List<Wikitty> payersWikitty = client.restore(new ArrayList<>(payersId));

        Map<String,Company> companiesById = new HashMap<>();

        List<String> companiesId = new ArrayList<>();

        Map <String, Wikitty> wikittyById = new HashMap<>();

        for (Wikitty wkt:payersWikitty){
            if (wkt.hasExtension(Company.EXT_COMPANY)){
                companiesById.put(wkt.getWikittyId(), client.castTo(Company.class, wkt));
            } else if (wkt.hasExtension(Employee.EXT_EMPLOYEE)){
                companiesId.add(wkt.getFieldAsWikitty(Employee.EXT_EMPLOYEE, Employee.FIELD_EMPLOYEE_COMPANY));
                wikittyById.put(wkt.getWikittyId(), wkt);
            }
        }

        List<Company> companies = client.restore(Company.class, companiesId, false);
        for (Company company:companies){
            if (company != null) {
                companiesById.put(company.getWikittyId(), company);
            }
        }

        //group bills by account and sum values
        for (FinancialTransaction bill:bills){
            String payerId = bill.getPayer();
            SalesData accountData;

            //if payer is an employee, replace payerId with the employee company
            if (!companiesById.containsKey(payerId)) {
                Wikitty wkt = wikittyById.get(payerId);
                if (wkt != null){
                    payerId = wkt.getFieldAsWikitty(Employee.EXT_EMPLOYEE, Employee.FIELD_EMPLOYEE_COMPANY);
                }
            }

            if (salesDataPerId.containsKey(payerId)){
                accountData = salesDataPerId.get(payerId);
            } else {
                accountData = new SalesData();
            }

            accountData.setSales(accountData.getSales() + bill.getAmount());
            accountData.setQuotations(accountData.getQuotations()+1);
            salesDataPerId.put(payerId, accountData);
        }

        //get accounts and create a map id->company
        List<Company> accounts = client.restore(Company.class, new ArrayList<>(salesDataPerId.keySet()), false);
        Map<String,Company> accountsById = new HashMap<>();
        for (Company account:accounts){
            if (account != null) {
                accountsById.put(account.getWikittyId(), account);
            }
        }

        //populate the output
        for (Map.Entry<String, SalesData> entry : salesDataPerId.entrySet()) {
            Company account = accountsById.get(entry.getKey());
            if (account != null) {
                salesData.put(account, entry.getValue());
            }
        }

        return salesData;
    }
}

package org.chorem.webmotion.actions;

/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.ChoremClient;
import org.chorem.webmotion.render.RenderWikitty;
import org.debux.webmotion.server.WebMotionController;
import org.debux.webmotion.server.render.Render;
import org.nuiton.wikitty.entities.Element;
import org.nuiton.wikitty.entities.FieldType;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryParser;
import org.nuiton.wikitty.query.WikittyQueryResult;
import org.nuiton.wikitty.query.conditions.Condition;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WikittyRestAction extends WebMotionController {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(WikittyRestAction.class);

    public Render getWikittiesFromQuery(ChoremClient client, String query) {
        WikittyQuery q = WikittyQueryParser.parse(query);
        WikittyQueryResult<Wikitty> qResult = client.findAllByQuery(Wikitty.class, q);
        return new RenderWikitty().setModelWikitty(qResult.getAll());
    }

    public Render getWikitties(ChoremClient client, List<String> id) {
        List<Wikitty> result = client.restore(id);
        return new RenderWikitty().setModelWikitty(result);
    }

    public Render getWikitty(ChoremClient client, String id) {
        Wikitty result = client.restore(id);
        return new RenderWikitty().setModelWikitty(result);
    }

    public Render getWikittyFieldValue(ChoremClient client, String id, String ext, String field) {
        RenderWikitty render = new RenderWikitty();

        Wikitty result = client.restore(id);
        if (StringUtils.isNotBlank(field)) {
            // seulement un champs
            String value = result.getFieldAsString(ext, field);
            render.setModelValue(value);
        } else {
            Map<String, String> values = new HashMap<String, String>();
            for (String f : result.getExtensionFields(ext)) {
                String value = result.getFieldAsString(ext, f);
                values.put(f, value);
            }
            render.setModelMap(values);
        }
        return render;
    }

    public Render select(ChoremClient client, String query) {
        WikittyQuery q = WikittyQueryParser.parse(query);
        WikittyQueryResult<Map<String, Object>> qResult = client.findAllByQueryAsMap(q);
        return new RenderWikitty().setModelSelect(qResult.castToMap(client, String.class).getAll());
    }

    public Render facet(ChoremClient client, String query, List<String> ff, List<String> fq) {
        WikittyQuery q = WikittyQueryParser.parse(query);
        if (CollectionUtils.isNotEmpty(ff)) {
            for (String f : ff) {
                if (StringUtils.isNotBlank(f)) {
                    q.addFacetField(Element.get(f));
                }
            }
        }
        if (CollectionUtils.isNotEmpty(fq)) {
            for (String f : fq) {
                if (StringUtils.isNotBlank(f)) {
                    Condition c = WikittyQueryParser.parse(f).getCondition();
                    q.addFacetQuery(f, c);
                }
            }
        }
        WikittyQueryResult<Map<String, Object>> qResult = client.findAllByQueryAsMap(q);
        return new RenderWikitty().setModelFacet(qResult.getFacets());
    }


    public Render getExtensions(ChoremClient client, List<String> idOrName) {
        if (CollectionUtils.isEmpty(idOrName)) {
            List<String> exts = client.getAllExtensionIds();
            return new RenderWikitty().setModelValue(exts);
        } else {
            List<WikittyExtension> exts = new ArrayList<WikittyExtension>(idOrName.size());
            for (String i : idOrName) {
                if (StringUtils.endsWith(i, "]")) {
                    exts.add(client.restoreExtension(i));
                } else {
                    exts.add(client.restoreExtensionLastVersion(i));
                }
            }
            return new RenderWikitty().setModelExtension(exts);
        }
    }

    public Render getExtension(ChoremClient client, String idOrName) {
        WikittyExtension ext;
        if (StringUtils.endsWith(idOrName, "]")) {
            ext = client.restoreExtension(idOrName);
        } else {
            ext = client.restoreExtensionLastVersion(idOrName);
        }
        return new RenderWikitty().setModelExtension(ext);
    }

    public Render getExtensionField(ChoremClient client, String idOrName, String field) {
        WikittyExtension ext;
        if (StringUtils.endsWith(idOrName, "]")) {
            ext = client.restoreExtension(idOrName);
        } else {
            ext = client.restoreExtensionLastVersion(idOrName);
        }
        FieldType type = ext.getFieldType(field);
        return new RenderWikitty().setModelValue(type);
    }


}

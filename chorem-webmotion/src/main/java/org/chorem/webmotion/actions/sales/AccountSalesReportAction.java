package org.chorem.webmotion.actions.sales;

/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.ChoremClient;
import org.chorem.entities.Accepted;
import org.chorem.entities.Quotation;
import org.debux.webmotion.server.WebMotionController;
import org.debux.webmotion.server.render.Render;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.query.conditions.Aggregate;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jcouteau <couteau@codelutin.com>
 */
public class AccountSalesReportAction extends WebMotionController {

    /**
     * Rend le graphe des devis envoyés par mois
     *
     * @param client
     * @return
     */
    public Render sales(ChoremClient client, String account, String from,
                        String to) {

        if (null == from) {
            from = String.valueOf(SalesReportHelper.getFirstYear(client));
        }

        if (null == to) {
            to = String.valueOf(SalesReportHelper.getLastYear());
        }

        Map<Integer, SalesData> salesData = new LinkedHashMap<Integer, SalesData>();

        List<Integer> listAllYears = SalesReportHelper.listAllYears(from, to);

        List<Integer> listAllYearsInChorem = SalesReportHelper.listAllYears(client);

        double previousYearValue = 0;

        for (Integer year:listAllYears){
            Date yearFirstDay = SalesReportHelper.getFirstDayOfYear(year);
            Date yearLastDay =  SalesReportHelper.getLastDayOfYear(year);

            SalesData yearData = new SalesData();

            WikittyQuery projectQuery = new WikittyQueryMaker()
                    .select().sum("Quotation.amount").where().and()
                    .eq(Quotation.FQ_FIELD_QUOTATION_CUSTOMER, account)
                    .bw(Accepted.FQ_FIELD_ACCEPTED_ACCEPTEDDATE, yearFirstDay,
                            yearLastDay)
                    .end();

            Double sales = client.findByQuery(Double.class, projectQuery);

            //TODO JC 2012-01-26 Find a way to replace two queries into one.
            WikittyQuery quotationsQuery = new WikittyQueryMaker().and()
                    .eq(Quotation.FQ_FIELD_QUOTATION_CUSTOMER, account)
                    .bw(Accepted.FQ_FIELD_ACCEPTED_ACCEPTEDDATE, yearFirstDay,
                            yearLastDay)
                    .end();

            List<Quotation> quotations = client.findAllByQuery(Quotation.class,
                    quotationsQuery).getAll();

            //Progression devis envoyés
            double salesProgression = 0;
            if (previousYearValue != 0){
                salesProgression = 100 * (sales - previousYearValue) / previousYearValue;
            }

            previousYearValue = sales;

            yearData.setSales(sales);
            yearData.setProgression(salesProgression);
            yearData.setQuotations(quotations.size());

            salesData.put(year, yearData);
        }

        return renderView("salesReports/accountSalesReport.jsp",
                "data", salesData,
                "allYears", listAllYearsInChorem,
                "fromYear", from,
                "toYear", to);
    }
}

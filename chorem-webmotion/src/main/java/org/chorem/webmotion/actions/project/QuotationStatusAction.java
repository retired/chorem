package org.chorem.webmotion.actions.project;

/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.ChoremClient;
import org.chorem.ChoremUtil;
import org.chorem.webmotion.actions.GenericAction;
import org.debux.webmotion.server.call.Call;
import org.debux.webmotion.server.render.Render;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.entities.WikittyImpl;

/**
 * Currently unsused class managing the ajax request for the quotation status changed.
 * May be deprecated with the completion of the transaction funnel.
 * @author gwenn
 *
 */
public class QuotationStatusAction extends GenericAction{

	/** to use log facility, just put in your code: log.info(\"...\"); */
	static private Log log = LogFactory.getLog(QuotationStatusAction.class);
	
	public Render upgrade(ChoremClient client, String id, String[] extension, Call call) {
        Wikitty w = upgradeInternal(client, id, extension, call);
        getContext().addInfoMessage("message", "Object saved");
        return renderURL("/wikitty/view/"+w.getWikittyId());
    }
	
	/**
     * Method used by save and saveJson to actually perform the save action
     * @param client
     * @param id
     * @param extension
     * @param call
     * @return the saved wikitty
     */
    protected Wikitty upgradeInternal(ChoremClient client, String id, String[] extensionStr, Call call) {

        log.debug("save: " + id);
        System.out.println(Arrays.toString(extensionStr));
        System.out.println(ChoremUtil.asList(null, extensionStr));
        Wikitty w = client.restore(id);
        if (w == null) { // Jamais sensé arriver
            w = new WikittyImpl();
        }
        
        //System.out.println("Nom de l'extension : " + extensions);
        List<String> extList = ChoremUtil.asList(null, extensionStr);
        if(extList == null)
        	return w;
        Collection<WikittyExtension> exts = client.restoreExtensionLastVersion(extList);
        
        w.addExtension(exts);
        Map<String, Object> params = call.getExtractParameters();
        String error = setWikittyField(w, "", params);
        client.store(w);

        return w;
    }
    
    
    public Render getExtension(ChoremClient client, String extensionName, String id) {
    	Collection<String> c = new ArrayList<String>();
    	c.add(extensionName);
    	Wikitty wikitty = client.restore(id);
    	List<WikittyExtension> extensions = client.restoreExtensionAndDependenciesLastVesion(c);
    	System.out.println(extensions);
    	extensions.removeAll(wikitty.getExtensions());
    	
    	
    	return renderView("simpleWikiFields.jsp", "extensions", extensions, "wikitty", wikitty);
    }
    
}

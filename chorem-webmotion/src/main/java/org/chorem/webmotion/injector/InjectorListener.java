/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem.webmotion.injector;

import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Date;
import java.util.Set;

import com.google.common.collect.Sets;
import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.beanutils.Converter;
import org.apache.commons.lang3.StringUtils;
import org.chorem.ChoremClient;
import org.chorem.webmotion.ChoremWebMotionUtil;
import org.chorem.webmotion.PaginatedResult;
import org.chorem.webmotion.bean.financial.ExpenseAccountBean;
import org.chorem.webmotion.bean.financial.ExpenseAccountEntryBean;
import org.chorem.webmotion.converters.JsonConverter;
import org.debux.webmotion.server.WebMotionServerListener;
import org.debux.webmotion.server.call.Call;
import org.debux.webmotion.server.call.ServerContext;
import org.debux.webmotion.server.handler.ExecutorParametersInjectorHandler.Injector;
import org.debux.webmotion.server.mapping.Mapping;
import org.nuiton.wikitty.WikittyUtil;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class InjectorListener implements WebMotionServerListener {

    protected static final Set<Class<?>> BEAN_TYPES = Sets.<Class<?>>newHashSet(
        ExpenseAccountBean.class,
        ExpenseAccountEntryBean.class,
        PaginatedResult.class
    );

    @Override
    public void onStart(Mapping mpng, ServerContext context) {
        
        // Declare injector
        context.addInjector(new Injector() {
            @Override
            public Object getValue(Mapping mapping, Call call,String s, Class<?> type, Type generic) {
                if (ChoremClient.class.isAssignableFrom(type)) {
                    // Get Client
                    ChoremClient client = ChoremWebMotionUtil.getClient(call);
                    return client;
                }
                return null;
            }
        });

        // ajout d'un converter pour support tous les formats de date (comme wikitty)
        context.addConverter(new Converter() {
            @Override
            public Object convert(Class type, Object value) {
                if (value.getClass().isArray()) {
                    if (Array.getLength(value) > 0) {
                        value = Array.get(value, 0);
                    } else {
                        value = null;
                    }
                }
                if (value instanceof Collection) {
                    Collection collection = (Collection)value;
                    if (collection.size() > 0) {
                        value = collection.iterator().next();
                    } else {
                        value = null;
                    }
                }

                Object result = null;
                if (value != null) {
                    if (String.class.isAssignableFrom(type)) {
                            result = WikittyUtil.toString(value);
                    } else if (Date.class.isAssignableFrom(type)) {
                        if (!(value instanceof String) || StringUtils.isNotBlank((String)value)) {
                            result = WikittyUtil.toDate(value);
                        }
                    } else {
                        throw new ConversionException(String.format(
                                "Chorem date converter can't convert '%s' to '%s'",
                                value, type));
                    }
                }
                return result;
            }
        }, Date.class);

        for (Class<?> beanType : BEAN_TYPES) {
            context.addConverter(JsonConverter.newConverter(beanType), beanType);
        }

    }

    @Override
    public void onStop(ServerContext context) {
        // Do nothing
    }

}

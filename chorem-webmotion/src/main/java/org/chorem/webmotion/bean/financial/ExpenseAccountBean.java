package org.chorem.webmotion.bean.financial;

/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.google.gson.Gson;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class ExpenseAccountBean implements Serializable {

    protected String id;
    protected String employeeName;
    protected String employeeId;
    protected long startDate;
    protected long endDate;
    protected Set<ExpenseAccountEntryBean> expenseAccountEntries;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public long getStartDate() {
        return startDate;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    public void setStartDate(Date startDate) {
        if (startDate != null) {
            this.startDate = startDate.getTime();
        }
    }

    public long getEndDate() {
        return endDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }

    public void setEndDate(Date endDate) {
        if (endDate != null) {
            this.endDate = endDate.getTime();
        }
    }

    public Set<ExpenseAccountEntryBean> getExpenseAccountEntries() {
        return expenseAccountEntries;
    }

    public void setExpenseAccountEntries(Set<ExpenseAccountEntryBean> expenseAccountEntries) {
        this.expenseAccountEntries = expenseAccountEntries;
    }

    public void addExpenseAccountEntry(ExpenseAccountEntryBean expenseAccountEntryBean) {
        if (this.expenseAccountEntries == null) {
            this.expenseAccountEntries = new HashSet<>();
        }
        this.expenseAccountEntries.add(expenseAccountEntryBean);
    }

    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}

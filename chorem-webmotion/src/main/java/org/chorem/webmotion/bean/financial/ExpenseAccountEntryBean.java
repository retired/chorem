package org.chorem.webmotion.bean.financial;

/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.util.Date;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class ExpenseAccountEntryBean implements Serializable {

    protected String id;
    protected String categoryName;
    protected String categoryId;
    protected long emittedDate;
    protected long paymentDate;
    protected String description;
    protected String justificationNumber;
    protected Double amount;
    protected Double VAT;
    protected String projectName;
    protected String projectId;
    protected Double total;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public long getEmittedDate() {
        return emittedDate;
    }

    public void setEmittedDate(long emittedDate) {
        this.emittedDate = emittedDate;
    }

    public void setEmittedDate(Date emittedDate) {
        if (emittedDate != null) {
            this.emittedDate = emittedDate.getTime();
        }
    }

    public long getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(long paymentDate) {
        this.paymentDate = paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        if (paymentDate != null) {
            this.paymentDate = paymentDate.getTime();
        }
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getJustificationNumber() {
        return justificationNumber;
    }

    public void setJustificationNumber(String justificationNumber) {
        this.justificationNumber = justificationNumber;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getVAT() {
        return VAT;
    }

    public void setVAT(Double VAT) {
        this.VAT = VAT;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }
}

package org.chorem.webmotion;
/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.ChoremClient;
import org.chorem.ChoremConfig;
import org.debux.webmotion.server.call.Call;
import org.debux.webmotion.server.call.HttpContext;
import org.nuiton.config.ApplicationConfig;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ChoremWebMotionUtil {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(ChoremWebMotionUtil.class);

    static public String getDomain(Call call) {
        String result = getDomain(call.getContext().getRequest());
        return result;
    }

    static public String getDomain(HttpContext context) {
        String result = getDomain(context.getRequest());
        return result;
    }

    static public String getDomain(HttpServletRequest request) {
        String result = request.getServerName();
        return result;
    }

    static public ApplicationConfig getConfig(HttpContext context) {
        String domain = getDomain(context);
        ApplicationConfig result = ChoremConfig.getConfig(domain);
        return result;
    }

    static public ChoremClient getClient(Call call) {
        ChoremClient result = getClient(call.getContext().getRequest());
        return result;
    }

    static public ChoremClient getClient(HttpContext context) {
        ChoremClient result = getClient(context.getRequest());
        return result;
    }

    /**
     * Recupere le ChoremClient pour cette utilisateur dans sa session. S'il n'a
     * pas encore de ChoremClient l'objet est cree et pousser dans la session.
     * Ce ChoremClient n'est pas encore authentifier (pas de token de securite)
     * @param request
     * @return
     */
    static public ChoremClient getClient(HttpServletRequest request) {
        String domain = getDomain(request);
        ChoremClient result = (ChoremClient)request.getSession().getAttribute(
                domain + "." + ChoremClient.class.getName());
        if (result == null) {
            result = ChoremClient.getClient(domain);
            request.getSession().setAttribute(domain + "." + ChoremClient.class.getName(), result);
        }

        return result;
    }


}

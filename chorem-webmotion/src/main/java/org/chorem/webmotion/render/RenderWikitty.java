package org.chorem.webmotion.render;

/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.gson.Gson;
import org.apache.commons.lang3.StringUtils;
import org.debux.webmotion.server.call.Call;
import org.debux.webmotion.server.call.HttpContext;
import org.debux.webmotion.server.mapping.Mapping;
import org.debux.webmotion.server.render.Render;
import org.nuiton.wikitty.entities.Wikitty;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.query.FacetTopic;

/**
 * Retourne les resultats des appels REST Wikitty.
 * Le resultat peut-etre en JSON, CSV (todo) ou XML (todo) suivant le header de
 * la requete
 *
 * @author Benjamin Poussin <poussin@codelutin.com>
 */
public class RenderWikitty extends Render {
    /**
     * model peut-etre:
     * <li>Map<String, Object> pour un wikitty
     * <li>List<Map<String, Object>> pour des wikitties
     * <li>Map<String, String> pour un select
     * <li>Map<String, List<FacetTopic>> pour les facets
     */
    protected Object model;

    public RenderWikitty() {
    }

    public RenderWikitty setModelWikitty(Wikitty wikitty) {
        model = createWikittyModel(wikitty);
        return this;
    }

    public RenderWikitty setModelWikitty(List<Wikitty> wikitties) {
        List tmp = new ArrayList(wikitties.size());
        for (Wikitty w : wikitties) {
            tmp.add(createWikittyModel(w));
        }
        model = tmp;
        return this;
    }

    public RenderWikitty setModelSelect(List<Map<String, String>> select) {
        model = select;
        return this;
    }

    public RenderWikitty setModelFacet(Map<String, List<FacetTopic>> facet) {
        model = facet;
        return this;
    }

    public RenderWikitty setModelValue(Object value) {
        model = value;
        return this;
    }

    public RenderWikitty setModelMap(Map values) {
        model = values;
        return this;
    }

    public RenderWikitty setModelExtension(List<WikittyExtension> ext) {
        model = ext;
        return this;
    }

    public RenderWikitty setModelExtension(WikittyExtension ext) {
        model = ext;
        return this;
    }

    public Object getModel() {
        return model;
    }

    @Override
    public void create(Mapping mapping, Call call) throws IOException, ServletException {
        HttpContext context = call.getContext();
        HttpServletResponse response = context.getResponse();
        HttpServletRequest request = context.getRequest();
        // accept est de la forme: 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
        String accept = request.getHeader("Accept");

        if (accept.equals("text/xml")) {
            // TODO poussin 20130702 un export en XML
            throw new UnsupportedOperationException("text/xml Not support yet");
        } else if (accept.equals("text/csv")) {
            // TODO poussin 20130702 un export en csv
            throw new UnsupportedOperationException("text/csv Not support yet");
        } else {
            // default
            response.setContentType("application/json");

            Gson gson = new Gson();
            String json = gson.toJson(model);
            PrintWriter out = context.getOut();
            out.print(json);
        }



    }

    Map<String,Object> getMeta(Wikitty wikitty){
        Map<String,Object> meta = new HashMap<String,Object>();
        meta.put("id", wikitty.getWikittyId());
        meta.put("version", wikitty.getWikittyVersion());
        meta.put("extensions", wikitty.getExtensionNames());
        
        Map<String,String> displayString = new HashMap<String,String>();
        displayString.put("", wikitty.toString()); // for complete object
        for (String ext : wikitty.getExtensionNames()) {
            displayString.put(ext, wikitty.toString(ext));
        }
        meta.put("displayString", displayString);
        return meta;
    }

    Map<String,Object> getData(Wikitty wikitty){
        return wikitty.getFieldValue();
    }

    void collectExtension(Wikitty wikitty, Map<String, Object> exts) {
        for (WikittyExtension e : wikitty.getExtensions()) {
            exts.put(e.getName(), e);
        }
    }

    void collectPreloaded(Wikitty wikitty,
            Map<String, Object> wikitties, Map<String, Object> exts){
        Map<String,Wikitty> preloaded = wikitty.getPreloaded();
        for (Map.Entry<String, Wikitty> entry : preloaded.entrySet()) {
            wikitties.put(entry.getKey(), createWikittyModel(entry.getValue(), wikitties, exts));
        }
    }

    Map<String,Object> createWikittyModel(Wikitty wikitty,
            Map<String, Object> wikitties, Map<String, Object> exts){

        collectPreloaded(wikitty, wikitties, exts);
        collectExtension(wikitty, exts);

        Map<String,Object> model = new HashMap<String,Object>();
        model.put("meta", getMeta(wikitty));
        model.put("data", getData(wikitty));

        return model;
    }

    Map<String,Object> createWikittyModel(Wikitty wikitty) {
        Map<String, Object> wikitties = new HashMap<String, Object>();
        Map<String, Object> exts = new HashMap<String, Object>();

        Map<String,Object> model = createWikittyModel(wikitty, wikitties, exts);

        Map<String,Object> cache = new HashMap<String,Object>();
        cache.put("wikitty", wikitties);
        cache.put("extension", exts);
        
        model.put("cache", cache);

        return model;
    }

}

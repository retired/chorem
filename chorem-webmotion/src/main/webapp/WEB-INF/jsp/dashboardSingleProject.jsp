<%--
  #%L
  Chorem :: webmotion
  %%
  Copyright (C) 2011 - 2012 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/WEB-INF/wikitty.tld" prefix="w"%>

<f:setLocale value="${locale}" />
<link rel="stylesheet" href="<c:url value='/css/jquery.fn.gantt.css'/>" />
<h1>${title}</h1>

<form class="well form-inline" method="GET" id="projectSearch">

	<div class="control-group">
		<div class="controls" style="display: inline">
			<select class="filterBox" name="quotationFilter" id="quotationFilter">
				<option value="open"
					<c:if test='${param.quotationFilter=="open"}'>selected</c:if>>Devis
					ouverts</option>
				<option value="all"
					<c:if test='${param.quotationFilter=="all"}'>selected</c:if>>Tous
					les devis</option>
				<c:forEach var="entry" items="${projects}">
					<c:forEach var="q" items="${entry.value}">
						<option value="${q.object.wikittyId}"
							<c:if test='${param.quotationFilter.equals(q.object.wikittyId)}'>selected</c:if>>${entry.key.name}
							- ${q.object.description}</option>
					</c:forEach>
				</c:forEach>
			</select>
			<script>
				$(function() {
					$("#project_text")
							.autocompleteByExtension(
									{
										source : "/chorem/wikitty-json/search?extension=Project",
										minLength : 2,
										select : function(event, ui) {
											$("#project_text").val(
													ui.item.label);
											$("#project_hidden")
													.val(ui.item.id);
											$("#project_hidden").change()
											return false;
										}
									});
				});
			</script>

			<input type="text" id="project_text" name="project_name"
				value="${param.project_name}" placeholder="project name" /> <input
				type="hidden" id="project_hidden" name="project_id"
				value='<c:if test='${!param.project_name.equals("") }'>${param.project_id}</c:if>' />

		</div>
	</div>

</form>

<c:forEach var="entry" items="${projects}">
	<c:set var="p" value="${entry.key}"/>
		<h2 style="display: inline;">${p.name}</h2>
		<a href="<c:url value="/wikitty/edit/${p.wikittyId}"/>"><i
			class="icon-pencil icon-black"></i><small>edit</small></a>
		<p>${p.description}</p>




		<c:forEach var="q" items="${entry.value}">

			<h3 style="display: inline;">
				<w:display wikitty="${q.object.wikitty}" fqfield="Quotation.description"
					label="" />
			</h3>
			<a href="<c:url value="/wikitty/edit/${q.object.wikittyId}"/>"><i
				class="icon-pencil icon-black"></i><small>edit</small></a>
			<p>
				Du
				<w:display wikitty="${q.object.wikitty}" fqfield="Interval.beginDate"
					label="" pattern="dd/MM/yyyy" />
				au
				<w:display wikitty="${q.object.wikitty}" fqfield="Interval.endDate"
					label="" pattern="dd/MM/yyyy" />
				<br />Statut :
				${q.object.wikitty.extensionNames.toArray()[q.object.wikitty.extensionNames.size()
				-1]}
			</p>


			<table class="table table-striped table-bordered table-condensed">
				<thead>
					<tr>
						<th>Responsable ${q.object.getSupplier(false).getCompany(false)}</th>
						<th>Responsable entreprise</th>
						<th>Jours estimés</th>
						<th>Jours réels</th>
						<th>TJM estimé</th>
						<th>TJM réel</th>
						<th>Montant</th>
						<th>Gain/perte</th>
					</tr>
				</thead>
				<tbody>
					<tr>

						<td><w:display wikitty="${q.object.wikitty}"
								fqfield="Quotation.supplier" label="" /></td>
						<td><w:display wikitty="${q.object.wikitty}"
								fqfield="Quotation.customer" label="" /></td>
						<td class="number"><w:display wikitty="${q.object.wikitty}"
								fqfield="Quotation.estimatedDays" label="" /></td>
						<td class="number"><f:formatNumber type="number"
								maxFractionDigits="2" value="${q.getRealDays()}" /></td>
						<td class="currency"><f:formatNumber type="currency"
								maxFractionDigits="2" value="${q.getAdr()}" /></td>
						<td class="currency"><f:formatNumber type="currency"
								maxFractionDigits="2" value="${q.getRealAdr()}" /></td>
						<td class="currency"><w:display wikitty="${q.object.wikitty}"
								fqfield="Quotation.amount" label="" /></td>
						<td class="currency"><f:formatNumber type="currency"
								maxFractionDigits="2"
								value="${q.getLossOrProfit()}" /></td>
					</tr>
				</tbody>
			</table>





			<div class="gantt gantt-${q.object.wikittyId}" wikittyId="${q.object.wikittyId}"
				data=''>Pas de tâche</div>
			<c:if test="${q.tasks.size() != 0}">
				<table class="table table-striped table-bordered table-condensed">
					<thead>
						<tr>
							<th>Nom</th>
							<th>Montant</th>
							<th>Jours estimés</th>
							<th>Jours réels</th>
							<th>Différence estimation/réel</th>
							<th>TJM estimé</th>
							<th>TJM réel</th>
							<th>Gain attendu</th>
							<th>Gain/perte</th>
							<th>CJM moyen</th>
							<th>CJM réel</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${q.tasksData}" var="task">
							<tr id="taskrow-${task.object.wikittyId}">
								<td <c:if test="${not empty task.alert}">class="task-alert"</c:if>><a
									href="<c:url value="/wikitty/view/${task.object.wikittyId}"/>"> <w:display
											wikitty="${task.object.wikitty}" fqfield="Task.name" label="" /></a>
									<c:if test="${not empty task.alert}">
										<i class="icon-warning-sign" title="${task.alert}"></i>
									</c:if>	
									<c:if test="${not empty task.info}">
										<i class="icon-info-sign" title="${task.info}"></i>
									</c:if>
											
											
								<td><w:display wikitty="${task.object.wikitty}"
										fqfield="Task.price" label="" /></td>
								<td class="number"><w:display wikitty="${task.object.wikitty}"
										fqfield="Task.estimatedDays" label="" /></td>
								<td class="number"><f:formatNumber type="number"
										maxFractionDigits="2" value="${task.getRealDays()}" /></td>
								<td class="number"><f:formatNumber type="number"
										maxFractionDigits="2" value="${task.getDeltaDays()}" /></td>
								<td class="number"><f:formatNumber type="number"
										maxFractionDigits="2" value="${task.getAdr()}" /></td>


								<td class="number"><f:formatNumber type="number"
										maxFractionDigits="2" value="${task.getRealAdr()}" /></td>

								<td class="currency"><f:formatNumber type="currency"
										maxFractionDigits="2"
										value="${task.getExpectedProfit()}" /></td>
								<td class="currency"><f:formatNumber type="currency"
										maxFractionDigits="2"
										value="${task.getLossOrProfit()}" /></td>
										<td class="number"><f:formatNumber type="number"
										maxFractionDigits="2" value="${task.getAvgReturn()}" /></td>
								<td class="number"><f:formatNumber type="number"
										maxFractionDigits="2" value="${task.getRealReturn()}" /></td>
							</tr>


						</c:forEach>
					</tbody>
				</table>
			</c:if>
			
			<table class="table table-striped table-bordered table-condensed">
				<thead>
					<th>Employé</th>
					<th>Pourcentage estimé</th>
					<th>Temps passé réel</th>
				</thead>
				<tbody>
					<c:forEach items="${q.getPercentages()}" var="entry">
						<tr>
							<td>${entry.key}</td>
							<td class="number"><f:formatNumber type="number"
										maxFractionDigits="2" value="${entry.value}"/>%</td>
							<td class="number"><f:formatNumber type="number"
										maxFractionDigits="2" 
										value="${q.getTimePercentages()[entry.key]}" />%
							(
							<f:formatNumber type="number"
									maxFractionDigits="2" 
									value="${q.getTimes()[entry.key]}" />
							heures )</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>

		</c:forEach>
	
</c:forEach>


<script src="<c:url value='/js/jquery.fn.gantt.js'/>"></script>

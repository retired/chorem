<%--
  #%L
  Chorem :: webmotion
  %%
  Copyright (C) 2011 - 2014 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%@ taglib uri="/WEB-INF/wikitty.tld" prefix="w"%>

<head>
    <script data-require="angular-resource@1.2.0-rc.3" data-semver="1.2.0-rc.3" src="<c:url value='http://code.angularjs.org/1.2.0-rc.3/angular-resource.js'/>"></script>
    <script data-require="ng-table@*" data-semver="0.3.0" src="<c:url value='http://bazalt-cms.com/assets/ng-table/0.3.0/ng-table.js'/>"></script>
    <link data-require="ng-table@*" data-semver="0.3.0" rel="stylesheet" href="<c:url value='http://bazalt-cms.com/assets/ng-table/0.3.0/ng-table.css'/>" />
    <script type="text/javascript" src="<c:url value='/js/projectPagination.js'/>"></script>
    <script>
        angular.module('Pagination').value('paginationInit', {
            url : "<c:url value="/rest/project/projects"/>",
            params : {page: 1, count: 10, sorting: {name: 'asc'}}
        });
    </script>
</head>

<div ng-app="Pagination" ng-controller="PageCtrl">

    <div loading-container="tableParams.settings().$loading">
      <table ng-table="tableParams" show-filter="true" class="table">
        <tbody ng-repeat="project in projects">
          <tr id="tr{{project.wikitty.target.id}}" ng-class-odd="'odd'" ng-class-even="'even'">
            <td data-title="'Name'" sortable="name">
                    {{project.wikitty.target.fieldValue["Project.name"]}}
                </td>
            <td data-title="'Description'" sortable="description">
                    {{project.wikitty.target.fieldValue['Project.description']}}
                </td>
            <td class="rowTd" ><input type=button id="editRowBtn{{project.wikitty.target.id}}" value="edit"
                                      ng-click="setEditProject(project)"></td>
          </tr>
          <tr ng-if="editProject.id===project.wikitty.target.id">
            <td colspan="7" ng-include src="'editProject.html'"></td>
        </tr>
        </tbody>
      </table>
    </div>

</div>

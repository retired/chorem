<%--
  #%L
  Chorem :: webmotion
  %%
  Copyright (C) 2011 - 2012 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%@ taglib uri="/WEB-INF/wikitty.tld" prefix="w"%>

<h1>${title}</h1>

<table class="table table-striped table-bordered table-condensed">
    <thead>
        <tr>
            <th>Project/Task</th>
            <th>Reference</th>
            <th>Description</th>
            <th>Customer</th>
            <th>Supplier</th>
            <th>Amount</th>
            <th>Posted date</th>
            <th>Expected date</th>
            <th>Payment date</th>
            <th>Attachment</th>
        </tr>
    </thead>
    <c:forEach var="q" items="${invoices}">
        <tbody>
        <tr>
            <td><w:display wikitty="${q.wikitty}" fqfield="FinancialTransaction.target" label=""/></td>
            <td><w:display wikitty="${q.wikitty}" toString="%FinancialTransaction.reference|no reference$s" label=""/></td>
            <td><w:display wikitty="${q.wikitty}" toString="%FinancialTransaction.description$s" label=""/></td>
            <td><w:display wikitty="${q.wikitty}" fqfield="FinancialTransaction.payer" label=""/></td>
            <td><w:display wikitty="${q.wikitty}" fqfield="FinancialTransaction.beneficiary" label=""/></td>
            <td class="currency"><w:display wikitty="${q.wikitty}" fqfield="FinancialTransaction.amount" label=""/></td>
            <td class="date"><w:display wikitty="${q.wikitty}" fqfield="FinancialTransaction.emittedDate" label=""/></td>
            <td class="date"><w:display wikitty="${q.wikitty}" fqfield="FinancialTransaction.expectedDate" label=""/></td>
            <td class="date"><w:display wikitty="${q.wikitty}" fqfield="FinancialTransaction.paymentDate" label=""/></td>
            <td><w:display wikitties="${attachments.get(q.wikittyId)}" toString="%Attachment.name|noname$s"/></td>
        </tr>
        </tbody>
    </c:forEach>
</table>


<dl class="dl-horizontal">
    <dt>Total</dt><dd><f:formatNumber type="currency" value="${amount}"/></dd>
    <dt>Total paid</dt><dd><f:formatNumber type="currency" value="${amountPaid}"/></dd>
    <dt>Total expected</dt><dd><f:formatNumber type="currency" value="${amountExpected}"/></dd>
</dl>

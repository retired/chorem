<%--
  #%L
  Chorem :: webmotion
  %%
  Copyright (C) 2011 - 2012 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%@ taglib uri="/WEB-INF/wikitty.tld" prefix="w"%>

<h1>${title}</h1>

<table class="table table-striped table-bordered table-condensed">
    <thead>
        <tr>
            <th>Project</th>
            <th>Category</th>
            <th>Description</th>
            <th>Estimated days</th>
            <th>Amount</th>
            <th>working days</th>
            <th>Hoped price day</th>
            <th>Real price day</th>
            <th>Hoped profit</th>
            <th>Real profit</th>
            <th>Status</th>
            <th>Attachment</th>
        </tr>
    </thead>
    <c:forEach var="q" items="${quotations}">
        <tbody>
        <tr>
            <td><w:display wikitty="${q.wikitty}" fqfield="Quotation.project" label=""/></td>
            <td><w:display wikitty="${q.wikitty}" fqfield="Quotation.category" label=""/></td>
            <td><w:display wikitty="${q.wikitty}" toString="%Quotation.description$s" label=""/></td>
            <td class="number"><w:display wikitty="${q.wikitty}" fqfield="Quotation.estimatedDays" label=""/></td>
            <td class="currency"><w:display wikitty="${q.wikitty}" fqfield="Quotation.amount" label=""/></td>
            <td class="number"><f:formatNumber type="number" value="${taskInfos.get(q.wikittyId).workingDays}"/></td>
            <td class="currency"><f:formatNumber type="currency" value="${taskInfos.get(q.wikittyId).hopedPriceDay}"/></td>
            <td class="currency"><f:formatNumber type="currency" value="${taskInfos.get(q.wikittyId).realPriceDay}"/></td>
            <td class="currency"><f:formatNumber type="currency" value="${taskInfos.get(q.wikittyId).hopedProfit}"/></td>
            <td class="currency"><f:formatNumber type="currency" value="${taskInfos.get(q.wikittyId).realProfit}"/></td>
            <td class="nowrap"><w:display wikitty="${q.wikitty}" fqfield="Quotation.status" label=""/></td>
            <td><w:display wikitties="${attachments.get(q.wikittyId)}" toString="%Attachment.name|noname$s"/></td>
        </tr>
        </tbody>
    </c:forEach>
</table>

<dl class="dl-horizontal">
    <dt>Total amount</dt><dd><f:formatNumber type="currency" value="${amount}"/></dd>
    <dt>Total hoped profit</dt><dd><f:formatNumber type="currency" value="${hopedProfit}"/></dd>
    <dt>Total real profit</dt><dd><f:formatNumber type="currency" value="${realProfit}"/></dd>
    <dt>Profit discordance</dt><dd><f:formatNumber type="currency" value="${realProfit - hopedProfit}"/></dd>
</dl>

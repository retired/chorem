<%--
  #%L
  Chorem :: webmotion
  %%
  Copyright (C) 2011 - 2012 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/WEB-INF/wikitty.tld" prefix="w"%>

<f:setLocale value="${locale}" />

<link rel="stylesheet" href="<c:url value='/css/employeeEdit.css'/>" />

<h1>${title}</h1>

<form method="GET" id="companyForm">

<select class="filterBox" name="companyId" id="companySelect">
			<option>Choisir une entreprise</option>
				<c:forEach var="c" items="${companies}">
					<option value="${c.wikittyId}"
					<c:if test="${company.equals(c)}">selected</c:if>
					>${c.name}</option>
				</c:forEach>
			</select>
</form>

<form class="well form-inline" method="POST" id="company">
<h4><a href="<c:url value="/wikitty/view/${company.wikittyId}"/>">${company.wikitty}</a></h4>
<input type="hidden" name="companyId" value="${company.wikittyId}"/>
<c:if test='${company.wikitty.hasExtension("CompanyHR")}'>
Heures par jour : <w:input wikitty="${company.wikitty}" fqfield="CompanyHR.dailyHoursWorked"/>
CJM : <w:input wikitty="${company.wikitty}" fqfield="CompanyHR.dailyReturn"/>
<button class="btn btn-success" id="companyBtn">
		<i class="icon-ok icon-white"></i>
	Mettre à jour
	</button>
</c:if>
<c:if test='${!company.wikitty.hasExtension("CompanyHR")}'>
Valeurs par défaut (<a href='variables'>Modifier</a>) :<br/>
Heures par jour : <input type="test" value="${client.getConfiguration().getDailyHoursWorked()}" disabled/>
CJM : <input type="test" value="${client.getConfiguration().getDailyReturn()}" disabled/>
<button class="btn btn-success" id="companyBtn">
		<i class="icon-plus icon-white"></i>
		<input type="hidden" name="addExtension" value="true"/>
	Ajouter extension HR
	</button>
</c:if>

</form>
<button class="btn btn-success" id="editAllBtn">
		<i class="icon-edit icon-white"></i>
	Tout éditer
	</button>
	
	<button class="editBtn btn btn-success" id="validateAllBtn" disabled>
		<i class="icon-ok icon-white"></i>
	Tout valider
	</button>
	
	<button class="editBtn btn btn-danger" id="cancelAllBtn" disabled>
		<i class="icon-remove icon-white"></i>
	Tout annuler
	</button>
	
<img class="spinner" style="display:none" src="<c:url value='/img/ajax-loader.gif'/>"/>
<form style='display:inline;' id='searchForm'>Recherche : <input type="text" id="searchInput"/></form>
<table class="table table-striped table-bordered table-condensed tableEdit">
	<thead>
		<th>Employé</th>
		<th>Salaire</th>
		<th>Autres rémunérations</th>
		<th>Heures par jour</th>
		<th>Temps de travail</th>
		<th>Taux de productivité</th>
		<th>CJM <a class="allCjmRefresh" style="cursor:pointer">
								<i class="icon icon-refresh"></i></a></th>
		<th>Edit</th>
	</thead>
	<tbody>
		<c:set var="count" value="0"/>
		<c:forEach items="${employees}" var="employee">
			<tr class="row${count}" id="${employee.object.wikittyId}">
				<td class="person"><w:display wikitty="${employee.object.wikitty}"
						fqfield="Employee.person" label="" />
						</td>
				<td class="edit salary">
					${employee.salary}
				</td>
				<td class="otherPayments">
					${employee.otherPayments}
				</td>
				<td class="dailyHoursWorked"><f:formatNumber type="number"
								maxFractionDigits="2" value="${employee.dailyHoursWorked}"/></td>
				<td class="partialTime"><f:formatNumber type="number"
								maxFractionDigits="2" value="${employee.partialTime}"/>%</td>
				<td class="productivityRate"><f:formatNumber type="number"
								maxFractionDigits="2" value="${employee.productivityRate}"/>%</td>
				
				<td class="dailyReturn"
				<c:if test='${employee.object.wikitty.hasExtension("EmployeeHR")}'>
				refresh="true"
				</c:if>
				><span class="adc"><f:formatNumber type="number"
								maxFractionDigits="2" value="${employee.dailyReturn}"/></span>
								<c:if test='${employee.object.wikitty.hasExtension("EmployeeHR")}'>
								<a class="cjmRefresh" style="cursor:pointer">
								<i class="icon icon-refresh"></i></a></c:if></td>
				<td class="cellEdit"><a class="employeeEdit" style="cursor:pointer"><i class="icon icon-edit"></i></a></td>
				
			</tr>
			<c:set var="count" value="${count + 1}"/>
		</c:forEach>
	</tbody>
</table>
<script src="<c:url value='/js/employeeEdit.js'/>"></script>



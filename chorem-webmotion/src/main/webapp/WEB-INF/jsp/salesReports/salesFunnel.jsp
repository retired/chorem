<%--
  #%L
  Chorem :: webmotion
  %%
  Copyright (C) 2011 - 2012 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%@ taglib uri="/WEB-INF/wikitty.tld" prefix="w"%>

<head>
  <link rel="stylesheet/less" href="<c:url value='/css/chorem-sales.css'/>">
  <script type="text/javascript" src="<c:url value='/js/ui-date.js'/>"></script>
  <script type="text/javascript" src="http://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.11.2.js"></script>
  <script type="text/javascript" src="<c:url value='/js/salesFunnel.js'/>"></script>
</head>

<body>

<div ng-app="salesFunnel">

<div class="row-fluid" ng-controller="salesFunnelController">

  <ul id="funnelTabs" class="nav nav-tabs" data-tabs="funnelTabs">
    <li class="active" >
      <a href="#lead" data-toggle="tab">Leads</a>
    </li>
    <li>
      <a href="#draft" data-toggle="tab">Draft</a>
    </li>
    <li>
      <a href="#sent" data-toggle="tab">Sent</a>
    </li>
  </ul>

  <div id="funnelTabsContent" class="tab-content">

    <!-- LEADS TAB -->
    <div class="tab-pane active" id="lead">
      <h4> Total : {{leadsAmount}}€ - Total espéré : {{leadsAmountHope}}€</h4>
      <ul class="unstyled leads">
        <li ng-repeat="lead in leads" class="salesFunnelItem">
          <div class="pull-left">
            <a href="../wikitty/edit/{{lead.meta.id}}"/>
              <i class="glyphicon glyphicon-edit"></i>
            </a>
            <small>
              <a href="../wikitty/Employee/view/{{lead.getField('Quotation', 'customer')}}">{{dependencies[dependencies[lead.getField('Quotation','customer')].getField('Employee','company')].getField('Company','name')}}</a>
            </small>
            <p>
              <span class="salesFunnelItemTitle">
                <a class="name-link" href="../wikitty/Project/view/{{lead.getField('Quotation','project')}}"/>{{dependencies[lead.getField('Quotation','project')].getField('Project','name')}}</a>
              </span> - <small>{{lead.getField('Quotation','description')}}</small>
            </p>
          </div>
          <a class="btn btn-success pull-right" ng-click="leadToDraft(lead)">Envoyer</a>
          <a class="btn btn-warning pull-right" ng-click="cancel(lead)">Annuler</a>
          <div class="pull-right">
            <p class="salesFunnelItemTitle">{{lead.getField('Quotation','amount')}} €</p>
            <small>{{dependencies[lead.getField('Quotation','category')].getField('WikittyTreeNode','name')}} - {{lead.getField('Quotation','estimatedDays')}}j - {{lead.getField('Quotation','conversionHope')}} %</small>
          </div>
          <div style="clear:both;"/>
        </li>
      </ul>
    </div>

    <!-- DRAFT TAB -->
    <div class="tab-pane" id="draft">
      <h4>Total : {{draftsAmount}}€ - Total espéré : {{draftsAmountHope}}€</h4>
      <ul class="unstyled drafts">
        <li ng-repeat="draft in drafts" class="salesFunnelItem">
          <div class="pull-left">
            <a href="../wikitty/edit/{{draft.meta.id}}"/><i class="glyphicon glyphicon-edit"></i></a>
            <small><a href="../wikitty/Employee/view/{{draft.getField('Quotation', 'customer')}}">{{dependencies[dependencies[draft.getField('Quotation','customer')].getField('Employee','company')].getField('Company','name')}}</a><span ng-hide="!draft.getField('Draft','sendingDate')"> - Deadline:{{draft.getField('Draft','sendingDate') | date:'dd/MM/yyyy'}}</span></small>
            <p>
              <span class="salesFunnelItemTitle">
                <a class="name-link" href="../wikitty/Project/view/{{draft.getField('Quotation','project')}}"/>{{dependencies[draft.getField('Quotation','project')].getField('Project','name')}}</a>
              </span> - <small>{{draft.getField('Quotation','description')}}</small>
            </p>
          </div>
          <a class="btn btn-success pull-right" ng-click="draftToSent(draft)">Envoyer</a>
          <a class="btn btn-warning pull-right" ng-click="cancel(draft)">Annuler</a>
          <div class="pull-right">
            <p class="salesFunnelItemTitle">{{draft.getField('Quotation','amount')}} €</p>
            <small>{{dependencies[draft.getField('Quotation','category')].getField('WikittyTreeNode','name')}} - {{draft.getField('Quotation','estimatedDays')}}j - {{draft.getField('Quotation','conversionHope')}} %</small>
          </div>
          <div style="clear:both;"/>
        </li>
      </ul>
    </div>

    <!-- SENT TAB -->
    <div class="tab-pane" id="sent">
      <h4> Total : {{sentsAmount}}€ - Total espéré : {{sentsAmountHope}}€</h4>
      <ul class="unstyled sents">
        <li ng-repeat="sent in sents" class="salesFunnelItem">
          <div class="pull-left">
            <a href="../wikitty/edit/{{sent.meta.id}}"/><i class="glyphicon glyphicon-edit"></i></a>
            <small><a href="../wikitty/Employee/view/{{sent.getField('Quotation', 'customer')}}">{{dependencies[dependencies[sent.getField('Quotation','customer')].getField('Employee','company')].getField('Company','name')}}</a><span ng-hide="!sent.getField('Sent','postedDate')"> - Répondu le:{{sent.getField('Sent','postedDate') | date:'dd/MM/yyyy'}}</span></a></small>
            <p>
              <span class="salesFunnelItemTitle">
                <a class="name-link" href="../wikitty/Project/view/{{sent.getField('Quotation','project')}}"/>{{dependencies[sent.getField('Quotation','project')].getField('Project','name')}}</a>
              </span> - <small>{{sent.getField('Quotation','description')}}</small>
            </p>
          </div>
          <a class="btn btn-success pull-right" ng-click="sentToAccepted(sent)">Accepté</a>
          <a class="btn btn-danger pull-right" ng-click="sentToRejected(sent)">Rejeté</a>
          <a class="btn btn-warning pull-right" ng-click="cancel(sent)">Annuler</a>
          <div class="pull-right">
            <p class="salesFunnelItemTitle">{{sent.getField('Quotation','amount')}} €</p>
            <small>{{dependencies[sent.getField('Quotation','category')].getField('WikittyTreeNode','name')}} - {{sent.getField('Quotation','estimatedDays')}}j - {{sent.getField('Quotation','conversionHope')}} %</small>
          </div>
          <div style="clear:both;"/>
        </li>
      </ul>
    </div>
  </div>

</div>
</div>

<div style="clear:both;"/>
</body>

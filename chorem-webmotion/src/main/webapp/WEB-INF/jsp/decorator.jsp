<%--
  #%L
  Chorem :: webmotion
  %%
  Copyright (C) 2011 - 2012 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="/WEB-INF/wikitty.tld" prefix="w"%>

<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Chorem</title>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=UTF-8" />
        <meta charset="UTF-8"/>

        <%-- import via nuiton-js to add js or css show WEB-INF/wro.xml --%>
        <link href="<c:url value='/nuiton-js/chorem-lib.css'/>" rel="stylesheet" type="text/css"/>

        <link href="<c:url value='/css/chorem.less'/>" rel="stylesheet/less">
        <link href="<c:url value='/css/chorem-crm.css'/>" rel="stylesheet" type="text/css"/>
        <link href="<c:url value='/css/chorem-sales.css'/>" rel="stylesheet" type="text/css"/>
        <link href="<c:url value='/css/chorem-bi.css'/>" rel="stylesheet" type="text/css"/>
        <link href="<c:url value='/css/ng-grid.min.css'/>" rel="stylesheet" type="text/css"/>
        <link href="<c:url value='/css/select2/select2.css'/>" rel="stylesheet" type="text/css"/>
        <link href="<c:url value='/nuiton-js/bootstrap.css'/>" rel="stylesheet" type="text/css" />

        <script type="text/javascript">
            var webContext = "<c:url value='/'/>";
        </script>

        <%-- import via nuiton-js to add js or css show WEB-INF/wro.xml --%>
        <script type="text/javascript" src="<c:url value='/nuiton-js/chorem-lib.js'/>?minimize=false"></script>

        <script type="text/javascript" src="<c:url value='/js/chorem.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/ng-wikitty.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/js-hypercube.min.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/ng-grid-2.0.7.debug.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/select2.min.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/ng-select2.js'/>"></script>
  </head>
    <body>
        <div class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="navbar-inner">
                <div class="container">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="<c:url value="/"/>">Chorem</a>

                    <!-- Everything you want hidden at 940px or less, place within here -->
                    <div class="navbar-collapse collapse">

                        <ul class="nav navbar-nav">
                            <li class="dropdown nav-group">
                                <a href="<c:url value="/wikitty/search?extension=Company&extension=Person&extension=Employee&extension=Touch"/>">Contact</a>
                                <a href="#"
                                   class="dropdown-toggle"
                                   data-toggle="dropdown">
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="<c:url value="/wikitty/Company/search"/>"><span class="glyphicon glyphicon-th-list"></span> All company</a></li>
                                    <li><a href="<c:url value="/wikitty/Company/edit/new"/>"><span class="glyphicon glyphicon-plus glyphicon-black"></span> Add company</a></li>
                                    <li class="divider"></li>
                                    <li><a href="<c:url value="/wikitty/Person/search"/>"><span class="glyphicon glyphicon-th-list"></span> All person</a></li>
                                    <li><a href="<c:url value="/wikitty/Person/edit/new"/>"><span class="glyphicon glyphicon-plus"></span> Add person</a></li>
                                    <li class="divider"></li>
                                    <li><a href="<c:url value="/wikitty/Employee/search"/>"><span class="glyphicon glyphicon-th-list"></span> All employee</a></li>
                                    <li><a href="<c:url value="/wikitty/Employee/edit/new"/>"><span class="glyphicon glyphicon-plus"></span> Add employee</a></li>
                                    <li class="divider"></li>
                                    <li><a href="<c:url value="/wikitty/ContactDetails/search"/>"><span class="glyphicon glyphicon-th-list"></span> All contact details</a></li>
                                    <li><a href="<c:url value="/wikitty/ContactDetails/edit/new"/>"><i class="glyphicon glyphicon-plus"></i> Add contact details</a></li>
                                    <li class="divider"></li>
                                    <li><a href="<c:url value="/wikitty/Touch/search"/>"><span class="glyphicon glyphicon-th-list"></span> All touch</a></li>
                                    <li><a href="<c:url value="/wikitty/Touch/edit/new"/>"><i class="glyphicon glyphicon-plus"></i> Add touch</a></li>
                                    <li class="divider"></li>
                                    <li><a href="<c:url value="/crm/export"/>"> Export</li>
                                </ul>
                            </li>
                            <li class="dropdown nav-group">
                                <!-- around projects -->
                                <a href="<c:url value="/report?report=quotation,projectOpen,projectClosed"/>">Project</a>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>Current project</li>
                                    <li><a href="<c:url value="/wikitty/Project/search"/>"><span class="glyphicon glyphicon-th-list"></span> All projects</a></li>
                                    <li><a href="<c:url value="/wikitty/Project/edit/new"/>"><span class="glyphicon glyphicon-plus"></span> Add project</a></li>
                                    <li><a href="<c:url value="/project"/>"><span class="glyphicon glyphicon-th-list"></span> Dashboard project</a></li>
                                    <li><a href="<c:url value="/project/multi"/>"><span class="glyphicon glyphicon-th-list"></span> Dashboard multi-project</a></li>
                                    <li class="divider"></li>
                                    <li>Propale</li>
                                    <li><a href="<c:url value="/wikitty/Quotation/search"/>"><span class="glyphicon glyphicon-th-list"></span> All quotations</a></li>
                                    <li><a href="<c:url value="/wikitty/Quotation/edit/new"/>"><span class="glyphicon glyphicon-plus"></span> Add quotation</a></li>
                                    <li class="divider"></li>
                                    <li>Task</li>
                                    <li><a href="<c:url value="/wikitty/Task/search"/>"><span class="glyphicon glyphicon-th-list"></span> All tasks</a></li>
                                    <li><a href="<c:url value="/wikitty/Task/edit/new"/>"><span class="glyphicon glyphicon-plus"></span> Add task</a></li>
                                    <li class="divider"></li>
                                    <li>Worker</li>
                                    <li><a href="<c:url value="/wikitty/Worker/search"/>"><span class="glyphicon glyphicon-th-list"></span> All workers</a></li>
                                    <li><a href="<c:url value="/wikitty/Worker/edit/new"/>"><span class="glyphicon glyphicon-plus"></span> Add worker</a></li>
                                    <li><a href="<c:url value="/project/employee"/>"><span class="glyphicon glyphicon-plus"></span> Employee dashboard</a></li>
                                    <li class="divider"></li>
                                    <li>Time</li>
                                    <li><a href="<c:url value="/wikitty/Time/search"/>"><span class="glyphicon glyphicon-th-list"></span> All times</a></li>
                                    <li><a href="<c:url value="/wikitty/Time/edit/new"/>"><span class="glyphicon glyphicon-plus"></span> Add time</a></li>
                                    <li class="divider"></li>
                                    <li>Report</li>
                                    <li><a href="<c:url value="/report?report=quotation"/>">Quotation</a></li>
                                    <li><a href="<c:url value="/report?report=projectOpen"/>">Project Open</a></li>
                                    <li><a href="<c:url value="/report?report=projectClosed"/>">Project Closed</a></li>
                                </ul>
                            </li>
                            <li class="dropdown nav-group">
                                <!-- around sales -->
                                <a>Commercial</a>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="<c:url value="/sales/funnel"/>"><span class="glyphicon glyphicon-tasks"></span> Tunnel de commande</a></li>
                                    <li><a href="<c:url value="/sales/report/sales"/>"><span class="glyphicon glyphicon-th-list"></span> Rapports</a></li><li class="divider"></li>
                                    <li>Propale</li>
                                    <li><a href="<c:url value="/wikitty/Quotation/search"/>"><span class="glyphicon glyphicon-th-list"></span> All quotations</a></li>
                                    <li><a href="<c:url value="/wikitty/Quotation/edit/new"/>"><span class="glyphicon glyphicon-plus"></span> Add quotation</a></li>
                                </ul>
                            </li>
                            <li class="dropdown nav-group">
                                <!-- around financial -->
                                <a href="<c:url value="/report?report=invoiceDebt,invoiceIncome,profitability,budget"/>">Financial</a>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                            <a href="<c:url value="/wikitty/Quotation/search"/>"><span class="glyphicon glyphicon-th-list"></span>Quotations</a>
                                            <a href="<c:url value="/wikitty/Quotation/edit/new"/>"><span class="glyphicon glyphicon-plus"></span></a>
                                    </li>
                                    <li>
                                            <a href="<c:url value="/wikitty/FinancialTransaction/search"/>"><span class="glyphicon glyphicon-th-list"></span>FinancialTransaction</a>
                                            <a href="<c:url value="/wikitty/FinancialTransaction/edit/new"/>"><span class="glyphicon glyphicon-plus"></span></a>
                                    </li>
                                    <li>
                                        <a href="<c:url value="/wikitty/Invoice/search"/>"><span class="glyphicon glyphicon-th-list"></span>Invoices</a>
                                        <a href="<c:url value="/wikitty/Invoice/edit/new"/>"><span class="glyphicon glyphicon-plus"></span></a>
                                    </li>
                                    <li>
                                        <a href="<c:url value="/wikitty/Invoice/search?query=FinancialTransaction.beneficiary%3D%22Code+Lutin%22"/>"><span class="glyphicon glyphicon-th-list"></span>Crédits-Ventes</a>
                                        <a href="<c:url value="/wikitty/Invoice/edit/new"/>"><span class="glyphicon glyphicon-plus"></span></a>
                                    </li>
                                    <li>
                                        <a href="<c:url value="/wikitty/Invoice/search?query=FinancialTransaction.payer%3D%22Code+Lutin%22"/>"><span class="glyphicon glyphicon-th-list"></span>Débits-Achats</a>
                                        <a href="<c:url value="/wikitty/Invoice/edit/new"/>"><span class="glyphicon glyphicon-plus"></span></a>
                                    </li>
                                    <li>
                                        <a href="<c:url value="/wikitty/Category/search"/>"><span class="glyphicon glyphicon-th-list"></span>Categories</a>
                                        <a href="<c:url value="/wikitty/Category/edit/new"/>"><span class="glyphicon glyphicon-plus"></span></a>
                                    </li>
                                    <li>
                                        <a href="<c:url value="/financial/expenseAccounts"/>"><span class="glyphicon glyphicon-th-list">Expense Accounts</span></a>
                                        <a href="<c:url value="/financial/expenseAccounts/new"/>"><span class="glyphicon glyphicon-plus">New Exp. Account</span></a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>Report</li>
                                    <li><a href="<c:url value="/report?report=vat"/>">TVA mensuel</a></li>
                                    <li><a href="<c:url value="/report?report=invoiceDebt"/>">Debt</a></li>
                                    <li><a href="<c:url value="/report?report=invoiceIncome"/>">Income</a></li>
                                    <li><a href="<c:url value="/report?report=profitability"/>">Profitability</a></li>
                                    <li><a href="<c:url value="/report?report=budget"/>">Budget</a></li>
                                    <li><a href="<c:url value="/report?report=annualProfit"/>">Annual profit</a></li>
                                    <li><a href="<c:url value="/financial/report/billing"/>">Facturation</a></li>
                                </ul>
                            </li>
                            <li class="dropdown nav-group">
                                <a href="<c:url value="/hr"/>">HR</a>
                                <a href="#"
                                   class="dropdown-toggle"
                                   data-toggle="dropdown">
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>Reference Year</li>
                                    <li><a href="<c:url value="/wikitty/ReferenceYear/search"/>"><span class="glyphicon glyphicon-th-list"></span> All years</a></li>
                                    <li><a href="<c:url value="/wikitty/ReferenceYear/edit/new"/>"><span class="glyphicon glyphicon-plus"></span> Add year</a></li>
                                    <li>Employee HR</li>
                                    <li><a href="<c:url value="/wikitty/EmployeeHR/search"/>"><span class="glyphicon glyphicon-th-list"></span> All employees HR</a></li>
                                    <li><a href="<c:url value="/wikitty/EmployeeHR/edit/new"/>"><span class="glyphicon glyphicon-plus"></span> Add employee HR</a>
                                    <li><a href="<c:url value="/hr/employeeEdit"/>">Employee edit</a></li>
                                    <li><a href="<c:url value="/hr/dashboardAdc"/>">Adc Dashboard</a></li>
                                    <li>Vacation</li>
                                    <li><a href="<c:url value="/wikitty/Vacation/search"/>"><span class="glyphicon glyphicon-th-list"></span> All vacations</a></li>
                                    <li><a href="<c:url value="/wikitty/Vacation/edit/new"/>"><span class="glyphicon glyphicon-plus"></span> Add vacation</a></li>
                                    <li>Vacation Request</li>
                                    <li><a href="<c:url value="/wikitty/VacationRequest/search"/>"><span class="glyphicon glyphicon-th-list"></span> All requests</a></li>
                                    <li><a href="<c:url value="/wikitty/VacationRequest/edit/new"/>"><span class="glyphicon glyphicon-plus"></span> Add request</a></li>
                                    <li><a href="<c:url value="/report?report=requestVacation"/>"><span class="glyphicon glyphicon-th-list"></span> Dashboard vacations</a></li>
                                    <li><a href="<c:url value="/hr/vacationRequest/edit/new"/>"><span class="glyphicon glyphicon-plus"></span> Add Leave Request</a></li>
                                    <li>Expense Accounts</li>
                                    <li><a href="<c:url value="/financial/ExpenseAccounts"/>"><span class="glyphicon glyphicon-th-list"></span>Expense Accounts</a></li>
                                    <li><a href="<c:url value="/financial/ExpenseAccounts/new"/>"><span class="glyphicon glyphicon-plus"></span>Add expense account</a></li>
                                </ul>
                            </li>
                            <li class="dropdown nav-group">
                                <a href="<c:url value="/admin"/>">Admin</a>
                                <a href="#"
                                   class="dropdown-toggle"
                                   data-toggle="dropdown">
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>User</li>
                                    <li><a href="<c:url value="/wikitty/WikittyUser/search"/>"><span class="glyphicon glyphicon-th-list"></span> All users</a></li>
                                    <li><a href="<c:url value="/wikitty/WikittyUser/edit/new"/>"><span class="glyphicon glyphicon-plus"></span> Add user</a></li>
                                    <li class="divider"></li>
                                    <li><a href="<c:url value="/admin/variables"/>">Variables</a></li>
                                    <li class="divider"></li>
                                    <li><a href="<c:url value="/admin/importExport"/>">Import/Export</a></li>
                                    <li class="divider"></li>
                                    <li><a href="<c:url value="/admin/reindex"/>">Reindex</a></li>
                                    <li class="divider"></li>

                                </ul>
                            </li>
                        </ul>

                        <form class="navbar-form navbar-right" action="<c:url value="/wikitty/search"/>">
                            <input type="text" class="search-query" placeholder="Search" name="query" value="${fn:escapeXml(param.query)}"/>
                        </form>
                    </div>

                    <c:if test="${client.user != null}">
                        <w:display wikitty="${client.user}"/>
                        <w:display wikitty="${client.defaultCompany}"/>
                        <a href="<c:url value='/logout'/>">Logout</a>
                    </c:if>

                </div>
            </div>
        </div>

        <div class="container">
            <c:if test="${not empty slotMenu}" >
                <jsp:include page="${slotMenu}"></jsp:include>
            </c:if>
        </div>

        <c:if test="${not empty flashMessages.errors.message}">
            <div class="alert alert-error">
                ...  ${flashMessages.errors.message}
            </div>
        </c:if>

        <c:if test="${not empty flashMessages.infos.message}">
            <div class="alert alert-info">
                ...  ${flashMessages.infos.message}
            </div>
        </c:if>

            <div class="container-fluid well">
                <c:if test="${not empty slotContent}" >
                    <jsp:include page="${slotContent}"></jsp:include>
                </c:if>
        </div>
    </body>
</html>

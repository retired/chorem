<%--
  #%L
  Chorem :: webmotion
  %%
  Copyright (C) 2011 - 2012 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/wikitty.tld" prefix="w"%>

<h1>Budget</h1>

<form>
    <input type="hidden" name="report" value="${report}"/>
    <input type="hidden" name="useDate" value="${useDate}" />
    <c:if test="${sum != 'year'}">
      <input type="hidden" name="sum" value="month" />
    </c:if>
    <c:if test="${sum == 'year'}">
      <input type="hidden" name="sum" value="year" />
    </c:if>
    <c:if test="${useDate != 'false'}">
        <input type="hidden" name="start" value="${fn:escapeXml(start)}"/>
        <input type="hidden" name="end" value="${fn:escapeXml(end)}"/>
    </c:if>
    <input type="hidden" name="query" value="${fn:escapeXml(query)}"/>

    profondeur <input type="text" name="depth" value="${depth}"/>
    <input type="submit" class="btn"/>
</form>

    <c:url var="asciireport" value="/ascii/budget">
        <c:param name="useDate" value="${useDate}"/>
        <c:param name="start" value="${start}"/>
        <c:param name="end" value="${end}"/>
        <c:param name="query" value="${query}"/>
        <c:param name="depth" value="${depth}"/>
    </c:url>

    <a href="${asciireport}">export ascii</a>

<table class="table table-striped table-bordered table-condensed">
    <thead>
        <tr>
            <th>Category</th>
            <c:forEach var="d" items="${data.dates}">
                <th>${d}</th>
            </c:forEach>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="c" items="${data.categoriesTree}">
            <tr>
                <td class="header"><span class="level level${c.level}" level="${c.level}">${c.userObject}</span></td>
                <c:forEach var="d" items="${data.dates}">
                    <td class="currency">
                       <c:if test="${sum != 'year'}">
                            <c:url var="addDebtUrl" value="/wikitty/FinancialTransaction/edit/new">
                                <c:param name="FinancialTransaction.payer" value="${companyId}"/>
                                <c:param name="FinancialTransaction.emittedDate" value="today"/>
                                <c:param name="FinancialTransaction.expectedDate" value="01/${d}"/>
                                <c:param name="FinancialTransaction.paymentDate" value="01/${d}"/>
                                <c:param name="FinancialTransaction.category" value="${c.userObject.wikittyId}"/>
                            </c:url>
                            <c:url var="addIncomeUrl" value="/wikitty/FinancialTransaction/edit/new">
                                <c:param name="FinancialTransaction.beneficiary" value="${companyId}"/>
                                <c:param name="FinancialTransaction.emittedDate"  value="today=${d.substring(3,7)}${d.substring(0,2)}"/>
                                <c:param name="FinancialTransaction.expectedDate"  value="today=${d.substring(3,7)}${d.substring(0,2)}+30DAYS"/>
                                <c:param name="FinancialTransaction.category"  value="${c.userObject.wikittyId}"/>
                            </c:url>
                        </c:if>
                        <c:choose>
                            <c:when test="${data.getInvoices(d, c.userObject).isEmpty()}">
                                <div class="withPopover"
                                     title='Aucune entrée <span><a href="${addDebtUrl}"><li class="icon-plus"></li>Debt</a><span> <span><a href="${addIncomeUrl}"><li class="icon-plus"></li>Income</a><span>'>
                                    <f:formatNumber type="currency" value="${data.getAmount(d, c.userObject)}"/>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <c:set var="prev" value="0"/>
                                <c:forEach var="invoice" items="${data.getInvoices(d, c.userObject)}}">
                                    <c:if test='${invoice.matches("(.*)PREV(.*)")}'>
                                        <c:set var="prev" value="1"/>
                                    </c:if>
                                </c:forEach>
                                <div class="withPopover"
                                     title='Detail pour <f:formatNumber type="currency" value="${data.getAmount(d, c.userObject)}"/> <span><a href="${addDebtUrl}"><li class="icon-plus"></li>Debt</a><span> <span><a href="${addIncomeUrl}"><li class="icon-plus"></li>Income</a><span>'
                                     data-content='<w:display wikitties="${data.getInvoices(d, c.userObject)}" toString="<span class=\"withTooltip\" title=\"%FinancialTransaction.category$s: %FinancialTransaction.payer$s -&gt; %FinancialTransaction.beneficiary$s\">%FinancialTransaction.reference|noref$s: %FinancialTransaction.amount|0.0$,.2f</span>"/>'>
                                    <c:if test="${prev==1}">
                                        <i class="icon-warning-sign icon-black"></i>
                                    </c:if>
                                    <f:formatNumber type="currency" value="${data.getAmount(d, c.userObject)}"/>
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </c:forEach>
            </tr>
        </c:forEach>
            <tr>
                <th><span>Sorties</span></th>
                <c:forEach var="d" items="${data.dates}">
                    <td class="currency"><span><f:formatNumber type="currency" value='${data.getDebt(d)}'/></span></td>
                </c:forEach>
            </tr>
            <tr>
                <th><span>Entrées</span></th>
                <c:forEach var="d" items="${data.dates}">
                    <td class="currency"><span><f:formatNumber type="currency" value='${data.getIncome(d)}'/></span></td>
                </c:forEach>
            </tr>
            <tr>
                <th><span>Total</span></th>
                <c:forEach var="d" items="${data.dates}">
                    <td class="currency"><span><f:formatNumber type="currency" value='${data.getTotal(d)}'/></span></td>
                </c:forEach>
            </tr>
            <tr>
                <th><span>Total cumulé</span></th>
                <c:forEach var="d" items="${data.dates}">
                    <td class="currency"><span><f:formatNumber type="currency" value='${data.getFinances(d)}'/></span></td>
                </c:forEach>
            </tr>
    </tbody>
</table>

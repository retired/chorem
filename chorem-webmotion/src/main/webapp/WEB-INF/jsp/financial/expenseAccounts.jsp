<%--
  #%L
  Chorem :: webmotion
  %%
  Copyright (C) 2011 - 2014 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/wikitty.tld" prefix="w"%>

<head>
    <link data-require="ng-table@*" data-semver="0.3.0" rel="stylesheet" href="<c:url value='http://bazalt-cms.com/assets/ng-table/0.3.0/ng-table.css'/>" />

    <script data-require="angular-resource@1.2.0-rc.3" data-semver="1.2.0-rc.3" src="<c:url value='http://code.angularjs.org/1.2.0-rc.3/angular-resource.js'/>"></script>
    <script data-require="ng-table@*" data-semver="0.3.0" src="<c:url value='http://bazalt-cms.com/assets/ng-table/0.3.0/ng-table.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/paginationTable.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/financial/expenseAccountListCtrl.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/angular-ui-bootstrap-tpls-0.11.0.js'/>"></script>
    <script>
        angular.module('Pagination').value('paginationInit', {
            url : "<c:url value="/rest/financial/expenseAccounts"/>",
            params: {page: 1, count: 10}
        });
    </script>
</head>

<div ng-app="expenseAccountPagination" ng-controller="expenseAccountListCtrl">

    <div>
        From <input type="text" ng-model="startTime" datepicker-popup="dd/MM/yyyy" is-open="startDateOpened" ng-click="startDateOpened = true">
        to <input type="text" ng-model="endTime" datepicker-popup="dd/MM/yyyy" is-open="endDateOpened" ng-click="endDateOpened = true"/>
    </div>

    <div loading-container="tableParams.settings().$loading">
      <table ng-table="tableParams" show-filter="false" class="table">
        <tbody ng-repeat="expenseAccount in elements">
          <tr id="tr{{expenseAccount.id}}" ng-class-odd="'odd'" ng-class-even="'even'">
            <td data-title="'Employee'">
                    <a href="<c:url value="/wikitty/view/{{expenseAccount.employeeId}}"/>">{{expenseAccount.employeeName}}</a>
                </td>
            <td data-title="'Start Date'">
                    {{expenseAccount.startDate |date : "dd/MM/yyyy"}}
                </td>
            <td data-title="'End Date'">
                    {{expenseAccount.endDate |date : "dd/MM/yyyy"}}
                </td>
            <td>
                    <a class="btn btn-success" href="<c:url value="/financial/expenseAccounts/{{expenseAccount.id}}"/>">See it</a>
                </td>
          </tr>
        </tbody>
      </table>
    </div>
</div>

<div>
  <a class="btn btn-success" href="<c:url value="/financial/expenseAccounts/new"/>"><i class="icon-list icon-white"></i> New one</a>
</div>

<%--
  #%L
  Chorem :: webmotion
  %%
  Copyright (C) 2011 - 2012 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/wikitty.tld" prefix="w"%>

<p>
    <c:if test="${not empty wikitty}">
        <a class="btn btn-success" href="<c:url value="/wikitty/view/${wikitty.id}"/>"><i class="icon-list icon-white"></i> View all extension</a>
        <a class="btn btn-success" href="<c:url value="/wikitty/edit/${wikitty.id}"/>"><i class="icon-pencil icon-white"></i> Edit object</a>
        <a class="btn btn-success" href="<c:url value="/wikitty/copy/${wikitty.id}"/>"><i class="icon-random icon-white"></i> Copy object</a>
        <a class="btn btn-danger" href="<c:url value="/wikitty/delete/${wikitty.id}?wmDecoratorNo=true"/>"><i class="icon-trash icon-white"></i> Delete object</a>
    </c:if>
</p>

<c:forEach var="ext" items="${extensions}">
    <dl><dt><a href="<c:url value="/wikitty/search?extension=${ext.name}"/>">${ext.name}</a>
        <a class="btn btn-success btn-xs" href="<c:url value="/wikitty/edit/${wikitty.id}?extension=${ext.name}"/>"><i class="icon-pencil icon-white"></i> Edit</a>
        <a class="btn btn-success btn-xs" href="<c:url value="/wikitty/editMulti/${wikitty.id}"/>"><i class="icon-pencil icon-white"></i> Edit Multi</a>
        <a class="btn btn-danger btn-xs" href="<c:url value="/wikitty/delete/${wikitty.id}?extension=${ext.name}&wmDecoratorNo=true"/>"><i class="icon-trash icon-white"></i> Delete</a></dt>
    <c:forEach var="fieldName" items="${ext.fieldNames}">
        <dd><w:display wikitty="${wikitty}" fqfield="${ext.name}.${fieldName}"/></dd>
    </c:forEach>
    </dl>
</c:forEach>

<div class="container">
    <c:if test="${not empty wikitty}">
        <a class="btn btn-success" href="<c:url value="/wikitty/Attachment/edit/new?Attachment.target=${wikitty.id}"/>"><i class="icon-plus icon-white"></i> Add Attachment</a>
        <a class="btn btn-success" href="<c:url value="/wikitty/Note/edit/new?Note.target=${wikitty.id}"/>"><i class="icon-plus icon-white"></i> Add Note</a>
    </c:if>

    <c:forEach var="viewAction" items="${viewActions}">
        <jsp:include page="${viewAction}"/>
    </c:forEach>
</div>

<div class="container">
    <jsp:include page="/wikitty/searchRelated?id=${wikitty.id}"/>
</div>

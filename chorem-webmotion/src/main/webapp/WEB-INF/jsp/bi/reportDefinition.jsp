<%--
  #%L
  Chorem :: webmotion
  %%
  Copyright (C) 2011 - 2012 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/wikitty.tld" prefix="w"%>

    <script>

        var fieldCtrl = ['$scope', 'Wikitty', 'Select', 'Facet', function ($scope, Wikitty, Select, Facet) {
            $scope.wikittyId = '';
            $scope.wikitty = {}
            $scope.filter = "";
            $scope.select = "";
            $scope.selected = {};
            $scope.facet = {};
            $scope.data = {};
            $scope.select2 = {};

            $scope.gridOptions = {
                data: 'data',
//                excludeProperties: "$$Hash",
                //enablePinning: true,
                enableColumnReordering: true,
                showFooter: true,
                showFilter: true,
                //showColumnMenu: true,
                showGroupPanel: true,
                aggregateTemplate:
                        "<div ng-click=\"row.toggleExpand()\" ng-style=\"rowStyle(row)\" class=\"ngAggregate\">" +
                        "    <span class=\"ngAggregateText\">{{row.label CUSTOM_FILTERS}} ({{sumChildren(row)}}) \#{{row.totalChildren()}}</span>" +
                        "    <div class=\"{{row.aggClass()}}\"></div>" +
                        "</div>"
            };

$scope.sumChildren = function (row) {
    if (row.aggChildren.length > 0) {
        var sum = 0;
        var recurse = function (cur) {
            if (cur.aggChildren.length > 0) {
                angular.forEach(cur.aggChildren, function (a) {
                    recurse(a);
                });
            } else {
                angular.forEach(cur.children, function(c) {
                    sum += parseFloat(c.entity.amount);
                });
            }
        };
        recurse(row);
        return sum;
    } else {
        var sum = 0;
        angular.forEach(row.children, function(c) {
            sum += parseFloat(c.entity.amount);
        });
        return sum;
    }
};

            $scope.select2Options = {
                closeOnSelect: false,
            };

            $scope.getFilter = function() {
                var result = $scope.filter;
                var sep = "";
                if (result != "") {
                    sep = " AND ";
                }
                angular.forEach($scope.select2, function(values, facetName) {
                    if (values && values.length) {
                        result += sep + "(";
                        var sep2 = "";
                        angular.forEach(values, function(topic) {
                            result += sep2 + facetName + "='" + topic + "'";
                            sep2 = " OR ";
                        });
                        result += ")";
                        sep = " AND ";
                    }
                });

                return result;
            }

            $scope.addFacet = function(data) {
                angular.forEach(data, function(facet, fieldName) {
                    var topicNames = facet.map(function(topic) {
                        return topic.topicName;
                    });

                    var info = $scope.wikitty.getFieldInfo(fieldName);
                    if (info.type === "WIKITTY") {
                        Wikitty.query(topicNames, function(w) {
                            $scope.facet[fieldName] = w.map(function(v) {
                                return {label: v.display(), value:v.getId()};
                            });
                        });
                    } else {
                        $scope.facet[fieldName] = topicNames.map(function(v) {
                                return {label: v, value: v};
                            });
                    }
                });
            };

        $scope.selectField = function(extName, field) {
            var fq = extName + '.' + field;
            if ($scope.selected[fq]) {
                Facet($scope.filter, fq, '', function(data) {
                    $scope.addFacet(data);
                });
            } else {
                delete $scope.facet[fq];
            }
        };

        $scope.refreshFacet = function() {
            Facet($scope.filter, $scope.facet, '', function(data) {
                $scope.addFacet(data);
            });
        };

        $scope.getSelectedField = function() {
            var result = [];
            angular.forEach($scope.selected, function(b, field) {
                if (b) {
                    result.push(field);
                }
            });
            return result;
        };

        $scope.refreshData = function() {
            var filter = $scope.getFilter();
            var select = angular.copy($scope.getSelectedField());
            if ($scope.select) {
                select.push($scope.select);
            }
            var query = "select " + select.join(",");
            if (filter) {
                query += " where (" + filter + ")";
            }
            query += " #limit 200000";
            Select(query, function(data) {
                $scope.data = data.map(function (e) {
                    // on est oblige de remplacer les '.' par des '_' car la grid ne suppoorte pas les '.' :(
                   var r = {};
                   for (var v in e) {
                       var value = e[v];
                       var info = $scope.wikitty.getFieldInfo(v);
                       if (info && info.type === "WIKITTY") {
                           value = $scope.wikitty.getWikitty(value).display();
                       }
                       var simpleName = v.replace(/.*?\./, "");
                       r[simpleName] = value;
                   }
                   return r;
                });
            });
        };

        $scope.loadWikitty = function(id) {
            $scope.wikittyId = id;
            Wikitty.get(id, function (o) {$scope.wikitty = o});
        };

        $scope.loadWikitty('${wikitty.id}');
        }];
    </script>


<div ng-app="wikitty">

<div ng-controller="fieldCtrl" zng-init="init('${wikitty.id}')">

    <dl ng-repeat="extName in wikitty.getExtensionNames()"><dt>{{extName}}
        <dd ng-repeat="(fieldName, fieldInfo) in wikitty.getExtension(extName).fields">
            <span ng-show="fieldInfo.type == 'DATE'"><input ng-click="selectField(extName, fieldName)" ng-model="selected[extName+'.'+fieldName]" type="checkbox"/>Year</span>
            <span ng-show="fieldInfo.type == 'DATE'"><input ng-click="selectField(extName, fieldName)" ng-model="selected[extName+'.'+fieldName]" type="checkbox"/>Quater</span>
            <span ng-show="fieldInfo.type == 'DATE'"><input ng-click="selectField(extName, fieldName)" ng-model="selected[extName+'.'+fieldName]" type="checkbox"/>Month</span>
            <span ng-show="fieldInfo.type == 'DATE'"><input ng-click="selectField(extName, fieldName)" ng-model="selected[extName+'.'+fieldName]" type="checkbox"/>Week</span>
            <span ng-show="fieldInfo.type == 'DATE'"><input ng-click="selectField(extName, fieldName)" ng-model="selected[extName+'.'+fieldName]" type="checkbox"/>Day</span>
            <input ng-click="selectField(extName, fieldName)" ng-model="selected[extName+'.'+fieldName]" type="checkbox"/>
            {{fieldName}} : {{wikitty.display(extName, fieldName)}}
                <i ng-show="fieldInfo.type == 'WIKITTY'" class="icon-plus icon-black"></i>
        </dd>
    </dl>

<div class="container">
    <input type="text" name="filter" ng-change="refreshFacet()" ng-model="filter" placeholder="filter"/>
    <input type="text" name="return" ng-model="select" placeholder="return value"/>
</div>

        <div ng-repeat="(field, topics) in facet">
            <h3>{{field}}</h3>
            <select class="select" ui-select2="select2Options" ng-model="select2[field]" multiple="true">
                <option ng-repeat="t in topics | orderBy:'t.label'" value="{{t.value}}"/>{{t.label}}</option>
            </select>
        </div>

    <button ng-click='refreshData()'>Refresh</button>

    <div class="gridStyle" ng-grid="gridOptions"></div>

</div>


</div>

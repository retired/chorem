<%--
  #%L
  Chorem :: webmotion
  %%
  Copyright (C) 2011 - 2012 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>

<h1>Bénéfice ou perte par année</h1>

<table class="table table-striped table-bordered table-condensed">
    <tr>
    <th>Année</th>
    <th>CA HT</th>
    <th>CA TTC</th>
    <th>Autre revenu HT</th>
    <th>Autre revenu TTC</th>
    <th>Dépense</th>
    <th>CA TTC - Dépense</th>
    <th>Bénéfice/perte</th>
    </tr>
    <c:forEach var="q" items="${annualProfit.keySet()}">
        <tr>
            <th>
                <c:set var="year"><f:formatDate pattern="yyyy" value="${q}"/></c:set>
                <a href='<c:url value="/report?report=budget&start=01/01/${year}&end=31/12/${year}"/>'>
                    ${year}
                </a>
            </th>
            <td>
                <a href='<c:url value="/wikitty/search?query=${annualProfit.get(q).get('incomesQuery')} %23limit=2147483647"/>'>
                    <f:formatNumber type="currency" value="${annualProfit.get(q).get('incomes')}"/>
                </a>
            </td>
            <td>
                <a href='<c:url value="/wikitty/search?query=${annualProfit.get(q).get('incomesQuery')} %23limit=2147483647"/>'>
                    <f:formatNumber type="currency" value="${annualProfit.get(q).get('incomesTTC')}"/>
                </a>
            </td>
            <td>
                <a href='<c:url value="/wikitty/search?query=${annualProfit.get(q).get('extraIncomesQuery')} %23limit=2147483647"/>'>
                    <f:formatNumber type="currency" value="${annualProfit.get(q).get('extraIncomes')}"/>
                </a>
            </td>
            <td>
                <a href='<c:url value="/wikitty/search?query=${annualProfit.get(q).get('extraIncomesQuery')} %23limit=2147483647"/>'>
                    <f:formatNumber type="currency" value="${annualProfit.get(q).get('extraIncomesTTC')}"/>
                </a>
            </td>
            <td>
                <a href='<c:url value="/wikitty/search?query=${annualProfit.get(q).get('debtsQuery')} %23limit=2147483647"/>'>
                    <f:formatNumber type="currency" value="${annualProfit.get(q).get('debtsTTC')}"/>
                </a>
            </td>
            <td>
                <f:formatNumber type="currency" value="${annualProfit.get(q).get('incomesTTC') - annualProfit.get(q).get('debtsTTC')}"/>
            </td>
            <td>
                <f:formatNumber type="currency" value="${annualProfit.get(q).get('incomesTTC') + annualProfit.get(q).get('extraIncomesTTC') - annualProfit.get(q).get('debtsTTC')}"/>
            </td>
        </tr>
    </c:forEach>
</table>

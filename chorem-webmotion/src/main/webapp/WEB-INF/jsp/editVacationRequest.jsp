<%--
  #%L
  Chorem :: webmotion
  %%
  Copyright (C) 2011 - 2012 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/wikitty.tld" prefix="w"%>
<link rel="stylesheet" href="<c:url value="/css/vacationRequest.css"/>">

<c:if test="${not empty ws}">
    <form  class="form-horizontal" method="post" accept-charset="ISO-8859-15" action="<c:url value="/hr/vacationRequest/save"/>" enctype="multipart/form-data">
        <c:forEach var="wikitty" items="${ws}">

            <!-- EmployeeHR -->
            <c:forEach var="ext" items="${wikitty.getExtensions()}">
                <c:if test="${ext.name eq 'EmployeeHR'}">
                    <input type="hidden" name="ids" value="${wikitty.id}"/>
                    <input type="hidden" name="${wikitty.id}.extension" value="${ext.name}">
                    <div class="pull-right">
                        <table>
                            <tr><td><strong>PaidLeave amount : </strong>
                                </td><td><strong> <w:display wikitty="${wikitty}" fqfield="EmployeeHR.paidLeave" label=""/>&nbsp;days</strong></td></tr>
                            <tr><td><strong>Rtt amount : </strong>
                                </td><td><strong> <w:display wikitty="${wikitty}" fqfield="EmployeeHR.rtt" label=""/>&nbsp;days </strong></td></tr>
                            <tr><td><strong>OtherLeave amount : </strong>
                                </td><td><strong> <w:display wikitty="${wikitty}" fqfield="EmployeeHR.otherLeave" label=""/>&nbsp;days </strong></td></tr>
                        </table>
                    </div>
                </c:if>
            </c:forEach>            

            <!-- get(1) est la VacationRequest -->
            <c:forEach var="ext" items="${wikitty.getExtensions()}">
                <c:if test="${ext.name eq 'VacationRequest'}">
                    <input type="hidden" name="ids" value="${wikitty.id}"/>
                    <input type="hidden" name="${wikitty.id}.extension" value="${ext.name}">
                    <c:if test="${wikitty.getFieldAsString('VacationRequest','employeeRequest') eq null}">
                        Employee : 
                        <w:input wikitty="${wikitty}" name="${wikitty.id}.VacationRequest.employeeRequest" fqfield="VacationRequest.employeeRequest"/>
                    </c:if>
                    <h3><w:display wikitty="${wikitty}" fqfield="VacationRequest.employeeRequest" label=""/> : Request for leave periods </h3>        
                    
                    <div hidden="hidden">
                        <w:input wikitty="${wikitty}" name="${wikitty.id}.VacationRequest.employeeRequest" fqfield="VacationRequest.employeeRequest" />
                        <w:input wikitty="${wikitty}" name="${wikitty.id}.VacationRequest.employeeWriter" fqfield="VacationRequest.employeeWriter" />
                    </div>
                    <br />
                    <div class="well">
                        <p> 
                            <button class="btn btn-success" type="submit" href="../../../report?report=requestVacation">
                                <i class="icon-ok icon-white"></i> Save
                            </button>
                            <a class="btn btn-info" href="../../../report?report=requestVacation">
                                <i class="icon-remove icon-white"></i> Cancel
                            </a>
                            <a class="btn btn-warning" href="#">
                                <i class="icon-plus icon-white"></i> Duplicate
                            </a>				
                        <h4>Status of request : 
                            <w:input wikitty="${wikitty}" name="${wikitty.id}.VacationRequest.statusRequest" fqfield="VacationRequest.statusRequest"/>
                        </h4>
                        </p>
                    </div>
                    <br />
                    <div id="bottom" class="row">
                        <br />
                        <input type="button" id="btn-vacation-add" class="btn btn-success" value="Add new period">        
                        <br /><br />
                        <label class="control-label">Comments : </label>
                        <w:input wikitty="${wikitty}" name="${wikitty.id}.VacationRequest.commentRequest" fqfield="VacationRequest.commentRequest" defaultValue="Commentaires"/>
                        <br /><br />
                       
                    </div>
                </c:if>
            </c:forEach>                
        </c:forEach> 
        <!-- Vacations -->         
        <c:forEach var="wikitty" items="${ws}">
            <c:forEach var="ext" items="${wikitty.getExtensions()}">
                <c:if test="${ext.name eq 'Vacation'}">
                    <div>
                        <jsp:include page="/hr/vacationDiv/${wikitty.id}"/>    
                    </div>
                </c:if>
            </c:forEach> 
        </c:forEach>
    </form>
</c:if>

/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
var wikitty = angular.module('wikitty', ['ngResource', 'ngGrid', 'ui.select2']);

wikitty.factory('Wikitty', function($resource){
    var result = $resource(webContext + 'rest/wikitty/:id', {id:'@meta.id'},
    { 'getNoCache':    {method:'GET'},
        'queryNoCache':  {method:'GET', isArray:true},
        'save':   {method:'POST'},
        'delete': {method:'DELETE'} }
    );

    /**
     * Cache commun a toutes les instances, permet de mutualise les informations
     * recupere depuis le serveur via des 'get' ou des 'query'
     */
    result.cache = {
        wikitty: {},
        extension: {}
    };

    /**
     * Retourne le cache utilise par toutes les instances
     * @return {Object}
     */
    result.prototype.getCache = function() {
        return result.cache;
    }

    result.cacheWikitty = function(wikitty) {
        if (angular.isArray(wikitty)) {
            angular.forEach(wikitty, function(w) {
                result.cacheWikitty(w);
            });
        } else {
            result.cache.wikitty[wikitty.meta.id] = new result(wikitty);
        }
    }

    result.cacheExtension = function(extension) {
        if (angular.isArray(extension)) {
            angular.forEach(extension, function(e) {
                result.cacheExtension(e)
            });
        } else {
            result.cache.extension[extension.name] = extension;
        }
    }

    /**
     * Permet de cree un tableau contenant le nom de l'extension et le nom du champs
     * @param {String} extensionOrFqFiel extension name or fq field
     * @param {String} field can be undefine
     * @return {unresolved}
     */
    result.prototype.__getFq__ = function(extensionOrFqFiel, field) {
        var fq;
        if (angular.isArray(extensionOrFqFiel)) {
            fq = extensionOrFqFiel;
        } else {
            fq = extensionOrFqFiel.split(".");
        }
        if (fq.length < 2 && field) {
            fq.push(field);
        }
        return fq;
    };

    result.prototype.getId = function () {
        return this.meta.id;
    };

    /**
     * Permet de retourne une representation humaine:
     * <li> d'un wikitty si pas d'argument
     * <li> d'un extension du wikitty sur seulement un nom d'extension en parametre
     * <li> d'un champs si un champs fq en parametre
     * @param {String} extensionOrFqFiel
     * @param {String} field
     * @return {String}
     */
    result.prototype.display = function (extensionOrFqFiel, field) {
        var result = 'error';
        if (extensionOrFqFiel) {
            var fq = this.__getFq__(extensionOrFqFiel, field);

            if (fq.length > 1) {
                var fieldInfo = this.getFieldInfo(fq);
                var value = this.getField(fq);
                if (value && fieldInfo && fieldInfo.type === 'WIKITTY') {
                    result = this.getCache().wikitty[value].display();
                } else {
                    result = value;
                }
            } else {
                result = this.meta.displayString[fq[0]];
            }
        } else {
            result = this.meta.displayString[""];
        }
        return result;
    };

    result.prototype.getWikitty = function(id, callback) {
        var w = this.getCache().wikitty[id];
        if (w) {
            if (callback) {
                callback(w);
            } else {
                return w;
            }
        } else {
            result.get(id, callback);
        }
    };

    /**
     * Recupere un wikitty sur le serveur et met en cache les informations
     * 
     * @param {String} id
     * @param {function} callback
     * @return {undefine}
     */
    result.get = function(id, callback) {
        var params = id;
        if (!angular.isObject(id)) {
            params = {id:id};
        }
        this.getNoCache(params, function(data) {
            result.cache.wikitty[data.meta.id] = data;
            angular.forEach(data.cache.wikitty, function (w, id) {
                result.cacheWikitty(w);
            });
            angular.forEach(data.cache.extension, function (e, name) {
                result.cacheExtension(e);
            });

            delete data.cache.wikitty;
            delete data.cache.extension;
            
            if (callback) {
                callback(data);
            }
        });
    };

    /**
     * Recupere sur le serveur un ensemble d'objet qui satisfont une requete
     * @param {String|Object|Array of id} query la query a utiliser pour selectionner les objets ou un objet contenant les parametres
     * @param {function} callback le callback a appeler une fois les donnees recuperees
     * @return {undefined}
     */
    result.query = function(query, callback) {
        var params;
        if (angular.isArray(query)) {
            var q = "id="+query.join(" OR id=");
            params = {q:q};
        } else if (!angular.isObject(query)) {
            params = {q:query};
        } else {
            params = query;
        }
        this.queryNoCache(params, function(data) {
            angular.forEach(data, function(v) {
                result.cache.wikitty[v.meta.id] = v;
                angular.extend(result.cache.wikitty, v.cache.wikitty);
                angular.extend(result.cache.extension, v.cache.extension);

                delete v.cache.wikitty;
                delete v.cache.extension;
            });
            if (callback) {
                callback(data);
            }
        });
    };

    /**
     * recupere les noms des extensions utilises par ce wikitty
     * @param {String} extensionOrFqFiel
     * @return {Object}
     */
    result.prototype.getExtensionNames = function() {
        var result = this.meta.extensions;
        return result;
    };

    /**
     * recupere une extension
     * @param {String} extensionOrFqField
     * @return {Object}
     */
    result.prototype.getExtension = function(extensionOrFqField) {
        var fq = this.__getFq__(extensionOrFqField);
        var result = this.getCache().extension[fq[0]];
        return result;
    };

    /**
     * Donne le nom des champs d'une extension
     * @param {String} extensionOrFqField
     * @return {Array of String}
     */
    result.prototype.getExtensionFieldNames = function(extensionOrFqField) {
        var fq = this.__getFq__(extensionOrFqField);
        var result = Object.keys(this.getExtension(fq[0]).fields);
        return result;
    };

    /**
     * Donne le nom des champs de toutes les extensions du wikitty
     * @return {Array of String}
     */
    result.prototype.getFieldNames = function() {
        var result = [];
        angular.forEach(this.getExtensionNames(), function(extName) {
            angular.extend(result, this.getExtensionFieldNames(extName));
        });
        return result;
    };

    /**
     * Recupere les informations sur un champs
     * @param {String} extensionOrFqField
     * @param {String} field
     * @return {Object}
     */
    result.prototype.getFieldInfo = function(extensionOrFqField, field) {
        var result;
        var fq = this.__getFq__(extensionOrFqField, field);
        var ext = this.getExtension(fq[0]);
        if (ext && fq.length > 1) {
            result = ext.fields[fq[1]];
        }
        return result;
    };

    /**
     * Recupere la valeur d'un champs
     * @param {String} extensionOrFqField
     * @param {String} field
     * @return {Object}
     */
    result.prototype.getField = function(extensionOrFqField, field) {
        var fq = this.__getFq__(extensionOrFqField, field);

        var result = this.data[fq[0] + "." + fq[1]];
        if (result && angular.isString(result)) {
            var fieldInfo = this.getFieldInfo(fq);
            if (fieldInfo.type === 'DATE') {
                result = new Date(result);
            }
        }
        return result;
    };

    return result;
});

wikitty.factory('Select', function($http) {
    var result = function(query, callback) {
        $http.get(webContext + 'rest/select', {params:{q:query}}).success(callback);
    };
    return result;
});

wikitty.factory('Facet', function($http) {
    var result = function(query, facetField, facetQuery, callback) {
        var filter = query;
        if (filter == "") {
            filter = "*:*";
        }

        var ff = "";
        var ffs = "";
        if (angular.isObject(facetField)) {
            var sep = "?"
            for (var field in facetField) {
                ffs += sep + "ff=" + field;
                sep = "&";
            }

        } else if (angular.isArray(facetField)) {
            var sep = "?"
            for (var i=0; i<facetField.length; i++) {
                var field = facetField[i];
                ffs += sep + "ff=" + field;
                sep = "&";
            }
        } else {
            ff = facetField;
        }

        $http.get(webContext + 'rest/facet' + ffs, {params:{q:filter, ff:ff, fq:facetQuery}}).success(callback);
    };
    return result;
});

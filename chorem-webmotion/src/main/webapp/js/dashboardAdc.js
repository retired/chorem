/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


$(document).ready(function() {




	$('#calculateBtn').click(function() {

		var rows =  $('.bodyRow');
		var str = "";
		var year = $(this).attr('year');
		for(var i = 0; i < rows.length; i++) {
			str = str + $(rows[i]).attr('id');
			if(i != (rows.length -1)) {
				str= str + ',';
			}

		}
		$(this).attr('disabled', 'disabled');
		$.get(createUrl("/hr/employeeEdit/json/requestMultipleAdc/",str,'?real=true&year=2012'),
				function(ret){
			$('#calculateBtn').removeAttr('disabled');
			var adcs = ret['adcs'];
			if($('.' + year).size() != 0) {
				for(var i in adcs) {
					var row = $('#'+i);
					row.find('.year' + year).text(adcs[i]);
				}
			}else {
				$('.headEmployee').after("<th class='"+year+"'>"+year+"</th>");
				for(var i in adcs) {
					
					var row = $('#'+i);
					row.find('.bodyEmployee').after('<td class="year'+year+'">'+adcs[i]+'</td>');

				}
			}
		});


	});




});

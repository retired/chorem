/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
var app = angular.module('expenseAccountPage', ['ngResource', 'ui.bootstrap']);

app.controller("expenseAccountView", function($scope, $resource, $modal, $log, expenseAccountInit) {

    $scope.showNewLine = false;
    $scope.expenseAccount = expenseAccountInit.expenseAccount;
    $scope.editPeriod = $scope.expenseAccount && $scope.expenseAccount.startDate && $scope.expenseAccount.endDate ? false : true;
    $scope.editEmployee = $scope.expenseAccount && $scope.expenseAccount.employeeId ? false : true;

    $scope.baseURL = expenseAccountInit.baseURL;

    $scope.newLine = function() {
        if ($scope.showNewLine === true) {
            $scope.showNewLine = false;
            delete $scope.newExpenseAccountEntry;
        } else {
            $scope.showNewLine = true;
            $scope.newExpenseAccountEntry = {
                justificationNumber: '',
                emittedDate : '',
                project: '',
                description: '',
                category: '',
                amount: '',
                VAT: '',
                total: ''
            };
        }
    }

    $scope.deleteLine = function(expenseAccountEntry) {
        if (expenseAccountEntry) {
            var toDelete = $scope.expenseAccount.expenseAccountEntries.indexOf(expenseAccountEntry);
            if (toDelete != -1) {
               $scope.expenseAccount.expenseAccountEntries.splice(toDelete, 1);
               $scope.hasChanged = true;
            }
        }
        console.log($scope.expenseAccount);
    }

    $scope.ac_options = {
        baseURL : $scope.baseURL,
    };

    $scope.addLine = function() {

        if(!$scope.expenseAccount.expenseAccountEntries) {
            $scope.expenseAccount.expenseAccountEntries = [];
        }
        $scope.newExpenseAccountEntry.total = $scope.newExpenseAccountEntry.amount + $scope.newExpenseAccountEntry.VAT;

        if (angular.isDate($scope.newExpenseAccountEntry.emittedDate)) {
            $scope.newExpenseAccountEntry.emittedDate = $scope.newExpenseAccountEntry.emittedDate.getTime();
        }

        $scope.expenseAccount.expenseAccountEntries.push($scope.newExpenseAccountEntry);

        $scope.hasChanged = true;
        $scope.showNewLine = false;
        delete $scope.newExpenseAccountEntry;
    };

    $scope.saveExpenseAccount = function() {

        console.log($scope.expenseAccount)

        url = $scope.baseURL + "financial/expenseAccounts/" + $scope.expenseAccount.id;
        if (angular.isDate($scope.expenseAccount.startDate)) {
            $scope.expenseAccount.startDate = $scope.expenseAccount.startDate.getTime();
        }
        if (angular.isDate($scope.expenseAccount.endDate)) {
            $scope.expenseAccount.endDate = $scope.expenseAccount.endDate.getTime();
        }

        $resource('url', { expenseAccountId : $scope.expenseAccount.id, expenseAccountBean : $scope.expenseAccount }, {'update' : {method:'PUT'}}).update(function(result) {
            $scope.hasChanged = false;
            $scope.editPeriod = $scope.expenseAccount && $scope.expenseAccount.startDate && $scope.expenseAccount.endDate ? false : true;
            $scope.editEmployee = $scope.expenseAccount && $scope.expenseAccount.employeeId ? false : true;
        });
    };

});


app.directive("autoComplete", function() {
	return {
		require  : 'ngModel',
		link 	 : function ($scope, element, attrs) {

			// prevent html5/browser auto completion
			attrs.$set('autocomplete','off');

			$scope.autocomplete = $(element).autocompleteByExtension({
                    source: $scope.baseURL + "wikitty-json/search?extension=" + $(element).data("source"),
                    minLength: 2,
                    select: function(event, ui) {
                        $scope[$(element).data("model")][$(element).data("baseField") + "Name"] = ui.item.label;
                        $scope[$(element).data("model")][$(element).data("baseField") + "Id"] = ui.item.id;
                        return true;
                    }
            });
		}
	}
});

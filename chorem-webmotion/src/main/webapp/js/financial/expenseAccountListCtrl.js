/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
var app = angular.module('expenseAccountPagination', ['ngResource', 'ui.bootstrap', 'Pagination']);

app.controller("expenseAccountListCtrl", function($scope, $resource, $timeout, $controller, ngTableParams, paginationInit) {

    var date = new Date();
    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    $scope.startTime = paginationInit.params.startTime = firstDay.getTime();
    $scope.endTime = paginationInit.params.endTime = lastDay.getTime();

    $controller("PageCtrl", {$scope : $scope, paginationInit : paginationInit, ngTableParams : ngTableParams});

    console.log($scope.tableParams);

    $scope.load = function() {

        if ($scope.tableParams.$params.startTime instanceof Date) {
            $scope.tableParams.$params.startTime = $scope.tableParams.$params.startTime.getTime();
        }

        if ($scope.tableParams.$params.endTime instanceof Date) {
            $scope.tableParams.$params.endTime = $scope.tableParams.$params.endTime.getTime();
        }
        $scope.tableParams.reload();
    }

    $scope.$watch("startTime", function () {
        if ($scope.startTime instanceof Date) {
            $scope.tableParams.$params.startTime = $scope.startTime.getTime();
        }
        $scope.tableParams.reload();
    });

    $scope.$watch("endTime", function () {
        if ($scope.endTime instanceof Date) {
            $scope.tableParams.$params.endTime = $scope.endTime.getTime();
        }
        $scope.tableParams.reload();
    });
});

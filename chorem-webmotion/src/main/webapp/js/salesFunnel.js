/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
var salesFunnel = angular.module('salesFunnel', ['wikitty', 'ui.bootstrap', 'ui.date']).config(["$controllerProvider", function($controllerProvider) {
    $controllerProvider.allowGlobals();
}]);

salesFunnel.directive('datepicker', function() {
    return {
        restrict: 'A',
        require : 'ngModel',
        link : function (scope, element, attrs, ngModelCtrl) {
            $(function(){
                element.datepicker({
                    dateFormat:'dd/mm/yy',
                    onSelect:function (date) {
                        ngModelCtrl.$setViewValue(date);
                        scope.$apply();
                    }
                });
            });
        }
    }
});



var salesFunnelController = ['$scope', '$modal', 'Wikitty', function ($scope, $modal, Wikitty) {
    $scope.wikittyId = '';
    $scope.wikitty = {};

    $scope.leads = {};
    $scope.leadsAmount=0;
    $scope.leadsAmountHope=0;
    $scope.drafts={};
    $scope.draftsAmount=0;
    $scope.draftsAmountHope=0;
    $scope.sents={};
    $scope.sentsAmount=0;
    $scope.sentsAmountHope=0;
    $scope.dependencies={};
    $scope.customerCompanyId=0;

    var getDependencies = function(w){
        for (var i in w)
        {
            if (i != "$promise" && i != "$resolved" && i != "length") {
                var categoryId = w[i].getField('Quotation','category');
                Wikitty.get(categoryId, function(cat){
                    $scope.dependencies[cat.getId()]=cat;
                });

                var customerId = w[i].getField('Quotation','customer');
                Wikitty.get(customerId, function(cust){
                    $scope.dependencies[cust.getId()]=cust;
                    var customerCompanyId = cust.getField('Employee','company');
                    Wikitty.get(customerCompanyId, function(comp){
                        $scope.dependencies[comp.getId()]=comp;
                    });
                });

                var projectId = w[i].getField('Quotation','project');
                Wikitty.get(projectId, function(project){
                    $scope.dependencies[project.getId()]=project;
                });
            }
        }
    }

    Wikitty.query("extension = Quotation AND extension != Draft AND extension != Cancelled", function(w) {
        $scope.leads= w;
        getDependencies(w);
    });

    Wikitty.query("extension = Draft AND extension != Sent AND extension != Cancelled", function(w) {
        $scope.drafts= w;
        getDependencies(w);
    });

    Wikitty.query("extension = Sent AND extension != Accepted AND extension != Rejected AND extension != Cancelled", function(w) {
        $scope.sents= w;
        getDependencies(w);
    });

    $scope.leadToDraft = function(lead){
        var d = $modal.open({
            templateUrl: './funnel/partial/dialog-leadToDraft.html',
            controller: 'LeadToDraftDialogController',
            resolve: {
                dialogModel: function() {
                    return {lead:lead, leads:$scope.leads, drafts:$scope.drafts};
                }
            }
        });
    };

    $scope.draftToSent = function(draft){
        var d = $modal.open({
            templateUrl: './funnel/partial/dialog-draftToSent.html',
            controller: 'DraftToSentDialogController',
            resolve: {
                dialogModel: function() {
                    return {draft:draft, drafts:$scope.drafts, sents:$scope.sents};
                }
            }
        });
    };

    $scope.sentToAccepted = function(sent){
        var d = $modal.open({
            templateUrl: './funnel/partial/dialog-sentToAccepted.html',
            controller: 'SentToAcceptedDialogController',
            resolve: {
                dialogModel: function() {
                    return {sent:sent, sents:$scope.sents};
                }
            }
        });
    };

    $scope.sentToRejected = function(sent){
        var d = $modal.open({
            templateUrl: './funnel/partial/dialog-sentToRejected.html',
            controller: 'SentToRejectedDialogController',
            resolve: {
                dialogModel: function() {
                    return {sent:sent, sents:$scope.sents};
                }
            }
        });
    };

    $scope.cancel = function(toCancel){
        var d = $modal.open({
            templateUrl: './funnel/partial/dialog-cancel.html',
            controller: 'CancelDialogController',
            resolve: {
                dialogModel: function() {
                    return {toCancel:toCancel, leads:$scope.leads, drafts:$scope.drafts, sents:$scope.sents};
                }
            }
        });
    };
}];



// Controller for the Lead To Draft Dialog
var LeadToDraftDialogController = ['$scope', '$http', '$modalInstance', 'dialogModel', 'Wikitty', function ($scope, $http, $modalInstance, dialogModel, Wikitty) {
  $scope.lead = dialogModel.lead;
  $scope.drafts = dialogModel.drafts;
  $scope.leads = dialogModel.leads;

  $scope.cancel = function(){
    $modalInstance.close();
  };

  $scope.validate = function(){
    //pass scope to the success function
    var scope = $scope;
    $http.get(createUrl('sales/funnel/json/answer/'+$scope.lead.meta.id),
    {params: {sendingDate:$scope.lead.sendingDate,reference:$scope.lead.reference}}).success(
      function(data,status){
        scope.drafts.push(new Wikitty(data));
        scope.leads.splice(scope.leads.indexOf(scope.lead),1);
        $modalInstance.close();
      }
    );
  }
}];



// Controller for the Draft To Sent Dialog
var DraftToSentDialogController = ['$scope', '$http', '$modalInstance', 'dialogModel', 'Wikitty', function ($scope, $http, $modalInstance, dialogModel, Wikitty) {
  $scope.draft = dialogModel.draft;
  $scope.drafts = dialogModel.drafts;
  $scope.sents = dialogModel.sents;
  //$scope.draft.sendingDate = new Date().toString("dd/mm/yy");

  $scope.cancel = function(){
    $modalInstance.close();
  };

  $scope.validate = function(){
    //pass scope to the success function
    var scope = $scope;
    $http.get(createUrl('sales/funnel/json/send/'+$scope.draft.meta.id),
    {params: {sendingDate:$scope.draft.sendingDate}}).success(
      function(data,status){
        scope.sents.push(new Wikitty(data));
        scope.drafts.splice(scope.drafts.indexOf(scope.draft),1);
        $modalInstance.close();
      }
    );
  }
}];



// Controller for the Sent to Accepted Dialog
var SentToAcceptedDialogController = ['$scope', '$http', '$modalInstance', 'dialogModel', function ($scope, $http, $modalInstance, dialogModel) {
  $scope.sent = dialogModel.sent;
  $scope.sents = dialogModel.sents;
  $scope.sent.acceptedDate = new Date();

  $scope.cancel = function(){
    $modalInstance.close();
  };

  $scope.validate = function(){
    //pass scope to the success function
    var scope = $scope;
    $http.get(createUrl('sales/funnel/json/accept/'+$scope.sent.meta.id),
    {params: {acceptedDate:$scope.sent.acceptedDate}}).success(
      function(data,status){
        scope.sents.splice(scope.sents.indexOf(scope.sent),1);
        $modalInstance.close();
      }
    );
  }
}];



// Controller for the Sent to Rejected Dialog
var SentToRejectedDialogController = ['$scope', '$http', '$modalInstance', 'dialogModel', function ($scope, $http, $modalInstance, dialogModel) {
  $scope.sent = dialogModel.sent;
  $scope.sents = dialogModel.sents;
  $scope.sent.rejectedDate = new Date();

  $scope.cancel = function(){
    $modalInstance.close();
  };

  $scope.validate = function(){
    //pass scope to the success function
    var scope = $scope;
    $http.get(createUrl('sales/funnel/json/reject/'+$scope.sent.meta.id),{params: {rejectedDate:$scope.sent.rejectedDate}}).success(
      function(data,status){
        scope.sents.splice(scope.sents.indexOf(scope.sent),1);
        $modalInstance.close();
      }
    );
  }
}];



// Controller for the Cancel Dialog
var CancelDialogController = ['$scope', '$http', '$modalInstance', 'dialogModel', function ($scope, $http, $modalInstance, dialogModel) {
  $scope.toCancel = dialogModel.toCancel;
  $scope.sents = dialogModel.sents;
  $scope.leads = dialogModel.leads;
  $scope.drafts = dialogModel.drafts;

  $scope.cancel = function(){
    $modalInstance.close();
  };

  $scope.validate = function(){
    //pass scope to the success function
    var scope = $scope;
    $http.get(createUrl('sales/funnel/json/cancel/'+$scope.toCancel.meta.id),{params: {cancelledDate:$scope.toCancel.cancelledDate, reason:$scope.toCancel.cancelledReason}}).success(
      function(data,status){
        if ($scope.sents.indexOf($scope.toCancel)!= -1) {
          $scope.sents.splice($scope.sents.indexOf($scope.toCancel),1);
        }
        if ($scope.drafts.indexOf($scope.toCancel)!= -1) {
          $scope.drafts.splice($scope.drafts.indexOf($scope.toCancel),1);
        }
        if ($scope.leads.indexOf($scope.toCancel)!= -1) {
          $scope.leads.splice($scope.leads.indexOf($scope.toCancel),1);
        }

       $modalInstance.close();
      }
    );
  }
}];

/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

function createUrl() {
	var result = webContext;
	for (var i = 0; i < arguments.length; i++) {
		result += arguments[i];
	}
	result = result.replace('//', '/');
	return result;
}

$(function() {
   	$('.withTooltip').tooltip({html:true});
   });

var enterShow = function (popover) {
    if (popover.data('state') === 'hover') {
        popover.popover('show');
    }
};
var exitHide = function (popover) {
    if (popover.data('state') === 'hover') {
        popover.popover('hide');
    }
};

var clickToggle = function (popover) {
    if (popover.data('state') === 'hover') {
        popover.data('state', 'pinned');
    } else {
        popover.data('state', 'hover')
        popover.popover('hover');
    }
};

/*$(function() {
	$('.withPopover').data('state','hover').popover({trigger: 'manual'})
        .on('mouseenter', enterShow($(this)))
        .on('mouseleave', exitHide($(this)))
        .on('click', clickToggle($(this)));
});*/

$(function() {
   	$('.withPopover').popover({html:true});
   });

$(function () {
	$.datepicker.setDefaults($.datepicker.regional['fr']);
	$.timepicker.setDefaults($.timepicker.regional['fr']);
});
function initFields() {
//	tout ce qui aura la classe datepicker servira a editer une date
	$(function()  {
		$( ".datepicker" ).datepicker({
			showWeek: true,
			firstDay: 1,
			changeMonth: true,
			changeYear: true
		});
//		"option", "gotoCurrent", true );
	});
	$(function() {
		$( ".timepicker" ).timepicker({
			showWeek: true,
			firstDay: 1,
			changeMonth: true,
			changeYear: true
		});
//		"option", "gotoCurrent", true );
	});
//	tout ce qui aura la classe datetimepicker servira a editer une date avec heure
	$(function() {
		$( ".datetimepicker" ).datetimepicker({
			showWeek: true,
			firstDay: 1,
			changeMonth: true,
			changeYear: true
		});
//		"option", "gotoCurrent", true );
	});


}
initFields();

//nouveau composant pour afficher les wikitties lors de l'edition classe par extension
$.widget( "custom.autocompleteByExtension", $.ui.autocomplete, {
	_renderMenu: function( ul, items ) {
		var self = this,
		currentExtension = "";
		// ajout d'un element pour permettre l'effacement de l'ancienne entree
		self._renderItemData( ul, {label:"None", value:"None", name:"None"} );
		$.each( items, function( index, item ) {
			if ( item.extension != currentExtension ) {
				ul.append( "<li class='ui-autocomplete-category'>" + item.extension + "</li>" );
				currentExtension = item.extension;
			}
			self._renderItemData( ul, item );
		});
	}
});


/*********** SALES FUNNEL FUNCTIONS**********************/
//ajout ble
//clic sur les cases à cocher half-day d'une vacation (VacationRequest)

function change12or00(dt,dtVal,tim) {
	if (tim) {
		dtVal = dtVal.substring(0,11)+"12:00";
	}
	else {
		dtVal = dtVal.substring(0,11)+"00:00";
	}
	dt.val(dtVal);
}

$(document).ready(function() {
	$('.beginDateTR :checkbox').change(function(){
		var dtetim = $(this).parent().parent().children('.beginDateTD').children('input');
		var dtetimVal = dtetim.attr("value");
		change12or00(dtetim,dtetimVal,$(this).attr("checked"));
	});
});

$(document).ready(function() {
	$('.endDateTR :checkbox').change(function(){
		var dtetim = $(this).parent().parent().children('.endDateTD').children('input');
		var dtetimVal = dtetim.attr("value");
		change12or00(dtetim,dtetimVal,$(this).attr("checked"));
	});
});


//ajout d'une vacation dans editVacationRequest
$(document).ready(function() {
	$('#btn-vacation-add').click(function(){

		jQuery.ajax({
			type: 'GET',
			url: createUrl('/hr/vacationDiv/new'),
			data:'',
			sucess: function(data, textStatus, jqXHR){

			},
			error: function(jqXHR, textStatus, errorThrown) {

			}
		})
	})

});

$(document).ready(function($) {

	// ********************************************************************
	// *                     ACCOUNT EDITING                              *
	// ********************************************************************

	//Edition inline des ContactDetails
	$(".contactDetail-edit-inline").click(function() {
		var button = $(this);
		var span = button.parent().children("span[wikittyId].value");
		var value = span.text();
		var id = span.attr('wikittyId');
		var input = $("<input type='text' value='"+value+"'></input>");
		input.keyup(function (e) {
			if (e.keyCode == 13) {
				value = input.val();
				$.get(createUrl("/wikitty-json/save?id=", id, "&extension=ContactDetails&ContactDetails.value=", value),
						function(){
					//success
					span.text(value);
					input.replaceWith(span);
					//ajoute un logo ok
					var okIcon = $("<i class='icon-ok'></i>");
					button.after(okIcon);
					//timeout 3s
					setTimeout(function() { okIcon.remove(); }, 3000);
				}).fail(function(){
					//fail
					input.replaceWith(span);
					//ajoute un logo erreur
					var errorIcon = $("<i class='icon-exclamation-sign'></i>");
					button.after(errorIcon);
					//timeout 3s
					setTimeout(function() { errorIcon.remove(); }, 3000);

				});

			} else if (e.keyCode == 27) {
				input.replaceWith(span);
			}
		});
		input.blur(function() {
			input.replaceWith(span);
		});

		span.replaceWith(input);
		input.focus();
	});

	//Ajout d'une note sur un compte client
	$(".add-note").submit(function() {
		var form = $(this).get(0);
		var title = $("#note-title").val();
		var content = $("#note-content").val();
		var id = $(".elementId").html();
		var date = $.datepicker.formatDate('dd/mm/yy', new Date());

		$.get(createUrl("/wikitty-json/save?extension=Note&Note.target=", id,
				"&Note.title=", title,
				"&Note.content=", content,
				"&Note.date=", date),function(data){
			//success
			var note;
			$.each(data, function(key, val) {
				if (key=="fieldValue"){
					var savedDate = new Date(val["Note.date"]);

					note = $('<li><h6 class="pull-left"><i class="icon-comment"></i> ' +
							val["Note.title"] +'</h6><small class="pull-right note-date">' +
							$.datepicker.formatDate('dd-mm-yy', savedDate) +
							'</small><div style="clear:both;"/><small>' +
							val["Note.content"] + '</small></li>');
				}
			});
			var notes = $(".notes");
			notes.prepend(note);
			form.reset();
		}).fail(function(){
			//fail
			//TODO JC20130210 retour utilisateur

		});

		return false;
	});


	// ********************************************************************
	// *                      PROJECT DASHBOARD                           *
	// ********************************************************************

	function upgradeQuotation() {


	}



	function displayFields() {
		var id = $(this).attr('wikittyId');
		$("#upgradeFields-" + id).slideUp();
		if(this.selectedIndex == 0) {
			$("#upgradeFields-" + id).text("");
		}
		else 
		{


			var extensionName =  this.item(this.selectedIndex).label;
			$.get(createUrl("/project/json/getExtension/", extensionName, "/" + $(this).attr('wikittyId')),
					function(data){
				//success
				var $jData = $(data);


				$("#upgradeFields-" + id).html($jData[0].outerHTML);//.innerHTML);
				initFields();
				$("#upgradeFields-" + id).slideDown();

			});
		}
	}

	$("a.upgrade").click(upgradeQuotation);
	$("select.extBox").change(displayFields);

	$("#project_hidden").change(function() {
		$("#projectSearch").submit();
	});
	$("#quotationFilter").change(function() {
			$("#projectSearch").submit();
	});
	$(".task-alert").tooltip();
	
	function initgantt(id) {
		"use strict";
		$.get(createUrl("/project/json/getGanttInfo/",id),
				function(ret){
			var data = ret['data'];
			if(data['source'].length != 0) {
				$(".gantt-"+id).gantt({
					source:  data['source'],
					dateStart: data['dateStart'],
					dateEnd:data['dateEnd'],
					extDate : data['extDate'],
					navigate: "scroll",
					scale: "days",
					maxScale: "months",
					minScale: "days",
					itemsPerPage: 10,
					onItemClick: function(data) {
						window.location ="wikitty/edit/" + data;
					},
					onAddClick: function(dt, rowId) {
						var d = new Date(parseInt(dt,10));
						var str = d.getDay() +"/"+ d.getMonth() +"/"+ d.getFullYear();
						window.location ="wikitty/Task/edit/new?Interval.beginDate=" + str +"&Task.quotation=" + id;
					},
					onRender: function() {
						if (window.console && typeof console.log === "function") {
							console.log("chart rendered");

						}
					}

				});
			}
		});


	}
	
	for(var i = 0; i < $('.gantt').length; i++) {
		var div = $('.gantt')[i];
		console.log(div);
		initgantt(div.attributes.wikittyid.nodeValue);
	}
	
	

});

/*
 * #%L
 * Chorem :: webmotion
 * %%
 * Copyright (C) 2011 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
var app = angular.module('Pagination', ['ngTable', 'ngResource']);

app.controller('PageCtrl',
        function($scope, $timeout, $resource, ngTableParams, paginationInit) {

    var Api = $resource(paginationInit.url);

    $scope.tableParams = new ngTableParams(
        paginationInit.params, {
        total: 0,           // length of data
        getData: function($defer, params) {

            var sorting = params.sorting();
            var orderBy = Object.keys(sorting)[0];
            var ordering = orderBy + "." + sorting[orderBy];

            // ajax request to api
            url = params.url();
            url['sorting.' + orderBy] = sorting[orderBy];
            Api.get(url, function(data) {
                $timeout(function() {
                    // update table params
                    params.total(data.total);
                    // set new data
                    $scope.projects = data.elements;
                    $defer.resolve(data);
                }, 500);
            });
        }
    });

    $scope.updateProject =  function() {

        saveParams = {extension : 'Project', id : $scope.editProject.id,'Project.name': $scope.editProject.name, 'Project.description': $scope.editProject.description}
        $resource('../wikitty/save', saveParams, {'update' : {method:'PUT'}}).update(function(result) {
            $scope.tableParams.reload();
        });
        delete $scope.editProject;
    };

    $scope.setEditProject =  function(project) {
        $scope.editProject = {id: project.wikitty.target.id, name: project.wikitty.target.fieldValue['Project.name'], description: project.wikitty.target.fieldValue['Project.description']};
    };

    $scope.cancelEdit =  function() {
        delete $scope.editProject;

    };

});

angular.module('Pagination').directive('loadingContainer', function () {
    return {
        restrict: 'A',
        scope: false,
        link: function(scope, element, attrs) {
            var loadingLayer = angular.element('<div class="loading"></div>');
            element.append(loadingLayer);
            element.addClass('loading-container');
            scope.$watch(attrs.loadingContainer, function(value) {
                loadingLayer.toggleClass('ng-hide', !value);
            });
        }
    };
});

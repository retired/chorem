package org.chorem;

/**
 * Contact details constants
 * 
 * @author vbriand
 */
public interface ContactDetailsConstants {
    public static final String CONTACT_DETAILS_POSTAL_ADDRESS = "Postal address";
    public static final String CONTACT_DETAILS_PHONE = "Phone number";
    public static final String CONTACT_DETAILS_WEBSITE = "Website";
    public static final String CONTACT_DETAILS_EMAIL = "Email";
}

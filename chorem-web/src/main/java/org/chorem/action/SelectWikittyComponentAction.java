package org.chorem.action;

import java.util.Collection;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.ChoremUtil;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.search.PagedResult;
import org.nuiton.wikitty.search.Search;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class SelectWikittyComponentAction extends ChoremBaseAction {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(SelectWikittyComponentAction.class);
    private static final long serialVersionUID = 1L;

    protected String wikittyExtension;
    protected String fulltextSearch;
    protected String sortField;
    protected boolean asc = true;
    protected String format;
    protected List<Wikitty> wikitties;

    @Override
    public String execute() {
        try {
            Search search = Search.query();

            if (StringUtils.isNotBlank(wikittyExtension)) {
                Collection<String> col = ChoremUtil.toCollection(wikittyExtension);
                search.exteq(col);
            }

            if (StringUtils.isNotBlank(fulltextSearch)) {
                // TODO poussin 20111106: ca ne serait pas au like d'ajouter les * ?
                String s = "*" + fulltextSearch + "*";
                // TODO poussin 20111106: voir pourquoi like fontion comme on souhaite et pas Keyword :(
                // keyword ne fonctionne pas avec les *
                search.like("#fulltext", s);
            } else {
                // si pas de filtre on recherche tout
                search.rTrue();
            }

            Criteria criteria = search.criteria();

            if (StringUtils.isNotBlank(sortField)) {
                if (asc) {
                    criteria.setSortAscending(sortField);
                } else {
                    criteria.setSortDescending(sortField);
                }
            }

            PagedResult<Wikitty> result = getChoremProxy().findAllByCriteria(criteria);

            setWikitties(result.getAll());

            if (log.isTraceEnabled()) {
                log.trace("result: " + getWikitties());
            }

            return SUCCESS;
        } catch (Exception eee) {
            log.error(String.format("Can't find wikitty '%s'", wikittyExtension), eee);
            throw new UnhandledException(eee);
        }
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getFulltextSearch() {
        return fulltextSearch;
    }

    public void setFulltextSearch(String fulltextSearch) {
        this.fulltextSearch = fulltextSearch;
    }

    public String getWikittyExtension() {
        return wikittyExtension;
    }

    public void setWikittyExtension(String wikittyExtension) {
        this.wikittyExtension = wikittyExtension;
    }

    public String getSortField() {
        return sortField;
    }

    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    public boolean isAsc() {
        return asc;
    }

    public void setAsc(boolean asc) {
        this.asc = asc;
    }

    public List<Wikitty> getWikitties() {
        return wikitties;
    }

    public void setWikitties(List<Wikitty> wikitties) {
        this.wikitties = wikitties;
    }

    
}

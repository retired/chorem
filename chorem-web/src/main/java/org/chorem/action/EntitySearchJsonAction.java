package org.chorem.action;

import com.opensymphony.xwork2.ActionContext;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.interceptor.ParameterAware;
import org.chorem.ChoremUtil;
import org.nuiton.wikitty.entities.FieldType;
import org.nuiton.wikitty.entities.Wikitty;
import org.nuiton.wikitty.entities.WikittyExtension;
import org.nuiton.wikitty.entities.WikittyImpl;
import org.nuiton.wikitty.query.WikittyQuery;
import org.nuiton.wikitty.query.WikittyQueryMaker;
import org.nuiton.wikitty.query.WikittyQueryResult;
import org.nuiton.wikitty.query.conditions.Element;

/**
 * Action qui d'interagir avec les jqgrid pour n'importe qu'elle type d'extension
 * de wikitty.
 * La grille doit être configuree en json et le model se nome 'gridModel'
 *
 * <h3>Recherche</h3>
 *
 * <li> id: si non vide l'id de ligne de la grille mere pour une sous grille
 * <li> wikittyExtension: les extensions que doivent avoir les wikitties
 *          recherches (master)
 * <li> wikittyLinkToLoad: listes des champs du wikitty master, qui represente
 *          des wikitties a charger en plus pour permettre un affichage plus
 *          lisible pour l'utilisateur
 * <li> filters: la valeur du filtre de la grille
 * <li> fulltextSearch: une contrainte de recherche des wikitties affiches
 *
 * si filters et fulltextSearch sont non vide, seul filters est utilise.
 *
 * <h3>Ajout</h3>
 *
 * <li> wikittyExtension: la liste des extensions a mettre sur le nouveau wikitty
 * <li> rowid: l'id de la derniere ligne ouverte lorsqu'on est en sous grille
 * <li> wikittyLinkFieldForRowId: le champs qui doit etre value avec rowid dans
 *          le nouvel objet wikitty
 *
 * <h3>Edition</h3>
 *
 * <li> id: l'id de l'objet a modifier
 *
 * L'ensemble des champs a mettre dans le wikitty sont passes dans les paramatres
 *
 * <h3>Suppression</h3>
 *
 * <li> id: l'id de l'objet a supprimer
 * <li> deleteAllLink: indique de supprimer tous les objets qui contiennent cet
 *          identifiant
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class EntitySearchJsonAction extends ChoremBaseAction implements ParameterAware {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(EntitySearchJsonAction.class);
    private static final long serialVersionUID = 1L;

    static public EntitySearchJsonAction getAction() {
        return (EntitySearchJsonAction)ActionContext.getContext().get(CONTEXT_ACTION_KEY);
    }

    //get how many rows we want to have into the grid - rowNum attribute in the grid
    protected int rows = 0;

    //Get the requested page. By default grid sets this to 1.
    protected int page = 0;

    // sorting order - asc or desc
    protected String sord;

    // get index row - i.e. user click to sort.
    protected String sidx;

    // Your Total Pages
    protected int total = 0;

    // All Record
    protected int records = 0;

    protected List<Wikitty> wikitties;

    protected List<Map<String, Object>> gridModel;

    /** la liste des extensions que doit avoir les objets recherchés*/
    protected String wikittyExtension;
    /**
     * la liste des lien entre wikitty qu'il faut charger
     * exemple: "Employee.person,Employee.company"
     */
    protected String wikittyLinkToLoad;
    protected Set<String> wikittyLinkToLoadCache;

    /** la recherche fulltext a faire sur les objets de base (pas les liens) pour les filtrer*/
    protected String fulltextSearch = "";

    /** Recherche multi-critere en syntaxe json
     * exemple: {"groupOp":"OR","rules":[{"field":"Person.lastName","op":"eq","data":"coq"},{"field":"Person.lastName","op":"ne","data":"Poussin"},{"field":"Person.firstName","op":"eq","data":"benjamin"}]}
     */
    protected String filters;

    /** action faite via le grid ['add', 'edit', 'del'], si vide alors une simple recherche */
    protected String oper;

    protected boolean deleteAllLink = false;
    /**
     * id ou  list d'id sur lequel l'action est faite, le separateur est la ','
     * id est aussi utilise pour les subgrid comme valeur de l'id du pere
     */
    protected String id;

    /** id de la derniere ligne ouverte (pour les tableaux ayant un sous tableau) */
    protected String rowid;

    /** champs wikitty dans lequel on doit stocker rowid si non vide */
    protected String wikittyLinkFieldForRowId;

    protected Map<String, String[]> parameters;

    @Override
    public String execute() {
        try {
            if (log.isDebugEnabled()) {
                for (String param : parameters.keySet()) {
                    log.debug("param:" + param + "=" + Arrays.toString(parameters.get(param)));
                }
            }

            if ("del".equalsIgnoreCase(oper)) {
                doDelete();
            } else if ("add".equalsIgnoreCase(oper)) {
                doAdd();
            } else if ("edit".equalsIgnoreCase(oper)) {
                doEdit();
            } else {
                doSearch();
            }
            return SUCCESS;
        } catch (Exception eee) {
            log.error(String.format("Can't find wikitty '%s'", wikittyExtension), eee);
            throw new UnhandledException(eee);
        }
    }

    protected void doDelete() {
        Collection<String>  ids = ChoremUtil.toCollection(id);

        // si l'utilisateur demande de supprimer aussi tous les objets qui ont
        // ces ids comme reference, on les recherche
        if (isDeleteAllLink()) {
            WikittyQueryMaker search = new WikittyQueryMaker().or();
            for(String id:ids) {
                search.keyword(id);
            }
            WikittyQuery criteria = search.end();
            WikittyQueryResult<String> result = getChoremProxy().findAllByQuery(criteria);
            ids = result.getAll();
        }
        getChoremProxy().delete(ids);
    }

    protected void doAdd() {
        Wikitty w = new WikittyImpl();
        Collection<String> extNames = ChoremUtil.toCollection(wikittyExtension);
        List<WikittyExtension> exts = getChoremProxy().restoreExtensionAndDependenciesLastVesion(extNames);
        w.addExtension(exts);
        if (StringUtils.isNotBlank(wikittyLinkFieldForRowId)
                && StringUtils.isNotBlank(rowid)) {
            w.setFqField(wikittyLinkFieldForRowId, rowid);
        }
        populateWikitty(w, parameters);
        getChoremProxy().store(w);
    }

    protected void doEdit() {
        Wikitty w = getChoremProxy().restore(id);
        populateWikitty(w, parameters);
        getChoremProxy().store(w);
    }

    /**
     * Recherche dans parameters toutes les champs du wikitty pour mettre a jour
     * le wikitty
     *
     * @param w le wikitty a mettre a jour
     * @param parameters la map contenant les nouvelles valeurs pour les champs
     */
    protected void populateWikitty(Wikitty w, Map<String, String[]> parameters) {
        if (w != null) {
            for (String param : parameters.keySet()) {
                if(w.hasField(param)) {
                    FieldType type = w.getFieldType(param);
                    String[] val = parameters.get(param);
                    if (type.isCollection()) {
                        w.setFqField(param, val);
                    } else {
                        w.setFqField(param, val[0]);
                    }
                }
            }
        }
    }

    protected void doSearch() {

        WikittyQueryMaker search = new WikittyQueryMaker().and();

        if (StringUtils.isNotBlank(wikittyExtension)) {
            Collection<String> col = ChoremUtil.toCollection(wikittyExtension);
            search.extContainsAll(col);
        }

        // on restraint la recherche pour les sous grilles a l'id du wikitty
        // pere dans la grille principale
        if (StringUtils.isNotBlank(id)) {
            search.keyword(id);
        }

        if (StringUtils.isNotBlank(filters)) {
            JSONObject jsonFilter = (JSONObject) JSONSerializer.toJSON( filters );
            String groupOp = jsonFilter.getString("groupOp");
            log.debug("groupOp: " + groupOp);
            if ("or".equalsIgnoreCase(groupOp)) {
                search.or();
            } else {
                search.and();
            }
            JSONArray rules = jsonFilter.getJSONArray("rules");
            int rulesCount = JSONArray.getDimensions(rules)[0];
            for (int i = 0; i < rulesCount; i++) {
                JSONObject rule = rules.getJSONObject(i);
                String field = rule.getString("field");
                String op = rule.getString("op");
                String data = rule.getString("data");
                addCondition(search, field, op, data);
            }

        } else if (StringUtils.isNotBlank(fulltextSearch)) {
            String s = fulltextSearch;
            WikittyQueryMaker or = new WikittyQueryMaker().or();
            or = or.keyword(s);
            log.debug("link: " + getWikittyLinkToLoadCache());
            for (String link:getWikittyLinkToLoadCache()) {
                or.containsOne(link).select(Element.ID).keyword(s);
            }
        } else {
            // si pas de filtre on recherche tout
            search.rTrue();
        }


        int first = rows * (page - 1);
        int last = rows;

        WikittyQuery criteria = search.end();
        criteria.setFirst(first);
        criteria.setLimit(last);

        if (StringUtils.isNotBlank(sidx)) {
            if ("asc".equalsIgnoreCase(sord)) {
                criteria.setSortAscending(Element.get(sidx));
            } else {
                criteria.setSortDescending(Element.get(sidx));
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("rows:" + rows);
            log.debug("page:" + page);

            log.debug("first:" + first);
            log.debug("last:" + last);

            log.debug("sord:" + sord);
            log.debug("sidx:" + sidx);

            log.debug("wikittyExtension:" + wikittyExtension);
            log.debug("searchString: " + fulltextSearch);
            log.debug("filters: " + filters);
            log.debug("criteria: " + criteria);
        }

        WikittyQueryResult<Wikitty> result =
                getChoremProxy().findAllByQuery(Wikitty.class, criteria);

        setWikitties(result.getAll());

        if (log.isTraceEnabled()) {
            log.trace("result: " + getWikitties());
        }

        setRecords(result.getTotalResult());
        setTotal((int) Math.ceil((double) getRecords() / (double) getRows()));

        gridModel = new ArrayList<Map<String, Object>>();
        for (Wikitty w:wikitties) {
            Map<String, Object> map = wikittyToMap(w);
            gridModel.add(map);
        }
    }

    protected Map<String, Object> wikittyToMap(Wikitty w) {
        Map<String, Object> result = new HashMap<String, Object>();
        if (w != null) {
            String wid = w.getId();

            result.put("id", wid);
            result.putAll(w.getFieldValue());
            for (String fieldToLoad:getWikittyLinkToLoadCache()) {
                String linkId = (String)w.getFqField(fieldToLoad);
                if (linkId != null) {
                    // TODO poussin 20111107 pas tres optimal de faire une requete a chaque besoin
                    // il faudrait collecter les ids, pour faire une seule demande pour tous
                    // mais tant que pour le deploiement, tout est sur le meme serveur
                    // c'est un peu moins genant
                    Wikitty wlink = getChoremProxy().restore(linkId);
                    Map<String, Object> map = wikittyToMap(wlink);
                    map.remove("id");
                    result.putAll(map);
                }
            }
        }
        return result;
    }

    protected void addToLoad(Map<String, List<String>> toLoad, String wid, String wfield) {
        List<String> l = toLoad.get(wid);
        if (l == null) {
            l = new ArrayList<String>();
            toLoad.put(wid, l);
        }
        l.add(wfield);
    }

    public Set<String> getWikittyLinkToLoadCache() {
        if (wikittyLinkToLoadCache == null) {
            wikittyLinkToLoadCache =
                    new HashSet<String>(ChoremUtil.toCollection(wikittyLinkToLoad));
        }
        return wikittyLinkToLoadCache;
    }

    public String getRowid() {
        return rowid;
    }

    public void setRowid(String rowid) {
        this.rowid = rowid;
    }

    public String getWikittyLinkFieldForRowId() {
        return wikittyLinkFieldForRowId;
    }

    public void setWikittyLinkFieldForRowId(String wikittyLinkFieldForRowId) {
        this.wikittyLinkFieldForRowId = wikittyLinkFieldForRowId;
    }

    public boolean isDeleteAllLink() {
        return deleteAllLink;
    }

    public void setDeleteAllLink(boolean deleteAllLink) {
        this.deleteAllLink = deleteAllLink;
    }

    public void setWikittyLinkToLoad(String wikittyLinkToLoad) {
        this.wikittyLinkToLoad = wikittyLinkToLoad;
    }

    public void setParameters(Map<String, String[]> parameters) {
        this.parameters = parameters;
    }
    public void setFilters(String filters) {
        this.filters = filters;
    }

    public void setWikittyExtension(String wikittyExtension) {
        this.wikittyExtension = wikittyExtension;
    }

    public void setFulltextSearch(String fulltextSearch) {
        this.fulltextSearch = fulltextSearch;
    }

    public void setOper(String oper) {
        this.oper = oper;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public void setRecords(Integer records) {
        this.records = records;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List getWikitties() {
        return wikitties;
    }

    public void setWikitties(List<Wikitty> wikitties) {
        this.wikitties = wikitties;
    }

    public List<Map<String, Object>> getGridModel() {
        return gridModel;
    }

    public void setGridModel(List<Map<String, Object>> gridModel) {
        this.gridModel = gridModel;
    }

    public String getFulltextSearch() {
        return fulltextSearch;
    }

    public int getTotal() {
        return total;
    }

    public int getRecords() {
        return records;
    }

    public int getRows() {
        return rows;
    }

    public int getPage() {
        return page;
    }

    public String getSord() {
        return sord;
    }

    public String getSidx() {
        return sidx;
    }

    /**
     * Ajoute une contrainte a une recherche
     * 
     * @param search la requete ou il faut ajouter la contrainte
     * @param field le fq du champs a traiter
     * @param op ['eq','ne','cn','nc','bw','bn','ew','en','lt','le','gt','ge','nu','nn','in','ni']
     * @param data la valuer de la condition
     */
    protected void addCondition(WikittyQueryMaker search, String field, String op, String data) {
        if ("eq".equalsIgnoreCase(op)) {
            search.eq(field, data);
        } else if ("ne".equalsIgnoreCase(op)) {
            search.ne(field, data);
        } else if ("lt".equalsIgnoreCase(op)) {
            search.lt(field, data);
        } else if ("le".equalsIgnoreCase(op)) {
            search.le(field, data);
        } else if ("gt".equalsIgnoreCase(op)) {
            search.gt(field, data);
        } else if ("ge".equalsIgnoreCase(op)) {
            search.ge(field, data);
        } else if ("bw".equalsIgnoreCase(op)) {
            search.sw(field, data);
        } else if ("bn".equalsIgnoreCase(op)) {
            search.notsw(field, data);
        } else if ("ew".equalsIgnoreCase(op)) {
            search.ew(field, data);
        } else if ("en".equalsIgnoreCase(op)) {
            search.notew(field, data);
        } else if ("cn".equalsIgnoreCase(op)) {
            search.like(field, data);
        } else if ("nc".equalsIgnoreCase(op)) {
            search.unlike(field, data);
        } else if ("nu".equalsIgnoreCase(op)) {
            search.isNull(field);
        } else if ("nn".equalsIgnoreCase(op)) {
            search.isNotNull(field);
        } else {
            Collection<String> col = ChoremUtil.toCollection(data);
            if ("in".equalsIgnoreCase(op)) {
                search.containsOne(field, col);
            } else if ("ni".equalsIgnoreCase(op)) {
                search.not().containsOne(field, col);
            } else {
                log.error(String.format("Unreconize Operator %s for field %s and value %s", op, field, data));
            }
        }

    }
}

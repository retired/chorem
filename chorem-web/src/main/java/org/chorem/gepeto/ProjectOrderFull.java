package org.chorem.gepeto;

import org.chorem.entities.Company;
import org.chorem.entities.Project;
import org.chorem.entities.ProjectOrder;
import org.chorem.entities.Quotation;

/**
 * @author vbriand
 */
public class ProjectOrderFull {
    protected ProjectOrder projectOrder;
    protected Quotation quotation;
    protected Project project;
    protected Company company;
    
    /**
     * @return the projectOrder
     */
    public ProjectOrder getProjectOrder() {
        return projectOrder;
    }
    
    /**
     * @param projectOrder the projectOrder to set
     */
    public void setProjectOrder(ProjectOrder projectOrder) {
        this.projectOrder = projectOrder;
    }
    
    /**
     * @return the quotation
     */
    public Quotation getQuotation() {
        return quotation;
    }
    
    /**
     * @param quotation the quotation to set
     */
    public void setQuotation(Quotation quotation) {
        this.quotation = quotation;
    }
    
    /**
     * @return the project
     */
    public Project getProject() {
        return project;
    }
    
    /**
     * @param project the project to set
     */
    public void setProject(Project project) {
        this.project = project;
    }

    /**
     * @return the company
     */
    public Company getCompany() {
        return company;
    }

    /**
     * @param company the company to set
     */
    public void setCompany(Company company) {
        this.company = company;
    }
}

package org.chorem.gepeto.action;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.ChoremProxy;
import org.chorem.action.ChoremBaseAction;
import org.chorem.entities.Company;
import org.chorem.entities.Project;
import org.chorem.entities.ProjectImpl;
import org.chorem.entities.ProjectOrder;
import org.chorem.entities.Quotation;
import org.chorem.gepeto.ProjectOrderFull;
import org.nuiton.wikitty.search.Criteria;
import org.nuiton.wikitty.search.PagedResult;
import org.nuiton.wikitty.search.Search;

import com.opensymphony.xwork2.ActionContext;

import static org.nuiton.i18n.I18n.n_;

/**
 * Project management class
 *
 * @author vbriand
 */
public class ProjectAction extends ChoremBaseAction {

    private static final long serialVersionUID = 498267854350348906L;
    
    private static final Log log = LogFactory.getLog(ProjectAction.class);
    
    static public ProjectAction getAction() {
        return (ProjectAction)ActionContext.getContext().get(CONTEXT_ACTION_KEY);
    }
    
    /**
     * Adds a new project
     * 
     * @return INPUT if the mandatory fields haven't all been filled in, 
     * SUCCESS if the project has been added, 
     * ERROR if an error occurred
     */
    public String add() {
        String result = INPUT;
        
        if (name != null && description != null) {
            //If the mandatory fields have been filled in
            if (!name.isEmpty() && !description.isEmpty()) {
                //If the project has been created successfully
                if (addProject()) {
                    result = SUCCESS;
                } else {
                    result = ERROR;
                }
            }
        }
        return result;        
    }  
    
    /**
     * Retrieves the name and the description of the project referenced by the 
     * projectId
     * 
     * @return SUCCESS if the name and the description have been retrieved, 
     * ERROR either if the UUID is invalid or the projectId doesn't exist
     */
    public String projectDetails() {
        String result = SUCCESS;
        ChoremProxy proxy = getChoremProxy();
        
        try {
            if (projectId == null) {
                result = ERROR;
            } else {
                //If projectId isn't a valid UUID, an exception is thrown
                UUID.fromString(projectId);
                
                Project project = proxy.restore(Project.class, projectId);
                
                if (project != null) { //If the projectId exists
                    setName(project.getName());
                    setDescription(project.getDescription());
                } else {
                    result = ERROR;
                }
            }
        } catch (IllegalArgumentException e) {
            //projectId is invalid
            result = ERROR;
        }
        return result;
    }

    /**
     * Tests whether the year is valid or not
     * 
     * @return true if the year is valid, false otherwise
     */
    public String projectsByYear() {
        String result = SUCCESS;
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
        
        if (year != null) {
            try {
                Date date = formatter.parse(year);
                cal.setTime(date);
                yearInt = cal.get(Calendar.YEAR);
            } catch (ParseException e) {
                result = ERROR;
            }
        } else {
            result = ERROR;
        }
        return result;
    }
    
    /**
     * Modifies the project's information
     * 
     * @return SUCCESS if the project has been successfully modified,
     * ERROR if the id is invalid
     */
    public String modify() {
        String result = SUCCESS;
        Project project;
        ChoremProxy proxy = getChoremProxy();
        
        project = proxy.restore(Project.class, projectId);
        if (project != null) { //If the id exists
            project.setDescription(description);
            proxy.store(project);
        } else {
            result = ERROR;
        }
        return result;
    }
    
    protected int yearInt;
    protected String year;
    protected String name;
    protected String description;
    protected String projectId;

    /**
     * Stores the new project through the proxy 
     * 
     * @return true if the project has been stored properly, 
     * false if a problem occurred
     */
    protected boolean addProject() {
        boolean result = true;
        
        try {
            ChoremProxy proxy = getChoremProxy();
            ProjectImpl newProject = new ProjectImpl();
            
            newProject.setDescription(description);
            newProject.setName(name);
            projectId = newProject.getWikittyId();
            proxy.store(newProject);
        } catch (Exception e) {
            result = false;
            addActionError(getText(n_("chorem.create.error")));
            log.error("An error occurred while creating a new project", e);
        }
        return result;
    }
    
    /**
     * Gets all existing projects
     * 
     * @return all projects in database
     */
    public List<Project> getAllProjects() {
        ChoremProxy proxy = getChoremProxy();
        Search search = Search.query();
        Criteria criteria = search.exteq(Project.EXT_PROJECT).criteria();
        PagedResult<Project> result = proxy.findAllByCriteria(Project.class,
                criteria);
        List<Project> projects = result.getAll();
        return projects;
    }
    
    /**
     * Gets the projects by year
     * 
     * @return the list of projects
     */
    public List<Project> getProjectsByYear() {
        ChoremProxy proxy = getChoremProxy();
        Calendar cal = Calendar.getInstance();
        Search search = Search.query();
        Criteria criteria = search.exteq(ProjectOrder.EXT_PROJECTORDER).criteria();
        PagedResult<ProjectOrder> result = proxy.
        findAllByCriteria(ProjectOrder.class, criteria);
        List<ProjectOrder> projectOrders = result.getAll();
        List<Project> projects = new ArrayList<Project>();
        Quotation quotation;
        Project project;
        int beginYear, endYear;

        for (ProjectOrder projectOrder : projectOrders) {
            cal.setTime(projectOrder.getBeginDate());
            beginYear = cal.get(Calendar.YEAR);
            cal.setTime(projectOrder.getEndDate());
            endYear = cal.get(Calendar.YEAR);
            if (beginYear <= yearInt && yearInt <= endYear) {
                quotation = proxy.restore(Quotation.class,
                        projectOrder.getQuotation());
                project = proxy.restore(Project.class, quotation.getProject());
                projects.add(project);
            }
        }
        return projects;
    }
    
    public List<ProjectOrderFull> getProjectOrdersByYear() {
        ChoremProxy proxy = getChoremProxy();
        Calendar cal = Calendar.getInstance();
        Search search = Search.query();
        Criteria criteria = search.exteq(ProjectOrder.EXT_PROJECTORDER).criteria();
        PagedResult<ProjectOrder> result = proxy.
        findAllByCriteria(ProjectOrder.class, criteria);
        List<ProjectOrder> projectOrders = result.getAll();
        List<ProjectOrderFull> projects = new ArrayList<ProjectOrderFull>();
        Quotation quotation;
        Project project;
        ProjectOrderFull projectFull;
        int beginYear, endYear;

        for (ProjectOrder projectOrder : projectOrders) {
            cal.setTime(projectOrder.getBeginDate());
            beginYear = cal.get(Calendar.YEAR);
            cal.setTime(projectOrder.getEndDate());
            endYear = cal.get(Calendar.YEAR);
            if (beginYear <= yearInt && yearInt <= endYear) {
                projectFull = new ProjectOrderFull();
                projectFull.setProjectOrder(projectOrder);
                quotation = proxy.restore(Quotation.class,
                        projectOrder.getQuotation());
                projectFull.setQuotation(quotation);
                project = proxy.restore(Project.class, quotation.getProject());
                projectFull.setProject(project);
                projects.add(projectFull);
            }
        }
        return projects;
    }
    
    /**
     * Gets the projects without project order but with quotation by year
     * 
     * @return the list of projects
     */
    public List<Project> getProjectsWithoutProjectOrderByYear() {
        ChoremProxy proxy = getChoremProxy();
        Calendar cal = Calendar.getInstance();
        Collection<Quotation> allQuotations = new ArrayList<Quotation>(
                getAllQuotations());
        
        allQuotations.removeAll(getQuotationsWithProjectOrder());
        List<Project> projects = new ArrayList<Project>();
        Project project;
        int quotationYear;
        
        for (Quotation quotation : allQuotations) {
            cal.setTime(quotation.getPostedDate());
            quotationYear = cal.get(Calendar.YEAR);
            if (yearInt == quotationYear) {
                project = proxy.restore(Project.class, quotation.getProject());
                //If the project isn't already in the list
                if (!projects.contains(project)) {
                    projects.add(project);
                }
            }
        }
        return projects;
    }
    
    /**
     * Gets all quotations with project order
     * 
     * @return all quotations with project order
     */
    public List<Quotation> getQuotationsWithProjectOrder() {
        ChoremProxy proxy = getChoremProxy();
        Search search = Search.query();
        Criteria criteria = search.exteq(ProjectOrder.EXT_PROJECTORDER).criteria();
        PagedResult<ProjectOrder> result = proxy.
        findAllByCriteria(ProjectOrder.class, criteria);
        List<ProjectOrder> projectOrders = result.getAll();
        List<Quotation> quotations = new ArrayList<Quotation>();
        Quotation quotation;
        
        for (ProjectOrder projectOrder : projectOrders) {
            quotation = proxy.restore(Quotation.class, 
                    projectOrder.getQuotation());
            quotations.add(quotation);
        }
        return quotations;
    }
    
    /**
     * Gets all quotations, whether they have a project order or not
     * 
     * @return all quotations
     */
    public List<Quotation> getAllQuotations() {
        ChoremProxy proxy = getChoremProxy();
        Search search = Search.query();
        Criteria criteria = search.exteq(Quotation.EXT_QUOTATION).criteria();
        PagedResult<Quotation> result = proxy.findAllByCriteria(Quotation.class, 
                criteria);
        List<Quotation> quotations = result.getAll();
        return quotations;
    }
    
    /**
     * Gets the quotations attached with this project
     * 
     * @return the quotations linked with this project
     */
    public List<Quotation> getAttachedQuotations() {
        ChoremProxy proxy = getChoremProxy();
        Search search = Search.query();
        Criteria criteria = search.eq(Quotation.FQ_FIELD_QUOTATION_PROJECT,
                projectId).criteria();
        PagedResult<Quotation> result = proxy.findAllByCriteria(Quotation.class,
                criteria);
        List<Quotation> quotations = result.getAll();
        return quotations;
    }
    
    /**
     * Gets the quotations with a project order attached with them
     * 
     * @return the quotations with a project order
     */
    public List<Quotation> getAttachedQuotationsWithProjectOrder() {
        ChoremProxy proxy = getChoremProxy();
        List<ProjectOrder> projectOrders = getProjectOrders();
        List<Quotation> quotations = new ArrayList<Quotation>();
        
        for (ProjectOrder projectOrder : projectOrders) {
            quotations.add(proxy.restore(Quotation.class,
                    projectOrder.getQuotation()));
        }
        return quotations;
    }
    
    /**
     * Gets the quotations without a project order attached with them
     * 
     * @return the quotations without a project order
     */
    public List<Quotation> getAttachedQuotationsWithoutProjectOrder() {
        Collection<Quotation> allQuotations = new ArrayList<Quotation>(
                getAttachedQuotations());
        
        //Gets all quotations and removes the ones with a project order
        allQuotations.removeAll(getAttachedQuotationsWithProjectOrder());
        List<Quotation> withoutProjectOrder = (List<Quotation>)allQuotations;
        
        return withoutProjectOrder;
    }
    
    /**
     * Gets the project orders
     * 
     * @return the project orders
     */
    public List<ProjectOrder> getProjectOrders() {
        ChoremProxy proxy = getChoremProxy();
        Search search = Search.query();
        Criteria criteria = search.exteq(ProjectOrder.EXT_PROJECTORDER).
        associated(ProjectOrder.FQ_FIELD_PROJECTORDER_QUOTATION).
        eq(Quotation.FQ_FIELD_QUOTATION_PROJECT, projectId).criteria();
        PagedResult<ProjectOrder> result = proxy.
        findAllByCriteria(ProjectOrder.class, criteria);
        List<ProjectOrder> projectOrders = result.getAll();
        
        return projectOrders;
    }
    
    /**
     * Gets the project orders of the given project
     * 
     * @param projectId the project's id
     * @return the project orders
     */
    public List<ProjectOrder> getProjectOrders(String projectId) {
        ChoremProxy proxy = getChoremProxy();
        Search search = Search.query();
        Criteria criteria = search.exteq(ProjectOrder.EXT_PROJECTORDER).
        associated(ProjectOrder.FQ_FIELD_PROJECTORDER_QUOTATION).
        eq(Quotation.FQ_FIELD_QUOTATION_PROJECT, projectId).criteria();
        PagedResult<ProjectOrder> result = proxy.
        findAllByCriteria(ProjectOrder.class, criteria);
        List<ProjectOrder> projectOrders = result.getAll();
        
        return projectOrders;
    }
    
    /**
     * Gets the customers of a project
     * 
     * @param projectId the project's id
     * @return the customers
     */
    public List<Company> getCustomers(String projectId) {
        ChoremProxy proxy = getChoremProxy();
        List<ProjectOrder> projectOrders = getProjectOrders(projectId);
        List<Company> customers = new ArrayList<Company>();
        Company company;
        
        for (ProjectOrder projectOrder : projectOrders) {
            company = proxy.restore(Company.class, projectOrder.getCompany());
            customers.add(company);
        }
        return customers;
    }
    
    /**
     * Gets the customers of a project by year
     * 
     * @param projectId the project's id
     * @return the customers
     */
    public List<Company> getCustomersByYear(String projectId) {
        ChoremProxy proxy = getChoremProxy();
        Calendar cal = Calendar.getInstance();
        List<ProjectOrder> projectOrders = getProjectOrders(projectId);
        List<Company> customers = new ArrayList<Company>();
        Company company;
        int beginYear, endYear;
        
        for (ProjectOrder projectOrder : projectOrders) {
            cal.setTime(projectOrder.getBeginDate());
            beginYear = cal.get(Calendar.YEAR);
            cal.setTime(projectOrder.getEndDate());
            endYear = cal.get(Calendar.YEAR);
            if (beginYear <= yearInt && yearInt <= endYear) {
                company = proxy.restore(Company.class, projectOrder.getCompany());
                if (!customers.contains(company)) {
                    customers.add(company);
                }
            }
        }
        return customers;
    }
    
    /**
     * Gets the projects without any quotation
     * 
     * @return the projects without quotation
     */
    public List<Project> getProjectsWithoutQuotation() {
        ChoremProxy proxy = getChoremProxy();
        List<Quotation> quotations = getAllQuotations();
        Collection<Project> projects = new ArrayList<Project>(getAllProjects());
        List<Project> projectsWithQuotation = new ArrayList<Project>();
        Project project;
        
        for (Quotation quotation : quotations) {
            project = proxy.restore(Project.class, quotation.getProject());
            projectsWithQuotation.add(project);
        }
        projects.removeAll(projectsWithQuotation);
        return (List<Project>) projects;
    }
    
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name: the project name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the project description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description: the project description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the year
     */
    public String getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(String year) {
        this.year = year;
    }

    /**
     * @return the projectId
     */
    public String getProjectId() {
        return projectId;
    }

    /**
     * @param projectId the projectId to set
     */
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }
}

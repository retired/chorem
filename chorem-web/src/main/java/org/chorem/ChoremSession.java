package org.chorem;


import java.io.Serializable;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.wikitty.entities.WikittyUser;

/**
 * Le seul objet qui doit être mis en session pour l'application. S'il y a 
 * d'autres informations à mettre en session, ces informations doivent être 
 * ajoutées ici.
 *
 * Une fois l'utilisateur authentifié, il faut que le securityToken soit mis
 * à jour.
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ChoremSession implements Serializable {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    final static private Log log = LogFactory.getLog(ChoremSession.class);
    private static final long serialVersionUID = 1L;

    final static private String CHOREM_SESSION_KEY = ChoremSession.class.getSimpleName();

    protected String securityToken = null;
    protected WikittyUser user = null;
    transient protected ChoremProxy proxy = null;

    public ChoremSession() {
    }

    /**
     * You must use this method to login user, don't use proxy.login method
     * directly
     *
     * @param login
     * @param password
     */
    public void login(String login, String password) {
        getProxy().login(login, password);
        securityToken = getProxy().getSecurityToken();
        user = proxy.getUser();
    }

    public ChoremProxy getProxy() {
        if (proxy == null) {
            proxy = ChoremProxy.getProxy(securityToken);
        }
        return proxy;
    }

    public WikittyUser getUser() {
        return user;
    }

    public void setUser(WikittyUser user) {
        this.user = user;
    }

    /**
     * logout the user
     * 
     * @param session
     */
    static public void invalidate(Map<String, Object> session) {
        ChoremSession choremSession = getChoremSession(session);
        choremSession.getProxy().logout();
        session.remove(CHOREM_SESSION_KEY);
    }

    static public ChoremSession getChoremSession(HttpServletRequest request) {
        HttpSession session = request.getSession();
        ChoremSession result = getChoremSession(session);
        return result;
    }

    static public ChoremSession getChoremSession(HttpSession httpSession) {
        ChoremSession result = (ChoremSession) httpSession.getAttribute(CHOREM_SESSION_KEY);
        if (result == null) {
            result = new ChoremSession();
            httpSession.setAttribute(CHOREM_SESSION_KEY, result);
        }
        return result;
    }

    static public ChoremSession getChoremSession(Map<String, Object> session) {
        ChoremSession result = (ChoremSession)session.get(CHOREM_SESSION_KEY);
        if (result == null) {
            result = new ChoremSession();
            session.put(CHOREM_SESSION_KEY, result);
        }
        return result;
    }

}

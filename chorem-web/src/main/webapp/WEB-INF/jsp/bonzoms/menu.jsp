<%@taglib prefix="s" uri="/struts-tags" %>

<div id="menu">
    <ul>
        <li><s:a action="companies"><s:text name="chorem.bonzoms.company.list" /></s:a></li>
        <li><s:a action="persons"><s:text name="chorem.bonzoms.person.list" /></s:a></li>
    </ul>
</div>

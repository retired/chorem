<%-- 
    Document   : Home
    Created on : 15 mars 2011, 16:26:56
    Author     : poussin
--%>

<%@page import="java.util.Arrays"%>
<%@page import="org.chorem.action.HomeAction"%>
<%@page import="java.util.Map"%>
<%@page import="com.opensymphony.xwork2.ActionContext"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!-- <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd"> -->

<%
HomeAction action = HomeAction.getAction();
%>

<html xmlns:s="http://java.sun.com/xml/ns/j2ee/web-jsptaglibrary_2_0.xsd"
      xmlns:jsp="http://java.sun.com/JSP/Page">
    <head>
        <title><s:text name="chorem.home" /></title>
    </head>
    <body>
        <h1>Overview</h1>

        <div class="block">
            <ul>
                <li>Company: <%=action.getNbCompany()%></li>
                <li>People: <%=action.getNbPerson()%></li>
                <li>Touch
                    last 7 days <%=action.getNbTouchForLast7DaysForMe()%>/<%=action.getNbTouchForLast7Days()%>
                    planned for next 7 days <%=action.getNbTouchForNext7DaysForMe()%>/<%=action.getNbTouchForNext7Days()%>
                </li>
            </ul>
        </div>

        <div class="block">
            <ul>
                <li>Project status: <%=action.getNbProjectOrderStatus()%></li>
                <li>Task:
                    <%=action.getNbTaskOpenForMe()%>/<%=action.getNbTaskOpen()%>
                </li>
                <li>Invoice to send <%=action.getNbInvoiceToSend()%>
                    for <%=action.getInvoiceAmountToSend()%>
                </li>
            </ul>
        </div>

        <div class="block">
            <ul>
                <li>Cash flow <%=Arrays.toString(action.getCashFlow())%></li>
                <li>Sales turnover <%=action.getSalesTurnoverThisYear()%>
                    (<%=action.getSalesTurnoverLastYear()%>)
                </li>
                <li>Invoice send <%=action.getNbInvoiceSendThisYear()%>
                    (<%=action.getNbInvoiceSendLastYear()%>)
                </li>
                <li>Unpaid invoice <%=action.getNbInvoiceNotPaid()%>
                    and late <%=action.getNbInvoiceNotPaidAndLate()%>
                </li>
            </ul>
        </div>
            
    </body>
</html>

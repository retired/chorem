<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>

<div id="footer">
    <a shape="rect" href="http://www.chorem.org/projects/show/chorem">Chorem</a>
    <a shape="rect" href="http://www.gnu.org/licenses/agpl.html">Licence AGPL</a> -
    <span title="Copyright">©2009-2011</span>
    <a shape="rect" href="http://www.codelutin.com">Code Lutin</a> -
    <a shape="rect" href="http://www.chorem.org/projects/chorem/issues">Rapport de bug</a> -
    <a shape="rect" href="http://list.chorem.org/cgi-bin/mailman/listinfo/chorem-users">Support utilisateur</a>
</div>

<s:url var="choremjs" value="/js/chorem.js" />
<script type="text/javascript" src="${choremjs}"></script>

<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.List" %>
<%@page import="org.chorem.bonzoms.EmployeeFull" %>
<%@page import="org.chorem.entities.Person" %>
<%@page import="org.chorem.billy.action.QuotationAction" %>
<%@page import="org.chorem.JspUtils" %>
<%@taglib prefix="s" uri="/struts-tags" %>

<html xmlns:s="http://java.sun.com/xml/ns/j2ee/web-jsptaglibrary_2_0.xsd"
      xmlns:jsp="http://java.sun.com/JSP/Page">
    <head>
        <title><s:text name="chorem.billy.quotation.add" /></title>
        <s:head />
    </head>
    <body>
        <h2>
            <s:text name="chorem.billy.quotation.addToProject">
                <s:param><%= QuotationAction.getAction().getProjectName() %></s:param>
            </s:text>
        </h2>
        <s:actionerror />
        <s:url action="addQuotation" var="addQuotation">
            <s:param name="projectId"><%= QuotationAction.getAction().getProjectId() %></s:param>
        </s:url>
        <form action="${addQuotation}" method="post">
            <fieldset>
                <legend>
                    <s:text name="chorem.billy.quotation" />
                </legend>
                <s:textfield key="chorem.billy.quotation.type" name="type" labelSeparator=": " />
                <br />
                <s:textfield key="chorem.billy.quotation.reference" name="reference" labelSeparator=": " />
                <br />
                <s:textarea key="chorem.billy.quotation.description" name="description" cols="50" rows="10" labelposition="top" labelSeparator=": " />
                <br />
                <s:textfield key="chorem.billy.quotation.amount" name="amount" labelSeparator=": " size="6" />
                <br />
                <s:textfield key="chorem.billy.quotation.vat" name="vat" labelSeparator=": " size="2" />%
                <br />
                <s:textfield key="chorem.billy.quotation.beginDate" name="beginDate" labelSeparator=": " size="7" value="%{dayDate}" />
                <br />
                <s:textfield key="chorem.billy.quotation.endDate" name="endDate" labelSeparator=": " size="7" value="%{dayDate}" />
                <br />
                <s:textfield key="chorem.billy.quotation.postedDate" name="postedDate" labelSeparator=": " size="7" value="%{dayDate}" />
                <br />
                <s:textfield key="chorem.billy.quotation.conversionHope" name="conversionHope" labelSeparator=": " />
                <br />
                <label for="supplierId" class="label"><s:text name="chorem.billy.quotation.supplier" />: </label>
                <select name="supplierId" id="supplierId">
                    <%
                       List<EmployeeFull> employees = QuotationAction.getAction().getAllEmployees();

                       for (EmployeeFull employee : employees) {
                    %>
                    <option value="<%= employee.getEmployee().getWikittyId() %>"><%= employee.getCompany().getName() %> - <%= employee.getPerson().getLastName() %>&nbsp;<%= employee.getPerson().getFirstName() %> (<%= JspUtils.dateFormat(employee.getPerson().getBirthDate()) %>)</option>
                    <% } %>
                </select>
                <br />
                <label for="customerId" class="label"><s:text name="chorem.billy.quotation.customer" />: </label>
                <select name="customerId" id="customerId">
                    <%
                       for (EmployeeFull employee : employees) {
                    %>
                        <option value="<%= employee.getEmployee().getWikittyId() %>"><%= employee.getCompany().getName() %> - <%= employee.getPerson().getLastName() %>&nbsp;<%= employee.getPerson().getFirstName() %> (<%= JspUtils.dateFormat(employee.getPerson().getBirthDate()) %>)</option>
                    <% } %>
                </select>
                <br />
                <br />
                <s:submit key="chorem.billy.quotation.add" name="submit" />
            </fieldset>
        </form>
    </body>
</html>

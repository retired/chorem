<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.List" %>
<%@page import="org.chorem.entities.Project" %>
<%@page import="org.chorem.gepeto.action.ProjectAction" %>
<%@taglib prefix="s" uri="/struts-tags" %>

<html xmlns:s="http://java.sun.com/xml/ns/j2ee/web-jsptaglibrary_2_0.xsd"
      xmlns:jsp="http://java.sun.com/JSP/Page">
	<head>
		<title><s:text name="chorem.gepeto.projectsWithoutQuotation.title" /></title>
		<s:head />
	</head>
	<body>
	    <h2><s:text name="chorem.gepeto.projectsWithoutQuotation.title" /></h2>
	    <p>
    	    <%
    	    List<Project> projects = ProjectAction.getAction().getProjectsWithoutQuotation();
    	    
    	    for (Project project : projects) {
   	        %>
                <s:url action="projectDetails" var="projectDetails">
    	            <s:param name="projectId"><%= project.getWikittyId() %></s:param>
    	        </s:url>
        	    <s:url namespace="/billy" action="addQuotationInput" var="addQuotation">
    	            <s:param name="projectId"><%= project.getWikittyId() %></s:param>
    	        </s:url>
    	        <a href="${projectDetails}"><%= project.getName() %></a> (<a href="${addQuotation}"><s:text name="chorem.billy.quotation.add" /></a>)<br />
       	    <% } %>
        </p>
	</body>
</html>

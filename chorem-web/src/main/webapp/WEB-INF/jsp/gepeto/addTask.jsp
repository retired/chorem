<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.chorem.gepeto.action.TaskAction" %>
<%@taglib prefix="s" uri="/struts-tags" %>

<html xmlns:s="http://java.sun.com/xml/ns/j2ee/web-jsptaglibrary_2_0.xsd"
      xmlns:jsp="http://java.sun.com/JSP/Page">
	<head>
		<title><s:text name="chorem.gepeto.task.add" /></title>
		<s:head />
	</head>
	<body>
	    <h2><s:text name="chorem.gepeto.task.add" /></h2>
	    <s:actionerror />
	    <s:url namespace="/gepeto" action="addTask" var="addTask">
	        <s:param name="projectOrderId"><%= TaskAction.getAction().getProjectOrderId() %></s:param>
	    </s:url>
	    <form action="${addTask}" method="post">
	        <fieldset>
	            <legend>
	                <s:text name="chorem.gepeto.task">
	                    <s:param></s:param>
	                </s:text>
	            </legend>
	            <p>
    	            <s:textfield key="chorem.gepeto.task.name" name="name" labelSeparator=": " />
    	            <br />
    	            <br />
    	            <s:textarea key="chorem.gepeto.task.description" name="description" cols="50" rows="10" labelposition="top" />
    	            <br />
    	            <br />
    	            <s:textfield key="chorem.gepeto.task.price" name="price" labelSeparator=": " size="4" />
    	            <br />
    	            <s:textfield key="chorem.gepeto.task.estimatedDays" name="estimatedDays" labelSeparator=": " size="3" />
    	            <br />
    	            <s:textfield key="chorem.gepeto.task.beginDate" name="beginDate" labelSeparator=": " size="7" />
    	            <br />
    	            <s:textfield key="chorem.gepeto.task.estimatedEndDate" name="estimatedEndDate" labelSeparator=": " size="7" />
    	            <br />
    	            <br />
    	            <s:submit key="chorem.gepeto.task.add" name="submit" />
                </p>
	        </fieldset>
	    </form>
	    <p>
	        <s:url namespace="/gepeto" action="projectOrderDetails" var="projectOrderDetails">
	            <s:param name="projectOrderId"><%= TaskAction.getAction().getProjectOrderId() %></s:param>
	        </s:url>
	        <a href="${projectOrderDetails}"><s:text name="chorem.gepeto.task.backToProjectDetails" /></a>
	    </p>
	</body>
</html>

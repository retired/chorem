<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.List" %>
<%@page import="org.chorem.entities.Company" %>
<%@page import="org.chorem.entities.Project" %>
<%@page import="org.chorem.gepeto.ProjectOrderFull" %>
<%@page import="org.chorem.gepeto.action.ProjectAction" %>
<%@page import="org.chorem.JspUtils" %>
<%@taglib prefix="s" uri="/struts-tags" %>

<html xmlns:s="http://java.sun.com/xml/ns/j2ee/web-jsptaglibrary_2_0.xsd"
      xmlns:jsp="http://java.sun.com/JSP/Page">
	<head>
		<title>
		    <s:text name="chorem.gepeto.projectsByYear.title">
		        <s:param><%= ProjectAction.getAction().getYear() %></s:param>
		    </s:text>    
		</title>
		<s:head />
	</head>
	<body>
	    <h2>
	        <s:text name="chorem.gepeto.projectsByYear.title">
		        <s:param><%= ProjectAction.getAction().getYear() %></s:param>
		    </s:text>
		</h2>
	    <table>
	        <caption>
	            <s:text name="chorem.gepeto.projectsByYear.withProjectOrder" />
	        </caption>
	        <thead>
	            <tr>
    	            <th><s:text name="chorem.gepeto.projectsByYear.thead.name" /></th>
    	            <th><s:text name="chorem.gepeto.projectsByYear.thead.company" /></th>
    	            <th><s:text name="chorem.gepeto.projectsByYear.thead.start" /></th>
    	            <th><s:text name="chorem.gepeto.projectsByYear.thead.end" /></th>
                <tr>
	        </thead>
	        <tbody>
	            <%
	            List<ProjectOrderFull> projectsFull = ProjectAction.getAction().getProjectOrdersByYear();
	            
	            for (ProjectOrderFull project : projectsFull) {
	            %>
    	            <tr>
    	                <s:url action="projectDetails" var="projectDetails">
    	                    <s:param name="projectId"><%= project.getProject().getWikittyId() %></s:param>
    	                </s:url>
    	                <td><a href="${projectDetails}"><%= project.getProject().getName() %></a></td>
    	                <td>
    	                    <%
    	                    List<Company> customers = ProjectAction.getAction().getCustomersByYear(project.getProject().getWikittyId());
    	                    
    	                    for (Company customer : customers) {
    	                    %>
    	                        <s:url namespace="/bonzoms" action="companyDetails" var="companyDetails">
    	                            <s:param name="companyId"><%= customer.getWikittyId() %></s:param>
    	                        </s:url>
    	                        <a href="${companyDetails}"><%= customer.getName() %></a> 
    	                    <%   
    	                    }
    	                    %>
    	                </td>
    	                <td><%= JspUtils.dateFormat(project.getProjectOrder().getBeginDate()) %></td>
    	                <td><%= JspUtils.dateFormat(project.getProjectOrder().getEndDate()) %></td>
    	            </tr>
	            <% } %>
	        </tbody>
	    </table>
	    <br />
	    <table>
	        <caption>
	            <s:text name="chorem.gepeto.projectsByYear.withoutProjectOrder" />
	        </caption>
	        <thead>
	            <tr>
    	            <th><s:text name="chorem.gepeto.projectsByYear.thead.name" /></th>
	            </tr>
	        </thead>
	        <tbody>
	            <%
	            List<Project> projects = ProjectAction.getAction().getProjectsWithoutProjectOrderByYear();
	            
	            for (Project project : projects) {
                        if (project != null) {
	            %>
	                <tr>
	                    <s:url action="projectDetails" var="projectDetails">
    	                    <s:param name="projectId"><%= project.getWikittyId() %></s:param>
    	                </s:url>
    	                <td><a href="${projectDetails}"><%= project.getName() %></a></td>
	                </tr>
	            <%
                        }
                    }
                    %>
	        </tbody>
	    </table>
	    <br />
	</body>
</html>

<%@taglib prefix="s" uri="/struts-tags" %>

<div id="menu">
    <ul>
        <li><s:a action="projects"><s:text name="chorem.gepeto.projects" /></s:a></li>
        <li><s:a action="addProjectInput"><s:text name="chorem.gepeto.project.add" /></s:a></li>
        <s:url action="projectsByYear" var="projectsByYear">
            <s:param name="year">2011</s:param>
        </s:url>
            <li><a href="${projectsByYear}"><s:text name="chorem.gepeto.projects" /></a></li>
            <li><s:a action="projectsWithoutQuotation"><s:text name="chorem.gepeto.projectsWithoutQuotation" /></s:a></li>
    </ul>
</div>
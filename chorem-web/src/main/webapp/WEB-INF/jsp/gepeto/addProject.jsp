<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>

<html xmlns:s="http://java.sun.com/xml/ns/j2ee/web-jsptaglibrary_2_0.xsd"
      xmlns:jsp="http://java.sun.com/JSP/Page">
	<head>
		<title><s:text name="chorem.gepeto.project.add" /></title>
		<s:head />
	</head>
	<body>
	    <h2><s:text name="chorem.gepeto.project.add" /></h2>
	    <s:actionerror />
	    <s:form action="addProject" method="post">
	        <fieldset>
	            <legend>
	                <s:text name="chorem.gepeto.project">
	                    <s:param></s:param>
	                </s:text>
	            </legend>
	            <s:textfield key="chorem.gepeto.project.name" name="name" labelSeparator=": " labelposition="top" />
	            <br />
	            <br />
	            <s:textarea key="chorem.gepeto.project.description" name="description" cols="50" rows="10" labelposition="top" />
	            <br />
	            <s:submit key="chorem.gepeto.project.add" name="submit" />
	        </fieldset>
	    </s:form>
	</body>
</html>

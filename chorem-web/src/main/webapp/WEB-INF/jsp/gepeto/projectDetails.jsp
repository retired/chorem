<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.List" %>
<%@page import="org.chorem.entities.ProjectOrder" %>
<%@page import="org.chorem.entities.Quotation" %>
<%@page import="org.chorem.gepeto.action.ProjectAction" %>
<%@page import="org.chorem.JspUtils" %>
<%@taglib prefix="s" uri="/struts-tags" %>

<html xmlns:s="http://java.sun.com/xml/ns/j2ee/web-jsptaglibrary_2_0.xsd"
      xmlns:jsp="http://java.sun.com/JSP/Page">
	<head>
		<title>
		    <s:text name="chorem.gepeto.projectDetails.title">
		        <s:param><%= ProjectAction.getAction().getName() %></s:param>
		    </s:text>
		</title>
		<s:head />
	</head>
	<body>
	    <h2>
	        <s:text name="chorem.gepeto.project">
	            <s:param><%= ProjectAction.getAction().getName() %></s:param>
	        </s:text>
	    </h2>
	    <s:url namespace="/gepeto" action="modifyProject" var="modifyProject">
	        <s:param name="projectId"><%= ProjectAction.getAction().getProjectId() %></s:param>
	    </s:url>
	    <form action="${modifyProject}" method="post">
    	    <p>
    	        <s:textarea name="description" key="chorem.gepeto.project.description" cols="50" rows="10" labelposition="top" />
    	        <br />
    	        <br />
    	        <s:url namespace="/billy" action="addQuotationInput" var="addQuotation">
    	            <s:param name="projectId"><%= ProjectAction.getAction().getProjectId() %></s:param>
    	        </s:url>
    	        <s:submit key="chorem.misc.modify" name="modify" />
    	    </p>
	    </form>
        <a href="${addQuotation}"><s:text name="chorem.billy.quotation.add" /></a>
	    <p>
	        <s:text name="chorem.gepeto.projectDetails.projectOrders" />
	        <br />
	        <br />
	        <%
	        List<ProjectOrder> projectOrders = ProjectAction.getAction().getProjectOrders();
	        
	        if (projectOrders.isEmpty()) {
	        %>
	            <em><s:text name="chorem.gepeto.projectDetails.noProjectOrder" /></em><br />
	        <%
	        }
	        for (ProjectOrder projectOrder : projectOrders) {
	        %>
	            <s:url namespace="/gepeto" action="projectOrderDetails" var="projectOrderDetails">
	                <s:param name="projectOrderId"><%= projectOrder.getWikittyId() %></s:param>
	            </s:url>
	            <a href="${projectOrderDetails}">
    	            <s:text name="chorem.gepeto.projectDetails.projectOrder">
    	                <s:param>
    	                    <%= JspUtils.dateFormat(projectOrder.getBeginDate()) %>
    	                </s:param>
    	                <s:param>
    	                    <%= JspUtils.dateFormat(projectOrder.getEndDate()) %>
    	                </s:param>
    	            </s:text>
	            </a>
	            <br />
	        <% } %>
	        <br />
	        <s:text name="chorem.gepeto.projectDetails.quotationWithProjectOrder" />
	        <br />
	        <br />
	        <%
	        List<Quotation> quotationsProjectOrder = ProjectAction.getAction().getAttachedQuotationsWithProjectOrder();
	        
	        if (quotationsProjectOrder.isEmpty()) {
	        %>
	            <em><s:text name="chorem.gepeto.projectDetails.noQuotations" /></em><br />
	        <%   
	        }
	        for (Quotation quotation : quotationsProjectOrder) {
	        %>
	            <s:url namespace="/billy" action="quotationDetails" var="quotationDetails">
	                <s:param name="quotationId"><%= quotation.getWikittyId() %></s:param>
	            </s:url>
	            <s:text name="chorem.gepeto.projectDetails.quotation">
	                <s:param>
	                    <%= JspUtils.dateFormat(quotation.getPostedDate()) %>
	                </s:param>
	                <s:param>
	                    <a href="${quotationDetails}"><%= quotation.getReference() %></a>
	                </s:param>
	            </s:text>
	            <br />
	        <% } %>
	        <br />
	        <s:text name="chorem.gepeto.projectDetails.quotationsWithoutProjectOrder" />
	        <br />
	        <br />
	        <% 
	        List<Quotation> quotations = ProjectAction.getAction().getAttachedQuotationsWithoutProjectOrder();
	        
	        if (quotations.isEmpty()) {
	        %>
	            <em><s:text name="chorem.gepeto.projectDetails.noQuotations" /></em><br />
	        <%   
	        }
	        for (Quotation quotation : quotations) {
	        %>
	            <s:url namespace="/billy" action="quotationDetails" var="quotationDetails">
	                <s:param name="quotationId"><%= quotation.getWikittyId() %></s:param>
	            </s:url>
	            <s:text name="chorem.gepeto.projectDetails.quotation">
	                <s:param>
	                    <%= JspUtils.dateFormat(quotation.getPostedDate()) %>
	                </s:param>
	                <s:param>
	                    <a href="${quotationDetails}"><%= quotation.getReference() %></a>
	                </s:param>
	            </s:text> -
	            <s:url namespace="/gepeto" action="addProjectOrderInput" var="addProjectOrder">
	                <s:param name="quotationId"><%= quotation.getWikittyId() %></s:param>
	            </s:url>
	            <a href="${addProjectOrder}"><s:text name="chorem.billy.quotation.addProjectOrder" /></a>
	            <br />
	        <% } %>
	    </p>
	</body>
</html>

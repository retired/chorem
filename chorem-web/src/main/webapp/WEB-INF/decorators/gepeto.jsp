<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<%@taglib prefix="page" uri="http://www.opensymphony.com/sitemesh/page" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:s="http://java.sun.com/xml/ns/j2ee/web-jsptaglibrary_2_0.xsd"
      xmlns:jsp="http://java.sun.com/JSP/Page"
      xmlns:decorator="http://www.opensymphony.com/sitemesh/decorator">
    <head>
        <title>Chorem - gepeto : <decorator:title default="no title" /></title>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
        <meta name="description" content="" />
        <s:url var="CSS" value="/css/style.css" />
        <link href="${CSS}" rel="stylesheet" type="text/css" media="all" />
        <s:head />
        <sj:head locale="fr" jqueryui="true" jquerytheme="redmond"/>
        <decorator:head />
    </head>
    <body>
        <s:include value="/WEB-INF/jsp/common/inc/header.jsp" />
        <s:include value="/WEB-INF/jsp/gepeto/menu.jsp" />
        <div id="content">
            <decorator:body />
        </div>
        <s:include value="/WEB-INF/jsp/common/inc/footer.jsp" />
    </body>
</html>

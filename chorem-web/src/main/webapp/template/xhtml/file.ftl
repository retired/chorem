<#--
/*
 * $Id$
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
-->
<#-- vbriand-2001-04-08 replaced /controlheader.ftl by /controlheader-core.ftl 
because the first one includes the second one and only opens a new table 
element -->
<#include "/${parameters.templateDir}/xhtml/controlheader-core.ftl" />
<#include "/${parameters.templateDir}/simple/file.ftl" />
<#-- vbriand-2001-04-08 removed /xhtml/controlfooter.ftl which closes a table row-->

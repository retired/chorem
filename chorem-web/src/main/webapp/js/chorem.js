/**
 * Permet de faire des recherches fulltext qui s'applique sur une grille
 * @param gridId l'identifiant de la grille
 * @param inputId l'identifiant du champs input
 * @param url l'url initiale de la grille sur lequel on ajoute la contrainte de recherche
 */
var timeoutHnd;

function doSearch(gridId, inputId, url) {
    if(timeoutHnd) {
        clearTimeout(timeoutHnd)
    }
    timeoutHnd = setTimeout(function() {
        var filter = jQuery(inputId).val();
        var gridUrl = url + "&fulltextSearch=" + filter;
        jQuery(gridId).jqGrid('setGridParam',{url:gridUrl,page:1}).trigger("reloadGrid");
    }, 500);
}
